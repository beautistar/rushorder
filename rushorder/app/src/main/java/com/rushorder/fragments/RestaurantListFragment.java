package com.rushorder.fragments;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.rushorder.R;
import com.rushorder.activities.FilterActivity;
import com.rushorder.activities.MainActivity;
import com.rushorder.apis.Apis;
import com.rushorder.cache.CacheData;
import com.rushorder.constants.Constants;
import com.rushorder.fragments.adapters.RestaurantListAdapter;
import com.rushorder.interfaces.ClickCallBack;
import com.rushorder.interfaces.VolleyCallBack;
import com.rushorder.managers.PrefManager;
import com.rushorder.models.Cart;
import com.rushorder.models.FiltersModel;
import com.rushorder.models.RestaurantsResponse;
import com.rushorder.models.SignInResponse;
import com.rushorder.utils.AlertUtils;
import com.rushorder.utils.JsonTranslator;
import com.rushorder.utils.PaginationScrollListener;
import com.rushorder.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RestaurantListFragment extends BaseFragment implements View.OnClickListener, ClickCallBack, SwipeRefreshLayout.OnRefreshListener {

    private double lat, lng;
    private String postCode;
    private Dialog infoDialog;
    private EditText etSearchBar;
    private ProgressBar pbLoader;
    private RelativeLayout rlCart;
    private ImageView ivMenu, ivFilter;
    private RecyclerView rvRestaurants;
    private RestaurantListAdapter adapter;
    private SwipeRefreshLayout srAdapter;
    private LinearLayoutManager linearLayoutManager;
    private TextView tvMap, tvEmpty, tvRestaurantsCount, tvChangeLocation, tvCartCount;

    /**
     * For RecyclerView Adapter Pagination
     */
    private int currentPage = 0;
    protected int TOTAL_PAGES = 0;
    private boolean isLoading = false;
    private boolean isLastPage = false;
    private static final int PAGE_START = 1;

    private boolean performNextApiCall = true;
    private static final int FILTER_REQUEST_CODE = 905;

    private String sort = "";
    private String cuisine = "";
    private String searchKey = "";

    MainActivity mainActivity;
    public static RestaurantListFragment getInstance() {
        return new RestaurantListFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_restaurant_list, container, false);
        initViews(view);
        setListeners();
        setValues();
        return view;
    }

    @Override
    protected void initViews(View view) {
        mainActivity = (MainActivity)getActivity();
        tvMap = view.findViewById(R.id.tvMap);
        ivMenu = view.findViewById(R.id.ivMenu);
        rlCart = view.findViewById(R.id.rlCart);
        tvEmpty = view.findViewById(R.id.tvEmpty);
        pbLoader = view.findViewById(R.id.pbLoader);
        ivFilter = view.findViewById(R.id.ivFilter);
//        srAdapter = view.findViewById(R.id.srAdapter);
        tvCartCount = view.findViewById(R.id.tvCartCount);
        etSearchBar = view.findViewById(R.id.etSearchBar);
        rvRestaurants = view.findViewById(R.id.rvRestaurants);
        tvChangeLocation = view.findViewById(R.id.tvChangeLocation);
        tvRestaurantsCount = view.findViewById(R.id.tvRestaurantsCount);


        super.initViews(view);
    }

    private void getRewardPoints() {
//        http://apis.rushorder.online/api/Account/GetReward?userid=[USERID]
        String userId = "";
        try {
            SignInResponse response = JsonTranslator.getInstance().parse(PrefManager.getInstance().getString(PrefManager.USER_INFO), SignInResponse.class);
            userId = response.getUserID();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        String url = "http://apis.rushorder.online/api/Account/GetReward?userid=" + userId;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                response -> {
                    try {
                        Log.e("responseFacebook", response);
                        JSONObject jsonObject = new JSONObject(response);
                        String point = jsonObject.getString("Point");
                        String points = "Points = " + point;
                        if(mainActivity!=null) {
                            mainActivity.tvEmail.setText(points);

                            int rewardPoint = Integer.valueOf(point);
                            mainActivity.tvLevelDrawer.setText(Utils.getLevel(rewardPoint));

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
//                    pbLogin.setVisibility(View.GONE);
                },
                error -> {
//                    pbLogin.setVisibility(View.GONE);
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
//                params.put("emailId", email);

                Log.e("paramsLogin", params.toString());
                return params;
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(stringRequest);

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }

    @Override
    protected void setValues() {
        sort = "";
        searchKey = "";
        cuisine = "";

        resetState();

        if (getArguments() != null) {
            lat = Double.parseDouble(getArguments().getString(Constants.Keys.LAT));
            lng = Double.parseDouble(getArguments().getString(Constants.Keys.LNG));
        }

        loadFirstPage();


//        postCode = PrefManager.getInstance().getString(PrefManager.POST_CODE);
//
//        if (postCode != null && !postCode.isEmpty()) {
//            loadFirstPage();
//        } else {
//            showErrorDialog("Please search the restaurants by postcode");
//            getParentActivity().clearAllFragments();
//        }


        super.setValues();
    }


    @Override
    protected void setListeners() {
        tvMap.setOnClickListener(this);
        ivMenu.setOnClickListener(this);
        ivFilter.setOnClickListener(this);
//        srAdapter.setOnRefreshListener(this);
        tvCartCount.setOnClickListener(this);
        tvChangeLocation.setOnClickListener(this);


//        etSearchBar.setOnEditorActionListener(new TextView.OnEditorActionListener() {
//            @Override
//            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
//                if (actionId == EditorInfo.IME_ACTION_DONE) {
//
//                    if (!etSearchBar.getText().toString().isEmpty()) {
//                        searchKey = etSearchBar.getText().toString();
//                        onRefresh();
//                    }
//                }
//                return false;
//            }
//        });

        etSearchBar.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (adapter != null && adapter.getFilter() != null) {
                    adapter.getFilter().filter(s);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        super.setListeners();
    }


    private void setAdapter(List<RestaurantsResponse> restaurantList) {

        adapter = new RestaurantListAdapter(this, restaurantList);
        linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        rvRestaurants.setLayoutManager(linearLayoutManager);
        rvRestaurants.setAdapter(adapter);
//        addScrollListener();

    }

    private void addScrollListener() {

        rvRestaurants.addOnScrollListener(new PaginationScrollListener(linearLayoutManager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                currentPage += 1;

                if (performNextApiCall) {
//                    loadNextPage();
                }
            }

            @Override
            public int getTotalPageCount() {
                return TOTAL_PAGES;
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });

    }

    @Override
    public void onClick(View v) {
        if (v == ivMenu) {
            getParentActivity().openDrawer();
        } else if (v == ivFilter) {
            startFilterActivity();
        } else if (v == tvChangeLocation) {
//            clearAllFragments();
            switchFragment(HomeFragment.getInstance());
//            getParentActivity().selectBottomMenu(R.id.action_nav_home);
        } else if (v == tvCartCount) {
            switchFragment(CartDetailFragment.getInstance());
        } else {
            switchFragment(RestaurantsMapFragment.getInstance());
        }
    }

    private void startFilterActivity() {
        startActivityForResult(new Intent(getActivity(), FilterActivity.class), FILTER_REQUEST_CODE);
    }

    @Override
    public void callBack() {
//        loadNextPage();
    }

    @Override
    public void callBack(Object _model, int position) {

        Utils.hideSoftKeyboard(getActivity(), etSearchBar);

        String apiKey = ((RestaurantsResponse) _model).getId();

        if (Cart.getInstance().isCartFull()) {
            if (apiKey.equals(PrefManager.getInstance().getString(PrefManager.RES_ID))) {
                showRestaurantDetails(position, apiKey);
            } else {
                showWarningDialog(getString(R.string.cart_will_be_cleared), position, apiKey);
            }
        } else {
            showRestaurantDetails(position, apiKey);
        }
    }


    private void showRestaurantDetails(int position, String apiKey) {
        Bundle bundle = new Bundle();
        bundle.putInt(Constants.Keys.ITEM_POSITION_KEY, position);
        PrefManager.getInstance().saveSelectedRestaurantApiKey(apiKey);
        switchFragment(RestaurantDetailsFragment.getInstance(), bundle);
    }

    private void showWarningDialog(String message, final int position, final String apiKey) {

        AlertUtils.showAlert(getActivity(), "Warning !", message, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                Cart.getInstance().ClearCart();
                showRestaurantDetails(position, apiKey);

            }
        });

    }


    private void loadFirstPage() {

        FiltersModel model = getParams();
        Apis.Restaurant.getNearestRestaurants(getParentActivity(), false, model, "", new VolleyCallBack.ArrayResponse() {
            @Override
            public void onResponse(JSONArray _response, String _tag) {
                try {

                    checkProgressbar();
                    checkSwipeRefresh();


                    List<RestaurantsResponse> restaurantList = JsonTranslator.getInstance().parseRestaurantJsonToList(_response.toString());

                    if (restaurantList.size() > 0) {

                        CacheData.Restaurants.getInstance().setRestaurantsList(restaurantList);
                        setAdapter(restaurantList);

                    } else {
                        tvEmpty.setVisibility(View.VISIBLE);
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

            }

            @Override
            public void onErrorMessage(String _errorMessage) {
                checkProgressbar();
                checkSwipeRefresh();
                clearAllFragments();
                showErrorDialog("" + _errorMessage);
            }

            @Override
            public void onErrorResponse(VolleyError _error, String _tag) {
                checkProgressbar();
                checkSwipeRefresh();
                showErrorDialog(getString(R.string.something_went_wrong));
            }

            @Override
            public void isConnected(boolean _connected, String _tag) {
                checkProgressbar();
                checkSwipeRefresh();
                showErrorDialog(getString(R.string.no_internet));
            }
        });
    }

    private FiltersModel getParams() {

//        return new FiltersModel.Builder()
//                .Lat(lat)
//                .lng(lng)
//                .build();

        return new FiltersModel.Builder()
                .Lat(29.2799549689806)
                .lng(48.06936468731395)
                .build();
    }


    private void checkSwipeRefresh() {
//        if (srAdapter.isRefreshing()) {
//            srAdapter.setRefreshing(false);
//            adapter.clearAll();
//        }

    }

    private void checkProgressbar() {
        if (pbLoader.getVisibility() == View.VISIBLE) {
            pbLoader.setVisibility(View.GONE);
        } else if (tvEmpty.getVisibility() == View.VISIBLE) {
            tvEmpty.setVisibility(View.GONE);
        }

    }


    @Override
    public void onRefresh() {
        resetState();
        startRefresh();
        tvRestaurantsCount.setText(0 + " restaurants found in " + postCode);
        loadFirstPage();
    }

    /**
     * Pagination State reset
     */
    private void resetState() {
        isLoading = false;
        isLastPage = false;
        performNextApiCall = true;
        currentPage = PAGE_START;
        CacheData.Restaurants.getInstance().clearRestaurantList();

        if (adapter != null) {
            adapter.notifyDataSetChanged();
        }
    }

    public void startRefresh() {
//        srAdapter.post(new Runnable() {
//            @Override
//            public void run() {
//                srAdapter.setRefreshing(true);
//            }
//        });
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == getActivity().RESULT_OK) {
            resetFilters();
            sort = data.getStringExtra(Constants.Keys.SORT);
            cuisine = data.getStringExtra(Constants.Keys.CUISINE);
            onRefresh();
        }
//        super.onActivityResult(requestCode, resultCode, data);
    }

    private void resetFilters() {
        sort = "";
        cuisine = "";
        searchKey = "";
    }

    @Override
    public void onResume() {

        if (Cart.getInstance().getOrderDetail().size() > 0) {
            rlCart.setVisibility(View.VISIBLE);
            updateCartValue();
        } else {
            rlCart.setVisibility(View.GONE);
        }


//        Log.e("CalledResumeRestaurent","CalledResume");
//        if (PrefManager.getInstance().getBoolean(PrefManager.IS_LOGIN))
//            getRewardPoints();


        super.onResume();
    }

    void updateCartValue() {
        ((TextView) rlCart.findViewById(R.id.tvCartCount)).setText("Go to cart " + Cart.getInstance().getOrderDetail().size());
    }
}
