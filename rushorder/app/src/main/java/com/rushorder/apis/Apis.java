package com.rushorder.apis;

import android.app.Activity;


import com.rushorder.R;
import com.rushorder.constants.Constants;
import com.rushorder.interfaces.VolleyCallBack;
import com.rushorder.managers.ConnectionManager;
import com.rushorder.managers.PrefManager;
import com.rushorder.models.Cart;
import com.rushorder.models.FiltersModel;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by Aleem on 12/4/2017.
 */

public class Apis {

    public static final String baseUrl = "http://apis.rushorder.online/api/";
    public static final String GET_ORDER_LOCATION =baseUrl+ "Order/GetOrderLocation?";
    public static final String GetDriverLocation =baseUrl+ "Account/GetDriverLocation?";
    public static final String UpdateOrderLocation =baseUrl+ "Order/UpdateOrderLocation?";
    public static final String DriverLogin =baseUrl+ "Account/DriverLogin";
    public static final String changeOrderStatus =baseUrl+ "order/changeOrderStatus?";
    public static final String CheckOrderAvailable =baseUrl+ "Order/CheckOrderAvailable?";
    public static final String ChangeDriverStatus =baseUrl+ "Account/ChangeDriverStatus?";
    public static final String UpdateDriverLocation =baseUrl+ "Account/UpdateDriverLocation?";
    public static final String GetOrderLocation =baseUrl+ "Order/GetOrderLocation?";


    public static class User {


//        public static void signUp(Activity activity,
////                                  boolean isDialog,
////                                  String firstName,
////                                  String lastName,
////                                  String email,
////                                  String phone,
////                                  String password,
////                                  String token,
////                                  VolleyCallBack.StringResponse stringResponse) {
////
////            String requestTag = Constants.Tags.SIGN_UP;
////            String requestURL = Constants.URL.USER_REGISTER;
////
////            HashMap<String, String> params = new HashMap<>();
////
////            params.put("Email", "" + email);
////            params.put("LastName", "" + lastName);
////            params.put("Password", "" + password);
////            params.put("PhoneNumber", "" + phone);
////            params.put("FirstName", "" + firstName);
////            params.put("ConfirmPassword", "" + password);
////
////
////            ConnectionManager
////                    .getInstance()
////                    .volleyStringRequest(activity, isDialog, requestURL, requestTag, params, stringResponse);
////
////        }


        /**
         * Method Post
         *
         * @param activity
         * @param isDialog
         * @param email
         * @param password
         * @param confirmPassword
         * @param token
         * @param jsonResponse
         */

        public static void signUp(Activity activity,
                                  boolean isDialog,
                                  String email,
                                  String password,
                                  String confirmPassword,
                                  String token,
                                  VolleyCallBack.JSONResponse jsonResponse) {

            String requestTag = Constants.Tags.SIGN_UP;
            String requestURL = Constants.URL.USER_REGISTER;

            JSONObject jsonParamObject = new JSONObject();
            try {
                jsonParamObject.put("Email", "" + email);
                jsonParamObject.put("Password", "" + password);
                jsonParamObject.put("ConfirmPassword", "" + confirmPassword);

            } catch (JSONException e) {
                e.printStackTrace();
            }

            ConnectionManager
                    .getInstance()
                    .volleyJsonObjectRequest(activity, isDialog, requestURL, requestTag, jsonParamObject, jsonResponse);

        }


        /**
         * Method Post
         *
         * @param activity
         * @param isDialog
         * @param email
         * @param password
         * @param token
         * @param stringResponse
         */


        public static void signIn(Activity activity,
                                  boolean isDialog,
                                  String email,
                                  String password,
                                  String token,
                                  VolleyCallBack.StringResponse stringResponse) {

            String requestTag = Constants.Tags.SIGN_IN;
//            String requestURL = Constants.URL.USER_LOGIN;
            String requestURL = "http://apis.rushorder.online/token";
//            String requestURL = "http://finderapi.saffronscissett.co.uk/token";
//            String requestURL = "http://rushorderapis.mindslab.info/token/api/Account/login";


//            HashMap<String, String> params = new HashMap<>();
//            params.put("Email", "" + email);
//            params.put("Password", "" + password);
//            params.put("grant_type", "password");
            String params = "grant_type=password&UserName=" + email + "&Password=" + password;


            HashMap<String, String> headers = new HashMap<>();
            headers.put("Content-Type", "application/x-www-form-urlencoded");

            ConnectionManager
                    .getInstance()
                    .volleyStringBodyRequest(activity, isDialog, requestURL, requestTag, params, stringResponse);

//
//            String params = "grant_type=password&Email=" + email + "&Password=" + password;

//
//            HashMap<String, String> params = new HashMap<>();
//            params.put("Email", "" + email);
//            params.put("Password", "" + password);
//            params.put("grant_type", "password");

//            ConnectionManager
//                    .getInstance()
//                    .volleyStringRequest(activity, isDialog, requestURL, requestTag, params, headers, stringResponse);
////
//            ConnectionManager
//                    .getInstance()
//                    .volleyStringBodyRequest(activity, isDialog, requestURL, requestTag, params, stringResponse);
        }

        /**
         * Method Post
         *
         * @param activity
         * @param isDialog
         * @param socialId
         * @param email
         * @param fName
         * @param lName
         * @param type
         * @param jsonResponse
         */

        public static void socialLogin(Activity activity,
                                       boolean isDialog,
                                       String socialId,
                                       String email,
                                       String fName,
                                       String lName,
                                       String type,
                                       VolleyCallBack.JSONResponse jsonResponse) {

            String requestTag = "";
            String requestURL = Constants.URL.USER_SOCIAL_LOGIN;

            JSONObject jsonParamObject = new JSONObject();
            try {
                jsonParamObject.put("social_id", "" + socialId);
                jsonParamObject.put("email", "" + email);
                jsonParamObject.put("firstname", "" + fName);
                jsonParamObject.put("lastname", "" + lName);
                jsonParamObject.put("type", "" + type);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            HashMap<String, String> headers = new HashMap<>();
            headers.put("Resapi-Key", activity.getString(R.string.api_key));


            ConnectionManager
                    .getInstance()
                    .volleyJsonObjectRequest(activity, isDialog, requestURL, requestTag, jsonParamObject, headers, jsonResponse);
        }

        /**
         * Method Post
         *
         * @param activity
         * @param isDialog
         * @param email
         * @param token
         * @param jsonResponse
         */


        public static void recoverPassword(Activity activity,
                                           boolean isDialog,
                                           String email,
                                           String token,
                                           VolleyCallBack.JSONResponse jsonResponse) {

            String requestTag = "";
            String requestURL = Constants.URL.FORGOT_PASSWORD + "email=" + email;
//            String requestURL = Constants.URL.FORGOT_PASSWORD;

            JSONObject jsonParamObject = new JSONObject();
//            try {
//                jsonParamObject.put("email", "" + email);
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }


            ConnectionManager
                    .getInstance()
                    .volleyJsonObjectRequest(activity, isDialog, requestURL, requestTag, jsonParamObject, jsonResponse);
        }

        /**
         * Method Post
         *
         * @param activity
         * @param isDialog
         * @param postCode
         * @param jsonResponse
         */

        public static void searchPostCode(Activity activity,
                                          boolean isDialog,
                                          String postCode,
                                          String resId,
                                          VolleyCallBack.JSONResponse jsonResponse) {

            String requestTag = "";
            String requestURL = Constants.URL.SEARCH_POST_CODE + "postCode=" + postCode + "&restId=" + resId;

            JSONObject jsonParamObject = new JSONObject();
            ConnectionManager
                    .getInstance()
                    .volleyJsonObjectRequest(activity, isDialog, requestURL, requestTag, jsonParamObject, jsonResponse);
        }

        /**
         * Method post
         *
         * @param activity
         * @param isDialog
         * @param token
         * @param jsonResponse
         */


        public static void registerDevice(Activity activity,
                                          boolean isDialog,
                                          String token,
                                          VolleyCallBack.JSONResponse jsonResponse) {

            String requestTag = "";
            String requestURL = Constants.URL.REGISTER_DEVICE;

            JSONObject jsonParamObject = new JSONObject();
            try {
                jsonParamObject.put("type", "android");
                jsonParamObject.put("device_id", "" + token);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            HashMap<String, String> headers = new HashMap<>();
            headers.put("Resapi-Key", activity.getString(R.string.api_key));


            ConnectionManager
                    .getInstance()
                    .volleyJsonObjectRequest(activity, isDialog, requestURL, requestTag, jsonParamObject, headers, jsonResponse);
        }

        /**
         * Method Get
         *
         * @param activity
         * @param isDialog
         * @param couponCode
         * @param token
         * @param jsonResponse
         */


        public static void validateCouponCode(Activity activity,
                                              boolean isDialog,
                                              String couponCode,
                                              String restId,
                                              String token,
                                              VolleyCallBack.JSONResponse jsonResponse) {

            String requestTag = "";
            String requestURL = Constants.URL.VALIDATE_COUPON_CODE + "code=" + couponCode + "&restID=" + restId;

            ConnectionManager
                    .getInstance()
                    .volleyJsonObjectRequest(activity, isDialog, requestURL, requestTag, jsonResponse);
        }


        /**
         * Method Post
         *
         * @param activity
         * @param isDialog
         * @param firstName
         * @param lastName
         * @param email
         * @param phone
         * @param postCode
         * @param street
         * @param town
         * @param token
         * @param jsonResponse
         */

        public static void updateUserInfo(Activity activity,
                                          boolean isDialog,
                                          String firstName,
                                          String lastName,
                                          String email,
                                          String phone,
                                          String postCode,
                                          String street,
                                          String town,
                                          String doorNum,
                                          String token,
                                          VolleyCallBack.JSONResponse jsonResponse) {

            String requestTag = "";
            String requestURL = Constants.URL.UPDATE_USER_INFO;

            JSONObject jsonParamObject = new JSONObject();
            try {
                jsonParamObject.put("firstname", "" + firstName);
                jsonParamObject.put("lastname", "" + lastName);
                jsonParamObject.put("postcode", "" + postCode);
                jsonParamObject.put("houseno", "" + doorNum);
                jsonParamObject.put("street", "" + street);
                jsonParamObject.put("email", "" + email);
                jsonParamObject.put("phone", "" + phone);
                jsonParamObject.put("town", "" + town);

            } catch (JSONException e) {
                e.printStackTrace();
            }

            HashMap<String, String> headers = new HashMap<>();
            headers.put("Resapi-Key", activity.getString(R.string.api_key));
            headers.put("Token", "" + token);

            ConnectionManager
                    .getInstance()
                    .volleyJsonObjectRequest(activity, isDialog, requestURL, requestTag, jsonParamObject, headers, jsonResponse);

        }

    }


    public static class Menu {
        /**
         * Method Get
         *
         * @param activity
         * @param isDialog
         * @param jsonResponse
         */

        public static void getMenu(Activity activity,
                                   boolean isDialog,
                                   String apiKey,
                                   VolleyCallBack.JSONResponse jsonResponse) {
            String requestTag = "";
            apiKey = (apiKey != null) ? apiKey : activity.getString(R.string.api_key);
            String requestURL = Constants.URL.RESTAURANT_MENU;
            HashMap<String, String> headers = new HashMap<>();
            headers.put("Resapi-Key", apiKey);
            ConnectionManager
                    .getInstance()
                    .volleyJsonObjectRequest(activity, isDialog, requestURL, requestTag, headers, jsonResponse);
        }


        /**
         * Method Post
         *
         * @param activity
         * @param isDialog
         * @param addOnCatId
         * @param token
         * @param jsonResponse
         */

        public static void getMenuAddOns(Activity activity,
                                         boolean isDialog,
                                         String addOnCatId,
                                         String token,
                                         VolleyCallBack.JSONResponse jsonResponse) {

            String requestTag = "";
            String requestURL = Constants.URL.MENU_ADDONS;

            JSONObject jsonParamObject = new JSONObject();
            try {
                jsonParamObject.put("addon_cat_id", "" + addOnCatId);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            HashMap<String, String> headers = new HashMap<>();
            headers.put("Resapi-Key", PrefManager.getInstance().getString(PrefManager.RES_ID));
//            headers.put("Resapi-Key", activity.getString(R.string.api_key));
//            headers.put("Token", activity.getString(R.string.token));

            ConnectionManager
                    .getInstance()
                    .volleyJsonObjectRequest(activity, isDialog, requestURL, requestTag, jsonParamObject, headers, jsonResponse);

        }
    }


    public static class Order {

        /**
         * Method Get
         *
         * @param activity
         * @param isDialog
         * @param resId
         * @param jsonResponse
         */


        public static void getRestaurantTime(Activity activity,
                                             boolean isDialog,
                                             String resId,
                                             VolleyCallBack.JSONResponse jsonResponse) {
            String requestTag = "";
            String requestURL = Constants.URL.PRE_ORDER_DATA + "id=" + resId;


            ConnectionManager
                    .getInstance()
                    .volleyJsonObjectRequest(activity, isDialog, requestURL, requestTag, jsonResponse);
        }

        /**
         * Method Post
         *
         * @param activity
         * @param isDialog
         * @param resId
         * @param jsonResponse
         */


        public static void checkShopOpen(Activity activity,
                                         boolean isDialog,
                                         String resId,
                                         VolleyCallBack.JSONResponse jsonResponse) {
            String requestTag = "";
            String requestURL = Constants.URL.CHECK_SHOP_OPEN + "id=" + resId;

            ConnectionManager
                    .getInstance()
                    .volleyJsonObjectRequest(activity, isDialog, requestURL, requestTag, jsonResponse);

        }


        /**
         * Method Post
         *
         * @param activity
         * @param isDialog
         * @param token
         * @param arrayResponse
         */

        public static void getOrderHistory(Activity activity,
                                           boolean isDialog,
                                           String token,
                                           String userId,
                                           VolleyCallBack.ArrayResponse arrayResponse) {

            String requestTag = "";
//            userId = "1f872eb6-1d6a-4887-9f18-6ef110659d26";
            String requestURL = Constants.URL.ORDER_HISTORY + "userID=" + userId;

//            HashMap<String, String> params = new HashMap<>();
//            params.put("f", "" + userId);
            ConnectionManager
                    .getInstance()
                    .volleyJsonArrayRequest(activity, isDialog, requestURL, requestTag, arrayResponse);
        }

        /**
         * Method Post
         *
         * @param activity
         * @param isDialog
         * @param star1
         * @param star2
         * @param star3
         * @param star4
         * @param userComments
         * @param resId
         * @param userId
         * @param avg
         * @param date
         * @param orderId
         * @param token
         * @param jsonResponse
         */


        public static void saveFeedBack(Activity activity,
                                        boolean isDialog,
                                        String star1,
                                        String star2,
                                        String star3,
                                        String star4,
                                        String userComments,
                                        String resId,
                                        String userId,
                                        String avg,
                                        String date,
                                        String orderId,
                                        String token,
                                        VolleyCallBack.JSONResponse jsonResponse) {


            String requestTag = "";
            String requestURL = Constants.URL.SAVE_FEED_BACK;

            JSONObject jsonParamObject = new JSONObject();
            try {

                float rating = Float.parseFloat(star1) + Float.parseFloat(star2) + Float.parseFloat(star3);

                jsonParamObject.put("UserId", "" + userId);
                jsonParamObject.put("Raiting", "" + rating / 3);
                jsonParamObject.put("Comment", "" + userComments);
                jsonParamObject.put("RestId", "" + resId);
                jsonParamObject.put("Id", "" + orderId);

            } catch (JSONException e) {
                e.printStackTrace();
            }


            ConnectionManager
                    .getInstance()
                    .volleyJsonObjectRequest(activity, isDialog, requestURL, requestTag, jsonParamObject, jsonResponse);

        }

        /**
         * Method Post
         *
         * @param activity
         * @param isDialog
         * @param token
         * @param jsonResponse
         */

        public static void saveOrder(Activity activity,
                                     boolean isDialog,
                                     String token,
                                     VolleyCallBack.JSONResponse jsonResponse) {

            String requestTag = "";
            String requestURL = Constants.URL.SAVE_ORDER;
            JSONObject jsonParamObject = null;


            try {
                Gson gson = new Gson();
                jsonParamObject = new JSONObject(gson.toJson(Cart.getInstance()));
            } catch (Exception e) {
                e.printStackTrace();
            }

            HashMap<String, String> headers = new HashMap<>();
            headers.put("Authorization", token);


            ConnectionManager
                    .getInstance()
                    .volleyJsonObjectRequest(activity, isDialog, requestURL, requestTag, jsonParamObject, jsonResponse);
        }

    }

    public static class Restaurant {

        /**
         * Method POST
         *
         * @param activity
         * @param isDialog
         * @param token
         * @param arrayResponse
         */

        public static void getNearestRestaurants(Activity activity,
                                                 boolean isDialog,
                                                 FiltersModel model,
                                                 String token,
                                                 VolleyCallBack.ArrayResponse arrayResponse) {

            String requestTag = "";

            String requestURL = Constants.URL.SEARCH_RESTAURANTS;

//                    + "CousID=" + model.getCousID()
//                    +"&Raiting=" + model.isRaiting()
//                    +"&Delivery=" + model.isDelivery()
//                    +"&NewArrival=" + model.isNewArrival()
//                    +"&BestMatch=" + model.getBestMatch()
//                    +"&Distance=" + model.isDistance()
//                    +"&lng=" + model.getLng()
//                    +"&Lat=" + model.getLat();
//                    +"&Discount=" + model.isDiscount()
//                    +"&Deals=" + model.isDeals()
//                    +"&PageNo=" + model.getPageNo()
//                    +"&NoOfRec=" + model.getNoOfRec();
//
//
//            HashMap<String, String> params = new HashMap<>();
//            params.put("lng", "" + model.getLng());
//            params.put("Lat", "" + model.getLat());
//
//            if (!sort.isEmpty()) {
//                requestURL = Constants.URL.SEARCH_RESTAURANTS + "postcode=" + postCode + "&page=" + page + "&ListBy=" + sort;
//            }
//            if (!cuisines.isEmpty()) {
//                requestURL = Constants.URL.SEARCH_RESTAURANTS + "postcode=" + postCode + "&page=" + page + "&cuisine=" + cuisines;
//
//            }
//            if (!cuisines.isEmpty() && !sort.isEmpty()) {
//                requestURL = Constants.URL.SEARCH_RESTAURANTS + "postcode=" + postCode + "&page=" + page + "&ListBy=" + sort + "&cuisine=" + cuisines;
//            }
//
//            if (!sort.isEmpty()) {
//                requestURL = Constants.URL.SEARCH_RESTAURANTS + "postcode=" + postCode + "&page=" + page + "&ListBy=" + sort;
//            }
//            if (!cuisines.isEmpty()) {
//                requestURL = Constants.URL.SEARCH_RESTAURANTS + "postcode=" + postCode + "&page=" + page + "&cuisine=" + cuisines;
//
//            }
//            if (!cuisines.isEmpty() && !sort.isEmpty()) {
//                requestURL = Constants.URL.SEARCH_RESTAURANTS + "postcode=" + postCode + "&page=" + page + "&ListBy=" + sort + "&cuisine=" + cuisines;
//            }


            JSONObject jsonParamObject = new JSONObject();

            try {
                jsonParamObject.put("lng", "" + model.getLng());
                jsonParamObject.put("Lat", "" + model.getLat());
            } catch (JSONException e) {
                e.printStackTrace();
            }

            ConnectionManager
                    .getInstance()
                    .volleyArrayRequest(activity, isDialog, requestURL, requestTag, jsonParamObject, arrayResponse);


        }


        /**
         * Method Post
         *
         * @param activity
         * @param isDialog
         * @param restaurantId
         * @param jsonResponse
         */

        public static void getRestaurantDetails(Activity activity,
                                                boolean isDialog,
                                                String restaurantId,
                                                VolleyCallBack.JSONResponse jsonResponse) {
            // TODO: 12/5/18 uncomment
            String requestTag = "";
            String requestURL = Constants.URL.RESTAURANT_MENU_DETAILS + "restId=" + restaurantId;


            ConnectionManager
                    .getInstance()
                    .volleyJsonObjectRequest(activity, isDialog, requestURL, requestTag, jsonResponse);
        }

        /**
         * Method Post
         *
         * @param activity
         * @param isDialog
         * @param token
         * @param arrayResponse
         */

        public static void getCustomerReviews(Activity activity,
                                              boolean isDialog,
                                              String token,
                                              String restaurantId,
                                              String page,
                                              VolleyCallBack.ArrayResponse arrayResponse) {
            String requestTag = "";
            String requestURL = Constants.URL.CUSTOMER_REVIEWS + "restId=" + restaurantId;


            ConnectionManager
                    .getInstance()
                    .volleyJsonArrayRequest(activity, isDialog, requestURL, requestTag, arrayResponse);

        }


        /**
         * Method Get
         *
         * @param activity
         * @param isDialog
         * @param token
         * @param stringResponse
         */


        public static void getTermsAndConditions(Activity activity,
                                                 boolean isDialog,
                                                 String token,
                                                 VolleyCallBack.StringResponse stringResponse) {
            String requestTag = "";
            String requestURL = Constants.URL.TERMS_AND_CONDITIONS;
            ConnectionManager
                    .getInstance()
                    .volleyStringRequest(activity, isDialog, requestURL, requestTag, stringResponse);
        }


        /**
         * Method Get
         *
         * @param activity
         * @param isDialog
         * @param token
         * @param stringResponse
         */

        public static void getPrivacyPolicy(Activity activity,
                                            boolean isDialog,
                                            String token,
                                            VolleyCallBack.StringResponse stringResponse) {
            String requestTag = "";
            String requestURL = Constants.URL.PRIVACY_POLICY;
            ConnectionManager
                    .getInstance()
                    .volleyStringRequest(activity, isDialog, requestURL, requestTag, stringResponse);
        }

        /**
         * Method GET
         *
         * @param activity
         * @param isDialog
         * @param token
         * @param orderId
         * @param stringResponse
         */


        public static void trackOrder(Activity activity,
                                      boolean isDialog,
                                      String token,
                                      String orderId,
                                      VolleyCallBack.StringResponse stringResponse) {
            String requestTag = "";
            String requestURL = Constants.URL.ORDER_STATUS + "orderId=" + orderId;
            ConnectionManager
                    .getInstance()
                    .volleyStringRequest(activity, isDialog, requestURL, requestTag, stringResponse);
        }

        /**
         * Method GET
         *
         * @param activity
         * @param isDialog
         * @param token
         * @param arrayResponse
         */

        public static void getCuisines(Activity activity,
                                       boolean isDialog,
                                       String token,
                                       VolleyCallBack.ArrayResponse arrayResponse) {
            String requestTag = "";
            String requestURL = Constants.URL.CUISINES;
            HashMap<String, String> headers = new HashMap<>();

            ConnectionManager
                    .getInstance()
                    .volleyJsonArrayRequest(activity, isDialog, requestURL, requestTag, arrayResponse);
        }

        /**
         * Method Post
         *
         * @param activity
         * @param isDialog
         * @param token
         * @param name
         * @param email
         * @param phone
         * @param subject
         * @param message
         * @param jsonResponse
         */

        public static void contactQuery(Activity activity,
                                        boolean isDialog,
                                        String token,
                                        String name,
                                        String email,
                                        String phone,
                                        String subject,
                                        String message,
                                        VolleyCallBack.JSONResponse jsonResponse) {
            String requestTag = "";
            String requestURL = Constants.URL.CONTACT_QUERY;

            JSONObject jsonParamObject = new JSONObject();

            try {
                jsonParamObject.put("name", "" + name);
                jsonParamObject.put("Email", "" + email);
                jsonParamObject.put("subject", "" + subject);
                jsonParamObject.put("message", "" + message);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            ConnectionManager
                    .getInstance()
                    .volleyJsonObjectRequest(activity, isDialog, requestURL, requestTag, jsonParamObject, jsonResponse);

        }


        /**
         * Method Post
         *
         * @param activity
         * @param isDialog
         * @param resId
         * @param token
         * @param jsonResponse
         */
        public static void getServiceCharges(Activity activity,
                                             boolean isDialog,
                                             String resId,
                                             String token,
                                             VolleyCallBack.JSONResponse jsonResponse) {

            String requestTag = "";

            String requestURL = Constants.URL.SERVICE_CHARGE + "restId=" + resId;

            JSONObject jsonParamObject = new JSONObject();

            ConnectionManager
                    .getInstance()
                    .volleyJsonObjectRequest(activity, isDialog, requestURL, requestTag, jsonParamObject, jsonResponse);
        }


    }


}
