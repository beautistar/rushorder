package com.rushorder.utils;


import android.widget.EditText;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Aleem on 10/22/2017.
 */


public class FieldsValidator {
    private Pattern pattern;
    private Matcher matcher;

//    private static final String NAME_PATTERN = "^[A-Za-z-]{1,100}";
    private static final String NAME_PATTERN = "^[A-Za-z\\s]{1,}[\\.]{0,1}[A-Za-z\\s]{0,}$";
    private static final String USERNAME_PATTERN = "^[.,:;!�#:() A-Za-z0-9-]{1,100}";
    private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+([\\.\\+-][_A-Za-z0-9]+)*@[A-Za-z0-9]+([\\.-][A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    private static final String ADDRESS_PATTERN = "^[ ,.-/_ #'()_A-Za-z0-9-]{2,15}$";
    private static final String NUMBER_PATTERN = "^[0-9]{1,100}";
    ;

    private static FieldsValidator instance;

    /**
     * Get instance of FieldsValidator
     *
     * @return
     */
    public static synchronized FieldsValidator getInstance() {
        if (instance == null) {
            instance = new FieldsValidator();
        }
        return instance;
    }

    /**
     * Email Validate
     *
     * @param email
     * @return
     */
    public boolean validateEmail(final String email) {
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email);
        return matcher.matches();

    }

    /**
     * User Name Validate
     *
     * @param name
     * @return
     */
    public boolean validateUserName(final String name) {
        pattern = Pattern.compile(USERNAME_PATTERN);
        matcher = pattern.matcher(name);
        return matcher.matches();

    }

    /**
     * Name Validate
     *
     * @param name
     * @return
     */
    public boolean validateName(final String name) {
        pattern = Pattern.compile(NAME_PATTERN);
        matcher = pattern.matcher(name);
        return matcher.matches();

    }

    /**
     * Address Validate
     *
     * @param name
     * @return
     */
    public boolean validateAddress(final String name) {
        pattern = Pattern.compile(ADDRESS_PATTERN);
        matcher = pattern.matcher(name);
        return matcher.matches();

    }

    /**
     * Number Validate
     *
     * @param name
     * @return
     */
    public boolean validateNumber(final String name) {
        pattern = Pattern.compile(NUMBER_PATTERN);
        matcher = pattern.matcher(name);
        return matcher.matches();
    }

    public boolean isEditTextEmpty(EditText editText) {
        if (editText.getText().toString().trim().equals(""))
            return true;
        else
            return false;
    }

    public boolean isEightCharacters(String password) {
        if (password.length() < 8) {
            return false;
        } else {
            return true;
        }
    }
}
