package com.rushorder.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.rushorder.R;
import com.rushorder.activities.MainActivity;
import com.rushorder.apis.Apis;
import com.rushorder.app.AppController;
import com.rushorder.constants.Constants;
import com.rushorder.interfaces.VolleyCallBack;
import com.rushorder.managers.PrefManager;
import com.rushorder.utils.AlertHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.List;

public class SignInChoiceFragment extends BaseFragment implements View.OnClickListener {

    private TextView tvSignUp;

    private AlertHelper loader;
    private boolean isFromCart;
    private LoginButton loginButton;
    private ProfileTracker profileTracker;
    private LinearLayout llLogin, llFLogin;
    private CallbackManager callbackManager;
    private AccessTokenTracker accessTokenTracker;
    private String socialId = "", socialFName = "", socialLName = "", socialDob = "", socialEmail = "", type;


    public static SignInChoiceFragment getInstance() {
        return new SignInChoiceFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sign_in_choice, container, false);
        initViews(view);
        setListeners();
        setValues();
        return view;
    }

    @Override
    protected void initViews(View view) {
        llLogin = view.findViewById(R.id.llLogin);
        llFLogin = view.findViewById(R.id.llFLogin);
        tvSignUp = view.findViewById(R.id.tvSignUpLink);
        loginButton = view.findViewById(R.id.loginFaceBook_button);

        super.initViews(view);
    }

    @Override
    protected void setListeners() {
        llLogin.setOnClickListener(this);
        llFLogin.setOnClickListener(this);
        tvSignUp.setOnClickListener(this);

        super.setListeners();
    }


    @Override
    protected void setValues() {
        callbackManager = CallbackManager.Factory.create();

        if (getArguments() != null && getArguments().containsKey(Constants.Keys.IS_FROM_CART_KEY)) {
            isFromCart = getArguments().getBoolean(Constants.Keys.IS_FROM_CART_KEY);
        }
        super.setValues();
    }

    @Override
    public void onClick(View v) {
        if (v == llLogin) {
            startLoginFragment();
        } else if (v == llFLogin) {
            handleFacebookConnect();
        } else {
            startSignUpFragment();
        }
    }


    private void handleFacebookConnect() {

        List<String> permissionNeeds = Arrays.asList("user_birthday", "public_profile", "email");
        loginButton.setReadPermissions(permissionNeeds);
        loginButton.setFragment(this);
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                loader = AlertHelper.showProgress(getActivity(), true);
                GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {

                        try {
                            if (object.has("id"))
                                socialId = object.getString("id");
                            if (object.has("name"))
                                socialFName = object.getString("first_name");
                            socialLName = object.getString("last_name");
                            if (object.has("email"))
                                socialEmail = object.getString("email");
                            if (object.has("birthday"))
                                socialDob = object.getString("birthday");
                            type = "facebook";

                            performSocialLogin();
                            startFacebookProfileTracker();

                        } catch (JSONException e) {
                            loader.dismissProgress();
                            Toast.makeText(getActivity(), e.toString(), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,first_name,last_name,email,gender,birthday");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
                loader.dismissProgress();
                Toast.makeText(getActivity(), "Cancelled", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(FacebookException error) {
                loader.dismissProgress();
                Toast.makeText(getActivity(), error.toString(), Toast.LENGTH_SHORT).show();
            }

        });

        loginButton.performClick();
    }

    private void performSocialLogin() {

        Apis.User.socialLogin(getActivity(), false, socialId, socialEmail, socialFName, socialLName, type, new VolleyCallBack.JSONResponse() {

            @Override
            public void onResponse(JSONObject _response, String _tag) {
                try {
                    loader.dismissProgress();
                    if (_response.getBoolean(Constants.STATUS_SUCCESS)) {
                        showSuccessDialog("Successfully logged in");
                        PrefManager.getInstance().saveUserInfo(_response.toString(), true);
                        startUserLoginConfiguration();
                    }

                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            @Override
            public void onErrorMessage(String _errorMessage) {

                loader.dismissProgress();
                showErrorDialog("" + _errorMessage);

            }

            @Override
            public void onErrorResponse(VolleyError _error, String _tag) {
                loader.dismissProgress();
                showErrorDialog(getString(R.string.something_went_wrong));

            }

            @Override
            public void isConnected(boolean _connected, String _tag) {
                loader.dismissProgress();
                showErrorDialog(getString(R.string.no_internet));
            }
        });


    }

    private void startFacebookProfileTracker() {

        accessTokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(AccessToken oldToken, AccessToken newToken) {
                if (newToken == null) {
//                    ((ChatActivity) AppController.getCurrentActivity()).logoutUser();
                }
            }
        };

        profileTracker = new ProfileTracker() {
            @Override
            protected void onCurrentProfileChanged(Profile oldProfile, Profile newProfile) {


            }
        };
        accessTokenTracker.startTracking();
        profileTracker.startTracking();
    }

    private void startSignUpFragment() {
        MainActivity activity = (MainActivity) AppController.getCurrentActivity();
        if (activity != null) {
            Bundle bundle = new Bundle();
            bundle.putBoolean(Constants.Keys.IS_FROM_CART_KEY, isFromCart);
            SignUpFragment fragment = SignUpFragment.getInstance();
            fragment.setArguments(bundle);
            activity.switchFragmentWithBackStack(fragment);
        }
    }

    private void startLoginFragment() {
        Bundle bundle = new Bundle();
        bundle.putBoolean(Constants.Keys.IS_FROM_CART_KEY, isFromCart);
        switchFragment(SignInFragment.getInstance(), bundle);
    }

    private void startUserLoginConfiguration() {
        MainActivity activity = (MainActivity) AppController.getCurrentActivity();
        if (activity != null) {
            activity.checkUserLogin();
            if (isFromCart) {
                activity.onBackPressed();
            } else {
                activity.clearAllFragments();
            }

        }

    }


}
