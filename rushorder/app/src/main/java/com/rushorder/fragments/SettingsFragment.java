package com.rushorder.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.rushorder.R;

/**
 * Created by Aleem on 1/27/2018.
 */

public class SettingsFragment extends BaseFragment implements View.OnClickListener {

    private ImageView ivMenu;

    public static SettingsFragment getInstance() {
        return new SettingsFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_settings, container, false);
        initViews(view);
        return view;
    }

    @Override
    protected void initViews(View view) {
        ivMenu = view.findViewById(R.id.ivMenu);
        setListeners();
        super.initViews(view);
    }

    @Override
    protected void setListeners() {
        ivMenu.setOnClickListener(this);
        super.setListeners();
    }

    @Override
    public void onClick(View v) {
        if (v == ivMenu) {
            getParentActivity().openDrawer();
        } else {

        }
    }
}
