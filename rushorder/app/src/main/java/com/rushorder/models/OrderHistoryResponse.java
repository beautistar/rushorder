package com.rushorder.models;


import com.rushorder.R;

import java.util.List;

import tellh.com.stickyheaderview_rv.adapter.DataBean;
import tellh.com.stickyheaderview_rv.adapter.StickyHeaderViewAdapter;

/**
 * Created by Aleem on 12/16/2017.
 */

public class OrderHistoryResponse {


    private List<OrdersBean> orders;

    public List<OrdersBean> getOrders() {
        return orders;
    }

    public void setOrders(List<OrdersBean> orders) {
        this.orders = orders;
    }

    public static class OrdersBean {
        /**
         * id : 16400
         * restaurant_id : 9
         * user_id : 2596
         * total : 0
         * description :
         * coupon :
         * coupon_value :
         * coupon_type :
         * coupon_applies_to :
         * coupon_categories :
         * discount_amount : null
         * discount_desc : null
         * status : cooking
         * date : 2018-02-27 14:19:16
         * order_date : 2018-02-26
         * order_count : 2
         * type : mobileapp
         * is_mobile_order : no
         * order_stats : false
         * payment_mode : cash
         * payment_status : completed
         * delivery_type : collected
         * delivery_charges : 0
         * card_surcharge : 0
         * addby : mobileapp
         * addon :
         * carry_bag : 0
         * order_format_status : fresh
         * customer_phone : 0334556046
         * customer_name : aleem azhar
         * merchant_responce :
         * is_preorder : no
         * preorder_date : null
         * preorder_time : null
         * print_delay : no
         * eatinn_table_id : 0
         * eatinn_table_name : null
         * eatinn_floorname : null
         * eatinn_payment_status : pending
         * bill_persons : null
         * name : I Fry
         * address : 35 Hylton Road
         * cell_number : 1915655535
         * icon_name : https://s3.eu-west-2.amazonaws.com/jayeat/restaurant_icons/logo_1518909532.png
         * resturent_detail : {"id":"9","client_id":"8","domain":"ifryhylton.com","name":"I Fry","seo_url":"i-fry","description":"Fish and Chips","cuisines":"","icon_name":"https://s3.eu-west-2.amazonaws.com/jayeat/restaurant_icons/logo_1518909532.png","address":"35 Hylton Road","city":"1","cell_number":"1915655535","res_cellnumber":null,"postcode":"SR47AF","latitude":"54.9067728","longitude":"-1.3959758999999394","delivery_type":"collection","status":"enable_website,enable_jayeat,enable_epos","created_at":null,"logo":""}
         * order_description :
         * cart : [{"id":"57731","order_id":"16400","item_id":"50580","item_name":"Chicken Wrap","item_comments":null,"item_qty":"1","item_price":"4.5","item_parent":"0","item_parent_index":"0","item_index":"0","status":"cooking","bill_no":null,"SoldAddonItem":[{"id":"57732","order_id":"16400","item_id":"50581","item_name":"Chilli","item_comments":null,"item_qty":"1","item_price":"0","item_parent":"50580","item_parent_index":"0","item_index":"1","status":"cooking","bill_no":null},{"id":"57733","order_id":"16400","item_id":"50582","item_name":"Chilli","item_comments":null,"item_qty":"1","item_price":"0","item_parent":"50580","item_parent_index":"0","item_index":"2","status":"cooking","bill_no":null},{"id":"57734","order_id":"16400","item_id":"50583","item_name":"With Salad","item_comments":null,"item_qty":"1","item_price":"0","item_parent":"50580","item_parent_index":"0","item_index":"3","status":"cooking","bill_no":null}]}]
         * show_feedback_button : true
         */

        private String id;
        private String restaurant_id;
        private String user_id;
        private String total;
        private String description;
        private String coupon;
        private String coupon_value;
        private String coupon_type;
        private String coupon_applies_to;
        private String coupon_categories;
        private String discount_amount;
        private String discount_desc;
        private String status;
        private String date;
        private String order_date;
        private String order_count;
        private String type;
        private String is_mobile_order;
        private String order_stats;
        private String payment_mode;
        private String payment_status;
        private String delivery_type;
        private String delivery_charges;
        private String card_surcharge;
        private String addby;
        private String addon;
        private String carry_bag;
        private String order_format_status;
        private String customer_phone;
        private String customer_name;
        private String merchant_responce;
        private String is_preorder;
        private String preorder_date;
        private String preorder_time;
        private String print_delay;
        private String eatinn_table_id;
        private String eatinn_table_name;
        private String eatinn_floorname;
        private String eatinn_payment_status;
        private String bill_persons;

        private String name;
        private String address;
        private String cell_number;
        private String icon_name;
        private ResturentDetailBean resturent_detail;
        private String order_description;
        private boolean show_feedback_button;


        private List<CartBean> cart;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getRestaurant_id() {
            return restaurant_id;
        }

        public void setRestaurant_id(String restaurant_id) {
            this.restaurant_id = restaurant_id;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getTotal() {
            return total;
        }

        public void setTotal(String total) {
            this.total = total;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getCoupon() {
            return coupon;
        }

        public void setCoupon(String coupon) {
            this.coupon = coupon;
        }

        public String getCoupon_value() {
            return coupon_value;
        }

        public void setCoupon_value(String coupon_value) {
            this.coupon_value = coupon_value;
        }

        public String getCoupon_type() {
            return coupon_type;
        }

        public void setCoupon_type(String coupon_type) {
            this.coupon_type = coupon_type;
        }

        public String getCoupon_applies_to() {
            return coupon_applies_to;
        }

        public void setCoupon_applies_to(String coupon_applies_to) {
            this.coupon_applies_to = coupon_applies_to;
        }

        public String getCoupon_categories() {
            return coupon_categories;
        }

        public void setCoupon_categories(String coupon_categories) {
            this.coupon_categories = coupon_categories;
        }

        public String getDiscount_amount() {
            return discount_amount;
        }

        public void setDiscount_amount(String discount_amount) {
            this.discount_amount = discount_amount;
        }

        public String getDiscount_desc() {
            return discount_desc;
        }

        public void setDiscount_desc(String discount_desc) {
            this.discount_desc = discount_desc;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public String getOrder_date() {
            return order_date;
        }

        public void setOrder_date(String order_date) {
            this.order_date = order_date;
        }

        public String getOrder_count() {
            return order_count;
        }

        public void setOrder_count(String order_count) {
            this.order_count = order_count;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getIs_mobile_order() {
            return is_mobile_order;
        }

        public void setIs_mobile_order(String is_mobile_order) {
            this.is_mobile_order = is_mobile_order;
        }

        public String getOrder_stats() {
            return order_stats;
        }

        public void setOrder_stats(String order_stats) {
            this.order_stats = order_stats;
        }

        public String getPayment_mode() {
            return payment_mode;
        }

        public void setPayment_mode(String payment_mode) {
            this.payment_mode = payment_mode;
        }

        public String getPayment_status() {
            return payment_status;
        }

        public void setPayment_status(String payment_status) {
            this.payment_status = payment_status;
        }

        public String getDelivery_type() {
            return delivery_type;
        }

        public void setDelivery_type(String delivery_type) {
            this.delivery_type = delivery_type;
        }

        public String getDelivery_charges() {
            return delivery_charges;
        }

        public void setDelivery_charges(String delivery_charges) {
            this.delivery_charges = delivery_charges;
        }

        public String getCard_surcharge() {
            return card_surcharge;
        }

        public void setCard_surcharge(String card_surcharge) {
            this.card_surcharge = card_surcharge;
        }

        public String getAddby() {
            return addby;
        }

        public void setAddby(String addby) {
            this.addby = addby;
        }

        public String getAddon() {
            return addon;
        }

        public void setAddon(String addon) {
            this.addon = addon;
        }

        public String getCarry_bag() {
            return carry_bag;
        }

        public void setCarry_bag(String carry_bag) {
            this.carry_bag = carry_bag;
        }

        public String getOrder_format_status() {
            return order_format_status;
        }

        public void setOrder_format_status(String order_format_status) {
            this.order_format_status = order_format_status;
        }

        public String getCustomer_phone() {
            return customer_phone;
        }

        public void setCustomer_phone(String customer_phone) {
            this.customer_phone = customer_phone;
        }

        public String getCustomer_name() {
            return customer_name;
        }

        public void setCustomer_name(String customer_name) {
            this.customer_name = customer_name;
        }

        public String getMerchant_responce() {
            return merchant_responce;
        }

        public void setMerchant_responce(String merchant_responce) {
            this.merchant_responce = merchant_responce;
        }

        public String getIs_preorder() {
            return is_preorder;
        }

        public void setIs_preorder(String is_preorder) {
            this.is_preorder = is_preorder;
        }

        public String getPreorder_date() {
            return preorder_date;
        }

        public void setPreorder_date(String preorder_date) {
            this.preorder_date = preorder_date;
        }

        public String getPreorder_time() {
            return preorder_time;
        }

        public void setPreorder_time(String preorder_time) {
            this.preorder_time = preorder_time;
        }

        public String getPrint_delay() {
            return print_delay;
        }

        public void setPrint_delay(String print_delay) {
            this.print_delay = print_delay;
        }

        public String getEatinn_table_id() {
            return eatinn_table_id;
        }

        public void setEatinn_table_id(String eatinn_table_id) {
            this.eatinn_table_id = eatinn_table_id;
        }

        public String getEatinn_table_name() {
            return eatinn_table_name;
        }

        public void setEatinn_table_name(String eatinn_table_name) {
            this.eatinn_table_name = eatinn_table_name;
        }

        public String getEatinn_floorname() {
            return eatinn_floorname;
        }

        public void setEatinn_floorname(String eatinn_floorname) {
            this.eatinn_floorname = eatinn_floorname;
        }

        public String getEatinn_payment_status() {
            return eatinn_payment_status;
        }

        public void setEatinn_payment_status(String eatinn_payment_status) {
            this.eatinn_payment_status = eatinn_payment_status;
        }

        public String getBill_persons() {
            return bill_persons;
        }

        public void setBill_persons(String bill_persons) {
            this.bill_persons = bill_persons;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getCell_number() {
            return cell_number;
        }

        public void setCell_number(String cell_number) {
            this.cell_number = cell_number;
        }

        public String getIcon_name() {
            return icon_name;
        }

        public void setIcon_name(String icon_name) {
            this.icon_name = icon_name;
        }

        public ResturentDetailBean getResturent_detail() {
            return resturent_detail;
        }

        public void setResturent_detail(ResturentDetailBean resturent_detail) {
            this.resturent_detail = resturent_detail;
        }

        public String getOrder_description() {
            return order_description;
        }

        public void setOrder_description(String order_description) {
            this.order_description = order_description;
        }

        public boolean isShow_feedback_button() {
            return show_feedback_button;
        }

        public void setShow_feedback_button(boolean show_feedback_button) {
            this.show_feedback_button = show_feedback_button;
        }

        public List<CartBean> getCart() {
            return cart;
        }

        public void setCart(List<CartBean> cart) {
            this.cart = cart;
        }

        public static class ResturentDetailBean {
            /**
             * id : 9
             * client_id : 8
             * domain : ifryhylton.com
             * name : I Fry
             * seo_url : i-fry
             * description : Fish and Chips
             * cuisines :
             * icon_name : https://s3.eu-west-2.amazonaws.com/jayeat/restaurant_icons/logo_1518909532.png
             * address : 35 Hylton Road
             * city : 1
             * cell_number : 1915655535
             * res_cellnumber : null
             * postcode : SR47AF
             * latitude : 54.9067728
             * longitude : -1.3959758999999394
             * delivery_type : collection
             * status : enable_website,enable_jayeat,enable_epos
             * created_at : null
             * logo :
             */

            private String id;
            private String client_id;
            private String domain;
            private String name;
            private String seo_url;
            private String description;
            private String cuisines;
            private String icon_name;
            private String address;
            private String city;
            private String cell_number;
            private String res_cellnumber;
            private String postcode;
            private String latitude;
            private String longitude;
            private String delivery_type;
            private String status;
            private String created_at;
            private String logo;
            private String api_key;
            private float minimum_spending;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getClient_id() {
                return client_id;
            }

            public void setClient_id(String client_id) {
                this.client_id = client_id;
            }

            public String getDomain() {
                return domain;
            }

            public void setDomain(String domain) {
                this.domain = domain;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getSeo_url() {
                return seo_url;
            }

            public void setSeo_url(String seo_url) {
                this.seo_url = seo_url;
            }

            public String getDescription() {
                return description;
            }

            public void setDescription(String description) {
                this.description = description;
            }

            public String getCuisines() {
                return cuisines;
            }

            public void setCuisines(String cuisines) {
                this.cuisines = cuisines;
            }

            public String getIcon_name() {
                return icon_name;
            }

            public void setIcon_name(String icon_name) {
                this.icon_name = icon_name;
            }

            public String getAddress() {
                return address;
            }

            public void setAddress(String address) {
                this.address = address;
            }

            public String getCity() {
                return city;
            }

            public void setCity(String city) {
                this.city = city;
            }

            public String getCell_number() {
                return cell_number;
            }

            public void setCell_number(String cell_number) {
                this.cell_number = cell_number;
            }

            public String getRes_cellnumber() {
                return res_cellnumber;
            }

            public void setRes_cellnumber(String res_cellnumber) {
                this.res_cellnumber = res_cellnumber;
            }

            public String getPostcode() {
                return postcode;
            }

            public void setPostcode(String postcode) {
                this.postcode = postcode;
            }

            public String getLatitude() {
                return latitude;
            }

            public void setLatitude(String latitude) {
                this.latitude = latitude;
            }

            public String getLongitude() {
                return longitude;
            }

            public void setLongitude(String longitude) {
                this.longitude = longitude;
            }

            public String getDelivery_type() {
                return delivery_type;
            }

            public void setDelivery_type(String delivery_type) {
                this.delivery_type = delivery_type;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public String getCreated_at() {
                return created_at;
            }

            public void setCreated_at(String created_at) {
                this.created_at = created_at;
            }

            public String getLogo() {
                return logo;
            }

            public void setLogo(String logo) {
                this.logo = logo;
            }

            public String getApi_key() {
                return api_key;
            }

            public void setApi_key(String api_key) {
                this.api_key = api_key;
            }

            public float getMinimum_spending() {
                return minimum_spending;
            }

            public void setMinimum_spending(float minimum_spending) {
                this.minimum_spending = minimum_spending;
            }
        }

        public static class CartBean extends DataBean {
            /**
             * id : 57731
             * order_id : 16400
             * item_id : 50580
             * item_name : Chicken Wrap
             * item_comments : null
             * item_qty : 1
             * item_price : 4.5
             * item_parent : 0
             * item_parent_index : 0
             * item_index : 0
             * status : cooking
             * bill_no : null
             * SoldAddonItem : [{"id":"57732","order_id":"16400","item_id":"50581","item_name":"Chilli","item_comments":null,"item_qty":"1","item_price":"0","item_parent":"50580","item_parent_index":"0","item_index":"1","status":"cooking","bill_no":null},{"id":"57733","order_id":"16400","item_id":"50582","item_name":"Chilli","item_comments":null,"item_qty":"1","item_price":"0","item_parent":"50580","item_parent_index":"0","item_index":"2","status":"cooking","bill_no":null},{"id":"57734","order_id":"16400","item_id":"50583","item_name":"With Salad","item_comments":null,"item_qty":"1","item_price":"0","item_parent":"50580","item_parent_index":"0","item_index":"3","status":"cooking","bill_no":null}]
             */

            private String id;
            private String order_id;
            private String item_id;
            private String item_name;
            private String item_comments;
            private String item_qty;
            private String item_price;
            private String item_parent;
            private String item_parent_index;
            private String item_index;
            private String status;
            private String bill_no;
            private transient int cartItemSize;
            private List<ChildsBean> childs;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getOrder_id() {
                return order_id;
            }

            public void setOrder_id(String order_id) {
                this.order_id = order_id;
            }

            public String getItem_id() {
                return item_id;
            }

            public void setItem_id(String item_id) {
                this.item_id = item_id;
            }

            public String getItem_name() {
                return item_name;
            }

            public void setItem_name(String item_name) {
                this.item_name = item_name;
            }

            public String getItem_comments() {
                return item_comments;
            }

            public void setItem_comments(String item_comments) {
                this.item_comments = item_comments;
            }

            public String getItem_qty() {
                return item_qty;
            }

            public void setItem_qty(String item_qty) {
                this.item_qty = item_qty;
            }

            public String getItem_price() {
                return item_price;
            }

            public void setItem_price(String item_price) {
                this.item_price = item_price;
            }

            public String getItem_parent() {
                return item_parent;
            }

            public void setItem_parent(String item_parent) {
                this.item_parent = item_parent;
            }

            public String getItem_parent_index() {
                return item_parent_index;
            }

            public void setItem_parent_index(String item_parent_index) {
                this.item_parent_index = item_parent_index;
            }

            public String getItem_index() {
                return item_index;
            }

            public void setItem_index(String item_index) {
                this.item_index = item_index;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public String getBill_no() {
                return bill_no;
            }

            public void setBill_no(String bill_no) {
                this.bill_no = bill_no;
            }

            public List<ChildsBean> getChilds() {
                return childs;
            }

            public void setChilds(List<ChildsBean> childs) {
                this.childs = childs;
            }

            public int getCartItemSize() {
                return cartItemSize;
            }

            public void setCartItemSize(int cartItemSize) {
                this.cartItemSize = cartItemSize;
            }

            @Override
            public int getItemLayoutId(StickyHeaderViewAdapter stickyHeaderViewAdapter) {
                return R.layout.rv_order_history_child_row;

            }

            public static class ChildsBean {
                /**
                 * id : 57732
                 * order_id : 16400
                 * item_id : 50581
                 * item_name : Chilli
                 * item_comments : null
                 * item_qty : 1
                 * item_price : 0
                 * item_parent : 50580
                 * item_parent_index : 0
                 * item_index : 1
                 * status : cooking
                 * bill_no : null
                 */

                private String id;
                private String order_id;
                private String item_id;
                private String item_name;
                private String item_comments;
                private String item_qty;
                private String item_price;
                private String item_parent;
                private String item_parent_index;
                private String item_index;
                private String status;
                private String bill_no;

                public String getId() {
                    return id;
                }

                public void setId(String id) {
                    this.id = id;
                }

                public String getOrder_id() {
                    return order_id;
                }

                public void setOrder_id(String order_id) {
                    this.order_id = order_id;
                }

                public String getItem_id() {
                    return item_id;
                }

                public void setItem_id(String item_id) {
                    this.item_id = item_id;
                }

                public String getItem_name() {
                    return item_name;
                }

                public void setItem_name(String item_name) {
                    this.item_name = item_name;
                }

                public String getItem_comments() {
                    return item_comments;
                }

                public void setItem_comments(String item_comments) {
                    this.item_comments = item_comments;
                }

                public String getItem_qty() {
                    return item_qty;
                }

                public void setItem_qty(String item_qty) {
                    this.item_qty = item_qty;
                }

                public String getItem_price() {
                    return item_price;
                }

                public void setItem_price(String item_price) {
                    this.item_price = item_price;
                }

                public String getItem_parent() {
                    return item_parent;
                }

                public void setItem_parent(String item_parent) {
                    this.item_parent = item_parent;
                }

                public String getItem_parent_index() {
                    return item_parent_index;
                }

                public void setItem_parent_index(String item_parent_index) {
                    this.item_parent_index = item_parent_index;
                }

                public String getItem_index() {
                    return item_index;
                }

                public void setItem_index(String item_index) {
                    this.item_index = item_index;
                }

                public String getStatus() {
                    return status;
                }

                public void setStatus(String status) {
                    this.status = status;
                }

                public String getBill_no() {
                    return bill_no;
                }

                public void setBill_no(String bill_no) {
                    this.bill_no = bill_no;
                }
            }
        }
    }
}