package com.rushorder.driver.track_driver.presenter;

import android.location.Location;

public interface IDriverLocationPresenter {
    void doneOrderDelivery(String orderId, String status);
    void updateUserLocation(String orderId, Location location);
    void location();
    void getOrderDestination(String orderId);
}
