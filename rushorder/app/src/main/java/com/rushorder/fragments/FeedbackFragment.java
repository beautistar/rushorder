package com.rushorder.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.VolleyError;

import com.rushorder.R;
import com.rushorder.apis.Apis;
import com.rushorder.constants.Constants;
import com.rushorder.interfaces.VolleyCallBack;
import com.rushorder.managers.PrefManager;
import com.rushorder.models.SignInResponse;
import com.rushorder.utils.Utils;
import com.iarcuschin.simpleratingbar.SimpleRatingBar;

import org.json.JSONObject;

/**
 * Created by Aleem on 12/17/2017.
 */

public class FeedbackFragment extends BaseFragment implements View.OnClickListener, SimpleRatingBar.OnRatingBarChangeListener {
    private String orderId;
    private EditText etUserNote;
    private LinearLayout llAddComment;
    private TextView tvSubmit, tvOrderId, tvCancel;
    private SimpleRatingBar rbAmount, rbTaste, rbDelivery;
    private String amountRating = "", foodTasteRating = "", deliveryRating = "", userComments = "";
    private String resId;

    public static FeedbackFragment getInstance() {
        return new FeedbackFragment();
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_feedback, container, false);
        initViews(view);
        return view;
    }


    @Override
    protected void initViews(View view) {

        tvCancel = view.findViewById(R.id.tvCancel);
        tvSubmit = view.findViewById(R.id.tvSubmit);
        rbTaste = view.findViewById(R.id.rbFoodTaste);
        tvOrderId = view.findViewById(R.id.tvOrderId);
        rbAmount = view.findViewById(R.id.rbAmountRating);
        llAddComment = view.findViewById(R.id.llAddComment);
        etUserNote = view.findViewById(R.id.etFeedbackNote);
        rbDelivery = view.findViewById(R.id.rbDeliveryRating);
        orderId = getArguments().getString(Constants.Keys.ORDER_ID);
        resId = getArguments().getString(Constants.Keys.RESTAURANT_KEY);
        setValues();
        setListeners();
        super.initViews(view);
    }

    @Override
    protected void setValues() {
        tvOrderId.setText("Order# " + orderId);
        super.setValues();
    }

    @Override
    protected void setListeners() {
        rbAmount.setOnRatingBarChangeListener(this);
        rbDelivery.setOnRatingBarChangeListener(this);
        rbTaste.setOnRatingBarChangeListener(this);
        llAddComment.setOnClickListener(this);
        tvSubmit.setOnClickListener(this);
        tvCancel.setOnClickListener(this);
        super.setListeners();

    }


    private void saveFeedBack() {
        SignInResponse signInResponse = PrefManager.getInstance().getObject(PrefManager.USER_INFO, SignInResponse.class);
        Apis.Order.saveFeedBack(getActivity(), true, amountRating, foodTasteRating, deliveryRating, "0", userComments, resId, signInResponse.getUserID(), "5", "", orderId, "", new VolleyCallBack.JSONResponse() {
            @Override
            public void onResponse(JSONObject _response, String _tag) {
                try {
                    showSuccessDialog("Successfully Posted");
                    onBackPressed();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            @Override
            public void onErrorMessage(String _errorMessage) {
                showErrorDialog("" + _errorMessage);
            }

            @Override
            public void onErrorResponse(VolleyError _error, String _tag) {
                showErrorDialog("" + getString(R.string.something_went_wrong));
            }

            @Override
            public void isConnected(boolean _connected, String _tag) {
                showErrorDialog("" + getString(R.string.no_internet));
            }
        });


    }

    @Override
    public void onClick(View view) {
        if (view == llAddComment) {
            etUserNote.requestFocus();
            Utils.showSoftKeyboard(getActivity(), etUserNote);
        } else if (view == tvCancel) {
            onBackPressed();
        } else {
            userComments = etUserNote.getText().toString();
            if (validateFields()) {
                saveFeedBack();
            }
        }

    }

    private boolean validateFields() {
        if (!amountRating.isEmpty() && !deliveryRating.isEmpty() && !foodTasteRating.isEmpty() && !userComments.isEmpty()) {
            return true;
        } else {
            showErrorDialog(getString(R.string.please_fill_all_fields));
        }
        return false;
    }


    @Override
    public void onRatingChanged(SimpleRatingBar ratingBar, float rating, boolean fromUser) {
        int ratingBarId = ratingBar.getId();
        if (ratingBarId == R.id.rbAmountRating) {
            amountRating = (rating == 0.0) ? "" : "" + rating;
        } else if (ratingBarId == R.id.rbDeliveryRating) {
            deliveryRating = (rating == 0.0) ? "" : "" + rating;
        } else {
            foodTasteRating = (rating == 0.0) ? "" : "" + rating;
        }
    }
}
