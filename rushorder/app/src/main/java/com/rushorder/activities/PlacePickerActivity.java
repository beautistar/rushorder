package com.rushorder.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import com.google.android.gms.location.places.Place;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.rushorder.R;
import com.rushorder.location.LocationAutoCompleteView;
import com.rushorder.location.interfaces.OnQueryCompleteListener;


public class PlacePickerActivity extends AppCompatActivity implements OnQueryCompleteListener, OnMapReadyCallback {

    private GoogleMap map;
    private LinearLayout llSubmit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_place_picker);


        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        llSubmit = findViewById(R.id.llSubmit);
        llSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

    }

    @Override
    public void onTextClear() {
        map.clear();
    }

    @Override
    public void onPlaceSelected(Place selectedPlace) {
        map.clear();
        map.addMarker(new MarkerOptions().position(selectedPlace.getLatLng()));
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(selectedPlace.getLatLng(), 16));
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        map = googleMap;

        LatLng madrid = new LatLng(31.549841992986963, 74.3918302074096);
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(madrid, 16));

        LocationAutoCompleteView autoCompleteLocation = findViewById(R.id.autocomplete_view);
        autoCompleteLocation.setOnQueryCompleteListener(this);
    }
}
