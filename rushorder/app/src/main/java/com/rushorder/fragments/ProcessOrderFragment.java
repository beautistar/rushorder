package com.rushorder.fragments;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.res.ResourcesCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;
import com.rushorder.R;
import com.rushorder.apis.Apis;
import com.rushorder.constants.Constants;
import com.rushorder.interfaces.VolleyCallBack;
import com.rushorder.managers.PrefManager;
import com.rushorder.models.Cart;
import com.rushorder.models.CouponCodeResponse;
import com.rushorder.models.PostCodeResponse;
import com.rushorder.models.SignInResponse;
import com.rushorder.utils.JsonTranslator;
import com.rushorder.utils.Utils;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ProcessOrderFragment extends BaseFragment implements View.OnClickListener {

    private String cardCharges;
    private float discount = 0.0f;
    private float totalPrice = 0.0f;
    private float grandPrice = 0.0f;
    private LinearLayout llPostCode;
    private float payWithAmount = 0.0f;
    private float deliveryCharge = 0.0f;
    private float minimumSpending = 0.0f;
    private SignInResponse signInResponse;
    private CouponCodeResponse codeResponse;
    private PostCodeResponse postCodeResponse;
    private String orderType = Constants.DELIVERY;
    private String preOrderTime, preOrderDate, preOrderNote;
    private boolean isPreOrder, isDiscountApplied, isDeliveryAvailable;
    private String phone, postCode, doorNumber, couponeCode, street, town, userComments;
    private LinearLayout llDelivery, llCollection, llAddress, llDoorNumber, llPayWithCard, llCashOnDelivery;
    private EditText etPostCode, etPhone, etAddressLineOne, etAddressLineTwo, etDoorNumber, etCouponeCode, etComments;
    private TextView tvCheck, tvApply, tvDeliveryCharge, tvDiscount, tvTotal, tvGrandTotal, tvDeliveryAvailable, tvPayWithCard, tvCollection, tvDelivery;

    int PLACE_PICKER_REQUEST = 1;

    public static ProcessOrderFragment getInstance() {
        return new ProcessOrderFragment();
    }

    @BindView(R.id.tvPickLocation)
    TextView tvPickLocation;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_process_order, container, false);

        ButterKnife.bind(this,view);
        initViews(view);
        setListeners();
        setValues();
        return view;
    }
    String latitude="", longitude ="";
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == Activity.RESULT_OK) {
                Place place = PlacePicker.getPlace(Objects.requireNonNull(getActivity()),data);
                LatLng latLng = place.getLatLng();
                 latitude = latLng.latitude+"";
                longitude = latLng.longitude+"";
                Log.e("latitude",latitude+"-----"+longitude);
//                String toastMsg = String.format("Place: %s", place.getName());
//                Toast.makeText(getActivity(), toastMsg, Toast.LENGTH_LONG).show();
                tvPickLocation.setText(place.getAddress());
            }
        }
    }


    //    Pay with formula (Grand Total /100) * 2.5 + 0.20
    @OnClick(R.id.tvPickLocation)
    public void onPickLocationclicked()
    {
        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();

        try {
            startActivityForResult(builder.build(Objects.requireNonNull(getActivity())), PLACE_PICKER_REQUEST);
        } catch (GooglePlayServicesRepairableException e) {
            e.printStackTrace();
        } catch (GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }

    }
    @Override
    protected void initViews(View view) {

        tvApply = view.findViewById(R.id.tvApply);
        tvCheck = view.findViewById(R.id.tvCheck);
        etPhone = view.findViewById(R.id.etPhone);
        llAddress = view.findViewById(R.id.llAddress);
        tvTotal = view.findViewById(R.id.tvTotalPrice);
        tvDiscount = view.findViewById(R.id.tvDiscount);
        llDelivery = view.findViewById(R.id.llDelivery);
        llPostCode = view.findViewById(R.id.llPostCode);
        etComments = view.findViewById(R.id.etComments);
        etPostCode = view.findViewById(R.id.etPostCode);
        tvGrandTotal = view.findViewById(R.id.tvGrandTotal);
        tvCollection = view.findViewById(R.id.tvCollection);
        llDoorNumber = view.findViewById(R.id.llDoorNumber);
        llCollection = view.findViewById(R.id.llCollection);
        etDoorNumber = view.findViewById(R.id.etDoorNumber);
        tvDelivery = view.findViewById(R.id.tvDeliveryTitle);
        llPayWithCard = view.findViewById(R.id.llPayWithCard);
        tvPayWithCard = view.findViewById(R.id.tvPayWithCard);
        etCouponeCode = view.findViewById(R.id.etCouponeCode);
        tvDeliveryCharge = view.findViewById(R.id.tvDeliveryCharge);
        llCashOnDelivery = view.findViewById(R.id.llCashOnDelivery);
        etAddressLineTwo = view.findViewById(R.id.etAddressLineTwo);
        etAddressLineOne = view.findViewById(R.id.etAddressLineOne);
//        tvDeliveryAvailable = view.findViewById(R.id.tvDeliveryAvailable);
        signInResponse = PrefManager.getInstance().getObject(PrefManager.USER_INFO, SignInResponse.class);


        super.initViews(view);
    }


    @Override
    protected void setValues() {
        discount = getArguments().getFloat(Constants.Keys.DISCOUNT, 0.0f);
        grandPrice = getArguments().getFloat(Constants.Keys.GRAND_TOTAL, 0.0f);
        totalPrice = getArguments().getFloat(Constants.Keys.TOTAL_PRICE, 0.0f);
        isPreOrder = getArguments().getBoolean(Constants.Keys.IS_PREORDER, false);
        preOrderNote = getArguments().getString(Constants.Keys.PREORDER_NOTE, "");
        preOrderTime = getArguments().getString(Constants.Keys.PREORDER_TIME, "");
        preOrderDate = getArguments().getString(Constants.Keys.PREORDER_DATE, "");

//        etPostCode.setText((signInResponse.getData().getZipcode() != null) ? signInResponse.getData().getZipcode() : "");
//        etPhone.setText((signInResponse.getData().getPhonenumber() != null) ? signInResponse.getData().getPhonenumber() : "");
//        etAddressLineOne.setText((signInResponse.getData().getAddress() != null) ? signInResponse.getData().getAddress() : "");
//        etAddressLineTwo.setText((signInResponse.getData().getTown() != null) ? signInResponse.getData().getTown() : "");
//        etDoorNumber.setText((signInResponse.getData().getHouse() != null) ? signInResponse.getData().getHouse() : "");

        etPhone.setText("" + PrefManager.getInstance().getString(PrefManager.PHONE));
        etAddressLineOne.setText("" + PrefManager.getInstance().getString(PrefManager.STREET));
        etAddressLineTwo.setText("" + PrefManager.getInstance().getString(PrefManager.TOWN));
        etDoorNumber.setText("" + PrefManager.getInstance().getString(PrefManager.DOOR_NUMBER));
        etComments.setText("" + preOrderNote);

        calculatePrice();

//        getServiceCharges();


        super.setValues();
    }

    private void getServiceCharges() {
        Apis.Restaurant.getServiceCharges(getActivity(), true, PrefManager.getInstance().getString(PrefManager.RES_ID), "", new VolleyCallBack.JSONResponse() {
            @Override
            public void onResponse(JSONObject _response, String _tag) {
                try {

                    payWithAmount = Float.parseFloat(_response.getString("ServiceCharge"));

                } catch (Exception ex) {
                    ex.printStackTrace();
                }

            }

            @Override
            public void onErrorMessage(String _errorMessage) {
                showErrorDialog(_errorMessage);
            }

            @Override
            public void onErrorResponse(VolleyError _error, String _tag) {
                showErrorDialog(getString(R.string.something_went_wrong));
            }

            @Override
            public void isConnected(boolean _connected, String _tag) {
                showErrorDialog(getString(R.string.no_internet));
            }
        });
    }

    private void calculatePrice() {
        try {

            if (orderType.equals(Constants.DELIVERY)) {
                grandPrice = (totalPrice + deliveryCharge) - discount;
            } else {
                grandPrice = totalPrice - discount;
            }

//            payWithAmount = (grandPrice / 100) * 2.5f + 0.20f;
            cardCharges = Utils.formatToTwoDecimalPlaces(payWithAmount);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        updatePrice();
    }


    private void updatePrice() {
//        tvPayWithCard.setText("Pay With Card ");
        tvTotal.setText(getString(R.string.pound_sign) + " " + Utils.formatToTwoDecimalPlaces(totalPrice));
        tvDiscount.setText(getString(R.string.pound_sign) + " " + Utils.formatToTwoDecimalPlaces(discount));
        tvGrandTotal.setText(getString(R.string.pound_sign) + " " + Utils.formatToTwoDecimalPlaces(grandPrice));
        tvDeliveryCharge.setText(getString(R.string.pound_sign) + " " + (orderType.equals(Constants.DELIVERY) ? Utils.formatToTwoDecimalPlaces(deliveryCharge) : "0.00"));
    }

    @Override
    protected void setListeners() {
        llBack.setOnClickListener(this);
        tvCheck.setOnClickListener(this);
        tvApply.setOnClickListener(this);
        llDelivery.setOnClickListener(this);
        llCollection.setOnClickListener(this);
        llPayWithCard.setOnClickListener(this);
        llCashOnDelivery.setOnClickListener(this);
        super.setListeners();
    }


    @Override
    public void onClick(View view) {
        if (view == llBack) {
            onBackPressed();
        } else if (view == llDelivery) {
            showDeliveryFields();
        } else if (view == llCollection) {
            showCollectionFields();
        } else if (view == tvCheck) {
            validatePostCode();
        } else if (view == tvApply) {
            validateCouponCode();
        } else if (view == llPayWithCard) {
            showErrorDialog("Sorry this area is not covered");
//            validateOrderInformation(true);
        } else if (view == llCashOnDelivery) {
            validateOrderInformation(false);
        }

    }


    private void validateOrderInformation(boolean payWithCard) {

        phone = etPhone.getText().toString();
        postCode = etPostCode.getText().toString();
        town = etAddressLineTwo.getText().toString();
        userComments = etComments.getText().toString();
//        street = etAddressLineOne.getText().toString();
        street = tvPickLocation.getText().toString();
        doorNumber = etDoorNumber.getText().toString();
        couponeCode = etCouponeCode.getText().toString();

        if (orderType.equals(Constants.DELIVERY)) {

            if (!phone.isEmpty() && !town.isEmpty() && !street.isEmpty() && !doorNumber.isEmpty()) {

//                if (postCodeResponse != null) {
//                    if (isDeliveryAvailable) {
                setOrderDetails(payWithCard);
//                    } else {
//                        showErrorDialog("Delivery not available");
////                        showErrorDialog(postCodeResponse.getDeliverPostCode_status());
//                    }
//                } else {
//                    showErrorDialog("Please check postcode");
//                }
            } else {
                showErrorDialog("Please fill the required fields");
            }
        } else {

            if (!phone.isEmpty()) {
                setOrderDetails(payWithCard);
            } else {
                showErrorDialog("Please fill the required fields");
            }
        }

    }


    private void validatePostCode() {
        if (!etPostCode.getText().toString().isEmpty()) {
            postCode = etPostCode.getText().toString();
            searchPostCode();
        } else {
            showErrorDialog("Please enter the postcode");
        }
    }


    private void validateCouponCode() {
        if (!etCouponeCode.getText().toString().isEmpty()) {
            couponeCode = etCouponeCode.getText().toString();
            getDiscount();
        } else {
            showErrorDialog("Please enter the couponcode");
        }
    }


    private void showDeliveryFields() {
        orderType = Constants.DELIVERY;

        tvDelivery.setTextColor(getResources().getColor(R.color.white));
        tvCollection.setTextColor(getResources().getColor(R.color.colorPrimary));
        llDelivery.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.left_selected_shape, null));
        llCollection.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.right_unselected_shape, null));

        llAddress.setVisibility(View.VISIBLE);
//        llPostCode.setVisibility(View.VISIBLE);
        llDoorNumber.setVisibility(View.VISIBLE);
        calculatePrice();
    }

    private void showCollectionFields() {
        orderType = Constants.COLLECTION;
        tvDelivery.setTextColor(getResources().getColor(R.color.colorPrimary));
        tvCollection.setTextColor(getResources().getColor(R.color.white));
        llDelivery.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.left_unselected_shape, null));
        llCollection.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.right_selected_shape, null));
        llAddress.setVisibility(View.GONE);
//        llPostCode.setVisibility(View.GONE);
        llDoorNumber.setVisibility(View.GONE);

        calculatePrice();
    }


    private void searchPostCode() {
        Utils.hideSoftKeyboard(getActivity(), etPostCode);
        SignInResponse signInModel = PrefManager.getInstance().getObject(PrefManager.USER_INFO, SignInResponse.class);
        ;

        Apis.User.searchPostCode(getActivity(), true, postCode, PrefManager.getInstance().getString(PrefManager.RES_ID), new VolleyCallBack.JSONResponse() {

            @Override
            public void onResponse(JSONObject _response, String _tag) {
                try {
                    postCodeResponse = JsonTranslator.getInstance().parse(_response.toString(), PostCodeResponse.class);

                    isDeliveryAvailable = postCodeResponse.isStatus();
                    if (isDeliveryAvailable) {
                        showSuccessDialog("Delivery available");

                        updateDeliveryInfo();
                        calculatePrice();
                    } else {
                        showErrorDialog("Delivery not available");
                    }
//                    tvDeliveryAvailable.setText("" + postCodeResponse.getDeliverPostCode_status());
//                    if (postCodeResponse.getDeliverPostCode_status().contains("Delivery avaliable"))
//                    {

//                        showSuccessDialog(postCodeResponse.getDeliverPostCode_status());

//                    } else {

////                        tvDeliveryAvailable.setTextColor(ContextCompat.getColor(getActivity(), R.color.order_process_btn_clr));
//                        showErrorDialog(postCodeResponse.getDeliverPostCode_status());
//
//
//                    }
//                    tvDeliveryAvailable.setVisibility(View.VISIBLE);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            @Override
            public void onErrorMessage(String _errorMessage) {
                isDeliveryAvailable = false;
                showErrorDialog("" + _errorMessage);
            }

            @Override
            public void onErrorResponse(VolleyError _error, String _tag) {
                showErrorDialog(getString(R.string.something_went_wrong));
            }

            @Override
            public void isConnected(boolean _connected, String _tag) {
                showErrorDialog(getString(R.string.no_internet));
            }
        });
    }

    private void updateDeliveryInfo() {
        isDeliveryAvailable = true;
        deliveryCharge = Float.parseFloat(postCodeResponse.getPrice());
        etAddressLineOne.setText("" + postCodeResponse.getStreet());
        etAddressLineTwo.setText("" + postCodeResponse.getPostTown());
//        tvDeliveryAvailable.setTextColor(ContextCompat.getColor(getActivity(), R.color.check_out_btn_clr));
    }

    private void getDiscount() {
        Utils.hideSoftKeyboard(getActivity(), etCouponeCode);
        Apis.User.validateCouponCode(getActivity(), true, couponeCode, "", "", new VolleyCallBack.JSONResponse() {

            @Override
            public void onResponse(JSONObject _response, String _tag) {
                try {
//                    codeResponse = JsonTranslator.getInstance().parse(_response.toString(), CouponCodeResponse.class);
//                    discount = Float.parseFloat((codeResponse.getDiscount_price() != null && !codeResponse.getDiscount_price().isEmpty())
//                            ? codeResponse.getDiscount_price() : "0.0");
//                    showSuccessDialog("" + codeResponse.getDiscount_description());
//                    isDiscountApplied = true;
//                    calculatePrice();

                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            @Override
            public void onErrorMessage(String _errorMessage) {
                isDiscountApplied = false;
                showErrorDialog("" + _errorMessage);
            }

            @Override
            public void onErrorResponse(VolleyError _error, String _tag) {
                showErrorDialog(getString(R.string.something_went_wrong));

            }

            @Override
            public void isConnected(boolean _connected, String _tag) {
                showErrorDialog(getString(R.string.no_internet));
            }
        });
    }


    void setOrderDetails(boolean payWithCard) {
        try {

            Cart.getInstance().setTown(town);
            Cart.getInstance().setPhonenumber(phone);
            Cart.getInstance().setAddress(street);
            Cart.getInstance().setPreorder(isPreOrder);
            Cart.getInstance().setDoornumber(doorNumber);
            Cart.getInstance().setOrderType(orderType);
            Cart.getInstance().setUserComment(userComments);

            Cart.getInstance().setDeliveryAddress(street);
            Cart.getInstance().setLat(latitude);
            Cart.getInstance().setLong(longitude);

            if (preOrderDate.isEmpty() && preOrderTime.isEmpty()) {
                preOrderDate = getCurrentDateTime()[0];
                preOrderTime = getCurrentDateTime()[1];
            }

            Cart.getInstance().setPreOrderTime(preOrderTime);
            Cart.getInstance().setPreOrderDate(preOrderDate);

            Cart.getInstance().setDiscount_info(isDiscountApplied);
            Cart.getInstance().setPreorder_is_preorder(isPreOrder);

            Cart.getInstance().setTotalAmount(String.valueOf(totalPrice + deliveryCharge));

            Cart.getInstance().setRestaurant_Id(Integer.parseInt(PrefManager.getInstance().getString(PrefManager.RES_ID)));
//            if (codeResponse != null)
//            {
//                Cart.getInstance().setDiscountAmount(codeResponse.getDiscount_price());
//                Cart.getInstance().setCoupon_Id(codeResponse.getCoupon_data().getCode());
//                Cart.getInstance().setCoupon_type(codeResponse.getCoupon_data().getType());
//                Cart.getInstance().setDiscount_description(codeResponse.getDiscount_description());
//                Cart.getInstance().setCoupon_value(Integer.parseInt(codeResponse.getCoupon_data().getCoupon_value()));
//            }

            if (postCodeResponse != null) {

                if (orderType.equals(Constants.DELIVERY)) {
                    Cart.getInstance().setDelivery_price(postCodeResponse.getPrice());
                    Cart.getInstance().setPostalcode(postCodeResponse.getPostCode());
                    Cart.getInstance().setDistance(String.valueOf(postCodeResponse.getMile()));
                }

            }

            Cart.getInstance().setApplicationUser_Id(signInResponse.getUserID());
            Cart.getInstance().setFirstname(signInResponse.getUserName());
            Cart.getInstance().setToken(signInResponse.getAccess_token());
//            Cart.getInstance().setEmail(PrefManager.getInstance().getString(PrefManager.EMAIL));
            Cart.getInstance().setEmail(signInResponse.getUserName());
            Cart.getInstance().setLastname(signInResponse.getUserName());

            if (payWithCard) {

                Bundle bundle = new Bundle();
                bundle.putFloat(Constants.Keys.GRAND_TOTAL, grandPrice);
                bundle.putString(Constants.Keys.ORDER_TYPE, "" + orderType);
                bundle.putString(Constants.Keys.PAY_WITH_AMOUNT, "" + cardCharges);
                bundle.putString(Constants.Keys.TOTAL, "" + Utils.formatToTwoDecimalPlaces(totalPrice));
                bundle.putString(Constants.Keys.DISCOUNT, "" + Utils.formatToTwoDecimalPlaces(discount));
                bundle.putString(Constants.Keys.DELIVERY_CHARGE, "" + Utils.formatToTwoDecimalPlaces(deliveryCharge));

                switchFragment(StripeFragment.getInstance(), bundle);
                Cart.getInstance().setPaymentType("card");

            } else {
                Cart.getInstance().setPaymentType("cash");
                saveOrder();
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    private String[] getCurrentDateTime() {
        Calendar calendar = Calendar.getInstance();
        String dateTimeString = String.valueOf(new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(calendar.getTime()));
        return dateTimeString.split(" ");
    }

    void saveOrder() {

        Apis.Order.saveOrder(getActivity(), true, signInResponse.getAccess_token(), new VolleyCallBack.JSONResponse() {

            @Override
            public void onResponse(JSONObject _response, String _tag) {
                try {

                    if (_response.has(Constants.MESSAGE)) {
                        showSuccessDialog(_response.getString(Constants.MESSAGE));
                    } else {
                        showSuccessDialog("Order Successfully Placed");
                    }

                    Cart.getInstance().ClearCart();
                    getParentActivity().clearAllFragments();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            @Override
            public void onErrorMessage(String _errorMessage) {
                showErrorDialog("" + _errorMessage);
            }

            @Override
            public void onErrorResponse(VolleyError _error, String _tag) {
                showErrorDialog(getString(R.string.something_went_wrong));

            }

            @Override
            public void isConnected(boolean _connected, String _tag) {
                showErrorDialog(getString(R.string.no_internet));
            }
        });

    }

}
