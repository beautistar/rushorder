package com.rushorder.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.rushorder.R;
import com.rushorder.apis.Apis;
import com.rushorder.app.AppController;
import com.rushorder.driver.config.SessionManagerDriver;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DriverLoginActivity extends AppCompatActivity {

    @BindView(R.id.llLoginDriver)
    LinearLayout llLoginDriver;
    @BindView(R.id.etEmailDriver)
    EditText etEmailDriver;
    @BindView(R.id.etPasswordDriver)
    EditText etPasswordDriver;
    @BindView(R.id.pbLoginDriver)
    ProgressBar pbLoginDriver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_driver_login);
        ButterKnife.bind(this);

        etEmailDriver.setText("driver@test.com");
        etPasswordDriver.setText("Test@123");
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(this, MainActivity.class));
    }

    @OnClick(R.id.llLoginDriver)
    public void onLogindriverClicked() {
        String email = etEmailDriver.getText().toString();
        String password = etPasswordDriver.getText().toString();
        if (TextUtils.isEmpty(email))
            Toast.makeText(this, "Enter Email Id", Toast.LENGTH_SHORT).show();
        else if (TextUtils.isEmpty(password))
            Toast.makeText(this, "Enter Password", Toast.LENGTH_SHORT).show();
        else
        driverLogin();
    }

    private void driverLogin() {
        pbLoginDriver.setVisibility(View.VISIBLE);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Apis.DriverLogin,
                response -> {
                    try {
                        Log.e("responseFacebook", response);
                        JSONObject jsonObject = new JSONObject(response);
                        String success = jsonObject.getString("success");
                        if (success.equals("1")) {


                            String data = jsonObject.getString("data");
                            JSONObject dataObj = new JSONObject(data);
                            String email = dataObj.getString("Email");
                            String Username = dataObj.getString("Username");
                            String Id = dataObj.getString("Id");

                            SessionManagerDriver sessionManagerDriver = new SessionManagerDriver(this);
                            sessionManagerDriver.createLoginSession(Username,email,Id);


                            startActivity(new Intent(this, OrderIdDriverActivity.class));
                            finish();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    pbLoginDriver.setVisibility(View.GONE);
                },
                error -> {
                    pbLoginDriver.setVisibility(View.GONE);
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("Email",etEmailDriver.getText().toString());
                params.put("password",etPasswordDriver.getText().toString());
                Log.e("paramsLogin", params.toString());
                return params;
            }

        };
        AppController.setmRequestQueue(stringRequest);
    }
}
