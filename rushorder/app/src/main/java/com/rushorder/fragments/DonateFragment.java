package com.rushorder.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.android.volley.VolleyError;
import com.rushorder.R;
import com.rushorder.apis.Apis;
import com.rushorder.constants.Constants;
import com.rushorder.interfaces.VolleyCallBack;
import com.rushorder.utils.FieldsValidator;
import com.rushorder.utils.Utils;

import org.json.JSONObject;

/**
 * Created by Aleem on 2/24/2018.
 */

public class DonateFragment extends BaseFragment implements View.OnClickListener {

    private ImageView ivMenu;
    private LinearLayout llDonate;
    private EditText etName, etEmail, etPhone, etSubject, etMessage;
    private ImageView ivFacebook, ivInstagram, ivTwitter, ivPintrest;
    private String name = "", email = "", phone = "", subject = "", message = "";

    public static DonateFragment getInstance() {
        return new DonateFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_donate, container, false);
        initViews(view);
        setListeners();
        return view;
    }

    @Override
    protected void initViews(View view) {
//        etName = view.findViewById(R.id.etName);
        ivMenu = view.findViewById(R.id.ivMenu);
//        etEmail = view.findViewById(R.id.etEmail);
//        etPhone = view.findViewById(R.id.etPhone);
//        ivTwitter = view.findViewById(R.id.ivTwitter);
//        etSubject = view.findViewById(R.id.etSubject);
//        etMessage = view.findViewById(R.id.etMessage);
//        ivFacebook = view.findViewById(R.id.ivFacebook);
//        ivPintrest = view.findViewById(R.id.ivPintrest);
//        ivInstagram = view.findViewById(R.id.ivInstagram);
        llDonate = view.findViewById(R.id.llDonate);

        setListeners();
        super.initViews(view);
    }

    @Override
    protected void setListeners() {

        ivMenu.setOnClickListener(this);
//        ivTwitter.setOnClickListener(this);
//        ivFacebook.setOnClickListener(this);
//        ivPintrest.setOnClickListener(this);
//        ivInstagram.setOnClickListener(this);
        llDonate.setOnClickListener(this);
        super.setListeners();
    }


    @Override
    public void onClick(View view) {

        if (view == ivMenu) {
            getParentActivity().openDrawer();
        }
        else
        {
            showSuccessDialog("Thanks for donation!");
        }


    }

    private void setFormVeriables() {
        name = etName.getText().toString();
        email = etEmail.getText().toString();
        phone = etPhone.getText().toString();
        subject = etSubject.getText().toString();
        message = etMessage.getText().toString();
    }

    private boolean validateFields() {
        if (name.isEmpty() || email.isEmpty() || subject.isEmpty() || message.isEmpty()) {
            return false;
        } else if (!FieldsValidator.getInstance().validateEmail(email)) {
            Utils.SnackBarMessage.showSnackBarShort(parentLayout, getString(R.string.please_enter_valid_email));
            return false;
        } else {
            return true;
        }

    }


    void contactQuery() {
        Apis.Restaurant.contactQuery(getActivity(), true, "", name, email, phone, subject, message, new VolleyCallBack.JSONResponse() {
            @Override
            public void onResponse(JSONObject _response, String _tag) {
                try {
                    showSuccessDialog(_response.getString(Constants.MESSAGE));
                    clearAllFragments();

                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            @Override
            public void onErrorMessage(String _errorMessage) {
                showErrorDialog(_errorMessage);
            }

            @Override
            public void onErrorResponse(VolleyError _error, String _tag) {
                showErrorDialog(getString(R.string.something_went_wrong));
            }

            @Override
            public void isConnected(boolean _connected, String _tag) {
                showErrorDialog(getString(R.string.no_internet));
            }
        });
    }
}


