package com.rushorder.fragments.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import com.rushorder.R;
import com.rushorder.interfaces.ClickCallBack;
import com.rushorder.models.RestaurantInfoResponse.CategoryViewModelsBean.SubCategoriesBean.ItemsBean.AddonCategoriesBean.AddonItemsBean;
import com.rushorder.models.Cart;
import com.rushorder.models.CartModel;
import com.rushorder.utils.Utils;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

/**
 * Created by Aleem on 12/10/2017.
 */

public class CartDetailAdapter extends RecyclerView.Adapter<CartDetailAdapter.Holder> {


    private ArrayList<CartModel> cartDetailList;
    private WeakReference<ClickCallBack> callback;


    public CartDetailAdapter(ClickCallBack callback) {

        this.callback = new WeakReference<>(callback);
        this.cartDetailList = Cart.getInstance().getOrderDetail();
    }


    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_cart_details, parent, false);
        return new Holder(itemView, callback);
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        CartModel model = cartDetailList.get(position);
        holder.tvName.setText("" + model.getItemName());
        ArrayList<String> addonsDetails = formatAddons(model);

        holder.tvPrice.setText("" + addonsDetails.get(0));
        holder.tvItemAddons.setText("" + addonsDetails.get(1));
        holder.tvQty.setText("" + model.getQuantity());

    }

    private ArrayList<String> formatAddons(CartModel model) {
        ArrayList<String> addonsDetails = new ArrayList<>();
        StringBuilder builder = new StringBuilder();
        float itemPrice = Float.parseFloat(model.getPrice());

        try {

            for (AddonItemsBean item : model.getSoldAddonItem()) {

                builder.append("" + item.getName() + " " + "KWD" + " " + item.getPrice());
                builder.append("\n");
//                itemPrice += Float.parseFloat((item.getPrice() == null || item.getPrice().isEmpty()) ? "0.0" : item.getPrice());
                itemPrice += item.getPrice();
            }

            itemPrice = itemPrice * model.getQuantity();
            addonsDetails.add("" + Utils.formatToTwoDecimalPlaces(itemPrice));
            addonsDetails.add(builder.toString());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return addonsDetails;
    }

    @Override
    public int getItemCount() {
        return cartDetailList.size();
    }


    public static class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView tvName, tvQty, tvPrice, tvItemAddons;
        private ImageView ivAdd, ivMinus, ivDelete;
        private WeakReference<ClickCallBack> callback;

        public Holder(View itemView, final WeakReference<ClickCallBack> callback) {
            super(itemView);
            this.callback = callback;

            tvQty = itemView.findViewById(R.id.tvQty);
            ivAdd = itemView.findViewById(R.id.ivAdd);
            tvName = itemView.findViewById(R.id.tvName);
            ivMinus = itemView.findViewById(R.id.ivMinus);
            tvPrice = itemView.findViewById(R.id.tvPrice);
            ivDelete = itemView.findViewById(R.id.ivDelete);
            tvItemAddons = itemView.findViewById(R.id.tvItemAddons);

            ivAdd.setOnClickListener(this);
            ivMinus.setOnClickListener(this);
            ivDelete.setOnClickListener(this);

//            itemView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    callback.get().callBack(Cart.getInstance().getOrderDetail().get(getLayoutPosition()), getLayoutPosition());
//                }
//            });
        }

        @Override
        public void onClick(View v) {

            if (v == ivAdd) {
                AddItemQuantity();
            } else if (v == ivMinus) {
                subtractItemQuantity();
            } else {
                Cart.getInstance().getOrderDetail().remove(getLayoutPosition());
                callback.get().callBack(null, getLayoutPosition());
            }
        }

        private void AddItemQuantity() {
            int itemQuantity = Integer.parseInt(tvQty.getText().toString());
            itemQuantity++;
            Cart.getInstance().getOrderDetail().get(getLayoutPosition()).setQuantity(itemQuantity);
            callback.get().callBack(null, getLayoutPosition());
        }

        private void subtractItemQuantity() {
            int itemQuantity = Integer.parseInt(tvQty.getText().toString());
            if (itemQuantity > 0) {
                itemQuantity--;
                Cart.getInstance().getOrderDetail().get(getLayoutPosition()).setQuantity(itemQuantity);
                if (itemQuantity == 0) {
                    Cart.getInstance().getOrderDetail().remove(getLayoutPosition());
                }
            }
            callback.get().callBack(null, getAdapterPosition());
        }


    }
}