package com.rushorder.interfaces;

/**
 * Created by Aleem on 12/5/2017.
 */

public interface ClickCallBack {

    void callBack();

    void callBack(Object _model, int position);
}
