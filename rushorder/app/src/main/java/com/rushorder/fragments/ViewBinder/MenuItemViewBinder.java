package com.rushorder.fragments.ViewBinder;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.rushorder.R;
import com.rushorder.cache.CacheData;
import com.rushorder.fragments.MenuItemsFragment;
import com.rushorder.interfaces.ClickCallBack;
import com.rushorder.models.RestaurantInfoResponse.CategoryViewModelsBean.SubCategoriesBean.ItemsBean;
import com.rushorder.models.RestaurantInfoResponse;

import java.lang.ref.WeakReference;

import tellh.com.stickyheaderview_rv.adapter.StickyHeaderViewAdapter;
import tellh.com.stickyheaderview_rv.adapter.ViewBinder;

public class MenuItemViewBinder extends ViewBinder<RestaurantInfoResponse.CategoryViewModelsBean.SubCategoriesBean.ItemsBean, MenuItemViewBinder.ViewHolder> {
    private WeakReference<ClickCallBack> callback;
    Context context;
    public MenuItemViewBinder(MenuItemsFragment menuItemsFragment,Context context) {
        callback = new WeakReference<ClickCallBack>(menuItemsFragment);
        this.context = context;
    }

    @Override
    public ViewHolder provideViewHolder(View itemView) {
        return new ViewHolder(itemView, callback);
    }

    @Override
    public void bindView(StickyHeaderViewAdapter stickyHeaderViewAdapter, ViewHolder viewHolder, int i, RestaurantInfoResponse.CategoryViewModelsBean.SubCategoriesBean.ItemsBean item) {
        viewHolder.tvName.setText("" + item.getName());
        viewHolder.tvPrice.setText("" + item.getPrice());
        viewHolder.tvDescription.setText("" + item.getDescription());

        Glide.with(context).applyDefaultRequestOptions(RequestOptions.placeholderOf(R.mipmap.ic_launcher))
                .load(item.getImagePath()).into(viewHolder.ivProductImage);
    }

    @Override
    public int getItemLayoutId(StickyHeaderViewAdapter adapter) {
        return R.layout.rv_menu_item_row;
    }

    public static class ViewHolder extends ViewBinder.ViewHolder {
        private LinearLayout llAdd;
        public TextView tvName, tvPrice, tvDescription;
        ImageView ivProductImage;
        public ViewHolder(View rootView, final WeakReference<ClickCallBack> callback) {
            super(rootView);
            llAdd = rootView.findViewById(R.id.llAdd);
            tvName = rootView.findViewById(R.id.tvName);
            tvPrice = rootView.findViewById(R.id.tvPrice);
            tvDescription = rootView.findViewById(R.id.tvDescription);
            ivProductImage = rootView.findViewById(R.id.ivProductImage);
            llAdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    ItemsBean model = (ItemsBean) CacheData.Menu.getInstance().getMenuItems().get(getLayoutPosition());

                    callback.get().callBack(model, getLayoutPosition());
                }
            });

        }
    }
}

