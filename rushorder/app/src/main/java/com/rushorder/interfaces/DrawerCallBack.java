package com.rushorder.interfaces;

/**
 * Created by tossdown on 3/30/18.
 */

public interface DrawerCallBack {

    void onMenuItemClicked(int menuItemId, int position);

}
