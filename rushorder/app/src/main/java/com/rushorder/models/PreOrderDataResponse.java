package com.rushorder.models;


import java.util.ArrayList;

public class PreOrderDataResponse {


    /**
     * $id : 1
     * Timimg : [{"$id":"2","Time":"Sunday 00:30:00"},{"$id":"3","Time":"Sunday 00:45:00"},{"$id":"4","Time":"Sunday 01:00:00"},{"$id":"5","Time":"Sunday 01:15:00"},{"$id":"6","Time":"Sunday 01:30:00"},{"$id":"7","Time":"Sunday 01:45:00"},{"$id":"8","Time":"Sunday 02:00:00"},{"$id":"9","Time":"Sunday 02:15:00"},{"$id":"10","Time":"Sunday 02:30:00"},{"$id":"11","Time":"Sunday 02:45:00"},{"$id":"12","Time":"Sunday 03:00:00"},{"$id":"13","Time":"Sunday 03:15:00"},{"$id":"14","Time":"Sunday 03:30:00"},{"$id":"15","Time":"Sunday 03:45:00"},{"$id":"16","Time":"Sunday 04:00:00"},{"$id":"17","Time":"Sunday 04:15:00"},{"$id":"18","Time":"Sunday 04:30:00"},{"$id":"19","Time":"Sunday 04:45:00"},{"$id":"20","Time":"Sunday 05:00:00"},{"$id":"21","Time":"Sunday 05:15:00"},{"$id":"22","Time":"Sunday 05:30:00"},{"$id":"23","Time":"Sunday 05:45:00"},{"$id":"24","Time":"Sunday 06:00:00"},{"$id":"25","Time":"Sunday 06:15:00"},{"$id":"26","Time":"Sunday 06:30:00"},{"$id":"27","Time":"Sunday 06:45:00"},{"$id":"28","Time":"Sunday 07:00:00"},{"$id":"29","Time":"Sunday 07:15:00"},{"$id":"30","Time":"Sunday 07:30:00"},{"$id":"31","Time":"Sunday 07:45:00"},{"$id":"32","Time":"Sunday 08:00:00"},{"$id":"33","Time":"Sunday 08:15:00"},{"$id":"34","Time":"Sunday 08:30:00"},{"$id":"35","Time":"Sunday 08:45:00"},{"$id":"36","Time":"Sunday 09:00:00"},{"$id":"37","Time":"Sunday 09:15:00"},{"$id":"38","Time":"Sunday 09:30:00"},{"$id":"39","Time":"Sunday 09:45:00"},{"$id":"40","Time":"Sunday 10:00:00"},{"$id":"41","Time":"Sunday 10:15:00"},{"$id":"42","Time":"Sunday 10:30:00"},{"$id":"43","Time":"Sunday 10:45:00"},{"$id":"44","Time":"Sunday 11:00:00"},{"$id":"45","Time":"Sunday 11:15:00"},{"$id":"46","Time":"Sunday 11:30:00"},{"$id":"47","Time":"Sunday 11:45:00"},{"$id":"48","Time":"Sunday 12:00:00"},{"$id":"49","Time":"Sunday 12:15:00"},{"$id":"50","Time":"Sunday 12:30:00"},{"$id":"51","Time":"Sunday 12:45:00"},{"$id":"52","Time":"Sunday 13:00:00"},{"$id":"53","Time":"Sunday 13:15:00"},{"$id":"54","Time":"Sunday 13:30:00"},{"$id":"55","Time":"Sunday 13:45:00"},{"$id":"56","Time":"Sunday 14:00:00"},{"$id":"57","Time":"Sunday 14:15:00"},{"$id":"58","Time":"Sunday 14:30:00"},{"$id":"59","Time":"Sunday 14:45:00"},{"$id":"60","Time":"Sunday 15:00:00"},{"$id":"61","Time":"Sunday 15:15:00"},{"$id":"62","Time":"Sunday 15:30:00"},{"$id":"63","Time":"Sunday 15:45:00"},{"$id":"64","Time":"Sunday 16:00:00"},{"$id":"65","Time":"Sunday 16:15:00"},{"$id":"66","Time":"Sunday 16:30:00"},{"$id":"67","Time":"Sunday 16:45:00"},{"$id":"68","Time":"Sunday 17:00:00"},{"$id":"69","Time":"Sunday 17:15:00"},{"$id":"70","Time":"Sunday 17:30:00"},{"$id":"71","Time":"Sunday 17:45:00"},{"$id":"72","Time":"Sunday 18:00:00"},{"$id":"73","Time":"Sunday 18:15:00"},{"$id":"74","Time":"Sunday 18:30:00"},{"$id":"75","Time":"Sunday 18:45:00"},{"$id":"76","Time":"Sunday 19:00:00"},{"$id":"77","Time":"Sunday 19:15:00"},{"$id":"78","Time":"Sunday 19:30:00"},{"$id":"79","Time":"Sunday 19:45:00"},{"$id":"80","Time":"Sunday 20:00:00"},{"$id":"81","Time":"Sunday 20:15:00"},{"$id":"82","Time":"Sunday 20:30:00"},{"$id":"83","Time":"Sunday 20:45:00"},{"$id":"84","Time":"Sunday 21:00:00"},{"$id":"85","Time":"Sunday 21:15:00"},{"$id":"86","Time":"Sunday 21:30:00"},{"$id":"87","Time":"Sunday 21:45:00"},{"$id":"88","Time":"Sunday 22:00:00"},{"$id":"89","Time":"Sunday 22:15:00"},{"$id":"90","Time":"Sunday 22:30:00"},{"$id":"91","Time":"Sunday 22:45:00"},{"$id":"92","Time":"Sunday 23:00:00"},{"$id":"93","Time":"Sunday 23:15:00"}]
     */

    private String $id;
    private ArrayList<TimimgBean> Timimg;

    public String get$id() {
        return $id;
    }

    public void set$id(String $id) {
        this.$id = $id;
    }

    public ArrayList<TimimgBean> getTimimg() {
        return Timimg;
    }

    public void setTimimg(ArrayList<TimimgBean> Timimg) {
        this.Timimg = Timimg;
    }

    public static class TimimgBean {
        /**
         * $id : 2
         * Time : Sunday 00:30:00
         */

        private String $id;
        private String Time;

        public String get$id() {
            return $id;
        }

        public void set$id(String $id) {
            this.$id = $id;
        }

        public String getTime() {
            return Time;
        }

        public void setTime(String Time) {
            this.Time = Time;
        }
    }
}
