package com.rushorder.models;


import com.rushorder.R;

import java.util.ArrayList;
import java.util.List;

import tellh.com.stickyheaderview_rv.adapter.StickyHeaderViewAdapter;

/**
 * Created by Aleem on 12/4/2017.
 */

public class MenuResponse {

    private boolean success;
    private DataBean data;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {

        private ArrayList<MenusBean> menus;
        private float minimum_spending;

        public ArrayList<MenusBean> getMenus() {
            return menus;
        }

        public float getMinimum_spending() {
            return minimum_spending;
        }

        public void setMinimum_spending(float minimum_spending) {
            this.minimum_spending = minimum_spending;
        }


        public void setMenus(ArrayList<MenusBean> menus) {
            this.menus = menus;
        }

        public static class MenusBean {


            private CategoriesBean categories;
            private ArrayList<SubCategoriesBean> sub_categories;

            public CategoriesBean getCategories() {
                return categories;
            }

            public void setCategories(CategoriesBean categories) {
                this.categories = categories;
            }

            public ArrayList<SubCategoriesBean> getSub_categories() {
                return sub_categories;
            }

            public void setSub_categories(ArrayList<SubCategoriesBean> sub_categories) {
                this.sub_categories = sub_categories;
            }

            public static class CategoriesBean {
                /**
                 * id : 48
                 * name : Pizzas
                 * restaurant_id : 3
                 * description :
                 * days : Monday,Tuesday,Wednesday,Thursday,Friday,Saturday,Sunday
                 * allow_coupon : no
                 * status : show
                 * delivery : no
                 * collection : no
                 * sort_order : 1
                 * user_side_orders : 0
                 * pic : https://jayeat.com/public/site-content/menu-icons/pizza.png
                 * free_item_cat : no
                 * spend_limit : 0
                 */

                private String id;
                private String name;
                private String restaurant_id;
                private String description;
                private String days;
                private String allow_coupon;
                private String status;
                private String delivery;
                private String collection;
                private String sort_order;
                private String user_side_orders;
                private String pic;
                private String free_item_cat;
                private String spend_limit;

                public String getId() {
                    return id;
                }

                public void setId(String id) {
                    this.id = id;
                }

                public String getName() {
                    return name;
                }

                public void setName(String name) {
                    this.name = name;
                }

                public String getRestaurant_id() {
                    return restaurant_id;
                }

                public void setRestaurant_id(String restaurant_id) {
                    this.restaurant_id = restaurant_id;
                }

                public String getDescription() {
                    return description;
                }

                public void setDescription(String description) {
                    this.description = description;
                }

                public String getDays() {
                    return days;
                }

                public void setDays(String days) {
                    this.days = days;
                }

                public String getAllow_coupon() {
                    return allow_coupon;
                }

                public void setAllow_coupon(String allow_coupon) {
                    this.allow_coupon = allow_coupon;
                }

                public String getStatus() {
                    return status;
                }

                public void setStatus(String status) {
                    this.status = status;
                }

                public String getDelivery() {
                    return delivery;
                }

                public void setDelivery(String delivery) {
                    this.delivery = delivery;
                }

                public String getCollection() {
                    return collection;
                }

                public void setCollection(String collection) {
                    this.collection = collection;
                }

                public String getSort_order() {
                    return sort_order;
                }

                public void setSort_order(String sort_order) {
                    this.sort_order = sort_order;
                }

                public String getUser_side_orders() {
                    return user_side_orders;
                }

                public void setUser_side_orders(String user_side_orders) {
                    this.user_side_orders = user_side_orders;
                }

                public String getPic() {
                    return pic;
                }

                public void setPic(String pic) {
                    this.pic = pic;
                }

                public String getFree_item_cat() {
                    return free_item_cat;
                }

                public void setFree_item_cat(String free_item_cat) {
                    this.free_item_cat = free_item_cat;
                }

                public String getSpend_limit() {
                    return spend_limit;
                }

                public void setSpend_limit(String spend_limit) {
                    this.spend_limit = spend_limit;
                }
            }

            public static class SubCategoriesBean {
                /**
                 * id : 117
                 * name : 8 Inch Deep Pan
                 * restaurant_id : 117
                 * description :
                 * parent_id : 48
                 * pic : test
                 * days : Monday,Tuesday,Wednesday,Thursday,Friday,Saturday,Sunday
                 * allow_coupon : no
                 * status : hidden
                 * delivery : no
                 * collection : no
                 * sort_order : 1
                 * free_item_cat : no
                 * spend_limit : 0
                 * items : [{"id":"1763","name":"Margherita","price":"3.6","description":"Extra mozzarella cheese, tomato","days":"","pic":"test","allow_coupon":"yes","sort_order":"1","addon_item_id":"150","kitchen_lines":"","sub_cat_id":"117","sub_cat_name":"8 Inch Deep Pan"},{"id":"1764","name":"Pepperoni","price":"4","description":"","days":"","pic":"test","allow_coupon":"yes","sort_order":"2","addon_item_id":"150","kitchen_lines":"","sub_cat_id":"117","sub_cat_name":"8 Inch Deep Pan"},{"id":"1765","name":"Hawaiian","price":"4","description":"ham, pineapple","days":"","pic":"test","allow_coupon":"yes","sort_order":"3","addon_item_id":"150","kitchen_lines":"","sub_cat_id":"117","sub_cat_name":"8 Inch Deep Pan"},{"id":"1766","name":"Funghi","price":"4","description":"Double mushrooms","days":"","pic":"test","allow_coupon":"yes","sort_order":"4","addon_item_id":"150","kitchen_lines":"","sub_cat_id":"117","sub_cat_name":"8 Inch Deep Pan"},{"id":"1767","name":"BBQ Chicken","price":"4","description":"BBQ sauce, chicken","days":"","pic":"test","allow_coupon":"yes","sort_order":"5","addon_item_id":"150","kitchen_lines":"","sub_cat_id":"117","sub_cat_name":"8 Inch Deep Pan"},{"id":"1768","name":"Donner Kebab Pizza","price":"4","description":"Donner meat","days":"","pic":"test","allow_coupon":"yes","sort_order":"6","addon_item_id":"150","kitchen_lines":"","sub_cat_id":"117","sub_cat_name":"8 Inch Deep Pan"},{"id":"1793","name":"Hot & Spicy","price":"4","description":"Chilli sauce, pepperoni","days":"","pic":"test","allow_coupon":"yes","sort_order":"7","addon_item_id":"150","kitchen_lines":"","sub_cat_id":"117","sub_cat_name":"8 Inch Deep Pan"},{"id":"1792","name":"Vegetarian Special","price":"4","description":"Mushroom, pepper, onion, sweetcorn, tomato, olives","days":"","pic":"test","allow_coupon":"yes","sort_order":"8","addon_item_id":"150","kitchen_lines":"","sub_cat_id":"117","sub_cat_name":"8 Inch Deep Pan"},{"id":"1795","name":"Vegetarian Chilli","price":"4","description":"Mushroom, pepper, onion, sweetcorn, tomato, olives","days":"Monday,Tuesday,Tuesday,Thursday,Friday,Saturday,Sunday","pic":"test","allow_coupon":"no","sort_order":"9","addon_item_id":"150","kitchen_lines":"","sub_cat_id":"117","sub_cat_name":"8 Inch Deep Pan"},{"id":"1794","name":"Vegetarian Special ","price":"4","description":"Mushroom, pepper, onion, sweetcorn, tomato, olives","days":"Monday,Tuesday,Tuesday,Thursday,Friday,Saturday,Sunday","pic":"test","allow_coupon":"no","sort_order":"9","addon_item_id":"150","kitchen_lines":"","sub_cat_id":"117","sub_cat_name":"8 Inch Deep Pan"},{"id":"1791","name":"Mighty Meat","price":"4","description":"Salami, pepperoni, ham, bolognese","days":"","pic":"test","allow_coupon":"yes","sort_order":"9","addon_item_id":"150","kitchen_lines":"","sub_cat_id":"117","sub_cat_name":"8 Inch Deep Pan"},{"id":"1769","name":"Mexican","price":"4","description":"Chilli sauce, bolognese, onions, peppers, jalapenos","days":"","pic":"test","allow_coupon":"yes","sort_order":"10","addon_item_id":"150","kitchen_lines":"","sub_cat_id":"117","sub_cat_name":"8 Inch Deep Pan"},{"id":"1770","name":"Chicken tikka","price":"4","description":"Chicken tikka, jalapeno, onion, pepper","days":"","pic":"test","allow_coupon":"yes","sort_order":"11","addon_item_id":"150","kitchen_lines":"","sub_cat_id":"117","sub_cat_name":"8 Inch Deep Pan"},{"id":"1771","name":"Bolognese","price":"4","description":"","days":"","pic":"test","allow_coupon":"yes","sort_order":"12","addon_item_id":"150","kitchen_lines":"","sub_cat_id":"117","sub_cat_name":"8 Inch Deep Pan"},{"id":"1772","name":"Special Bolognese","price":"4","description":"Mushroom, red onions, bolognese","days":"","pic":"test","allow_coupon":"yes","sort_order":"13","addon_item_id":"150","kitchen_lines":"","sub_cat_id":"117","sub_cat_name":"8 Inch Deep Pan"},{"id":"1773","name":"Calzone (Folded)","price":"4","description":"bolognese, red onion, pepperoni, pepper","days":"","pic":"test","allow_coupon":"yes","sort_order":"14","addon_item_id":"150","kitchen_lines":"","sub_cat_id":"117","sub_cat_name":"8 Inch Deep Pan"},{"id":"1774","name":"Volcano","price":"4","description":"Chilli sauce, pepperoni, onions, jalapenos","days":"","pic":"test","allow_coupon":"yes","sort_order":"15","addon_item_id":"150","kitchen_lines":"","sub_cat_id":"117","sub_cat_name":"8 Inch Deep Pan"},{"id":"1775","name":"Pollo","price":"4","description":"Mushroom, chicken, sweeetcorn","days":"","pic":"test","allow_coupon":"yes","sort_order":"16","addon_item_id":"150","kitchen_lines":"","sub_cat_id":"117","sub_cat_name":"8 Inch Deep Pan"},{"id":"1776","name":"Salami Special","price":"4","description":"Salami, onion, jalapeno, green peppers","days":"","pic":"test","allow_coupon":"yes","sort_order":"17","addon_item_id":"150","kitchen_lines":"","sub_cat_id":"117","sub_cat_name":"8 Inch Deep Pan"},{"id":"1777","name":"Peri peri Chicken","price":"4","description":"Peri Peri chicken, onion, peppers","days":"","pic":"test","allow_coupon":"yes","sort_order":"18","addon_item_id":"150","kitchen_lines":"","sub_cat_id":"117","sub_cat_name":"8 Inch Deep Pan"},{"id":"1778","name":"Spicy Chicken","price":"4","description":"Chicken, red onions, jalapenos","days":"","pic":"test","allow_coupon":"yes","sort_order":"19","addon_item_id":"150","kitchen_lines":"","sub_cat_id":"117","sub_cat_name":"8 Inch Deep Pan"},{"id":"1779","name":"New York","price":"4","description":"Chilli sauce, bolognese, onion, jalapenos, cheese","days":"","pic":"test","allow_coupon":"yes","sort_order":"20","addon_item_id":"150","kitchen_lines":"","sub_cat_id":"117","sub_cat_name":"8 Inch Deep Pan"},{"id":"1780","name":"Ham & Mushrooms","price":"4","description":"Tomato, ham, mushroom, cheese","days":"","pic":"test","allow_coupon":"yes","sort_order":"21","addon_item_id":"150","kitchen_lines":"","sub_cat_id":"117","sub_cat_name":"8 Inch Deep Pan"},{"id":"1781","name":"Chicken Kiev","price":"4","description":"Tomato, garlic, chicken, ham, mushroom, cheese","days":"","pic":"test","allow_coupon":"yes","sort_order":"22","addon_item_id":"150","kitchen_lines":"","sub_cat_id":"117","sub_cat_name":"8 Inch Deep Pan"},{"id":"1782","name":"Country Chicken","price":"4","description":"BBQ Sauce, green peppers, mushroom, sweetcorn, tandori chicken","days":"","pic":"test","allow_coupon":"yes","sort_order":"23","addon_item_id":"150","kitchen_lines":"","sub_cat_id":"117","sub_cat_name":"8 Inch Deep Pan"},{"id":"1783","name":"London Pizza","price":"4","description":"Chips","days":"","pic":"test","allow_coupon":"yes","sort_order":"24","addon_item_id":"150","kitchen_lines":"","sub_cat_id":"117","sub_cat_name":"8 Inch Deep Pan"},{"id":"1784","name":"Meatball Pizza","price":"4","description":"Meatball, onions, jalapenos","days":"","pic":"test","allow_coupon":"yes","sort_order":"25","addon_item_id":"150","kitchen_lines":"","sub_cat_id":"117","sub_cat_name":"8 Inch Deep Pan"},{"id":"1785","name":"Tandoori Pizza","price":"4","description":"Garlic, pepperoni, onion, jalapeno, pepper, tandoori chicken","days":"","pic":"test","allow_coupon":"yes","sort_order":"26","addon_item_id":"150","kitchen_lines":"","sub_cat_id":"117","sub_cat_name":"8 Inch Deep Pan"},{"id":"1786","name":"Quattro Stagioni","price":"4","description":"Mushroom, pepperoni, salami, pepper","days":"","pic":"test","allow_coupon":"yes","sort_order":"27","addon_item_id":"150","kitchen_lines":"","sub_cat_id":"117","sub_cat_name":"8 Inch Deep Pan"},{"id":"1787","name":"Hot Shot","price":"4","description":"Mushroom, pepper, jalapenos","days":"","pic":"test","allow_coupon":"yes","sort_order":"28","addon_item_id":"150","kitchen_lines":"","sub_cat_id":"117","sub_cat_name":"8 Inch Deep Pan"},{"id":"1788","name":"Seafood","price":"4.5","description":"Mushroom,  tuna, prawns, anchovies","days":"","pic":"test","allow_coupon":"yes","sort_order":"29","addon_item_id":"150","kitchen_lines":"","sub_cat_id":"117","sub_cat_name":"8 Inch Deep Pan"},{"id":"1789","name":"Nonna's Pizza","price":"4.5","description":"Pepperoni, salami, onion, pepper, jalapeno","days":"","pic":"test","allow_coupon":"yes","sort_order":"30","addon_item_id":"150","kitchen_lines":"","sub_cat_id":"117","sub_cat_name":"8 Inch Deep Pan"},{"id":"1790","name":"Create Your Own Pizza","price":"3.6","description":"Mushroom, tuna, prawns, anchovies","days":"","pic":"","allow_coupon":"yes","sort_order":"31","addon_item_id":"150","kitchen_lines":"","sub_cat_id":"117","sub_cat_name":"8 Inch Deep Pan"}]
                 */

                private String id;
                private String name;
                private String restaurant_id;
                private String description;
                private String parent_id;
                private String pic;
                private String days;
                private String allow_coupon;
                private String status;
                private String delivery;
                private String collection;
                private String sort_order;
                private String free_item_cat;
                private String spend_limit;
                private List<ItemsBean> items;

                public String getId() {
                    return id;
                }

                public void setId(String id) {
                    this.id = id;
                }

                public String getName() {
                    return name;
                }

                public void setName(String name) {
                    this.name = name;
                }

                public String getRestaurant_id() {
                    return restaurant_id;
                }

                public void setRestaurant_id(String restaurant_id) {
                    this.restaurant_id = restaurant_id;
                }

                public String getDescription() {
                    return description;
                }

                public void setDescription(String description) {
                    this.description = description;
                }

                public String getParent_id() {
                    return parent_id;
                }

                public void setParent_id(String parent_id) {
                    this.parent_id = parent_id;
                }

                public String getPic() {
                    return pic;
                }

                public void setPic(String pic) {
                    this.pic = pic;
                }

                public String getDays() {
                    return days;
                }

                public void setDays(String days) {
                    this.days = days;
                }

                public String getAllow_coupon() {
                    return allow_coupon;
                }

                public void setAllow_coupon(String allow_coupon) {
                    this.allow_coupon = allow_coupon;
                }

                public String getStatus() {
                    return status;
                }

                public void setStatus(String status) {
                    this.status = status;
                }

                public String getDelivery() {
                    return delivery;
                }

                public void setDelivery(String delivery) {
                    this.delivery = delivery;
                }

                public String getCollection() {
                    return collection;
                }

                public void setCollection(String collection) {
                    this.collection = collection;
                }

                public String getSort_order() {
                    return sort_order;
                }

                public void setSort_order(String sort_order) {
                    this.sort_order = sort_order;
                }

                public String getFree_item_cat() {
                    return free_item_cat;
                }

                public void setFree_item_cat(String free_item_cat) {
                    this.free_item_cat = free_item_cat;
                }

                public String getSpend_limit() {
                    return spend_limit;
                }

                public void setSpend_limit(String spend_limit) {
                    this.spend_limit = spend_limit;
                }

                public List<ItemsBean> getItems() {
                    return items;
                }

                public void setItems(List<ItemsBean> items) {
                    this.items = items;
                }


                public static class ItemsBean extends tellh.com.stickyheaderview_rv.adapter.DataBean {
                    /**
                     * id : 1763
                     * name : Margherita
                     * price : 3.6
                     * description : Extra mozzarella cheese, tomato
                     * days :
                     * pic : test
                     * allow_coupon : yes
                     * sort_order : 1
                     * addon_item_id : 150
                     * kitchen_lines :
                     * sub_cat_id : 117
                     * sub_cat_name : 8 Inch Deep Pan
                     */

                    private String id;
                    private String name;
                    private String price;
                    private String description;
                    private String days;
                    private String pic;
                    private String allow_coupon;
                    private String sort_order;
                    private String addon_item_id;
                    private String kitchen_lines;
                    private String sub_cat_id;
                    private String sub_cat_name;

                    public String getId() {
                        return id;
                    }

                    public void setId(String id) {
                        this.id = id;
                    }

                    public String getName() {
                        return name;
                    }

                    public void setName(String name) {
                        this.name = name;
                    }

                    public String getPrice() {
                        return price;
                    }

                    public void setPrice(String price) {
                        this.price = price;
                    }

                    public String getDescription() {
                        return description;
                    }

                    public void setDescription(String description) {
                        this.description = description;
                    }

                    public String getDays() {
                        return days;
                    }

                    public void setDays(String days) {
                        this.days = days;
                    }

                    public String getPic() {
                        return pic;
                    }

                    public void setPic(String pic) {
                        this.pic = pic;
                    }

                    public String getAllow_coupon() {
                        return allow_coupon;
                    }

                    public void setAllow_coupon(String allow_coupon) {
                        this.allow_coupon = allow_coupon;
                    }

                    public String getSort_order() {
                        return sort_order;
                    }

                    public void setSort_order(String sort_order) {
                        this.sort_order = sort_order;
                    }

                    public String getAddon_item_id() {
                        return addon_item_id;
                    }

                    public void setAddon_item_id(String addon_item_id) {
                        this.addon_item_id = addon_item_id;
                    }

                    public String getKitchen_lines() {
                        return kitchen_lines;
                    }

                    public void setKitchen_lines(String kitchen_lines) {
                        this.kitchen_lines = kitchen_lines;
                    }

                    public String getSub_cat_id() {
                        return sub_cat_id;
                    }

                    public void setSub_cat_id(String sub_cat_id) {
                        this.sub_cat_id = sub_cat_id;
                    }

                    public String getSub_cat_name() {
                        return sub_cat_name;
                    }

                    public void setSub_cat_name(String sub_cat_name) {
                        this.sub_cat_name = sub_cat_name;
                    }

                    @Override
                    public int getItemLayoutId(StickyHeaderViewAdapter adapter) {
                        return R.layout.rv_menu_item_row;
                    }
                }
            }
        }
    }
}
