package com.rushorder.fragments.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.iarcuschin.simpleratingbar.SimpleRatingBar;
import com.rushorder.R;
import com.rushorder.interfaces.ClickCallBack;
import com.rushorder.models.RestaurantsResponse;
import com.rushorder.utils.Utils;
import com.rushorder.views.TriangleRectangleLabelView;
import com.squareup.picasso.Picasso;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

public class RestaurantListAdapter extends RecyclerView.Adapter<RestaurantListAdapter.Holder> implements Filterable {

    private WeakReference<ClickCallBack> callback;
    private List<RestaurantsResponse> restaurantsList = new ArrayList<>();
    private List<RestaurantsResponse> filteredRestaurantsList = new ArrayList<>();

    public RestaurantListAdapter(ClickCallBack callback, List<RestaurantsResponse> restaurantsList) {
        this.callback = new WeakReference<>(callback);
        this.restaurantsList.addAll(restaurantsList);
        this.filteredRestaurantsList = restaurantsList;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View itemView = inflater.inflate(R.layout.rv_resturants, parent, false);
        Holder holder = new Holder(itemView, callback);
        return holder;

    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {

        RestaurantsResponse model = filteredRestaurantsList.get(position);
        try {

            holder.restaurantRating.setRating(model.getRaiting());
            holder.tvRestaurantName.setText("" + model.getName());
            holder.tvDelivery.setText("" + model.getCookingTime() + " Minutes");
            holder.tvRestaurantDesc.setText("" + model.getDescription());
            holder.tvMinSpent.setText(holder.itemView.getContext().getString(R.string.pound_sign) + " " + model.getMinDelPrice());
            holder.tvDistance.setText("" + Utils.formatToTwoDecimalPlaces(model.getDistance()));
//            holder.tvDistance.setText("" + model.getDistance());
//            holder.tvReviewsCount.setText("" + model.getReviewCount());

            Picasso.with(holder.itemView.getContext())
                    .load(model.getImg())
                    .placeholder(R.drawable.restaurant_logo)
                    .error(R.drawable.restaurant_logo)
                    .into(holder.ivRestaurants);
//
//
//            if (model.getDiscount() != null && !model.getDiscount().isEmpty()) {
//                holder.trvDiscount.setText("" + model.getDiscount());
//                holder.trvDiscount.setVisibility(View.VISIBLE);
//            } else {
                holder.trvDiscount.setVisibility(View.GONE);
//            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return (filteredRestaurantsList != null) ? filteredRestaurantsList.size() : 0;
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    filteredRestaurantsList = restaurantsList;
                } else {
                    ArrayList<RestaurantsResponse> filteredList = new ArrayList<>();
                    for (RestaurantsResponse row : restaurantsList) {
                        if (row.getName() != null) {
                            if (row.getName().toLowerCase().contains(charString.toLowerCase())) {
                                filteredList.add(row);
                            }
                        }
                    }
                    filteredRestaurantsList = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = filteredRestaurantsList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                filteredRestaurantsList = (ArrayList<RestaurantsResponse>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }


    public  class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private ImageView ivRestaurants;
        private SimpleRatingBar restaurantRating;
        private WeakReference<ClickCallBack> callback;
        private TriangleRectangleLabelView trvDiscount;
        private TextView tvRestaurantName, tvRestaurantDesc, tvDelivery, tvMinSpent, tvDistance, tvStatus, tvDiscount, tvReviewsCount;

        public Holder(View itemView, final WeakReference<ClickCallBack> callback) {
            super(itemView);
            this.callback = callback;

            tvMinSpent = itemView.findViewById(R.id.tvMin);
            tvDelivery = itemView.findViewById(R.id.tvDelivery);
            tvDiscount = itemView.findViewById(R.id.tvDiscount);
            trvDiscount = itemView.findViewById(R.id.trvDiscount);
            restaurantRating = itemView.findViewById(R.id.rbRating);
            ivRestaurants = itemView.findViewById(R.id.ivRestaurant);
            tvStatus = itemView.findViewById(R.id.tvRestaurantsStatus);
            tvDistance = itemView.findViewById(R.id.tvRestaurantDistance);
            tvRestaurantName = itemView.findViewById(R.id.tvRestaurantName);
            tvRestaurantDesc = itemView.findViewById(R.id.tvRestaurantDesc);
            tvReviewsCount = itemView.findViewById(R.id.tvReviewsCount);

            itemView.setOnClickListener(this);

            restaurantRating.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    return true;
                }
            });
        }

        @Override
        public void onClick(View v) {
            callback.get().callBack(filteredRestaurantsList.get(getLayoutPosition()), getLayoutPosition());
        }

    }

}
