package com.rushorder.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.rushorder.R;
import com.rushorder.cache.CacheData;
import com.rushorder.models.RestaurantInfoResponse;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;

/**
 * Created by Aleem on 1/27/2018.
 */

public class RestaurantInfoFragment extends BaseFragment implements OnMapReadyCallback, View.OnClickListener {

    private GoogleMap googleMap;
    private SupportMapFragment mapFragment;
    private ImageView ivFacebook, ivInstagram, ivTwitter, ivPintrest;
    private TextView tvMondayTime, tvTuesdayTime, tvWednesdayTime, tvThursdayTime, tvFridayTime, tvSaturdayTime, tvSundayTime, tvPhoneNumber, tvAddress;
    private RestaurantInfoResponse.ReataurantTimingsBean model;

    public static RestaurantInfoFragment getInstance() {
        return new RestaurantInfoFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_info, container, false);
        initViews(view);
        return view;
    }

    @Override
    protected void initViews(View view) {


        tvAddress = view.findViewById(R.id.tvAddress);
        ivTwitter = view.findViewById(R.id.ivTwitter);
        ivFacebook = view.findViewById(R.id.ivFacebook);
        tvPhoneNumber = view.findViewById(R.id.tvPhone);
        ivPintrest = view.findViewById(R.id.ivPintrest);
        ivInstagram = view.findViewById(R.id.ivInstagram);
        tvMondayTime = view.findViewById(R.id.tvMondayTime);
        tvFridayTime = view.findViewById(R.id.tvFridayTime);
        tvSundayTime = view.findViewById(R.id.tvSundayTime);
        tvTuesdayTime = view.findViewById(R.id.tvTuesdayTime);
        tvThursdayTime = view.findViewById(R.id.tvThursdayTime);
        tvSaturdayTime = view.findViewById(R.id.tvSaturdayTime);
        tvWednesdayTime = view.findViewById(R.id.tvWednesdayTime);

        setMapFragment();
        setListeners();

        super.initViews(view);
    }


    @Override
    protected void setValues() {

        model = CacheData
                .RestaurantsDetails.getInstance()
                .getRestaurantDetailsResponse()
                .getReataurantTimings();


        setOpeningTimings(model);
//        setMapLocation(model);
        setContactDetail(CacheData
                .RestaurantsDetails.getInstance()
                .getRestaurantDetailsResponse());
        super.setValues();
    }

    void setMapFragment() {

        mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.mapsFragment);
        mapFragment.getMapAsync(this);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        this.googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        this.googleMap.getUiSettings().setZoomControlsEnabled(true);
        this.googleMap.getUiSettings().setZoomGesturesEnabled(true);
        this.googleMap.getUiSettings().setCompassEnabled(true);
        this.googleMap.getUiSettings().setMyLocationButtonEnabled(true);
        setValues();
    }

    @Override
    protected void setListeners() {
        ivTwitter.setOnClickListener(this);
        ivFacebook.setOnClickListener(this);
        ivPintrest.setOnClickListener(this);
        ivInstagram.setOnClickListener(this);
        tvPhoneNumber.setOnClickListener(this);

        super.setListeners();
    }

    private void setContactDetail(RestaurantInfoResponse restaurant_details) {
        tvPhoneNumber.setText("" + restaurant_details.getPhone1());
        tvAddress.setText("" + restaurant_details.getAddress());
    }


    private void setOpeningTimings(RestaurantInfoResponse.ReataurantTimingsBean model) {

//        if (open_timings.get(index).getDay().equals("Monday")) {
            tvMondayTime.setText("" + model.getMondayOpenTime() + " -- " + model.getMondayCloseTime());

//        } else if (model.getDay().equals("Tuesday")) {
            tvTuesdayTime.setText("" + model.getTuesdayOpenTime() + " -- " + model.getTuesdayCloseTime());

//        } else if (model.getDay().equals("Wednesday")) {
            tvWednesdayTime.setText("" + model.getWednesdayOpenTime() + " -- " + model.getWednesdayCloseTime());

//        } else if (model.getDay().equals("Thursday")) {
            tvThursdayTime.setText("" + model.getThursdayOpenTime() + " -- " + model.getTuesdayCloseTime());

//        } else if (model.getDay().equals("Friday")) {
            tvFridayTime.setText("" + model.getFridayOpenTime() + " -- " + model.getFridayCloseTime());

//        } else if (model.getDay().equals("Saturday")) {
            tvSaturdayTime.setText("" + model.getSaturdayOpenTime() + " -- " + model.getSaturdayCloseTime());

//        } else if (model.getDay().equals("Sunday")) {
            tvSundayTime.setText("" + model.getSaturdayOpenTime() + " -- " + model.getSundayCloseTime());

//        }

    }

//    private void setMapLocation(RestaurantDetailsResponse.RestaurantDetailsBean restaurant_details) {
//        try {
//
//            double latitude = Double.parseDouble(restaurant_details.getShop_latitude());
//            double longitude = Double.parseDouble(restaurant_details.getShop_longitude());
//            googleMap.addMarker(new MarkerOptions().position(new LatLng(latitude, longitude)));
//            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitude, longitude), 15.0f));
//
////            googleMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(latitude, longitude)));
//        } catch (Exception ex) {
//            ex.printStackTrace();
//
//        }
//    }

    @Override
    public void onClick(View view) {
        Intent intent = new Intent();
        try {
            if (view == tvPhoneNumber) {
                dialNumber();
            } else if (view == ivFacebook) {
//                intent = new Intent(Intent.ACTION_VIEW, Uri.parse(model.getFacebook()));
            } else if (view == ivInstagram) {
//                intent = new Intent(Intent.ACTION_VIEW, Uri.parse(model.getInstagram()));
            } else if (view == ivTwitter) {
//                intent = new Intent(Intent.ACTION_VIEW, Uri.parse(model.getTwitter()));
            } else {
//                intent = new Intent(Intent.ACTION_VIEW, Uri.parse(model.getPinterest()));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }


//        startActivity(intent);

    }

    private void dialNumber() {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + tvPhoneNumber.getText().toString()));
        startActivity(intent);
    }
}
