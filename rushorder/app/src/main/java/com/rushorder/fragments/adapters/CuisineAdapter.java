package com.rushorder.fragments.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.rushorder.R;
import com.rushorder.interfaces.ClickCallBack;
import com.rushorder.models.CuisinesResponse;

import java.lang.ref.WeakReference;
import java.util.List;


public class CuisineAdapter extends RecyclerView.Adapter<CuisineAdapter.Holder> {


    private int selectedId = -1;
    private List<CuisinesResponse> cuisineList;
    private WeakReference<ClickCallBack> callback;

    public CuisineAdapter(ClickCallBack callback, List<CuisinesResponse> cuisineList) {

        this.callback = new WeakReference<>(callback);
        this.cuisineList = cuisineList;
    }


    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_cousine, parent, false);
        return new Holder(itemView, callback, cuisineList);
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        CuisinesResponse model = cuisineList.get(position);
        holder.tvName.setText("" + model.getName());

        if (selectedId == model.getId()) {
            holder.ivTick.setVisibility(View.VISIBLE);
        } else {
            holder.ivTick.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return cuisineList.size();
    }


    public class Holder extends RecyclerView.ViewHolder {

        private TextView tvName;
        private ImageView ivTick;

        public Holder(View itemView, final WeakReference<ClickCallBack> callback, final List<CuisinesResponse> cuisineList) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tvName);
            ivTick = itemView.findViewById(R.id.ivTick);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    selectedId = cuisineList.get(getLayoutPosition()).getId();
                    callback.get().callBack(cuisineList.get(getLayoutPosition()), getLayoutPosition());
                    notifyDataSetChanged();
                }
            });
        }
    }
}
