package com.rushorder.models;

import com.rushorder.R;

import java.util.List;

import tellh.com.stickyheaderview_rv.adapter.DataBean;
import tellh.com.stickyheaderview_rv.adapter.StickyHeaderViewAdapter;

public class RestaurantInfoResponse {


    private int Id;
    private String Name;
    private String Url;
    private String Img;
    private Object Description;
    private String Phone1;
    private String Phone2;
    private String PostCode;
    private String SupportEmail;
    private String Address;
    private int distance;
    private float MinDelPrice;
    private String CookingTime;
    private float Raiting;

    private String $id;
    private ReataurantTimingsBean ReataurantTimings;
    private List<CategoryViewModelsBean> CategoryViewModels;

    /**
     * Id : 3
     * Name : Foody
     * Url : Foody.com
     * Img : /Uploads/Restaurants/Logo_rush_order_.png
     * Description : null
     * Phone1 : 03223220009
     * Phone2 :
     * PostCode : null
     * SupportEmail : null
     * Address : null
     * distance : 0
     * MinDelPrice : 0
     * CookingTime : 0
     * Raiting : 0
     */


    public String get$id() {
        return $id;
    }

    public void set$id(String $id) {
        this.$id = $id;
    }

    public ReataurantTimingsBean getReataurantTimings() {
        return ReataurantTimings;
    }

    public void setReataurantTimings(ReataurantTimingsBean ReataurantTimings) {
        this.ReataurantTimings = ReataurantTimings;
    }

    public List<CategoryViewModelsBean> getCategoryViewModels() {
        return CategoryViewModels;
    }

    public void setCategoryViewModels(List<CategoryViewModelsBean> CategoryViewModels) {
        this.CategoryViewModels = CategoryViewModels;
    }

    public int getId() {
        return Id;
    }

    public void setId(int Id) {
        this.Id = Id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getUrl() {
        return Url;
    }

    public void setUrl(String Url) {
        this.Url = Url;
    }

    public String getImg() {
        return Img;
    }

    public void setImg(String Img) {
        this.Img = Img;
    }

    public Object getDescription() {
        return Description;
    }

    public void setDescription(Object Description) {
        this.Description = Description;
    }

    public String getPhone1() {
        return Phone1;
    }

    public void setPhone1(String Phone1) {
        this.Phone1 = Phone1;
    }

    public String getPhone2() {
        return Phone2;
    }

    public void setPhone2(String Phone2) {
        this.Phone2 = Phone2;
    }

    public Object getPostCode() {
        return PostCode;
    }

    public void setPostCode(String PostCode) {
        this.PostCode = PostCode;
    }

    public Object getSupportEmail() {
        return SupportEmail;
    }

    public void setSupportEmail(String SupportEmail) {
        this.SupportEmail = SupportEmail;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String Address) {
        this.Address = Address;
    }

    public int getDistance() {
        return distance;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

    public float getMinDelPrice() {
        return MinDelPrice;
    }

    public void setMinDelPrice(int MinDelPrice) {
        this.MinDelPrice = MinDelPrice;
    }

    public String getCookingTime() {
        return CookingTime;
    }

    public void setCookingTime(String CookingTime) {
        this.CookingTime = CookingTime;
    }

    public float getRaiting() {
        return Raiting;
    }

    public void setRaiting(float Raiting) {
        this.Raiting = Raiting;
    }

    public static class ReataurantTimingsBean {
        /**
         * $id : 2
         * Id : 34
         * MondayOpenTime : 2018-11-08T11:00:00
         * MondayCloseTime : 2018-11-08T23:30:00
         * MondayIsOpen : true
         * TuesdayOpenTime : 2018-11-08T11:00:00
         * TuesdayCloseTime : 2018-11-08T23:30:00
         * TuesdayIsOpen : true
         * WednesdayOpenTime : 2018-11-08T11:00:00
         * WednesdayCloseTime : 2018-11-08T23:30:00
         * WednesdayIsOpen : true
         * ThursdayOpenTime : 2018-11-08T23:00:00
         * ThursdayCloseTime : 2018-11-08T23:30:00
         * ThursdayIsOpen : true
         * FridayOpenTime : 2018-11-08T23:00:00
         * FridayCloseTime : 2018-11-08T11:00:00
         * FridayIsOpen : true
         * SaturdayOpenTime : 2018-11-08T23:00:00
         * SaturdayCloseTime : 2018-11-08T11:00:00
         * SaturdayIsOpen : true
         * SundayOpenTime : 2018-11-08T23:00:00
         * SundayCloseTime : 2018-11-08T23:30:00
         * SundayIsOpen : true
         */

        private String $id;
        private int Id;
        private String MondayOpenTime;
        private String MondayCloseTime;
        private boolean MondayIsOpen;
        private String TuesdayOpenTime;
        private String TuesdayCloseTime;
        private boolean TuesdayIsOpen;
        private String WednesdayOpenTime;
        private String WednesdayCloseTime;
        private boolean WednesdayIsOpen;
        private String ThursdayOpenTime;
        private String ThursdayCloseTime;
        private boolean ThursdayIsOpen;
        private String FridayOpenTime;
        private String FridayCloseTime;
        private boolean FridayIsOpen;
        private String SaturdayOpenTime;
        private String SaturdayCloseTime;
        private boolean SaturdayIsOpen;
        private String SundayOpenTime;
        private String SundayCloseTime;
        private boolean SundayIsOpen;

        public String get$id() {
            return $id;
        }

        public void set$id(String $id) {
            this.$id = $id;
        }

        public int getId() {
            return Id;
        }

        public void setId(int Id) {
            this.Id = Id;
        }

        public String getMondayOpenTime() {
            return MondayOpenTime;
        }

        public void setMondayOpenTime(String MondayOpenTime) {
            this.MondayOpenTime = MondayOpenTime;
        }

        public String getMondayCloseTime() {
            return MondayCloseTime;
        }

        public void setMondayCloseTime(String MondayCloseTime) {
            this.MondayCloseTime = MondayCloseTime;
        }

        public boolean isMondayIsOpen() {
            return MondayIsOpen;
        }

        public void setMondayIsOpen(boolean MondayIsOpen) {
            this.MondayIsOpen = MondayIsOpen;
        }

        public String getTuesdayOpenTime() {
            return TuesdayOpenTime;
        }

        public void setTuesdayOpenTime(String TuesdayOpenTime) {
            this.TuesdayOpenTime = TuesdayOpenTime;
        }

        public String getTuesdayCloseTime() {
            return TuesdayCloseTime;
        }

        public void setTuesdayCloseTime(String TuesdayCloseTime) {
            this.TuesdayCloseTime = TuesdayCloseTime;
        }

        public boolean isTuesdayIsOpen() {
            return TuesdayIsOpen;
        }

        public void setTuesdayIsOpen(boolean TuesdayIsOpen) {
            this.TuesdayIsOpen = TuesdayIsOpen;
        }

        public String getWednesdayOpenTime() {
            return WednesdayOpenTime;
        }

        public void setWednesdayOpenTime(String WednesdayOpenTime) {
            this.WednesdayOpenTime = WednesdayOpenTime;
        }

        public String getWednesdayCloseTime() {
            return WednesdayCloseTime;
        }

        public void setWednesdayCloseTime(String WednesdayCloseTime) {
            this.WednesdayCloseTime = WednesdayCloseTime;
        }

        public boolean isWednesdayIsOpen() {
            return WednesdayIsOpen;
        }

        public void setWednesdayIsOpen(boolean WednesdayIsOpen) {
            this.WednesdayIsOpen = WednesdayIsOpen;
        }

        public String getThursdayOpenTime() {
            return ThursdayOpenTime;
        }

        public void setThursdayOpenTime(String ThursdayOpenTime) {
            this.ThursdayOpenTime = ThursdayOpenTime;
        }

        public String getThursdayCloseTime() {
            return ThursdayCloseTime;
        }

        public void setThursdayCloseTime(String ThursdayCloseTime) {
            this.ThursdayCloseTime = ThursdayCloseTime;
        }

        public boolean isThursdayIsOpen() {
            return ThursdayIsOpen;
        }

        public void setThursdayIsOpen(boolean ThursdayIsOpen) {
            this.ThursdayIsOpen = ThursdayIsOpen;
        }

        public String getFridayOpenTime() {
            return FridayOpenTime;
        }

        public void setFridayOpenTime(String FridayOpenTime) {
            this.FridayOpenTime = FridayOpenTime;
        }

        public String getFridayCloseTime() {
            return FridayCloseTime;
        }

        public void setFridayCloseTime(String FridayCloseTime) {
            this.FridayCloseTime = FridayCloseTime;
        }

        public boolean isFridayIsOpen() {
            return FridayIsOpen;
        }

        public void setFridayIsOpen(boolean FridayIsOpen) {
            this.FridayIsOpen = FridayIsOpen;
        }

        public String getSaturdayOpenTime() {
            return SaturdayOpenTime;
        }

        public void setSaturdayOpenTime(String SaturdayOpenTime) {
            this.SaturdayOpenTime = SaturdayOpenTime;
        }

        public String getSaturdayCloseTime() {
            return SaturdayCloseTime;
        }

        public void setSaturdayCloseTime(String SaturdayCloseTime) {
            this.SaturdayCloseTime = SaturdayCloseTime;
        }

        public boolean isSaturdayIsOpen() {
            return SaturdayIsOpen;
        }

        public void setSaturdayIsOpen(boolean SaturdayIsOpen) {
            this.SaturdayIsOpen = SaturdayIsOpen;
        }

        public String getSundayOpenTime() {
            return SundayOpenTime;
        }

        public void setSundayOpenTime(String SundayOpenTime) {
            this.SundayOpenTime = SundayOpenTime;
        }

        public String getSundayCloseTime() {
            return SundayCloseTime;
        }

        public void setSundayCloseTime(String SundayCloseTime) {
            this.SundayCloseTime = SundayCloseTime;
        }

        public boolean isSundayIsOpen() {
            return SundayIsOpen;
        }

        public void setSundayIsOpen(boolean SundayIsOpen) {
            this.SundayIsOpen = SundayIsOpen;
        }
    }

    public static class CategoryViewModelsBean {
        /**
         * $id : 3
         * Id : 16
         * Name : Starters
         * SubCategories : [{"$id":"4","Id":16,"Name":"fresh","Category_Id":16,"Items":[{"$id":"5","Id":4,"Name":"burger 1 - fresh","Description":"this is desc","Price":10,"SortOrder":1,"SubCategory_Id":16,"AddonCategories":[{"$id":"6","Id":5,"Name":"burger 1 -t","MaxSelection":1,"Item_Id":4,"AddonItems":[{"$id":"7","Id":3,"Name":"burger","Description":"","Price":60,"SortOrder":1,"AddonCategory_Id":5},{"$id":"8","Id":8,"Name":"burger 22","Description":"","Price":15,"SortOrder":1,"AddonCategory_Id":5}]},{"$id":"9","Id":6,"Name":"burgerss -t","MaxSelection":2,"Item_Id":4,"AddonItems":[{"$id":"10","Id":9,"Name":"item 1","Description":"","Price":10,"SortOrder":1,"AddonCategory_Id":6},{"$id":"11","Id":10,"Name":"item 2","Description":"","Price":10,"SortOrder":1,"AddonCategory_Id":6},{"$id":"12","Id":11,"Name":"item 3","Description":"","Price":10,"SortOrder":1,"AddonCategory_Id":6}]},{"$id":"13","Id":12,"Name":"burger 2 -t","MaxSelection":0,"Item_Id":4,"AddonItems":[{"$id":"14","Id":12,"Name":"item 5","Description":"","Price":10,"SortOrder":1,"AddonCategory_Id":12},{"$id":"15","Id":13,"Name":"item 7","Description":"","Price":10,"SortOrder":1,"AddonCategory_Id":12},{"$id":"16","Id":14,"Name":"item 9","Description":"","Price":10,"SortOrder":1,"AddonCategory_Id":12},{"$id":"17","Id":15,"Name":"item 77","Description":"","Price":10,"SortOrder":1,"AddonCategory_Id":12}]}]}]},{"$id":"18","Id":17,"Name":"Non-Fresh","Category_Id":16,"Items":[{"$id":"19","Id":6,"Name":"burger 2","Description":"this is desc","Price":80,"SortOrder":2,"SubCategory_Id":17,"AddonCategories":[{"$id":"20","Id":7,"Name":"burger 2","MaxSelection":1,"Item_Id":6,"AddonItems":[{"$id":"21","Id":6,"Name":"zinger","Description":"","Price":120,"SortOrder":2,"AddonCategory_Id":7},{"$id":"22","Id":7,"Name":"burger 2","Description":"","Price":95,"SortOrder":1,"AddonCategory_Id":7}]}]},{"$id":"23","Id":10,"Name":"test non frsh","Description":"asdsd","Price":145,"SortOrder":1,"SubCategory_Id":17,"AddonCategories":[]}]},{"$id":"24","Id":22,"Name":"Starters ","Category_Id":16,"Items":[{"$id":"25","Id":11,"Name":"Mushroom Pakora","Description":"","Price":1.9,"SortOrder":1,"SubCategory_Id":22,"AddonCategories":[]},{"$id":"26","Id":12,"Name":"Chicken Pakora","Description":"","Price":3,"SortOrder":2,"SubCategory_Id":22,"AddonCategories":[]},{"$id":"27","Id":13,"Name":"Garlic Mushrooms","Description":"","Price":2.2,"SortOrder":3,"SubCategory_Id":22,"AddonCategories":[]},{"$id":"28","Id":14,"Name":"Shami Kebab (2)","Description":"","Price":1.9,"SortOrder":4,"SubCategory_Id":22,"AddonCategories":[]},{"$id":"29","Id":15,"Name":"Samosa (veg) (3)","Description":"","Price":1.9,"SortOrder":5,"SubCategory_Id":22,"AddonCategories":[]},{"$id":"30","Id":16,"Name":"Samosa (meat) (3)","Description":"","Price":1.9,"SortOrder":6,"SubCategory_Id":22,"AddonCategories":[]},{"$id":"31","Id":17,"Name":"Aloo Chaat","Description":"","Price":2.4,"SortOrder":7,"SubCategory_Id":22,"AddonCategories":[]},{"$id":"32","Id":18,"Name":"Chicken Chaat","Description":"","Price":2.8,"SortOrder":8,"SubCategory_Id":22,"AddonCategories":[]},{"$id":"33","Id":19,"Name":"Onion Bhaji","Description":"","Price":1.9,"SortOrder":9,"SubCategory_Id":22,"AddonCategories":[]},{"$id":"34","Id":20,"Name":"King Prawn on Puree","Description":"","Price":3.7,"SortOrder":10,"SubCategory_Id":22,"AddonCategories":[]},{"$id":"35","Id":21,"Name":"Bhuna Prawn on Puree","Description":"","Price":3.2,"SortOrder":11,"SubCategory_Id":22,"AddonCategories":[]},{"$id":"36","Id":22,"Name":"Mixed Tray","Description":"Seekh Kebab, Onion Bhaji, Chicken Pakora, Lamb Tikka & Chicken Tikka","Price":5,"SortOrder":12,"SubCategory_Id":22,"AddonCategories":[]},{"$id":"37","Id":23,"Name":"Mixed Starter","Description":"Seekh Kebab, Onion Bhaji & Shami Kebab","Price":3.5,"SortOrder":13,"SubCategory_Id":22,"AddonCategories":[{"$id":"38","Id":13,"Name":"test addon category","MaxSelection":2,"Item_Id":23,"AddonItems":[]}]}]},{"$id":"39","Id":23,"Name":"Chicken Pakora","Category_Id":16,"Items":[]},{"$id":"40","Id":24,"Name":"Garlic Mushroom","Category_Id":16,"Items":[]},{"$id":"41","Id":25,"Name":"Garlic Mushrooms","Category_Id":16,"Items":[]},{"$id":"42","Id":26,"Name":"Shami Kabab","Category_Id":16,"Items":[]},{"$id":"43","Id":27,"Name":"Samosa (veg) (3)","Category_Id":16,"Items":[]},{"$id":"44","Id":28,"Name":"Samosa (meat) (3)","Category_Id":16,"Items":[]},{"$id":"45","Id":29,"Name":"Aloo Chaat","Category_Id":16,"Items":[]},{"$id":"46","Id":30,"Name":"Chicken Chaat","Category_Id":16,"Items":[]},{"$id":"47","Id":31,"Name":"Onion Bhaji","Category_Id":16,"Items":[]},{"$id":"48","Id":32,"Name":"King Prawn on Puree","Category_Id":16,"Items":[]},{"$id":"49","Id":33,"Name":"Bhuna Prawn on Puree","Category_Id":16,"Items":[]},{"$id":"50","Id":34,"Name":"Mixed Tray","Category_Id":16,"Items":[]},{"$id":"51","Id":35,"Name":"Mixed Starter","Category_Id":16,"Items":[]},{"$id":"52","Id":56,"Name":"ali","Category_Id":16,"Items":[]}]
         */

        private String $id;
        private int Id;
        private String Name;
        private List<SubCategoriesBean> SubCategories;

        public String get$id() {
            return $id;
        }

        public void set$id(String $id) {
            this.$id = $id;
        }

        public int getId() {
            return Id;
        }

        public void setId(int Id) {
            this.Id = Id;
        }

        public String getName() {
            return Name;
        }

        public void setName(String Name) {
            this.Name = Name;
        }

        public List<SubCategoriesBean> getSubCategories() {
            return SubCategories;
        }

        public void setSubCategories(List<SubCategoriesBean> SubCategories) {
            this.SubCategories = SubCategories;
        }

        public static class SubCategoriesBean {
            /**
             * $id : 4
             * Id : 16
             * Name : fresh
             * Category_Id : 16
             * Items : [{"$id":"5","Id":4,"Name":"burger 1 - fresh","Description":"this is desc","Price":10,"SortOrder":1,"SubCategory_Id":16,"AddonCategories":[{"$id":"6","Id":5,"Name":"burger 1 -t","MaxSelection":1,"Item_Id":4,"AddonItems":[{"$id":"7","Id":3,"Name":"burger","Description":"","Price":60,"SortOrder":1,"AddonCategory_Id":5},{"$id":"8","Id":8,"Name":"burger 22","Description":"","Price":15,"SortOrder":1,"AddonCategory_Id":5}]},{"$id":"9","Id":6,"Name":"burgerss -t","MaxSelection":2,"Item_Id":4,"AddonItems":[{"$id":"10","Id":9,"Name":"item 1","Description":"","Price":10,"SortOrder":1,"AddonCategory_Id":6},{"$id":"11","Id":10,"Name":"item 2","Description":"","Price":10,"SortOrder":1,"AddonCategory_Id":6},{"$id":"12","Id":11,"Name":"item 3","Description":"","Price":10,"SortOrder":1,"AddonCategory_Id":6}]},{"$id":"13","Id":12,"Name":"burger 2 -t","MaxSelection":0,"Item_Id":4,"AddonItems":[{"$id":"14","Id":12,"Name":"item 5","Description":"","Price":10,"SortOrder":1,"AddonCategory_Id":12},{"$id":"15","Id":13,"Name":"item 7","Description":"","Price":10,"SortOrder":1,"AddonCategory_Id":12},{"$id":"16","Id":14,"Name":"item 9","Description":"","Price":10,"SortOrder":1,"AddonCategory_Id":12},{"$id":"17","Id":15,"Name":"item 77","Description":"","Price":10,"SortOrder":1,"AddonCategory_Id":12}]}]}]
             */

            private String $id;
            private int Id;
            private String Name;
            private int Category_Id;
            private List<ItemsBean> Items;

            public String get$id() {
                return $id;
            }

            public void set$id(String $id) {
                this.$id = $id;
            }

            public int getId() {
                return Id;
            }

            public void setId(int Id) {
                this.Id = Id;
            }

            public String getName() {
                return Name;
            }

            public void setName(String Name) {
                this.Name = Name;
            }

            public int getCategory_Id() {
                return Category_Id;
            }

            public void setCategory_Id(int Category_Id) {
                this.Category_Id = Category_Id;
            }

            public List<ItemsBean> getItems() {
                return Items;
            }

            public void setItems(List<ItemsBean> Items) {
                this.Items = Items;
            }

            public static class ItemsBean extends DataBean {
                /**
                 * $id : 5
                 * Id : 4
                 * Name : burger 1 - fresh
                 * Description : this is desc
                 * Price : 10
                 * SortOrder : 1
                 * SubCategory_Id : 16
                 * AddonCategories : [{"$id":"6","Id":5,"Name":"burger 1 -t","MaxSelection":1,"Item_Id":4,"AddonItems":[{"$id":"7","Id":3,"Name":"burger","Description":"","Price":60,"SortOrder":1,"AddonCategory_Id":5},{"$id":"8","Id":8,"Name":"burger 22","Description":"","Price":15,"SortOrder":1,"AddonCategory_Id":5}]},{"$id":"9","Id":6,"Name":"burgerss -t","MaxSelection":2,"Item_Id":4,"AddonItems":[{"$id":"10","Id":9,"Name":"item 1","Description":"","Price":10,"SortOrder":1,"AddonCategory_Id":6},{"$id":"11","Id":10,"Name":"item 2","Description":"","Price":10,"SortOrder":1,"AddonCategory_Id":6},{"$id":"12","Id":11,"Name":"item 3","Description":"","Price":10,"SortOrder":1,"AddonCategory_Id":6}]},{"$id":"13","Id":12,"Name":"burger 2 -t","MaxSelection":0,"Item_Id":4,"AddonItems":[{"$id":"14","Id":12,"Name":"item 5","Description":"","Price":10,"SortOrder":1,"AddonCategory_Id":12},{"$id":"15","Id":13,"Name":"item 7","Description":"","Price":10,"SortOrder":1,"AddonCategory_Id":12},{"$id":"16","Id":14,"Name":"item 9","Description":"","Price":10,"SortOrder":1,"AddonCategory_Id":12},{"$id":"17","Id":15,"Name":"item 77","Description":"","Price":10,"SortOrder":1,"AddonCategory_Id":12}]}]
                 */

                private int parentIndex;
                private String $id;
                private int Id;
                private String Name;
                private String Description;

                private String ImagePath;
                private float Price;
                private int SortOrder;
                private int SubCategory_Id;
                private List<AddonCategoriesBean> AddonCategories;
                public String getImagePath() {
                    return ImagePath;
                }

                public void setImagePath(String imagePath) {
                    ImagePath = imagePath;
                }


                public int getParentIndex() {
                    return parentIndex;
                }

                public void setParentIndex(int parentIndex) {
                    this.parentIndex = parentIndex;
                }

                public String get$id() {
                    return $id;
                }

                public void set$id(String $id) {
                    this.$id = $id;
                }

                public int getId() {
                    return Id;
                }

                public void setId(int Id) {
                    this.Id = Id;
                }

                public String getName() {
                    return Name;
                }

                public void setName(String Name) {
                    this.Name = Name;
                }

                public String getDescription() {
                    return Description;
                }

                public void setDescription(String Description) {
                    this.Description = Description;
                }

                public float getPrice() {
                    return Price;
                }

                public void setPrice(float Price) {
                    this.Price = Price;
                }

                public int getSortOrder() {
                    return SortOrder;
                }

                public void setSortOrder(int SortOrder) {
                    this.SortOrder = SortOrder;
                }

                public int getSubCategory_Id() {
                    return SubCategory_Id;
                }

                public void setSubCategory_Id(int SubCategory_Id) {
                    this.SubCategory_Id = SubCategory_Id;
                }

                public List<AddonCategoriesBean> getAddonCategories() {
                    return AddonCategories;
                }

                public void setAddonCategories(List<AddonCategoriesBean> AddonCategories) {
                    this.AddonCategories = AddonCategories;
                }

                public int getItemLayoutId(StickyHeaderViewAdapter adapter) {
                    return R.layout.rv_menu_item_row;
                }

                public static class AddonCategoriesBean {
                    /**
                     * $id : 6
                     * Id : 5
                     * Name : burger 1 -t
                     * MaxSelection : 1
                     * Item_Id : 4
                     * AddonItems : [{"$id":"7","Id":3,"Name":"burger","Description":"","Price":60,"SortOrder":1,"AddonCategory_Id":5},{"$id":"8","Id":8,"Name":"burger 22","Description":"","Price":15,"SortOrder":1,"AddonCategory_Id":5}]
                     */

                    private String $id;
                    private int Id;
                    private String Name;
                    private int MaxSelection;
                    private int Item_Id;
                    private List<AddonItemsBean> AddonItems;

                    public String get$id() {
                        return $id;
                    }

                    public void set$id(String $id) {
                        this.$id = $id;
                    }

                    public int getId() {
                        return Id;
                    }

                    public void setId(int Id) {
                        this.Id = Id;
                    }

                    public String getName() {
                        return Name;
                    }

                    public void setName(String Name) {
                        this.Name = Name;
                    }

                    public int getMaxSelection() {
                        return MaxSelection;
                    }

                    public void setMaxSelection(int MaxSelection) {
                        this.MaxSelection = MaxSelection;
                    }

                    public int getItem_Id() {
                        return Item_Id;
                    }

                    public void setItem_Id(int Item_Id) {
                        this.Item_Id = Item_Id;
                    }

                    public List<AddonItemsBean> getAddonItems() {
                        return AddonItems;
                    }

                    public void setAddonItems(List<AddonItemsBean> AddonItems) {
                        this.AddonItems = AddonItems;
                    }

                    public static class AddonItemsBean {
                        /**
                         * $id : 7
                         * Id : 3
                         * Name : burger
                         * Description :
                         * Price : 60
                         * SortOrder : 1
                         * AddonCategory_Id : 5
                         */

                        private String $id;
                        private int Id;
                        private int AddonItem_Id;
                        private String Name;
                        private String Description;
                        private float Price;
                        transient private int SortOrder;
                        private int AddonCategory_Id;


                        public String get$id() {
                            return $id;
                        }

                        public void set$id(String $id) {
                            this.$id = $id;
                        }

                        public int getId() {
                            return Id;
                        }

                        public void setId(int Id) {
                            this.AddonItem_Id = Id;
                            this.Id = Id;
                        }

                        public int getAddonItem_Id() {
                            return AddonItem_Id;
                        }

                        public void setAddonItem_Id(int addonItem_Id) {
                            AddonItem_Id = addonItem_Id;
                        }

                        public String getName() {
                            return Name;
                        }

                        public void setName(String Name) {
                            this.Name = Name;
                        }

                        public String getDescription() {
                            return Description;
                        }

                        public void setDescription(String Description) {
                            this.Description = Description;
                        }

                        public float getPrice() {
                            return Price;
                        }

                        public void setPrice(float Price) {
                            this.Price = Price;
                        }

                        public int getSortOrder() {
                            return SortOrder;
                        }

                        public void setSortOrder(int SortOrder) {
                            this.SortOrder = SortOrder;
                        }

                        public int getAddonCategory_Id() {
                            return AddonCategory_Id;
                        }

                        public void setAddonCategory_Id(int AddonCategory_Id) {
                            this.AddonCategory_Id = AddonCategory_Id;
                        }
                    }
                }
            }
        }
    }
}
