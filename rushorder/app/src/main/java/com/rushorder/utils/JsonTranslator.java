package com.rushorder.utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import com.rushorder.models.AddonItemsResponse;
import com.rushorder.models.CuisinesResponse;
import com.rushorder.models.CustomerReviewsResponse;
import com.rushorder.models.HistoryResponse;
import com.rushorder.models.RestaurantInfoResponse.CategoryViewModelsBean.SubCategoriesBean.ItemsBean.AddonCategoriesBean.AddonItemsBean;
import com.rushorder.models.RestaurantsResponse;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Aleem on 10/25/2017.
 */

public class JsonTranslator {

    private Gson gson;
    private static JsonTranslator mInstance;
    private String error_msg = "Unexpected parsing error. Please try again later.";

    private JsonTranslator() {
        gson = new GsonBuilder().serializeNulls().create();
    }

    /**
     * @return
     */
    public static synchronized JsonTranslator getInstance() {
        if (mInstance == null) {
            mInstance = new JsonTranslator();
        }
        return mInstance;
    }

    /**
     * Generic json object parser
     *
     * @param _response
     * @param _model
     * @param <T>
     * @return
     * @throws Exception
     */

    public <T> T parse(String _response, Class<T> _model) throws Exception {
        T parsedObject = null;

        try {
            if (_response != null) {
                parsedObject = gson.fromJson(_response, _model);
            }
        } catch (JsonSyntaxException _jse) {
            throw new Exception(_jse.toString());
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        }

        return parsedObject;
    }

    /**
     * Generic object to json string parser
     *
     * @param _model
     * @param <T>
     * @return
     * @throws Exception
     */
    public <T> String parseToString(Class<T> _model) throws Exception {
        String parsedObject = null;

        try {
            if (_model != null) {
                parsedObject = gson.toJson(_model);
            }
        } catch (JsonSyntaxException _jse) {
            throw new Exception(error_msg);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        }

        return parsedObject;
    }

    public String parseListString(List<AddonItemsBean> childs) {

        String listString = gson.toJson(
                childs,
                new TypeToken<List<AddonItemsResponse.ItemsBean>>() {
                }.getType());

        return listString;


    }


    public List<CuisinesResponse> parseJsonToList(String _JsonList) {
        List<CuisinesResponse> convertedList = new ArrayList<>();
        try {
            if (_JsonList != null) {
                convertedList = gson.fromJson(_JsonList, new TypeToken<List<CuisinesResponse>>() {
                }.getType());
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return convertedList;
    }

    public List<HistoryResponse> parseHistoryJsonToList(String _JsonList) throws Exception {
        List<HistoryResponse> convertedList = new ArrayList<>();
        try {
            if (_JsonList != null) {
                convertedList = gson.fromJson(_JsonList, new TypeToken<List<HistoryResponse>>() {
                }.getType());
            }
        } catch (Exception ex) {
            throw new Exception(ex);
        }
        return convertedList;
    }

    public List<RestaurantsResponse> parseRestaurantJsonToList(String _JsonList) throws Exception {
        List<RestaurantsResponse> convertedList = new ArrayList<>();
        try {
            if (_JsonList != null) {
                convertedList = gson.fromJson(_JsonList, new TypeToken<List<RestaurantsResponse>>() {
                }.getType());
            }
        } catch (Exception ex) {
            throw new Exception(ex);
        }
        return convertedList;
    }

    public List<CustomerReviewsResponse.ReviewsBean> parseReviewsJsonToList(String _JsonList) throws Exception {
        List<CustomerReviewsResponse.ReviewsBean> convertedList = new ArrayList<>();
        try {
            if (_JsonList != null) {
                convertedList = gson.fromJson(_JsonList, new TypeToken<List<CustomerReviewsResponse.ReviewsBean>>() {
                }.getType());
            }
        } catch (Exception ex) {
            throw new Exception(ex);
        }
        return convertedList;
    }
}
