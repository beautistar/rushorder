package com.rushorder.models;

public class FiltersModel {


    private int CousID;
    private boolean Raiting;
    private boolean Delivery;
    private boolean NewArrival;
    private String BestMatch;
    private String Postcode;
    private boolean Distance;
    private double lng;
    private double Lat;
    private boolean Discount;
    private boolean Deals;
    private int PageNo;
    private int NoOfRec;


    public int getCousID() {
        return CousID;
    }

    public void setCousID(int cousID) {
        CousID = cousID;
    }

    public boolean isRaiting() {
        return Raiting;
    }

    public void setRaiting(boolean raiting) {
        Raiting = raiting;
    }

    public boolean isDelivery() {
        return Delivery;
    }

    public void setDelivery(boolean delivery) {
        Delivery = delivery;
    }

    public boolean isNewArrival() {
        return NewArrival;
    }

    public void setNewArrival(boolean newArrival) {
        NewArrival = newArrival;
    }

    public String getBestMatch() {
        return BestMatch;
    }

    public void setBestMatch(String bestMatch) {
        BestMatch = bestMatch;
    }

    public String getPostcode() {
        return Postcode;
    }

    public void setPostcode(String postcode) {
        Postcode = postcode;
    }

    public boolean isDistance() {
        return Distance;
    }

    public void setDistance(boolean distance) {
        Distance = distance;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public double getLat() {
        return Lat;
    }

    public void setLat(double lat) {
        Lat = lat;
    }

    public boolean isDiscount() {
        return Discount;
    }

    public void setDiscount(boolean discount) {
        Discount = discount;
    }

    public boolean isDeals() {
        return Deals;
    }

    public void setDeals(boolean deals) {
        Deals = deals;
    }

    public int getPageNo() {
        return PageNo;
    }

    public void setPageNo(int pageNo) {
        PageNo = pageNo;
    }

    public int getNoOfRec() {
        return NoOfRec;
    }

    public void setNoOfRec(int noOfRec) {
        NoOfRec = noOfRec;
    }

    private FiltersModel(Builder builder) {
        CousID = builder.CousID;
        Raiting = builder.Raiting;
        Delivery = builder.Delivery;
        NewArrival = builder.NewArrival;
        BestMatch = builder.BestMatch;
        Postcode = builder.Postcode;
        Distance = builder.Distance;
        lng = builder.lng;
        Lat = builder.Lat;
        Discount = builder.Discount;
        Deals = builder.Deals;
        PageNo = builder.PageNo;
        NoOfRec = builder.NoOfRec;
    }


    public static final class Builder {
        private int CousID;
        private boolean Raiting;
        private boolean Delivery;
        private boolean NewArrival;
        private String BestMatch = "";
        private String Postcode = "";
        private boolean Distance;
        private double lng;
        private double Lat;
        private boolean Discount;
        private boolean Deals;
        private int PageNo;
        private int NoOfRec;

        public Builder() {
        }

        public Builder CousID(int val) {
            CousID = val;
            return this;
        }

        public Builder Raiting(boolean val) {
            Raiting = val;
            return this;
        }

        public Builder Delivery(boolean val) {
            Delivery = val;
            return this;
        }

        public Builder NewArrival(boolean val) {
            NewArrival = val;
            return this;
        }

        public Builder BestMatch(String val) {
            BestMatch = val;
            return this;
        }

        public Builder Postcode(String val) {
            Postcode = val;
            return this;
        }

        public Builder Distance(boolean val) {
            Distance = val;
            return this;
        }

        public Builder lng(double val) {
            lng = val;
            return this;
        }

        public Builder Lat(double val) {
            Lat = val;
            return this;
        }

        public Builder Discount(boolean val) {
            Discount = val;
            return this;
        }

        public Builder Deals(boolean val) {
            Deals = val;
            return this;
        }

        public Builder PageNo(int val) {
            PageNo = val;
            return this;
        }

        public Builder NoOfRec(int val) {
            NoOfRec = val;
            return this;
        }

        public FiltersModel build() {
            return new FiltersModel(this);
        }
    }
}
