package com.rushorder.managers;

import android.content.Context;
import android.content.SharedPreferences;

import com.rushorder.app.AppController;
import com.rushorder.utils.JsonTranslator;


/**
 * Created by Aleem on 10/25/2017.
 */

public class PrefManager {


    private int itemHeight;
    private static PrefManager mInstance;
    protected SharedPreferences.Editor editor;
    protected SharedPreferences sharedPreferences;
    public static final String EMAIL = "email";
    public static final String LAT = "lat";
    public static final String LNG = "lng";


    public static final String TOWN = "town";
    public static final String PHONE = "phone";
    public static final String STREET = "street";
    public static final String  DOOR_NUMBER= "doorNum";

    public static final String RES_ID = "resId";
    public static final String IS_LOGIN = "isLogin";
    public static final String POST_CODE = "postCode";
    public static final String USER_INFO = "userInfo";
    public static final String OBJECT_KEY = "objectKey";
    public static final String IS_SOCIAL_LOGIN = "isSocialLogin";
    public static final String DEVICE_REGISTERED = "deviceRegistered";
    public static final String DEVICE_REGISTERED_DISCOUNT = "deviceRegisteredDiscount";




    PrefManager() {
        sharedPreferences = AppController.getAppContext().getSharedPreferences(SharedPreferencesSettings.NAME, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
    }


    public static synchronized PrefManager getInstance() {
        if (mInstance == null) {
            mInstance = new PrefManager();
        }
        return mInstance;
    }

    /**
     * Save Object
     *
     * @param _model
     * @param <T>
     * @throws Exception
     */
    public <T> void saveObject(Class<T> _model) throws Exception {

        editor.putString(OBJECT_KEY, JsonTranslator.getInstance().parseToString(_model));
        editor.commit();
    }

    /**
     * Save user info
     *
     * @param response
     * @throws Exception
     */

    public void saveUserInfo(String response, String userEmail) throws Exception {
        editor.putString(USER_INFO, response);
        editor.putString(EMAIL, userEmail);
        editor.putBoolean(IS_LOGIN, true);
        editor.commit();
    }

    public void saveLocation(double lat, double lng) throws Exception {
        editor.putString(LAT, "" + lat);
        editor.putString(LNG, "" + lng);
        editor.commit();
    }


    public void saveAddress(String street , String town , String doorNumber , String phone) {
        editor.putString(TOWN, "" + town);
        editor.putString(PHONE, "" + phone);
        editor.putString(STREET, "" + street);
        editor.putString(DOOR_NUMBER, "" + doorNumber);
        editor.commit();
    }

    public void saveUserInfo(String response, boolean isSocialLogin) throws Exception {
        editor.putString(USER_INFO, response);
        editor.putBoolean(IS_SOCIAL_LOGIN, isSocialLogin);
        editor.putBoolean(IS_LOGIN, true);
        editor.commit();
    }


    public void saveSearchPostCode(String postCode) {
        editor.putString(POST_CODE, postCode);
        editor.commit();
    }

    public void saveSelectedRestaurantApiKey(String apiKey) {
        editor.putString(RES_ID, apiKey);
        editor.commit();
    }


    /**
     * Firebase token registration
     *
     * @param isRegistered
     */

    public void isDeviceRegistered(boolean isRegistered) {
        editor.putBoolean(DEVICE_REGISTERED, isRegistered);
        editor.commit();
    }

    public void saveDeviceRegistrationDiscount(String discount) {
        editor.putString(DEVICE_REGISTERED_DISCOUNT, discount);
        editor.commit();
    }

    /**
     * Get saved string value
     *
     * @param fieldName
     * @return
     */
    public String getString(String fieldName) {
        return sharedPreferences.getString(fieldName, SharedPreferencesSettings.DEFAULT_VALUE_STRING);
    }

    /**
     * Get saved integer value
     *
     * @param fieldName
     * @return
     */

    public int getInt(String fieldName) {
        return sharedPreferences.getInt(fieldName, SharedPreferencesSettings.DEFAULT_VALUE_INT);
    }

    /**
     * Get saved Long value
     *
     * @param fieldName
     * @return
     */
    public long getLong(String fieldName) {
        return sharedPreferences.getLong(fieldName, SharedPreferencesSettings.DEFAULT_VALUE_LONG);
    }

    /**
     * Get saved boolean
     *
     * @param fieldName
     * @return
     */
    public boolean getBoolean(String fieldName) {
        return sharedPreferences.getBoolean(fieldName, SharedPreferencesSettings.DEFAULT_VALUE_BOOLEAN);
    }

    /**
     * Get saved object
     *
     * @param _key
     * @param _model
     * @param <T>
     * @return
     * @throws Exception
     */

    public <T> T getObject(String _key, Class<T> _model) {
        T parsedObject = null;

        try {
            return JsonTranslator.getInstance().parse(getString(_key), _model);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return parsedObject;
    }

    /**
     * Clear all saved preferences
     */
    public void clearAll() {

        editor.clear();
        editor.commit();

    }


    /*
     * SharedPreferencesSettings
     */
    public static class SharedPreferencesSettings {
        public final static int MODE = 0;
        public final static String NAME = "jani_app";
        public final static int CUSTOM_VALUE_INT = -1;
        public final static int DEFAULT_VALUE_INT = 0;
        public final static long DEFAULT_VALUE_LONG = 0;
        public final static String DEFAULT_VALUE_STRING = "";
        public final static boolean DEFAULT_VALUE_BOOLEAN = false;
    }
}
