package com.rushorder.models;


import java.util.ArrayList;

public class CustomerReviewsResponse {


    /**
     * have_data : true
     * food_overall : 5
     * deliver_overall : 5
     * amount_overall : 5
     * reviews : [{"name":"atif","description":"test feedback","time":"86 days ago","rating":5},{"name":"Atif Rehman","description":"Great Taste!","time":"88 days ago","rating":5},{"name":"Adnan Aslam","description":"Great Taste!","time":"88 days ago","rating":5}]
     * total_overall : 5
     */

    private boolean have_data;
    private int total_reviews;
    private float food_overall;
    private float amount_overall;
    private float total_overall;
    private float service_overall;
    private float deliver_overall;
    private ArrayList<ReviewsBean> reviews;

    public boolean isHave_data() {
        return have_data;
    }

    public void setHave_data(boolean have_data) {
        this.have_data = have_data;
    }

    public int getTotal_reviews() {
        return total_reviews;
    }

    public void setTotal_reviews(int total_reviews) {
        this.total_reviews = total_reviews;
    }

    public float getFood_overall() {
        return food_overall;
    }

    public void setFood_overall(float food_overall) {
        this.food_overall = food_overall;
    }

    public float getDeliver_overall() {
        return deliver_overall;
    }

    public void setDeliver_overall(float deliver_overall) {
        this.deliver_overall = deliver_overall;
    }

    public float getAmount_overall() {
        return amount_overall;
    }

    public void setAmount_overall(float amount_overall) {
        this.amount_overall = amount_overall;
    }

    public float getTotal_overall() {
        return total_overall;
    }

    public void setTotal_overall(float total_overall) {
        this.total_overall = total_overall;
    }

    public float getService_overall() {
        return service_overall;
    }

    public void setService_overall(float service_overall) {
        this.service_overall = service_overall;
    }

    public ArrayList<ReviewsBean> getReviews() {

        if (reviews == null) {
            return new ArrayList<>();
        }
        return reviews;
    }

    public void setReviews(ArrayList<ReviewsBean> reviews) {
        this.reviews = reviews;
    }

    public static class ReviewsBean {
        /**
         * name : atif
         * description : test feedback
         * time : 86 days ago
         * rating : 5
         */





        private String name;
        private String time;
        private float rating;
        private String description;
        /**
         * Id : 2
         * UserName : aleem@yahoo.com
         * Comment : booy data
         * Raiting : 2.3
         */

        private int Id;
        private String UserName;
        private String Comment;
        private float Raiting;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getTime() {
            return time;
        }

        public void setTime(String time) {
            this.time = time;
        }

        public float getRating() {
            return rating;
        }

        public void setRating(float rating) {
            this.rating = rating;
        }

        public int getId() {
            return Id;
        }

        public void setId(int Id) {
            this.Id = Id;
        }

        public String getUserName() {
            return UserName;
        }

        public void setUserName(String UserName) {
            this.UserName = UserName;
        }

        public String getComment() {
            return Comment;
        }

        public void setComment(String Comment) {
            this.Comment = Comment;
        }

        public float getRaiting() {
            return Raiting;
        }

        public void setRaiting(float Raiting) {
            this.Raiting = Raiting;
        }
    }
}
