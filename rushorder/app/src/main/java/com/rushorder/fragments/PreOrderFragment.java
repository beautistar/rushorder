package com.rushorder.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.rushorder.R;
import com.rushorder.apis.Apis;
import com.rushorder.constants.Constants;
import com.rushorder.fragments.dialogs.PreOrderTimingDialog;
import com.rushorder.interfaces.VolleyCallBack;
import com.rushorder.managers.PrefManager;
import com.rushorder.models.Cart;
import com.rushorder.utils.Utils;

import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class PreOrderFragment extends BaseFragment implements View.OnClickListener {

    private String note;
    private EditText etNote;
    private String selectedTime;
    private RelativeLayout rlCart;
    private LinearLayout llSelectTime;
    private LinearLayout llProcessOrder, llNote;
    private TextView tvSelectTime, tvOtherRequirement;


    public static PreOrderFragment getInstance() {
        return new PreOrderFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_pre_order, container, false);
        initViews(view);
        setListeners();
        return view;
    }

    @Override
    protected void initViews(View view) {
        rlCart = view.findViewById(R.id.rlCart);
        llNote = view.findViewById(R.id.llNote);
        etNote = view.findViewById(R.id.etNoteDescription);
        tvSelectTime = view.findViewById(R.id.tvSelectTime);
        llProcessOrder = view.findViewById(R.id.llProcessOrder);
        tvOtherRequirement = view.findViewById(R.id.tvOtherRequirement);
        super.initViews(view);
    }

    @Override
    protected void setListeners() {
        rlCart.setOnClickListener(this);
        llNote.setOnClickListener(this);
        llBack.setOnClickListener(this);
        tvSelectTime.setOnClickListener(this);
        llProcessOrder.setOnClickListener(this);
        tvOtherRequirement.setOnClickListener(this);
        setValues();
        super.setListeners();
    }

    @Override
    protected void setValues() {
        note = getArguments().getString(Constants.Keys.ORDER_NOTE);
        ((TextView) rlCart.findViewById(R.id.tvCartCount)).setText("" + Cart.getInstance().getOrderDetail().size());
        etNote.setText(note);
        super.setValues();
    }

    @Override
    public void onClick(View v) {

        if (v == llBack || v == rlCart) {
            onBackPressed();
        } else if (v == tvSelectTime) {
            getRestaurantTime();
        } else if (v == llNote) {
            etNote.requestFocus();
            Utils.showSoftKeyboard(getActivity(), etNote);
        } else {
            Utils.hideSoftKeyboard(getActivity(), etNote);
            processOrder();
        }
    }


    public void getRestaurantTime() {
//        SignInResponse signInResponse = PrefManager.getInstance().getObject(PrefManager.USER_INFO, SignInResponse.class);

        Apis.Order.getRestaurantTime(getActivity(), true, PrefManager.getInstance().getString(PrefManager.RES_ID), new VolleyCallBack.JSONResponse() {
            @Override
            public void onResponse(JSONObject _response, String _tag) {
                showTimingDialog(_response.toString());
            }

            @Override
            public void onErrorMessage(String _errorMessage) {
                showErrorDialog(_errorMessage);
            }

            @Override
            public void onErrorResponse(VolleyError _error, String _tag) {
                showErrorDialog(getString(R.string.something_went_wrong));
            }

            @Override
            public void isConnected(boolean _connected, String _tag) {
                showErrorDialog(getString(R.string.no_internet));
            }
        });


    }

    private void showTimingDialog(String timingResponse) {
        Bundle bundle = new Bundle();
        bundle.putString(Constants.Keys.RESPONSE_KEY, timingResponse);
        PreOrderTimingDialog dialog = new PreOrderTimingDialog();
        dialog.setArguments(bundle);
        dialog.show(getChildFragmentManager(), "PreOrderDialog");
    }

    private void processOrder() {

        note = etNote.getText().toString();
        selectedTime = tvSelectTime.getText().toString();
        if (selectedTime.isEmpty()) {
            showErrorDialog("Please select time.");
        } else {
            startProcessOrderFragment();
        }
    }

    private void startProcessOrderFragment() {
        Bundle bundle = new Bundle();
        bundle.putBoolean(Constants.Keys.IS_PREORDER, true);
        bundle.putString(Constants.Keys.PREORDER_NOTE, note);
        bundle.putString(Constants.Keys.PREORDER_DATE, getDate());
        bundle.putString(Constants.Keys.PREORDER_TIME, getTime());
        bundle.putFloat(Constants.Keys.DISCOUNT, getArguments().getFloat(Constants.Keys.DISCOUNT));
        bundle.putFloat(Constants.Keys.TOTAL_PRICE, getArguments().getFloat(Constants.Keys.TOTAL_PRICE));
        bundle.putFloat(Constants.Keys.GRAND_TOTAL, getArguments().getFloat(Constants.Keys.GRAND_TOTAL));
        switchFragment(ProcessOrderFragment.getInstance(), bundle);
    }

    public void timeSelected(String selectedTime) {
        tvSelectTime.setText("" + selectedTime);
    }


    private String getDate() {
        Calendar calendar = Calendar.getInstance();
        String date = new SimpleDateFormat("yyyy-MM-dd").format(calendar.getTime());
        return date;
    }

    private String getTime() {
        return selectedTime.split(" ")[1];
    }

    private String[] getDateTime() {

        String[] dateTime = new String[3];
        try {
            Calendar calendar = Calendar.getInstance();
            dateTime[0] = String.valueOf(new SimpleDateFormat("dd-MM-yyyy").format(calendar.getTime()));

            Date time;
            SimpleDateFormat inputFormat = new SimpleDateFormat("hh:mm:ss");
            time = inputFormat.parse(selectedTime.split(" ")[1]);

            SimpleDateFormat outPutFormat = new SimpleDateFormat("hh:mmaa");
            dateTime[1] = outPutFormat.format(time);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return dateTime;

    }


}