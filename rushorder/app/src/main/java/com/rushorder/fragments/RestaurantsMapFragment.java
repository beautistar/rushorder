package com.rushorder.fragments;


import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.rushorder.R;
import com.rushorder.cache.CacheData;
import com.rushorder.constants.Constants;
import com.rushorder.fragments.adapters.RestaurantPagerAdapter;
import com.rushorder.interfaces.ClickCallBack;
import com.rushorder.managers.PrefManager;
import com.rushorder.models.Cart;
import com.rushorder.models.RestaurantsResponse;
import com.rushorder.utils.AlertUtils;
import com.rushorder.views.WrapContentViewPager;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.List;

public class RestaurantsMapFragment extends BaseFragment implements
        ClickCallBack, OnMapReadyCallback, ViewPager.OnPageChangeListener, View.OnClickListener, GoogleMap.OnMarkerClickListener {

    private GoogleMap googleMap;
    private SupportMapFragment mapFragment;
    private RestaurantPagerAdapter adapter;
    private WrapContentViewPager vpRestaurants;
    private TextView tvList, tvEmpty;
    private List<RestaurantsResponse> restaurantsList;

    public static RestaurantsMapFragment getInstance() {
        return new RestaurantsMapFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_restaurants_map, container, false);
        initViews(view);
        setListeners();
        setValues();
        return view;
    }

    @Override
    protected void initViews(View view) {
        tvEmpty = view.findViewById(R.id.tvEmpty);
        tvList = view.findViewById(R.id.tvList);
        vpRestaurants = view.findViewById(R.id.vpRestaurants);
        super.initViews(view);
    }

    @Override
    protected void setValues() {
        restaurantsList = CacheData.Restaurants.getInstance().getRestaurantsList();
        if (restaurantsList != null && restaurantsList.size() != 0) {
            restaurantsList.remove(restaurantsList.size() - 1);
            setMapFragment();
            setAdapter();

        } else {
            restaurantsList = new ArrayList<>();
            tvEmpty.setVisibility(View.VISIBLE);
        }


        super.setValues();
    }

    void setMapFragment() {
        mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.mapsFragment);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        this.googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        this.googleMap.getUiSettings().setCompassEnabled(true);
        this.googleMap.getUiSettings().setZoomControlsEnabled(true);
        this.googleMap.getUiSettings().setZoomGesturesEnabled(true);
        this.googleMap.getUiSettings().setMyLocationButtonEnabled(true);

        if (restaurantsList.size() != 0) {
            moveCameraToLocation(restaurantsList.get(0), 13.5f);
        }

        googleMap.setOnMarkerClickListener(this);
        setMarkersOnMap();
    }

    @Override
    protected void setListeners() {
        tvList.setOnClickListener(this);
        llBack.setOnClickListener(this);
        vpRestaurants.addOnPageChangeListener(this);
        super.setListeners();
    }

    private void setAdapter() {
        adapter = new RestaurantPagerAdapter(this, restaurantsList);
        vpRestaurants.setAdapter(adapter);
    }

    @Override
    public void callBack() {

    }

    @Override
    public void callBack(Object _model, int position) {

        String apiKey = ((RestaurantsResponse) _model).getId();
        if (Cart.getInstance().isCartFull()) {
            if (apiKey.equals(PrefManager.getInstance().getString(PrefManager.RES_ID))) {
                showRestaurantDetails(position, apiKey);
            } else {
                showWarningDialog(getString(R.string.cart_will_be_cleared), position, apiKey);
            }
        } else {
            showRestaurantDetails(position, apiKey);
        }
    }


    private void showRestaurantDetails(int position, String apiKey) {
        Bundle bundle = new Bundle();
        bundle.putInt(Constants.Keys.ITEM_POSITION_KEY, position);
        PrefManager.getInstance().saveSelectedRestaurantApiKey(apiKey);
        switchFragment(RestaurantDetailsFragment.getInstance(), bundle);
    }

    private void showWarningDialog(String message, final int position, final String apiKey) {

        AlertUtils.showAlert(getActivity(), "Warning !", message, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                Cart.getInstance().ClearCart();
                showRestaurantDetails(position, apiKey);

            }
        });

    }


    private void setMarkersOnMap() {
//        for (int index = 0; index < restaurantsList.size(); index++) {
//
//            RestaurantsResponse.Result model = restaurantsList.get(index);
//            try {
//                String lat = model.getLatitude();
//                String lon = model.getLongitude();
//
//                if (!lat.equals("") && !lon.equals("")) {
//                    double latitude = Double.parseDouble(lat);
//                    double longitude = Double.parseDouble(lon);
//                    addMarkerToLocations(latitude, longitude, index, model.getItemName());
//                }
//            } catch (NumberFormatException ex) {
//                ex.printStackTrace();
//            }
//        }
    }

    private void addMarkerToLocations(double mLat, double mLng, int index, String title) {
        LatLng latLng = new LatLng(mLat, mLng);
        Marker marker = googleMap.addMarker(new MarkerOptions().position(latLng));
        marker.setTitle(title);
        marker.setTag(index);
    }

    private void moveCameraToLocation(RestaurantsResponse model, float zoomLevel) {

//
//        String lat = model.getLatitude();
//        String lon = model.getLongitude();
//
//        if (lat != null && lon != null && !lat.equals("") && !lon.equals("")) {
//
//            double latitude = Double.parseDouble(lat);
//            double longitude = Double.parseDouble(lon);
//
//            LatLng latLng = new LatLng(latitude, longitude);
//            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, zoomLevel);
//            googleMap.animateCamera(cameraUpdate);
//        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        moveCameraToLocation(restaurantsList.get(position), 13.5f);
    }

    @Override
    public void onPageScrollStateChanged(int state) {
    }

    @Override
    public void onClick(View v) {
        if (v == llBack) {
            onBackPressed();
        } else {
            switchFragment(RestaurantListFragment.getInstance());
        }

    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        int restaurantPagerIndex = (int) marker.getTag();
        marker.showInfoWindow();
        movePagerToPosition(restaurantPagerIndex);
        return true;
    }

    private void movePagerToPosition(int restaurantPagerIndex) {
        vpRestaurants.setCurrentItem(restaurantPagerIndex, true);
    }
}
