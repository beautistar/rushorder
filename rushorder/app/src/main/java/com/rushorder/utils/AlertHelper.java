package com.rushorder.utils;


import android.app.Activity;

import com.rushorder.fragments.dialogs.AppProgressDialog;


public class AlertHelper {

    private AppProgressDialog progressDialog;

    public static synchronized AlertHelper showProgress(Activity activity, boolean isDialog) {
        return new AlertHelper(activity, isDialog);
    }

    public AlertHelper(Activity activity, boolean isDialog) {
        setUpProgress(activity, isDialog);

    }

    private void setUpProgress(Activity activity, boolean isDialog) {

        try {

            if (isDialog) {
                progressDialog = AppProgressDialog.newInstance();
                progressDialog.show(activity.getFragmentManager(), "");
                progressDialog.setCancelable(false);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    public void dismissProgress() {
        try {
            if (progressDialog == null || progressDialog.isHidden()) {
                return;
            }
            progressDialog.dismiss();
            progressDialog = null;

        } catch (Exception ex) {
            progressDialog.dismissAllowingStateLoss();
        }
    }

    public boolean isShowingProgress() {
        return progressDialog != null && progressDialog.isVisible();
    }
}
