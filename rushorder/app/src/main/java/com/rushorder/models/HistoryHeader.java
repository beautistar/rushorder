package com.rushorder.models;

import com.rushorder.R;

import tellh.com.stickyheaderview_rv.adapter.DataBean;
import tellh.com.stickyheaderview_rv.adapter.StickyHeaderViewAdapter;

public class HistoryHeader extends DataBean {


    private String $id;
    private int pkcode;
    private int fkrest;
    private String usercmnt;
    private float tamount;
    private int fkcoupon;
    private float discountamt;
    private String discoutcmnt;
    private int fkordersts;
    private String orderdate;
    private int dailytrno;
    private int fkordertypsorc;
    private String paymnttype;
    private String ordertype;
    private int fkmilechrg;
    private int servicechrg;
    private int carybagamt;
    private boolean preorder;
    private String prorderdate;
    private int fkfloor;
    private int fktable;
    private boolean isdelete;
    private int ServingTime;
    private String prordertime;

    public String get$id() {
        return $id;
    }

    public int getPkcode() {
        return pkcode;
    }

    public int getFkrest() {
        return fkrest;
    }

    public String getUsercmnt() {
        return usercmnt;
    }

    public float getTamount() {
        return tamount;
    }

    public int getFkcoupon() {
        return fkcoupon;
    }

    public float getDiscountamt() {
        return discountamt;
    }

    public String getDiscoutcmnt() {
        return discoutcmnt;
    }

    public int getFkordersts() {
        return fkordersts;
    }

    public String getOrderdate() {
        return orderdate;
    }

    public int getDailytrno() {
        return dailytrno;
    }

    public int getFkordertypsorc() {
        return fkordertypsorc;
    }

    public String getPaymnttype() {
        return paymnttype;
    }

    public String getOrdertype() {
        return ordertype;
    }

    public int getFkmilechrg() {
        return fkmilechrg;
    }

    public int getServicechrg() {
        return servicechrg;
    }

    public int getCarybagamt() {
        return carybagamt;
    }

    public boolean isPreorder() {
        return preorder;
    }

    public String getProrderdate() {
        return prorderdate;
    }

    public int getFkfloor() {
        return fkfloor;
    }

    public int getFktable() {
        return fktable;
    }

    public boolean isIsdelete() {
        return isdelete;
    }

    public int getServingTime() {
        return ServingTime;
    }

    public String getPrordertime() {
        return prordertime;
    }

    @Override
    public int getItemLayoutId(StickyHeaderViewAdapter stickyHeaderViewAdapter) {
        return R.layout.rv_order_history_header;
    }


    private HistoryHeader(Builder builder) {
        $id = builder.$id;
        pkcode = builder.pkcode;
        fkrest = builder.fkrest;
        usercmnt = builder.usercmnt;
        tamount = builder.tamount;
        fkcoupon = builder.fkcoupon;
        discountamt = builder.discountamt;
        discoutcmnt = builder.discoutcmnt;
        fkordersts = builder.fkordersts;
        orderdate = builder.orderdate;
        dailytrno = builder.dailytrno;
        fkordertypsorc = builder.fkordertypsorc;
        paymnttype = builder.paymnttype;
        ordertype = builder.ordertype;
        fkmilechrg = builder.fkmilechrg;
        servicechrg = builder.servicechrg;
        carybagamt = builder.carybagamt;
        preorder = builder.preorder;
        prorderdate = builder.prorderdate;
        fkfloor = builder.fkfloor;
        fktable = builder.fktable;
        isdelete = builder.isdelete;
        ServingTime = builder.ServingTime;
        prordertime = builder.prordertime;
    }


    public static final class Builder {
        private String $id;
        private int pkcode;
        private int fkrest;
        private String usercmnt;
        private float tamount;
        private int fkcoupon;
        private float discountamt;
        private String discoutcmnt;
        private int fkordersts;
        private String orderdate;
        private int dailytrno;
        private int fkordertypsorc;
        private String paymnttype;
        private String ordertype;
        private int fkmilechrg;
        private int servicechrg;
        private int carybagamt;
        private boolean preorder;
        private String prorderdate;
        private int fkfloor;
        private int fktable;
        private boolean isdelete;
        private int ServingTime;
        private String prordertime;


        public String get$id() {
            return $id;
        }

        public int getPkcode() {
            return pkcode;
        }

        public int getFkrest() {
            return fkrest;
        }

        public String getUsercmnt() {
            return usercmnt;
        }

        public float getTamount() {
            return tamount;
        }

        public int getFkcoupon() {
            return fkcoupon;
        }

        public float getDiscountamt() {
            return discountamt;
        }

        public String getDiscoutcmnt() {
            return discoutcmnt;
        }

        public int getFkordersts() {
            return fkordersts;
        }

        public String getOrderdate() {
            return orderdate;
        }

        public int getDailytrno() {
            return dailytrno;
        }

        public int getFkordertypsorc() {
            return fkordertypsorc;
        }

        public String getPaymnttype() {
            return paymnttype;
        }

        public String getOrdertype() {
            return ordertype;
        }

        public int getFkmilechrg() {
            return fkmilechrg;
        }

        public int getServicechrg() {
            return servicechrg;
        }

        public int getCarybagamt() {
            return carybagamt;
        }

        public boolean isPreorder() {
            return preorder;
        }

        public String getProrderdate() {
            return prorderdate;
        }

        public int getFkfloor() {
            return fkfloor;
        }

        public int getFktable() {
            return fktable;
        }

        public boolean isIsdelete() {
            return isdelete;
        }

        public int getServingTime() {
            return ServingTime;
        }

        public String getPrordertime() {
            return prordertime;
        }

        public Builder() {
        }

        public Builder $id(String val) {
            $id = val;
            return this;
        }

        public Builder pkcode(int val) {
            pkcode = val;
            return this;
        }

        public Builder fkrest(int val) {
            fkrest = val;
            return this;
        }

        public Builder usercmnt(String val) {
            usercmnt = val;
            return this;
        }

        public Builder tamount(float val) {
            tamount = val;
            return this;
        }

        public Builder fkcoupon(int val) {
            fkcoupon = val;
            return this;
        }

        public Builder discountamt(float val) {
            discountamt = val;
            return this;
        }

        public Builder discoutcmnt(String val) {
            discoutcmnt = val;
            return this;
        }

        public Builder fkordersts(int val) {
            fkordersts = val;
            return this;
        }

        public Builder orderdate(String val) {
            orderdate = val;
            return this;
        }

        public Builder dailytrno(int val) {
            dailytrno = val;
            return this;
        }

        public Builder fkordertypsorc(int val) {
            fkordertypsorc = val;
            return this;
        }

        public Builder paymnttype(String val) {
            paymnttype = val;
            return this;
        }

        public Builder ordertype(String val) {
            ordertype = val;
            return this;
        }

        public Builder fkmilechrg(int val) {
            fkmilechrg = val;
            return this;
        }

        public Builder servicechrg(int val) {
            servicechrg = val;
            return this;
        }

        public Builder carybagamt(int val) {
            carybagamt = val;
            return this;
        }

        public Builder preorder(boolean val) {
            preorder = val;
            return this;
        }

        public Builder prorderdate(String val) {
            prorderdate = val;
            return this;
        }

        public Builder fkfloor(int val) {
            fkfloor = val;
            return this;
        }

        public Builder fktable(int val) {
            fktable = val;
            return this;
        }

        public Builder isdelete(boolean val) {
            isdelete = val;
            return this;
        }

        public Builder ServingTime(int val) {
            ServingTime = val;
            return this;
        }

        public Builder prordertime(String val) {
            prordertime = val;
            return this;
        }

        public HistoryHeader build() {
            return new HistoryHeader(this);
        }
    }
}
