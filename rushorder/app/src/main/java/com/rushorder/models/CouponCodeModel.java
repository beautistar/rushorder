package com.rushorder.models;


import java.util.List;

public class CouponCodeModel {


    /**
     * coupon_code : test123
     * cart : [{"SoldAddonItem":[],"productid":1830,"price":"2","qty":1,"name":"Chocolate Cake"},{"SoldAddonItem":[],"productid":1831,"price":"2","qty":1,"name":"Cheese Cake"}]
     */

    private String coupon_code;
    private List<CartBean> cart;

    public String getCoupon_code() {
        return coupon_code;
    }

    public void setCoupon_code(String coupon_code) {
        this.coupon_code = coupon_code;
    }

    public List<CartBean> getCart() {
        return cart;
    }

    public void setCart(List<CartBean> cart) {
        this.cart = cart;
    }

    public static class CartBean {
        /**
         * SoldAddonItem : []
         * productid : 1830
         * price : 2
         * qty : 1
         * name : Chocolate Cake
         */

        private int productid;
        private String price;
        private int qty;
        private String name;
        private List<?> childs;

        public int getProductid() {
            return productid;
        }

        public void setProductid(int productid) {
            this.productid = productid;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public int getQty() {
            return qty;
        }

        public void setQty(int qty) {
            this.qty = qty;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public List<?> getChilds() {
            return childs;
        }

        public void setChilds(List<?> childs) {
            this.childs = childs;
        }
    }
}
