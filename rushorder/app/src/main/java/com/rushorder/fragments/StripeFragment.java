package com.rushorder.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.rushorder.R;
import com.rushorder.apis.Apis;
import com.rushorder.constants.Constants;
import com.rushorder.interfaces.VolleyCallBack;
import com.rushorder.managers.PrefManager;
import com.rushorder.models.Cart;
import com.rushorder.models.OrderPostResponse;
import com.rushorder.models.SignInResponse;
import com.rushorder.utils.AlertHelper;
import com.rushorder.utils.JsonTranslator;
import com.rushorder.utils.Utils;
import com.stripe.android.Stripe;
import com.stripe.android.TokenCallback;
import com.stripe.android.model.Card;
import com.stripe.android.model.Token;
import com.stripe.android.view.CardMultilineWidget;

import org.json.JSONObject;

public class StripeFragment extends BaseFragment implements View.OnClickListener {

    private Stripe stripe;
    private String orderType;
    private float cardCharges;
    private LinearLayout llPay;
    private Float grandTotal = 0.0f;
    private CardMultilineWidget multilineWidget;
    private TextView tvDelivery, tvDiscount, tvTotal, tvGrandTotal, tvPayWithCard, tvServiceCharge;
    private AlertHelper dialog;

    public static StripeFragment getInstance() {
        return new StripeFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_stripe, container, false);
        initViews(view);
        setValues();
        setListeners();
        return view;
    }

    @Override
    protected void initViews(View view) {
        llPay = view.findViewById(R.id.llPay);
        multilineWidget = view.findViewById(R.id.card_multiline_widget);
        multilineWidget.setShouldShowPostalCode(false);
        stripe = new Stripe(getActivity(), getString(R.string.stripe_public_key));

        tvPayWithCard = view.findViewById(R.id.tvPay);
        tvTotal = view.findViewById(R.id.tvTotalPrice);
        tvDiscount = view.findViewById(R.id.tvDiscount);
        tvGrandTotal = view.findViewById(R.id.tvGrandTotal);
        tvDelivery = view.findViewById(R.id.tvDeliveryCharge);
        tvServiceCharge = view.findViewById(R.id.tvServiceCharge);

        super.initViews(view);
    }

    @Override
    protected void setValues() {
        try {
            grandTotal = getArguments().getFloat(Constants.Keys.GRAND_TOTAL);
//            cardCharges = (grandTotal / 100) * 2.5f + 0.20f;
            grandTotal = grandTotal + Float.parseFloat(getArguments().getString(Constants.Keys.PAY_WITH_AMOUNT));

            tvPayWithCard.setText("Pay");
            orderType = getArguments().getString(Constants.Keys.ORDER_TYPE);
            tvTotal.setText(getString(R.string.pound_sign) + " " + getArguments().getString(Constants.Keys.TOTAL));
            tvGrandTotal.setText(getString(R.string.pound_sign) + " " + Utils.formatToTwoDecimalPlaces(grandTotal));
            tvDiscount.setText(getString(R.string.pound_sign) + " " + getArguments().getString(Constants.Keys.DISCOUNT));
            tvServiceCharge.setText(getString(R.string.pound_sign) + " " + getArguments().getString(Constants.Keys.PAY_WITH_AMOUNT));
            tvDelivery.setText(getString(R.string.pound_sign) + " " + (orderType.equals(Constants.DELIVERY) ? getArguments().getString(Constants.Keys.DELIVERY_CHARGE) : "0.00"));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        super.setValues();
    }


    @Override
    protected void setListeners() {
        llPay.setOnClickListener(this);
        llBack.setOnClickListener(this);
        super.setListeners();
    }

    @Override
    public void onClick(View view) {


        if (view == llBack) {
            onBackPressed();
        } else {
            Card cardToSave = multilineWidget.getCard();
            if (cardToSave == null) {
                showErrorDialog("Invalid Card Data");
                return;
            } else {
                dialog = AlertHelper.showProgress(getActivity(), true);
                stripe.createToken(
                        cardToSave,
                        new TokenCallback() {
                            public void onSuccess(Token token) {
                                Cart.getInstance().setStripeToken(token.getId());
                                saveOrder();
                            }

                            public void onError(Exception error) {
                                dialog.dismissProgress();
                                showErrorDialog(error.getLocalizedMessage());

                            }
                        }
                );
            }
        }

    }

    void saveOrder() {

        SignInResponse signInResponse = PrefManager.getInstance().getObject(PrefManager.USER_INFO, SignInResponse.class);
        Apis.Order.saveOrder(getActivity(), false, signInResponse.getUserID(), new VolleyCallBack.JSONResponse() {

            @Override
            public void onResponse(JSONObject _response, String _tag) {
                try {

                    dialog.dismissProgress();

                    OrderPostResponse orderPostResponse = JsonTranslator.getInstance().parse(_response.toString(), OrderPostResponse.class);
                    if (orderPostResponse.isStatus()) {
                        showSuccessDialog(orderPostResponse.getMessage());
                    }

                    Cart.getInstance().ClearCart();
                    clearAllFragments();

                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            @Override
            public void onErrorMessage(String _errorMessage) {
                dialog.dismissProgress();
                showErrorDialog("" + _errorMessage);
            }

            @Override
            public void onErrorResponse(VolleyError _error, String _tag) {
                dialog.dismissProgress();
                showErrorDialog(getString(R.string.something_went_wrong));

            }

            @Override
            public void isConnected(boolean _connected, String _tag) {
                dialog.dismissProgress();
                showErrorDialog(getString(R.string.no_internet));
            }
        });

    }

}
