package com.rushorder.animations;

import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.RelativeLayout;


public class MarginChangeAnimation extends Animation {

    View view;
    int startLeft, startTop;
    int targetLeft, targetTop;

    public MarginChangeAnimation(View view, int startLeft, int startTop, int targetLeft, int targetTop) {

        super();
        this.view = view;
        this.startLeft = startLeft;
        this.startTop = startTop;
        this.targetLeft = targetLeft;
        this.targetTop = targetTop;

    }

    @Override
    protected void applyTransformation(float interpolatedTime, Transformation t) {

        int left = (int) (startLeft + (targetLeft - startLeft) * interpolatedTime);
        int top = (int) (startTop + (targetTop - startTop) * interpolatedTime);

        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(view.getWidth(), view.getHeight());
        params.setMargins(left, top, 0, 0);

        view.setLayoutParams(params);

    }
}



