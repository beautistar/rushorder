package com.rushorder.models;


public class RestaurantDetailResponse {

    private boolean success;
    private DataBean data;
    private RestaurantDetailsBean restaurant_details;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public RestaurantDetailsBean getRestaurant_details() {
        return restaurant_details;
    }

    public void setRestaurant_details(RestaurantDetailsBean restaurant_details) {
        this.restaurant_details = restaurant_details;
    }

    public static class DataBean {
        /**
         * message : Restaurant details
         */

        private String message;

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class RestaurantDetailsBean {
        /**
         * shop_latitude : 54.82138
         * shop_longitude : -1.45249
         * street : Church Villas
         * town : Hetton-Le-Hole
         * zipcode : DH5 9PE
         * cell_no : Call Now
         * land_line : 01915266425
         * shop_name : DEEPBLUE  HETTON
         * facebook : http://facebook.com
         * twitter : http://twitter.com
         * google : http://google.com
         * instagram : http://instagram.com
         * pinterest : http://pinterest.com
         * address : Church Villas, Front Street, Hetton-LE-HOLE DH59PE
         * open_timings : {"monday_array":{"opening_time":"4:00 PM","closing_time":"10:45 PM","day":"Monday"},"tuesday_array":{"opening_time":"4:00 PM","closing_time":"10:45 PM","day":"Tuesday"},"wednesday_array":{"opening_time":"5:00 PM","closing_time":"10:45 PM","day":"Wednesday"},"thrusday_array":{"opening_time":"4:00 PM","closing_time":"10:45 PM","day":"Thursday"},"friday_array":{"opening_time":"4:00 PM","closing_time":"11:15 PM","day":"Friday"},"saturday_array":{"opening_time":"4:00 PM","closing_time":"11:15 PM","day":"Saturday"},"sunday_array":{"opening_time":"5:00 PM","closing_time":"10:45 PM","day":"Sunday"}}
         */

        private String shop_latitude;
        private String shop_longitude;
        private String street;
        private String town;
        private String zipcode;
        private String cell_no;
        private String land_line;
        private String shop_name;
        private String facebook;
        private String twitter;
        private String google;
        private String instagram;
        private String pinterest;
        private String address;
        private OpenTimingsBean open_timings;

        public String getShop_latitude() {
            return shop_latitude;
        }

        public void setShop_latitude(String shop_latitude) {
            this.shop_latitude = shop_latitude;
        }

        public String getShop_longitude() {
            return shop_longitude;
        }

        public void setShop_longitude(String shop_longitude) {
            this.shop_longitude = shop_longitude;
        }

        public String getStreet() {
            return street;
        }

        public void setStreet(String street) {
            this.street = street;
        }

        public String getTown() {
            return town;
        }

        public void setTown(String town) {
            this.town = town;
        }

        public String getZipcode() {
            return zipcode;
        }

        public void setZipcode(String zipcode) {
            this.zipcode = zipcode;
        }

        public String getCell_no() {
            return cell_no;
        }

        public void setCell_no(String cell_no) {
            this.cell_no = cell_no;
        }

        public String getLand_line() {
            return land_line;
        }

        public void setLand_line(String land_line) {
            this.land_line = land_line;
        }

        public String getShop_name() {
            return shop_name;
        }

        public void setShop_name(String shop_name) {
            this.shop_name = shop_name;
        }

        public String getFacebook() {
            return facebook;
        }

        public void setFacebook(String facebook) {
            this.facebook = facebook;
        }

        public String getTwitter() {
            return twitter;
        }

        public void setTwitter(String twitter) {
            this.twitter = twitter;
        }

        public String getGoogle() {
            return google;
        }

        public void setGoogle(String google) {
            this.google = google;
        }

        public String getInstagram() {
            return instagram;
        }

        public void setInstagram(String instagram) {
            this.instagram = instagram;
        }

        public String getPinterest() {
            return pinterest;
        }

        public void setPinterest(String pinterest) {
            this.pinterest = pinterest;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public OpenTimingsBean getOpen_timings() {
            return open_timings;
        }

        public void setOpen_timings(OpenTimingsBean open_timings) {
            this.open_timings = open_timings;
        }

        public static class OpenTimingsBean {
            /**
             * monday_array : {"opening_time":"4:00 PM","closing_time":"10:45 PM","day":"Monday"}
             * tuesday_array : {"opening_time":"4:00 PM","closing_time":"10:45 PM","day":"Tuesday"}
             * wednesday_array : {"opening_time":"5:00 PM","closing_time":"10:45 PM","day":"Wednesday"}
             * thrusday_array : {"opening_time":"4:00 PM","closing_time":"10:45 PM","day":"Thursday"}
             * friday_array : {"opening_time":"4:00 PM","closing_time":"11:15 PM","day":"Friday"}
             * saturday_array : {"opening_time":"4:00 PM","closing_time":"11:15 PM","day":"Saturday"}
             * sunday_array : {"opening_time":"5:00 PM","closing_time":"10:45 PM","day":"Sunday"}
             */

            private MondayArrayBean monday_array;
            private TuesdayArrayBean tuesday_array;
            private WednesdayArrayBean wednesday_array;
            private ThrusdayArrayBean thrusday_array;
            private FridayArrayBean friday_array;
            private SaturdayArrayBean saturday_array;
            private SundayArrayBean sunday_array;

            public MondayArrayBean getMonday_array() {
                return monday_array;
            }

            public void setMonday_array(MondayArrayBean monday_array) {
                this.monday_array = monday_array;
            }

            public TuesdayArrayBean getTuesday_array() {
                return tuesday_array;
            }

            public void setTuesday_array(TuesdayArrayBean tuesday_array) {
                this.tuesday_array = tuesday_array;
            }

            public WednesdayArrayBean getWednesday_array() {
                return wednesday_array;
            }

            public void setWednesday_array(WednesdayArrayBean wednesday_array) {
                this.wednesday_array = wednesday_array;
            }

            public ThrusdayArrayBean getThrusday_array() {
                return thrusday_array;
            }

            public void setThrusday_array(ThrusdayArrayBean thrusday_array) {
                this.thrusday_array = thrusday_array;
            }

            public FridayArrayBean getFriday_array() {
                return friday_array;
            }

            public void setFriday_array(FridayArrayBean friday_array) {
                this.friday_array = friday_array;
            }

            public SaturdayArrayBean getSaturday_array() {
                return saturday_array;
            }

            public void setSaturday_array(SaturdayArrayBean saturday_array) {
                this.saturday_array = saturday_array;
            }

            public SundayArrayBean getSunday_array() {
                return sunday_array;
            }

            public void setSunday_array(SundayArrayBean sunday_array) {
                this.sunday_array = sunday_array;
            }

            public static class MondayArrayBean {
                /**
                 * opening_time : 4:00 PM
                 * closing_time : 10:45 PM
                 * day : Monday
                 */

                private String opening_time;
                private String closing_time;
                private String day;

                public String getOpening_time() {
                    return opening_time;
                }

                public void setOpening_time(String opening_time) {
                    this.opening_time = opening_time;
                }

                public String getClosing_time() {
                    return closing_time;
                }

                public void setClosing_time(String closing_time) {
                    this.closing_time = closing_time;
                }

                public String getDay() {
                    return day;
                }

                public void setDay(String day) {
                    this.day = day;
                }
            }

            public static class TuesdayArrayBean {
                /**
                 * opening_time : 4:00 PM
                 * closing_time : 10:45 PM
                 * day : Tuesday
                 */

                private String opening_time;
                private String closing_time;
                private String day;

                public String getOpening_time() {
                    return opening_time;
                }

                public void setOpening_time(String opening_time) {
                    this.opening_time = opening_time;
                }

                public String getClosing_time() {
                    return closing_time;
                }

                public void setClosing_time(String closing_time) {
                    this.closing_time = closing_time;
                }

                public String getDay() {
                    return day;
                }

                public void setDay(String day) {
                    this.day = day;
                }
            }

            public static class WednesdayArrayBean {
                /**
                 * opening_time : 5:00 PM
                 * closing_time : 10:45 PM
                 * day : Wednesday
                 */

                private String opening_time;
                private String closing_time;
                private String day;

                public String getOpening_time() {
                    return opening_time;
                }

                public void setOpening_time(String opening_time) {
                    this.opening_time = opening_time;
                }

                public String getClosing_time() {
                    return closing_time;
                }

                public void setClosing_time(String closing_time) {
                    this.closing_time = closing_time;
                }

                public String getDay() {
                    return day;
                }

                public void setDay(String day) {
                    this.day = day;
                }
            }

            public static class ThrusdayArrayBean {
                /**
                 * opening_time : 4:00 PM
                 * closing_time : 10:45 PM
                 * day : Thursday
                 */

                private String opening_time;
                private String closing_time;
                private String day;

                public String getOpening_time() {
                    return opening_time;
                }

                public void setOpening_time(String opening_time) {
                    this.opening_time = opening_time;
                }

                public String getClosing_time() {
                    return closing_time;
                }

                public void setClosing_time(String closing_time) {
                    this.closing_time = closing_time;
                }

                public String getDay() {
                    return day;
                }

                public void setDay(String day) {
                    this.day = day;
                }
            }

            public static class FridayArrayBean {
                /**
                 * opening_time : 4:00 PM
                 * closing_time : 11:15 PM
                 * day : Friday
                 */

                private String opening_time;
                private String closing_time;
                private String day;

                public String getOpening_time() {
                    return opening_time;
                }

                public void setOpening_time(String opening_time) {
                    this.opening_time = opening_time;
                }

                public String getClosing_time() {
                    return closing_time;
                }

                public void setClosing_time(String closing_time) {
                    this.closing_time = closing_time;
                }

                public String getDay() {
                    return day;
                }

                public void setDay(String day) {
                    this.day = day;
                }
            }

            public static class SaturdayArrayBean {
                /**
                 * opening_time : 4:00 PM
                 * closing_time : 11:15 PM
                 * day : Saturday
                 */

                private String opening_time;
                private String closing_time;
                private String day;

                public String getOpening_time() {
                    return opening_time;
                }

                public void setOpening_time(String opening_time) {
                    this.opening_time = opening_time;
                }

                public String getClosing_time() {
                    return closing_time;
                }

                public void setClosing_time(String closing_time) {
                    this.closing_time = closing_time;
                }

                public String getDay() {
                    return day;
                }

                public void setDay(String day) {
                    this.day = day;
                }
            }

            public static class SundayArrayBean {
                /**
                 * opening_time : 5:00 PM
                 * closing_time : 10:45 PM
                 * day : Sunday
                 */

                private String opening_time;
                private String closing_time;
                private String day;

                public String getOpening_time() {
                    return opening_time;
                }

                public void setOpening_time(String opening_time) {
                    this.opening_time = opening_time;
                }

                public String getClosing_time() {
                    return closing_time;
                }

                public void setClosing_time(String closing_time) {
                    this.closing_time = closing_time;
                }

                public String getDay() {
                    return day;
                }

                public void setDay(String day) {
                    this.day = day;
                }
            }
        }
    }
}
