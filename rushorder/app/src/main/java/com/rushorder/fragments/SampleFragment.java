package com.rushorder.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.rushorder.R;


public class SampleFragment extends BaseFragment {

    public static SampleFragment getInstance() {
        return new SampleFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        initViews(view);
        setListeners();
        return view;
    }

    @Override
    protected void initViews(View view) {

        super.initViews(view);
    }

    @Override
    protected void setListeners() {
        super.setListeners();
    }
}
