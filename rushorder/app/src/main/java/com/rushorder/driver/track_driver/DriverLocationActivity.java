package com.rushorder.driver.track_driver;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.rushorder.R;
import com.rushorder.constants.Constants;
import com.rushorder.driver.track_driver.presenter.DriverLocationPresenterCompl;
import com.rushorder.driver.track_driver.view.IDriverLocationView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DriverLocationActivity extends FragmentActivity implements OnMapReadyCallback, LocationListener, GoogleMap.OnMarkerClickListener, IDriverLocationView {
    private GoogleMap mMap;
    private LocationManager mLocationManager = null;
    private String provider = null;
    private ArrayList<LatLng> traceOfMe = null;
    private Polyline mPolyline = null;
    private LatLng mSourceLatLng = null;
    private LatLng mDestinationLatLng = null;
    @BindView(R.id.pbUser)
    ProgressBar pbUser;
    @BindView(R.id.tvMenuNameToolbar)
    TextView tvMenuNameToolbar;
    @BindView(R.id.llBackToolbar)
    LinearLayout llBackToolbar;
    @BindView(R.id.llNavigateToGoogleMap)
    LinearLayout llNavigateToGoogleMap;

    @BindView(R.id.tvDoneOrder)
    TextView tvDoneOrder;

    String orderId;

    DriverLocationPresenterCompl driverLocationPresenterCompl;

    String currentLat = "";
    String currentLong = "";
    String destinationLat = "";
    String destinationLong = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_home);
        getIntents();
        ButterKnife.bind(this);
        init();
        setValues();

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

    }

    private void setValues() {
        tvMenuNameToolbar.setText("order id: " + orderId);
    }

    @Override
    protected void onResume() {
        super.onResume();

        driverLocationPresenterCompl.location();
        if (isProviderAvailable() && (provider != null)) {
            locateCurrentPosition();
        }

    }

    @OnClick(R.id.llBackToolbar)
    public void onBackClick() {
        finish();
    }

    @OnClick(R.id.llNavigateToGoogleMap)
    public void onGoogleMapNavigationClick() {
        if (currentLat.length() > 0 && currentLong.length() > 0 && destinationLat.length() > 0 && destinationLong.length() > 0) {
            Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                    Uri.parse("http://maps.google.com/maps?saddr=" + currentLat + "+," + currentLong + "+&daddr=" + destinationLat + "," + destinationLong));
            intent.setPackage("com.google.android.apps.maps");
            startActivity(intent);
        }
        else
        {
            showSnackbar("Not able to get Source or Destination address. Please try again");
        }
    }


    private void getIntents() {
        orderId = getIntent().getStringExtra("orderId");
    }

    private void init() {


        driverLocationPresenterCompl = new DriverLocationPresenterCompl(this, this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mMap.setMyLocationEnabled(true);

        Log.e("isProviderAvailable()",isProviderAvailable()+"");
//        if (isProviderAvailable() && (provider != null)) {
        if (isProviderAvailable() ) {
            locateCurrentPosition();
        }
        driverLocationPresenterCompl.getOrderDestination(orderId);
    }


    private void locateCurrentPosition() {
        pbUser.setVisibility(View.VISIBLE);
        int status = getPackageManager().checkPermission(Manifest.permission.ACCESS_COARSE_LOCATION,
                getPackageName());

        if (status == PackageManager.PERMISSION_GRANTED) {


            mLocationManager.requestLocationUpdates(
                    LocationManager.NETWORK_PROVIDER, 0, 0, new LocationListener() {
                        @Override
                        public void onStatusChanged(String provider, int status, Bundle extras) {


                        }
                        @Override
                        public void onProviderEnabled(String provider) {


                        }
                        @Override
                        public void onProviderDisabled(String provider) {

                        }
                        @Override
                        public void onLocationChanged(final Location location) {
                            //

                            Log.e("location--------", location + "");
                            Log.e("location", location + "");
                            updateWithNewLocation(location);
                        }
                    });
        }
    }

    private boolean isProviderAvailable() {
        mLocationManager = (LocationManager) getSystemService(
                Context.LOCATION_SERVICE);
        Criteria criteria = new Criteria();
        criteria.setAccuracy(Criteria.ACCURACY_FINE);
        criteria.setAltitudeRequired(false);
        criteria.setBearingRequired(false);
        criteria.setCostAllowed(true);
        criteria.setPowerRequirement(Criteria.POWER_LOW);


        if (mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            provider = LocationManager.GPS_PROVIDER;
            return true;
        }

        if (provider != null) {
            return true;
        }
        return false;
    }

    private void updateWithNewLocation(Location location) {
        if (location != null && provider != null) {
            double lng = location.getLongitude();
            double lat = location.getLatitude();
            currentLong = location.getLongitude() + "";
            currentLat = location.getLatitude() + "";

            mSourceLatLng = new LatLng(lat, lng);
            //Toast.makeText(this,lat+"------"+lng , Toast.LENGTH_SHORT).show();


            CameraPosition camPosition = new CameraPosition.Builder()
                    .target(new LatLng(lat, lng)).zoom(18f).build();

            if (mMap != null)
                mMap.animateCamera(CameraUpdateFactory
                        .newCameraPosition(camPosition));

            driverLocationPresenterCompl.updateUserLocation(orderId, location);
        } else {
            Log.e("Location error", "Something went wrong");
        }

    }


    @Override
    public void onLocationChanged(Location location) {

        updateWithNewLocation(location);
    }

    @Override
    public void onProviderDisabled(String provider) {

        updateWithNewLocation(null);
    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        switch (status) {
            case LocationProvider.OUT_OF_SERVICE:
                break;
            case LocationProvider.TEMPORARILY_UNAVAILABLE:
                break;
            case LocationProvider.AVAILABLE:
                break;
        }
    }


    @Override
    public boolean onMarkerClick(Marker marker) {
        mDestinationLatLng = marker.getPosition();
        marker.getTitle();


        return false;
    }


    @Override
    public void onResultOrderDone(boolean result) {

        if (result) {
            AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
            builder1.setMessage("Order Delivered Successfully");
            builder1.setCancelable(true);

            builder1.setPositiveButton(
                    "Yes",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                            finish();
                        }
                    });


            AlertDialog alert11 = builder1.create();
            alert11.show();

        } else {
            Snackbar snackbar = Snackbar
                    .make(tvDoneOrder, "Error while saving. Please try again", Snackbar.LENGTH_LONG);
            snackbar.show();

        }


    }

    @Override
    public void setProgressbarVisibility(int visibility) {
        pbUser.setVisibility(visibility);
    }

    @Override
    public void onResultOrderLocation(boolean result, String Destination_Lat, String Destination_Long,String DeliveryAddress) {
        if (result) {
            destinationLat = Destination_Lat;
            destinationLong = Destination_Long;

            if(!destinationLong.equals("null") && !(destinationLong.equals("null")))
            {
                Double lat = Double.valueOf(destinationLat);
                Double lnd = Double.valueOf(destinationLong);
                LatLng latLng = new LatLng(lat, lnd);
                mMap.addMarker(new MarkerOptions().position(latLng)
                        .title(DeliveryAddress));
            }

        } else {
            showSnackbar("Not able to get destination address. Please try again.");
        }
    }

    public void showSnackbar(String message) {

        Snackbar.make(tvDoneOrder, message, Snackbar.LENGTH_SHORT).show();
    }

    @OnClick(R.id.tvDoneOrder)
    public void onDoneClicked() {
        driverLocationPresenterCompl.doneOrderDelivery(orderId, Constants.ORDER_STATUS_DONE);
    }
}

