package com.rushorder.driver.config;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.rushorder.activities.MainActivity;

import java.util.HashMap;



public class SessionManagerDriver {
    private SharedPreferences pref;

    private Editor editor;
    private Context _context;

    private static final String PREF_NAME = "Rushorder_Pref";
    private static final String IS_LOGIN = "IsLoggedIn";

    public static final String KEY_NAME = "first_name";
    public static final String KEY_EMAIL = "email";
    public static final String KEY_USER_ID = "user_id";


    // Constructor
    public SessionManagerDriver(Context context) {
        this._context = context;
        int PRIVATE_MODE = 0;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
        editor.apply();
    }

    public void createLoginSession(String name, String email, String user_id) {
        // Storing login value as TRUE
        editor.putBoolean(IS_LOGIN, true);

        editor.putString(KEY_NAME, name);
        editor.putString(KEY_EMAIL, email);
        editor.putString(KEY_USER_ID, user_id);
        editor.commit();
    }

    public void updateUserDetails(String name) {
        editor.putString(KEY_NAME, name);
        editor.commit();
    }

    public void checkLogin() {
        if (!this.isLoggedIn()) {
            Intent i = new Intent(_context, MainActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            _context.startActivity(i);
        }
    }


    public HashMap<String, String> getUserDetails() {
        HashMap<String, String> user = new HashMap<String, String>();
        // user name
        user.put(KEY_NAME, pref.getString(KEY_NAME, null));
        user.put(KEY_EMAIL, pref.getString(KEY_EMAIL, null));
        user.put(KEY_USER_ID, pref.getString(KEY_USER_ID, null));

        // return user
        return user;
    }

    public String getUserId() {
        return getUserDetails().get(SessionManagerDriver.KEY_USER_ID);
    }

    public String getEmail() {
        return getUserDetails().get(SessionManagerDriver.KEY_EMAIL);

    }


    public void logoutUser() {
        editor.clear();
        editor.commit();
        Intent i = new Intent(_context, MainActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        _context.startActivity(i);

    }


    // Get Login State
    public boolean isLoggedIn() {
        return pref.getBoolean(IS_LOGIN, false);
    }

}
