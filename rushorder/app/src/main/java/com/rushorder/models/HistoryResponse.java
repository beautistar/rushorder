package com.rushorder.models;

import com.rushorder.R;

import java.util.List;

import tellh.com.stickyheaderview_rv.adapter.DataBean;
import tellh.com.stickyheaderview_rv.adapter.StickyHeaderViewAdapter;

public class HistoryResponse {


    /**
     * $id : 1
     * id : 1537
     * restaurant_id : 1003
     * usercmnt : descri
     * total : 1196
     * fkcoupon : 106
     * Discount : 20
     * discoutcmnt :
     * fkordersts : 47
     * orderdate : 2018-05-07T17:50:10.443
     * dailytrno : 0
     * fkordertypsorc : 1
     * paymnttype :
     * OrderType : Delivery
     * fkmilechrg : 0
     * servicechrg : 0
     * carybagamt : 0
     * is_preorder : false
     * prorderdate : 2018-05-07T00:00:00
     * fkfloor : 0
     * fktable : 0
     * isdelete : false
     * ServingTime : 40
     * prordertime : 20:50:31.2227466
     * OrderDetails : [{"$id":"2","id":675,"fkorder":1537,"fkitem":1012,"ItemName":"item1","itemnote":"item desc","Qty":2,"Price":1196,"fkitemparnt":0,"fkcoupon":0,"IsMisc":false,"AddonItems":[{"$id":"3","id":640,"fkorderdtl":675,"Name":"item1","Qty":0,"Price":1196,"note":null,"itemIndex":0,"fkAddonItem":1004}]}]
     */

    private String $id;
    private String usercmnt;
    private int fkcoupon;
    private String discoutcmnt;
    private int fkordersts;
    private String orderdate;
    private int dailytrno;
    private int fkordertypsorc;
    private String paymnttype;
    private int fkmilechrg;
    private int servicechrg;
    private int carybagamt;
    private String prorderdate;
    private int fkfloor;
    private int fktable;
    private boolean isdelete;
    private int ServingTime;
    private String prordertime;



    private int id;
    private float total;
    private float Discount;
    private int restaurant_id;
    private boolean is_preorder;
    private String OrderType;
    private String status;
    private List<ItemBean> OrderDetails;


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String get$id() {
        return $id;
    }

    public void set$id(String $id) {
        this.$id = $id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRestaurant_id() {
        return restaurant_id;
    }

    public void setRestaurant_id(int restaurant_id) {
        this.restaurant_id = restaurant_id;
    }

    public String getUsercmnt() {
        return usercmnt;
    }

    public void setUsercmnt(String usercmnt) {
        this.usercmnt = usercmnt;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }

    public int getFkcoupon() {
        return fkcoupon;
    }

    public void setFkcoupon(int fkcoupon) {
        this.fkcoupon = fkcoupon;
    }

    public float getDiscount() {
        return Discount;
    }

    public void setDiscount(float discount) {
        this.Discount = discount;
    }

    public String getDiscoutcmnt() {
        return discoutcmnt;
    }

    public void setDiscoutcmnt(String discoutcmnt) {
        this.discoutcmnt = discoutcmnt;
    }

    public int getFkordersts() {
        return fkordersts;
    }

    public void setFkordersts(int fkordersts) {
        this.fkordersts = fkordersts;
    }

    public String getOrderdate() {
        return orderdate;
    }

    public void setOrderdate(String orderdate) {
        this.orderdate = orderdate;
    }

    public int getDailytrno() {
        return dailytrno;
    }

    public void setDailytrno(int dailytrno) {
        this.dailytrno = dailytrno;
    }

    public int getFkordertypsorc() {
        return fkordertypsorc;
    }

    public void setFkordertypsorc(int fkordertypsorc) {
        this.fkordertypsorc = fkordertypsorc;
    }

    public String getPaymnttype() {
        return paymnttype;
    }

    public void setPaymnttype(String paymnttype) {
        this.paymnttype = paymnttype;
    }

    public String getOrderType() {
        return OrderType;
    }

    public void setOrderType(String orderType) {
        this.OrderType = orderType;
    }

    public int getFkmilechrg() {
        return fkmilechrg;
    }

    public void setFkmilechrg(int fkmilechrg) {
        this.fkmilechrg = fkmilechrg;
    }

    public int getServicechrg() {
        return servicechrg;
    }

    public void setServicechrg(int servicechrg) {
        this.servicechrg = servicechrg;
    }

    public int getCarybagamt() {
        return carybagamt;
    }

    public void setCarybagamt(int carybagamt) {
        this.carybagamt = carybagamt;
    }

    public boolean isIs_preorder() {
        return is_preorder;
    }

    public void setIs_preorder(boolean is_preorder) {
        this.is_preorder = is_preorder;
    }

    public String getProrderdate() {
        return prorderdate;
    }

    public void setProrderdate(String prorderdate) {
        this.prorderdate = prorderdate;
    }

    public int getFkfloor() {
        return fkfloor;
    }

    public void setFkfloor(int fkfloor) {
        this.fkfloor = fkfloor;
    }

    public int getFktable() {
        return fktable;
    }

    public void setFktable(int fktable) {
        this.fktable = fktable;
    }

    public boolean isIsdelete() {
        return isdelete;
    }

    public void setIsdelete(boolean isdelete) {
        this.isdelete = isdelete;
    }

    public int getServingTime() {
        return ServingTime;
    }

    public void setServingTime(int ServingTime) {
        this.ServingTime = ServingTime;
    }

    public String getPrordertime() {
        return prordertime;
    }

    public void setPrordertime(String prordertime) {
        this.prordertime = prordertime;
    }

    public List<ItemBean> getOrderDetails() {
        return OrderDetails;
    }

    public void setOrderDetails(List<ItemBean> Item) {
        this.OrderDetails = Item;
    }

    public static class ItemBean extends DataBean {
        /**
         * $id : 2
         * id : 675
         * fkorder : 1537
         * fkitem : 1012
         * ItemName : item1
         * itemnote : item desc
         * Qty : 2
         * Price : 1196
         * fkitemparnt : 0
         * fkcoupon : 0
         * IsMisc : false
         * AddonItems : [{"$id":"3","id":640,"fkorderdtl":675,"Name":"item1","Qty":0,"Price":1196,"note":null,"itemIndex":0,"fkAddonItem":1004}]
         */

        private int ItemId;
        private int Qty;
        private float Price;
        private String ItemName;
        private List<SoldAddonItemBean> AddonItems;



        private String $id;
        private int fkorder;
        private int fkitem;
        private String itemnote;
        private int fkitemparnt;
        private int fkcoupon;
        private boolean IsMisc;
        private transient String item_parent_index;
        private transient String item_index;
        private transient int cartItemSize;





        public int getCartItemSize() {
            return cartItemSize;
        }

        public void setCartItemSize(int cartItemSize) {
            this.cartItemSize = cartItemSize;
        }

        public String getItem_parent_index() {
            return item_parent_index;
        }

        public void setItem_parent_index(String item_parent_index) {
            this.item_parent_index = item_parent_index;
        }

        public String getItem_index() {
            return item_index;
        }

        public void setItem_index(String item_index) {
            this.item_index = item_index;
        }

        public String get$id() {
            return $id;
        }

        public void set$id(String $id) {
            this.$id = $id;
        }

        public int getItemId() {
            return ItemId;
        }

        public void setItemId(int itemId) {
            this.ItemId = itemId;
        }

        public int getFkorder() {
            return fkorder;
        }

        public void setFkorder(int fkorder) {
            this.fkorder = fkorder;
        }

        public int getFkitem() {
            return fkitem;
        }

        public void setFkitem(int fkitem) {
            this.fkitem = fkitem;
        }

        public String getItemName() {
            return ItemName;
        }

        public void setItemName(String itemName) {
            this.ItemName = itemName;
        }

        public String getItemnote() {
            return itemnote;
        }

        public void setItemnote(String itemnote) {
            this.itemnote = itemnote;
        }

        public int getQty() {
            return Qty;
        }

        public void setQty(int qty) {
            this.Qty = qty;
        }

        public float getPrice() {
            return Price;
        }

        public void setPrice(float price) {
            this.Price = price;
        }

        public int getFkitemparnt() {
            return fkitemparnt;
        }

        public void setFkitemparnt(int fkitemparnt) {
            this.fkitemparnt = fkitemparnt;
        }

        public int getFkcoupon() {
            return fkcoupon;
        }

        public void setFkcoupon(int fkcoupon) {
            this.fkcoupon = fkcoupon;
        }

        public boolean isIsMisc() {
            return IsMisc;
        }

        public void setIsMisc(boolean IsMisc) {
            this.IsMisc = IsMisc;
        }

        public List<SoldAddonItemBean> getAddonItems() {
            return AddonItems;
        }

        public void setAddonItems(List<SoldAddonItemBean> SoldAddonItem) {
            this.AddonItems = SoldAddonItem;
        }

        @Override
        public int getItemLayoutId(StickyHeaderViewAdapter stickyHeaderViewAdapter) {
            return R.layout.rv_order_history_child_row;

        }

        public static class SoldAddonItemBean {
            /**
             * $id : 3
             * id : 640
             * fkorderdtl : 675
             * Name : item1
             * Qty : 0
             * Price : 1196
             * note : null
             * itemIndex : 0
             * fkAddonItem : 1004
             */


            private int AddonItemID;
            private int Qty;
            private String Name;
            private float Price;


            private String $id;
            private String note;
            private int itemIndex;
            private int fkorderdtl;
            private int fkAddonItem;

            public String get$id() {
                return $id;
            }

            public void set$id(String $id) {
                this.$id = $id;
            }

            public int getAddonItemID() {
                return AddonItemID;
            }

            public void setAddonItemID(int addonItemID) {
                this.AddonItemID = addonItemID;
            }

            public int getFkorderdtl() {
                return fkorderdtl;
            }

            public void setFkorderdtl(int fkorderdtl) {
                this.fkorderdtl = fkorderdtl;
            }

            public String getName() {
                return Name;
            }

            public void setName(String name) {
                this.Name = name;
            }

            public int getQty() {
                return Qty;
            }

            public void setQty(int qty) {
                this.Qty = qty;
            }

            public float getPrice() {
                return Price;
            }

            public void setPrice(float price) {
                this.Price = price;
            }

            public String getNote() {
                return note;
            }

            public void setNote(String note) {
                this.note = note;
            }

            public int getItemIndex() {
                return itemIndex;
            }

            public void setItemIndex(int itemIndex) {
                this.itemIndex = itemIndex;
            }

            public int getFkAddonItem() {
                return fkAddonItem;
            }

            public void setFkAddonItem(int fkAddonItem) {
                this.fkAddonItem = fkAddonItem;
            }
        }
    }
}
