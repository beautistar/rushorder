package com.rushorder.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.rushorder.R;
import com.rushorder.apis.Apis;
import com.rushorder.interfaces.VolleyCallBack;
import com.rushorder.utils.FieldsValidator;

import org.json.JSONObject;

public class ForgotPasswordFragment extends BaseFragment implements View.OnClickListener {

    private String userEmail;
    private EditText etUserEmail;
    private TextView tvSignUpLink;
    private LinearLayout llRecoverPassword;

    public static ForgotPasswordFragment getInstance() {
        return new ForgotPasswordFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_forgot_password, container, false);
        initViews(view);
        return view;
    }


    @Override
    protected void initViews(View view) {

        etUserEmail = view.findViewById(R.id.etEmail);
        tvSignUpLink = view.findViewById(R.id.tvSignUpLink);
        llRecoverPassword = view.findViewById(R.id.llRecoverPassword);
        setListeners();
        super.initViews(view);
    }


    @Override
    protected void setListeners() {
        tvSignUpLink.setOnClickListener(this);
        llRecoverPassword.setOnClickListener(this);

        super.setListeners();
    }


    @Override
    public void onClick(View v) {
        if (v == llRecoverPassword) {
            getUserInfo();
        } else if (v == tvSignUpLink) {
            startSignUpFragment();
        }

    }

    private void getUserInfo() {
        userEmail = etUserEmail.getText().toString();

        if (!userEmail.isEmpty()) {
            if (validateFields()) {
                recoverPassword();
            }
        } else {
            showErrorDialog(getString(R.string.please_enter_email));
        }
    }

    private boolean validateFields() {
        if (!FieldsValidator.getInstance().validateEmail(userEmail)) {
            showErrorDialog(getString(R.string.please_enter_valid_email));
            return false;
        }
        return true;
    }


    private void recoverPassword() {

        Apis.User.recoverPassword(getActivity(), true, userEmail, "", new VolleyCallBack.JSONResponse() {

            @Override
            public void onResponse(JSONObject _response, String _tag) {
                try {
                        showSuccessDialog(_response.getString("message"));
//                        PrefManager.getInstance().saveUserInfo(_response.toString());

                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            @Override
            public void onErrorMessage(String _errorMessage) {
                showErrorDialog("" + _errorMessage);
            }

            @Override
            public void onErrorResponse(VolleyError _error, String _tag) {
                showErrorDialog(getString(R.string.something_went_wrong));

            }

            @Override
            public void isConnected(boolean _connected, String _tag) {
                showErrorDialog(getString(R.string.no_internet));
            }
        });
    }


    void startSignUpFragment() {
        switchFragment(SignUpFragment.getInstance());
    }

}