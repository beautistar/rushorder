package com.rushorder.models;

import com.rushorder.R;

import java.util.ArrayList;

import tellh.com.stickyheaderview_rv.adapter.StickyHeaderViewAdapter;

/**
 * Created by Aleem on 2/24/2018.
 */

public class RestaurantDetailsResponse {

    private boolean success;
    private DataBean data;
    private RestaurantDetailsBean restaurant_details;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public RestaurantDetailsBean getRestaurant_details() {
        return restaurant_details;
    }

    public void setRestaurant_details(RestaurantDetailsBean restaurant_details) {
        this.restaurant_details = restaurant_details;
    }

    public static class DataBean {

        private String message;
        private ArrayList<MenusBean> menus;

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public ArrayList<MenusBean> getMenus() {
            return menus;
        }

        public void setMenus(ArrayList<MenusBean> menus) {
            this.menus = menus;
        }

        public static class MenusBean {

            private CategoriesBean categories;
            private ArrayList<SubCategoriesBean> sub_categories;

            public CategoriesBean getCategories() {
                return categories;
            }

            public void setCategories(CategoriesBean categories) {
                this.categories = categories;
            }

            public ArrayList<SubCategoriesBean> getSub_categories() {
                return sub_categories;
            }

            public void setSub_categories(ArrayList<SubCategoriesBean> sub_categories) {
                this.sub_categories = sub_categories;
            }

            public static class CategoriesBean {
                /**
                 * id : 20
                 * name : Fish and Chips
                 * restaurant_id : 2
                 * descp :
                 * days : Monday,Tuesday,Wednesday,Thursday,Friday,Saturday,Sunday
                 * allow_coupon : yes
                 * status : show
                 * delivery : yes
                 * collection : yes
                 * sort_order : 1
                 * user_side_orders : 0
                 * pic : https://jayeat.com/public/site-content/menu-icons/seafood.png
                 * free_item_cat : no
                 * spend_limit : 0
                 */

                private String id;
                private String name;
                private String restaurant_id;
                private String description;
                private String days;
                private String allow_coupon;
                private String status;
                private String delivery;
                private String collection;
                private String sort_order;
                private String user_side_orders;
                private String pic;
                private String free_item_cat;
                private String spend_limit;

                public String getId() {
                    return id;
                }

                public void setId(String id) {
                    this.id = id;
                }

                public String getName() {
                    return name;
                }

                public void setName(String name) {
                    this.name = name;
                }

                public String getRestaurant_id() {
                    return restaurant_id;
                }

                public void setRestaurant_id(String restaurant_id) {
                    this.restaurant_id = restaurant_id;
                }

                public String getDescription() {
                    return description;
                }

                public void setDescription(String description) {
                    this.description = description;
                }

                public String getDays() {
                    return days;
                }

                public void setDays(String days) {
                    this.days = days;
                }

                public String getAllow_coupon() {
                    return allow_coupon;
                }

                public void setAllow_coupon(String allow_coupon) {
                    this.allow_coupon = allow_coupon;
                }

                public String getStatus() {
                    return status;
                }

                public void setStatus(String status) {
                    this.status = status;
                }

                public String getDelivery() {
                    return delivery;
                }

                public void setDelivery(String delivery) {
                    this.delivery = delivery;
                }

                public String getCollection() {
                    return collection;
                }

                public void setCollection(String collection) {
                    this.collection = collection;
                }

                public String getSort_order() {
                    return sort_order;
                }

                public void setSort_order(String sort_order) {
                    this.sort_order = sort_order;
                }

                public String getUser_side_orders() {
                    return user_side_orders;
                }

                public void setUser_side_orders(String user_side_orders) {
                    this.user_side_orders = user_side_orders;
                }

                public String getPic() {
                    return pic;
                }

                public void setPic(String pic) {
                    this.pic = pic;
                }

                public String getFree_item_cat() {
                    return free_item_cat;
                }

                public void setFree_item_cat(String free_item_cat) {
                    this.free_item_cat = free_item_cat;
                }

                public String getSpend_limit() {
                    return spend_limit;
                }

                public void setSpend_limit(String spend_limit) {
                    this.spend_limit = spend_limit;
                }
            }

            public static class SubCategoriesBean {

                private String id;
                private String name;
                private String restaurant_id;
                private String description;
                private String parent_id;
                private String pic;
                private String days;
                private String allow_coupon;
                private String status;
                private String delivery;
                private String collection;
                private String sort_order;
                private String free_item_cat;
                private String spend_limit;


                private ArrayList<ItemsBean> items;

                public String getId() {
                    return id;
                }

                public void setId(String id) {
                    this.id = id;
                }

                public String getName() {
                    return name;
                }

                public void setName(String name) {
                    this.name = name;
                }

                public String getRestaurant_id() {
                    return restaurant_id;
                }

                public void setRestaurant_id(String restaurant_id) {
                    this.restaurant_id = restaurant_id;
                }

                public String getDescription() {
                    return description;
                }

                public void setDescription(String description) {
                    this.description = description;
                }

                public String getParent_id() {
                    return parent_id;
                }

                public void setParent_id(String parent_id) {
                    this.parent_id = parent_id;
                }

                public String getPic() {
                    return pic;
                }

                public void setPic(String pic) {
                    this.pic = pic;
                }

                public String getDays() {
                    return days;
                }

                public void setDays(String days) {
                    this.days = days;
                }

                public String getAllow_coupon() {
                    return allow_coupon;
                }

                public void setAllow_coupon(String allow_coupon) {
                    this.allow_coupon = allow_coupon;
                }

                public String getStatus() {
                    return status;
                }

                public void setStatus(String status) {
                    this.status = status;
                }

                public String getDelivery() {
                    return delivery;
                }

                public void setDelivery(String delivery) {
                    this.delivery = delivery;
                }

                public String getCollection() {
                    return collection;
                }

                public void setCollection(String collection) {
                    this.collection = collection;
                }

                public String getSort_order() {
                    return sort_order;
                }

                public void setSort_order(String sort_order) {
                    this.sort_order = sort_order;
                }

                public String getFree_item_cat() {
                    return free_item_cat;
                }

                public void setFree_item_cat(String free_item_cat) {
                    this.free_item_cat = free_item_cat;
                }

                public String getSpend_limit() {
                    return spend_limit;
                }

                public void setSpend_limit(String spend_limit) {
                    this.spend_limit = spend_limit;
                }

                public ArrayList<ItemsBean> getItems() {
                    return items;
                }

                public void setItems(ArrayList<ItemsBean> items) {
                    this.items = items;
                }

                public static class ItemsBean extends tellh.com.stickyheaderview_rv.adapter.DataBean {
                    /**
                     * id : 2149
                     * name : Cod & Chips
                     * price : 6
                     * descp :
                     * days :
                     * pic :
                     * allow_coupon : yes
                     * sort_order : 0
                     * parent_id : 0
                     * kitchen_lines :
                     * sub_cat_id : 51
                     * sub_cat_name : R : Fish and Chips
                     */

                    private String pkcode;
                    private String fkrestaurant;
                    private String name;
                    private String descp;
                    private String price;
                    private String parent_id;
                    private String fkaddonitem;
                    private String fkkitchnline;
                    private transient int parentIndex = -1;
                    private ArrayList<ItemsBean.AddonCategories> addon_categories;

                    public ArrayList<AddonCategories> getAddon_categories() {
                        return addon_categories;
                    }

                    public void setAddon_categories(ArrayList<AddonCategories> addon_categories) {
                        this.addon_categories = addon_categories;
                    }

                    public int getParentIndex() {
                        return parentIndex;
                    }

                    public void setParentIndex(int parentIndex) {
                        this.parentIndex = parentIndex;
                    }

                    public String getId() {
                        return pkcode;
                    }

                    public void setId(String pkcode) {
                        this.pkcode = pkcode;
                    }

                    public String getPkcode() {
                        return pkcode;
                    }

                    public void setPkcode(String pkcode) {
                        this.pkcode = pkcode;
                    }

                    public String getFkrestaurant() {
                        return fkrestaurant;
                    }

                    public void setFkrestaurant(String fkrestaurant) {
                        this.fkrestaurant = fkrestaurant;
                    }

                    public String getName() {
                        return name;
                    }

                    public void setName(String name) {
                        this.name = name;
                    }

                    public String getDescp() {
                        return descp;
                    }

                    public void setDescp(String descp) {
                        this.descp = descp;
                    }

                    public String getPrice() {
                        return price;
                    }

                    public void setPrice(String price) {
                        this.price = price;
                    }

                    public String getParent_id() {
                        return parent_id;
                    }

                    public void setParent_id(String parent_id) {
                        this.parent_id = parent_id;
                    }

                    public String getFkaddonitem() {
                        return fkaddonitem;
                    }

                    public void setFkaddonitem(String fkaddonitem) {
                        this.fkaddonitem = fkaddonitem;
                    }

                    public String getFkkitchnline() {
                        return fkkitchnline;
                    }

                    public void setFkkitchnline(String fkkitchnline) {
                        this.fkkitchnline = fkkitchnline;
                    }

                    @Override
                    public int getItemLayoutId(StickyHeaderViewAdapter adapter) {
                        return R.layout.rv_menu_item_row;
                    }

                    public static class AddonCategories {

                        /**
                         * $id : 7
                         * addon_item : [{"$id":"8","id":1000,"restaurant_id":1000,"name":"item 1","price":10,"parent_id":1000,"next_move_id":2,"sort_order":1},{"$id":"9","id":1001,"restaurant_id":1000,"name":"item 2","price":25,"parent_id":1000,"next_move_id":3,"sort_order":2}]
                         * id : 1000
                         * fkrestaurant : 1000
                         * name : Addon Category 1
                         * descp : jkh
                         * parent_id : 1012
                         * max_sellection : 1
                         */

                        private String $id;
                        private int pkcode;
                        private int fkrestaurant;
                        private String name;
                        private String descp;
                        private int parent_id;
                        private int max_sellection;
                        private ArrayList<AddonItemBean> addon_item;

                        public String get$id() {
                            return $id;
                        }

                        public void set$id(String $id) {
                            this.$id = $id;
                        }

                        public int getPkcode() {
                            return pkcode;
                        }

                        public void setPkcode(int pkcode) {
                            this.pkcode = pkcode;
                        }

                        public int getFkrestaurant() {
                            return fkrestaurant;
                        }

                        public void setFkrestaurant(int fkrestaurant) {
                            this.fkrestaurant = fkrestaurant;
                        }

                        public String getName() {
                            return name;
                        }

                        public void setName(String name) {
                            this.name = name;
                        }

                        public String getDescp() {
                            return descp;
                        }

                        public void setDescp(String descp) {
                            this.descp = descp;
                        }

                        public int getParent_id() {
                            return parent_id;
                        }

                        public void setParent_id(int parent_id) {
                            this.parent_id = parent_id;
                        }

                        public int getMax_sellection() {
                            return max_sellection;
                        }

                        public void setMax_sellection(int max_sellection) {
                            this.max_sellection = max_sellection;
                        }

                        public ArrayList<AddonItemBean> getAddon_item() {
                            return addon_item;
                        }

                        public void setAddon_item(ArrayList<AddonItemBean> addon_item) {
                            this.addon_item = addon_item;
                        }

                        public static class AddonItemBean {
                            /**
                             * $id : 8
                             * id : 1000
                             * restaurant_id : 1000
                             * name : item 1
                             * price : 10
                             * parent_id : 1000
                             * next_move_id : 2
                             * sort_order : 1
                             */

                            private String $id;
                            private int pkcode;
                            private int id;
                            private String name;
                            private float price;
                            private int parent_id;
                            private String addon_cat_id;
                            private int sort_order;
                            private int next_move_id;
                            private int restaurant_id;

                            public int getId() {
                                return id;
                            }

                            public void setId(int id) {
                                this.id = id;
                            }

                            public String getAddon_cat_id() {
                                return addon_cat_id;
                            }

                            public void setAddon_cat_id(String addon_cat_id) {
                                this.addon_cat_id = addon_cat_id;
                            }

                            public String get$id() {
                                return $id;
                            }

                            public void set$id(String $id) {
                                this.$id = $id;
                            }

                            public int getPkcode() {
                                return pkcode;
                            }

                            public void setPkcode(int pkcode) {
                                this.pkcode = pkcode;
                            }

                            public int getRestaurant_id() {
                                return restaurant_id;
                            }

                            public void setRestaurant_id(int restaurant_id) {
                                this.restaurant_id = restaurant_id;
                            }

                            public String getName() {
                                return name;
                            }

                            public void setName(String name) {
                                this.name = name;
                            }

                            public float getPrice() {
                                return price;
                            }

                            public void setPrice(float price) {
                                this.price = price;
                            }

                            public int getParent_id() {
                                return parent_id;
                            }

                            public void setParent_id(int parent_id) {
                                this.parent_id = parent_id;
                            }

                            public int getNext_move_id() {
                                return next_move_id;
                            }

                            public void setNext_move_id(int next_move_id) {
                                this.next_move_id = next_move_id;
                            }

                            public int getSort_order() {
                                return sort_order;
                            }

                            public void setSort_order(int sort_order) {
                                this.sort_order = sort_order;
                            }
                        }
                    }

                }
            }
        }
    }

    public static class RestaurantDetailsBean {
        /**
         * shop_latitude : 54.8213812
         * shop_longitude : -1.4524850000000242
         * street : null
         * town : null
         * zipcode : DH59PE
         * cell_no : Call Now
         * land_line : 1915266425
         * shop_name : DeepBlue
         * delivery_time : 45
         * collection_time : 30
         * facebook : http://facebook.com
         * twitter : http://twitter.com
         * google : http://google.com
         * instagram : http://instagram.com
         * pinterest : http://pinterest.com
         * address : Church villas Front Street,DH59PE
         * open_timings : {"monday_array":{"opening_time":"4:00 PM","closing_time":"10:45 PM","day":"Monday"},"tuesday_array":{"opening_time":"4:00 PM","closing_time":"10:45 PM","day":"Tuesday"},"wednesday_array":{"opening_time":"5:00 PM","closing_time":"10:45 PM","day":"Wednesday"},"thrusday_array":{"opening_time":"4:00 PM","closing_time":"10:45 PM","day":"Thursday"},"friday_array":{"opening_time":"4:00 PM","closing_time":"11:15 PM","day":"Friday"},"saturday_array":{"opening_time":"4:00 PM","closing_time":"11:15 PM","day":"Saturday"},"sunday_array":{"opening_time":"5:00 PM","closing_time":"10:45 PM","day":"Sunday"}}
         * perorder_pref : active
         * perorder_time : ["Saturday 16:00","Saturday 16:15","Saturday 16:30","Saturday 16:45","Saturday 17:00","Saturday 17:15","Saturday 17:30","Saturday 17:45","Saturday 18:00","Saturday 18:15","Saturday 18:30","Saturday 18:45","Saturday 19:00","Saturday 19:15","Saturday 19:30","Saturday 19:45","Saturday 20:00","Saturday 20:15","Saturday 20:30","Saturday 20:45","Saturday 21:00","Saturday 21:15","Saturday 21:30","Saturday 21:45","Saturday 22:00","Saturday 22:15","Saturday 22:30","Saturday 22:45","Saturday 23:00","Saturday 23:15"]
         * orders_status : Off timing
         * minimum_spending : 7
         */

        private String shop_latitude;
        private String shop_longitude;
        private Object street;
        private Object town;
        private String zipcode;
        private String cell_no;
        private String land_line;
        private String shop_name;
        private String delivery_time;
        private String collection_time;
        private String facebook;
        private String twitter;
        private String google;
        private String instagram;
        private String pinterest;
        private String address;
        private String perorder_pref;
        private String orders_status;
        private float minimum_spending;
        private ArrayList<OpenTimingsBean> open_timings;


        public String getShop_latitude() {
            return shop_latitude;
        }

        public void setShop_latitude(String shop_latitude) {
            this.shop_latitude = shop_latitude;
        }

        public String getShop_longitude() {
            return shop_longitude;
        }

        public void setShop_longitude(String shop_longitude) {
            this.shop_longitude = shop_longitude;
        }

        public Object getStreet() {
            return street;
        }

        public void setStreet(Object street) {
            this.street = street;
        }

        public Object getTown() {
            return town;
        }

        public void setTown(Object town) {
            this.town = town;
        }

        public String getZipcode() {
            return zipcode;
        }

        public void setZipcode(String zipcode) {
            this.zipcode = zipcode;
        }

        public String getCell_no() {
            return cell_no;
        }

        public void setCell_no(String cell_no) {
            this.cell_no = cell_no;
        }

        public String getLand_line() {
            return land_line;
        }

        public void setLand_line(String land_line) {
            this.land_line = land_line;
        }

        public String getShop_name() {
            return shop_name;
        }

        public void setShop_name(String shop_name) {
            this.shop_name = shop_name;
        }

        public String getDelivery_time() {
            return delivery_time;
        }

        public void setDelivery_time(String delivery_time) {
            this.delivery_time = delivery_time;
        }

        public String getCollection_time() {
            return collection_time;
        }

        public void setCollection_time(String collection_time) {
            this.collection_time = collection_time;
        }

        public String getFacebook() {
            return facebook;
        }

        public void setFacebook(String facebook) {
            this.facebook = facebook;
        }

        public String getTwitter() {
            return twitter;
        }

        public void setTwitter(String twitter) {
            this.twitter = twitter;
        }

        public String getGoogle() {
            return google;
        }

        public void setGoogle(String google) {
            this.google = google;
        }

        public String getInstagram() {
            return instagram;
        }

        public void setInstagram(String instagram) {
            this.instagram = instagram;
        }

        public String getPinterest() {
            return pinterest;
        }

        public void setPinterest(String pinterest) {
            this.pinterest = pinterest;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public ArrayList<OpenTimingsBean> getOpen_timings() {
            return open_timings;
        }

        public void setOpen_timings(ArrayList<OpenTimingsBean> open_timings) {
            this.open_timings = open_timings;
        }

        public String getPerorder_pref() {
            return perorder_pref;
        }

        public void setPerorder_pref(String perorder_pref) {
            this.perorder_pref = perorder_pref;
        }

        public String getOrders_status() {
            return orders_status;
        }

        public void setOrders_status(String orders_status) {
            this.orders_status = orders_status;
        }

        public float getMinimum_spending() {
            return minimum_spending;
        }

        public void setMinimum_spending(float minimum_spending) {
            this.minimum_spending = minimum_spending;
        }


        public static class OpenTimingsBean {

            private String opening_time;
            private String closing_time;
            private String day;

            public String getOpening_time() {
                return opening_time;
            }

            public void setOpening_time(String opening_time) {
                this.opening_time = opening_time;
            }

            public String getClosing_time() {
                return closing_time;
            }

            public void setClosing_time(String closing_time) {
                this.closing_time = closing_time;
            }

            public String getDay() {
                return day;
            }

            public void setDay(String day) {
                this.day = day;
            }
        }
    }
}
