package com.rushorder.models;


import com.rushorder.R;
import com.rushorder.models.OrderHistoryResponse.OrdersBean.ResturentDetailBean;

import tellh.com.stickyheaderview_rv.adapter.DataBean;
import tellh.com.stickyheaderview_rv.adapter.StickyHeaderViewAdapter;

/**
 * Created by Aleem on 12/16/2017.
 */

public class OrderHistoryHeader extends DataBean {

    private String id;
    private String restaurant_id;
    private String user_id;
    private String total;
    private String description;
    private String coupon;
    private String coupon_value;
    private String coupon_type;
    private String coupon_applies_to;
    private String coupon_categories;
    private String discount_amount;
    private String discount_desc;
    private String status;
    private String date;
    private String order_count;
    private String type;
    private String is_mobile_order;
    private String order_stats;
    private String payment_mode;
    private String payment_status;
    private String delivery_type;
    private String delivery_charges;
    private String card_surcharge;
    private String addby;
    private String addon;
    private String carry_bag;
    private String order_format_status;
    private String customer_phone;
    private String customer_name;
    private String merchant_responce;
    private String is_preorder;
    private String preorder_date;
    private String preorder_time;
    private String print_delay;
    private String eatinn_table_id;
    private String eatinn_table_name;
    private String eatinn_floorname;
    private String eatinn_payment_status;
    private String bill_persons;
    private String order_description;
    private String api_key;
    private boolean show_feedback_button;
    private ResturentDetailBean resturent_detail;
    private int childSize;

    public OrderHistoryHeader(IBinder iBinder) {

        this.id = iBinder.id;
        this.restaurant_id = iBinder.restaurant_id;
        this.user_id = iBinder.user_id;
        this.total = iBinder.total;
        this.description = iBinder.description;
        this.coupon = iBinder.coupon;
        this.coupon_value = iBinder.coupon_value;
        this.coupon_type = iBinder.coupon_type;
        this.coupon_applies_to = iBinder.coupon_applies_to;
        this.coupon_categories = iBinder.coupon_categories;
        this.discount_amount = iBinder.discount_amount;
        this.discount_desc = iBinder.discount_desc;
        this.status = iBinder.status;
        this.date = iBinder.date;
        this.order_count = iBinder.order_count;
        this.type = iBinder.type;
        this.is_mobile_order = iBinder.is_mobile_order;
        this.order_stats = iBinder.order_stats;
        this.payment_mode = iBinder.payment_mode;
        this.payment_status = iBinder.payment_status;
        this.delivery_type = iBinder.delivery_type;
        this.delivery_charges = iBinder.delivery_charges;
        this.card_surcharge = iBinder.card_surcharge;
        this.addby = iBinder.addby;
        this.addon = iBinder.addon;
        this.carry_bag = iBinder.carry_bag;
        this.order_format_status = iBinder.order_format_status;
        this.customer_phone = iBinder.customer_phone;
        this.customer_name = iBinder.customer_name;
        this.merchant_responce = iBinder.merchant_responce;
        this.is_preorder = iBinder.is_preorder;
        this.preorder_date = iBinder.preorder_date;
        this.preorder_time = iBinder.preorder_time;
        this.print_delay = iBinder.print_delay;
        this.eatinn_table_id = iBinder.eatinn_table_id;
        this.eatinn_table_name = iBinder.eatinn_table_name;
        this.eatinn_floorname = iBinder.eatinn_floorname;
        this.eatinn_payment_status = iBinder.eatinn_payment_status;
        this.bill_persons = iBinder.bill_persons;
        this.order_description = iBinder.order_description;
        this.api_key = iBinder.api_key;
        this.show_feedback_button = iBinder.show_feedback_button;
        this.resturent_detail = iBinder.resturent_detail;
        this.childSize = iBinder.childSize;

    }


    public String getId() {
        return id;
    }

    public String getRestaurant_id() {
        return restaurant_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public String getTotal() {
        return total;
    }

    public String getDescription() {
        return description;
    }

    public String getCoupon() {
        return coupon;
    }

    public String getCoupon_value() {
        return coupon_value;
    }

    public String getCoupon_type() {
        return coupon_type;
    }

    public String getCoupon_applies_to() {
        return coupon_applies_to;
    }

    public String getCoupon_categories() {
        return coupon_categories;
    }

    public String getDiscount_amount() {
        return discount_amount;
    }

    public String getDiscount_desc() {
        return discount_desc;
    }

    public String getStatus() {
        return status;
    }

    public String getDate() {
        return date;
    }

    public String getOrder_count() {
        return order_count;
    }

    public String getType() {
        return type;
    }

    public String getIs_mobile_order() {
        return is_mobile_order;
    }

    public String getOrder_stats() {
        return order_stats;
    }

    public String getPayment_mode() {
        return payment_mode;
    }

    public String getPayment_status() {
        return payment_status;
    }

    public String getDelivery_type() {
        return delivery_type;
    }

    public String getDelivery_charges() {
        return delivery_charges;
    }

    public String getCard_surcharge() {
        return card_surcharge;
    }

    public String getAddby() {
        return addby;
    }

    public String getAddon() {
        return addon;
    }

    public String getCarry_bag() {
        return carry_bag;
    }

    public String getOrder_format_status() {
        return order_format_status;
    }

    public String getCustomer_phone() {
        return customer_phone;
    }

    public String getCustomer_name() {
        return customer_name;
    }

    public String getMerchant_responce() {
        return merchant_responce;
    }

    public String getIs_preorder() {
        return is_preorder;
    }

    public String getPreorder_date() {
        return preorder_date;
    }

    public String getPreorder_time() {
        return preorder_time;
    }

    public String getPrint_delay() {
        return print_delay;
    }

    public String getEatinn_table_id() {
        return eatinn_table_id;
    }

    public String getEatinn_table_name() {
        return eatinn_table_name;
    }

    public String getEatinn_floorname() {
        return eatinn_floorname;
    }

    public String getEatinn_payment_status() {
        return eatinn_payment_status;
    }

    public String getBill_persons() {
        return bill_persons;
    }

    public String getOrder_description() {
        return order_description;
    }

    public String getApi_key() {
        return api_key;
    }

    public boolean isShow_feedback_button() {
        return show_feedback_button;
    }

    public ResturentDetailBean getResturent_detail() {
        return resturent_detail;
    }

    public int getChildSize() {
        return childSize;
    }

    @Override
    public int getItemLayoutId(StickyHeaderViewAdapter stickyHeaderViewAdapter) {
        return R.layout.rv_order_history_header;
    }

    public static class IBinder {


        private String id;
        private String restaurant_id;
        private String user_id;
        private String total;
        private String description;
        private String coupon;
        private String coupon_value;
        private String coupon_type;
        private String coupon_applies_to;
        private String coupon_categories;
        private String discount_amount;
        private String discount_desc;
        private String status;
        private String date;
        private String order_count;
        private String type;
        private String is_mobile_order;
        private String order_stats;
        private String payment_mode;
        private String payment_status;
        private String delivery_type;
        private String delivery_charges;
        private String card_surcharge;
        private String addby;
        private String addon;
        private String carry_bag;
        private String order_format_status;
        private String customer_phone;
        private String customer_name;
        private String merchant_responce;
        private String is_preorder;
        private String preorder_date;
        private String preorder_time;
        private String print_delay;
        private String eatinn_table_id;
        private String eatinn_table_name;
        private String eatinn_floorname;
        private String eatinn_payment_status;
        private String bill_persons;
        private String order_description;
        private String api_key;
        private boolean show_feedback_button;
        private ResturentDetailBean resturent_detail;
        private int childSize;


        public IBinder setId(String id) {
            this.id = id;
            return this;
        }


        public IBinder setRestaurant_id(String restaurant_id) {
            this.restaurant_id = restaurant_id;
            return this;
        }

        public IBinder setUser_id(String user_id) {
            this.user_id = user_id;
            return this;
        }

        public IBinder setTotal(String total) {
            this.total = total;
            return this;
        }

        public IBinder setDescription(String description) {
            this.description = description;
            return this;
        }

        public IBinder setCoupon(String coupon) {
            this.coupon = coupon;
            return this;
        }

        public IBinder setCoupon_value(String coupon_value) {
            this.coupon_value = coupon_value;
            return this;
        }

        public IBinder setCoupon_type(String coupon_type) {
            this.coupon_type = coupon_type;
            return this;
        }

        public IBinder setCoupon_applies_to(String coupon_applies_to) {
            this.coupon_applies_to = coupon_applies_to;
            return this;
        }

        public IBinder setCoupon_categories(String coupon_categories) {
            this.coupon_categories = coupon_categories;
            return this;
        }

        public IBinder setDiscount_amount(String discount_amount) {
            this.discount_amount = discount_amount;
            return this;
        }

        public IBinder setDiscount_desc(String discount_desc) {
            this.discount_desc = discount_desc;
            return this;
        }

        public IBinder setStatus(String status) {
            this.status = status;
            return this;
        }

        public IBinder setDate(String date) {
            this.date = date;
            return this;
        }

        public IBinder setOrder_count(String order_count) {
            this.order_count = order_count;
            return this;
        }

        public IBinder setType(String type) {
            this.type = type;
            return this;
        }

        public IBinder setIs_mobile_order(String is_mobile_order) {
            this.is_mobile_order = is_mobile_order;
            return this;
        }

        public IBinder setOrder_stats(String order_stats) {
            this.order_stats = order_stats;
            return this;
        }

        public IBinder setPayment_mode(String payment_mode) {
            this.payment_mode = payment_mode;
            return this;
        }

        public IBinder setPayment_status(String payment_status) {
            this.payment_status = payment_status;
            return this;
        }

        public IBinder setDelivery_type(String delivery_type) {
            this.delivery_type = delivery_type;
            return this;
        }

        public IBinder setDelivery_charges(String delivery_charges) {
            this.delivery_charges = delivery_charges;
            return this;
        }

        public IBinder setCard_surcharge(String card_surcharge) {
            this.card_surcharge = card_surcharge;
            return this;
        }

        public IBinder setAddby(String addby) {
            this.addby = addby;
            return this;
        }

        public IBinder setAddon(String addon) {
            this.addon = addon;
            return this;
        }

        public IBinder setCarry_bag(String carry_bag) {
            this.carry_bag = carry_bag;
            return this;
        }

        public IBinder setOrder_format_status(String order_format_status) {
            this.order_format_status = order_format_status;
            return this;
        }

        public IBinder setCustomer_phone(String customer_phone) {
            this.customer_phone = customer_phone;
            return this;
        }

        public IBinder setCustomer_name(String customer_name) {
            this.customer_name = customer_name;
            return this;
        }

        public IBinder setMerchant_responce(String merchant_responce) {
            this.merchant_responce = merchant_responce;
            return this;
        }

        public IBinder setIs_preorder(String is_preorder) {
            this.is_preorder = is_preorder;
            return this;
        }

        public IBinder setPreorder_date(String preorder_date) {
            this.preorder_date = preorder_date;
            return this;
        }

        public IBinder setPreorder_time(String preorder_time) {
            this.preorder_time = preorder_time;
            return this;
        }

        public IBinder setPrint_delay(String print_delay) {
            this.print_delay = print_delay;
            return this;
        }

        public IBinder setEatinn_table_id(String eatinn_table_id) {
            this.eatinn_table_id = eatinn_table_id;
            return this;
        }

        public IBinder setEatinn_table_name(String eatinn_table_name) {
            this.eatinn_table_name = eatinn_table_name;
            return this;
        }

        public IBinder setEatinn_floorname(String eatinn_floorname) {
            this.eatinn_floorname = eatinn_floorname;
            return this;
        }

        public IBinder setEatinn_payment_status(String eatinn_payment_status) {
            this.eatinn_payment_status = eatinn_payment_status;
            return this;
        }

        public IBinder setBill_persons(String bill_persons) {
            this.bill_persons = bill_persons;
            return this;
        }

        public IBinder setOrder_description(String order_description) {
            this.order_description = order_description;
            return this;
        }

        public IBinder setShow_feedback_button(boolean show_feedback_button) {
            this.show_feedback_button = show_feedback_button;
            return this;
        }

        public IBinder setApi_key(String api_key) {
            this.api_key = api_key;
            return this;
        }

        public IBinder setResturent_detail(ResturentDetailBean resturent_detail) {
            this.resturent_detail = resturent_detail;
            return this;
        }

        public IBinder setChildSize(int childSize) {
            this.childSize = childSize;
            return this;
        }

        public OrderHistoryHeader build() {
            return new OrderHistoryHeader(this);
        }
    }

}
