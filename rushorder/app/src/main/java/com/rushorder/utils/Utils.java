package com.rushorder.utils;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;


import com.rushorder.R;
import com.rushorder.app.AppController;
import com.rushorder.constants.Constants;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;


/**
 * Created by Aleem on 10/22/2017.
 */

public class Utils {

    private static DecimalFormat decimalFormat = new DecimalFormat(Constants.DECIMAL_PLACE);

    public static String getLevel(int points) {
        String level = "";
        if (points < 25)
            level = Constants.BLUE_LEVEL;
        else if (points < 50)
            level = Constants.BRONZE;
        else if (points < 100)
            level = Constants.SILVER;
        else level = Constants.PLATINUM;
        return level;
    }

    public static class SnackBarMessage {

        public static void showSnackBarShort(View parentLayout, String message) {
            Snackbar snackbar = Snackbar.make(parentLayout, message, Snackbar.LENGTH_SHORT);
            setSnackBarCustomView(snackbar.getView());
            snackbar.show();
        }

        public static void showSnackBarLong(View parentLayout, String message) {

            Snackbar snackbar = Snackbar.make(parentLayout, message, Snackbar.LENGTH_LONG);
            setSnackBarCustomView(snackbar.getView());
            snackbar.show();
        }

        public static void showSnackBarInfinite(View parentLayout, String message, String actionText, View.OnClickListener listener) {

            Snackbar snackbar = Snackbar.make(parentLayout, message, Snackbar.LENGTH_INDEFINITE);
            setSnackBarCustomView(snackbar.getView());
            snackbar.setAction(actionText, listener);
            snackbar.show();
        }


        private static void setSnackBarCustomView(View snackBarView) {
            snackBarView.setBackgroundColor(ContextCompat.getColor(AppController.getAppContext(), R.color.colorPrimary));
            TextView tvSnackBar = snackBarView.findViewById(android.support.design.R.id.snackbar_text);
            TextView tvSnackBarAction = snackBarView.findViewById(android.support.design.R.id.snackbar_action);
            tvSnackBar.setTextColor(ContextCompat.getColor(AppController.getAppContext(), R.color.white));
            tvSnackBarAction.setTextColor(ContextCompat.getColor(AppController.getAppContext(), R.color.white));
        }

    }


    public static class Encryption {

        public static String MD5(String md5) {
            try {
                java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
                byte[] array = md.digest(md5.getBytes());
                StringBuffer sb = new StringBuffer();
                for (int i = 0; i < array.length; ++i) {
                    sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1, 3));
                }
                return sb.toString();
            } catch (java.security.NoSuchAlgorithmException e) {
            }
            return null;
        }

        public static void getSha1(Context context, String logTag) {
            try {
                PackageInfo info = context.getPackageManager().getPackageInfo(context.getPackageName(), PackageManager.GET_SIGNATURES);
                for (Signature signature : info.signatures) {
                    MessageDigest md = MessageDigest.getInstance("SHA");
                    md.update(signature.toByteArray());
                    Log.d(logTag, Base64.encodeToString(md.digest(), Base64.DEFAULT));
                }
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();

            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }
        }
    }


    public static void hideStatusBar(Activity activity) {
        activity.requestWindowFeature(Window.FEATURE_NO_TITLE);
        activity.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }


    public static void hideSoftKeyboard(Activity activity, EditText pEditText) {
        try {
            InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(pEditText.getWindowToken(), 0);
        } catch (Exception e) {
        }
    }

    public static void showSoftKeyboard(Activity activity, EditText pEditText) {
        try {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(pEditText, InputMethodManager.SHOW_IMPLICIT);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    public static String formatToTwoDecimalPlaces(float value) {

        return decimalFormat.format(value);
    }


    public static String formatToTwoDecimalPlaces(String value) {
        String formattedValue = "";
        try {
            formattedValue = formatToTwoDecimalPlaces(Float.parseFloat(value));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return formattedValue;
    }


}
