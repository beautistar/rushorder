package com.rushorder.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Aleem on 12/5/2017.
 */

public class SignInResponse {


    /**
     * access_token : H_h9GSb3RS54MOgDysaXHnhSjOBiSvSDuEqdH5Uik_XwBTRn-upUepPDKnEHUlyf5i9C_S5L4uPiWLRUMvcU7AAwU5RJzYwLYpFLGKiJDGNKme3x0kp-K4jwU3zt4Vt6GintgUJLfF9d0IxFeOV_FloriinHws5TP71MT7p1TvPckgR6FrHT4W2e9tMReU0pID2GMDLmVaryweDbVBlQI8HYvpRS30wchmMcKynXJbMLkrxSgfBAZxY4CxHCS3Ls2e4rJcqGGnnRkRuPGlouFV8t40y8xeB4JtVGdHb5jdceo901-UY7AgVJlWj7r2V4r1exKdQTr6-NNarJ-JTazz63jBrKwA80IdqAiCYO-PjfKJLmrafTUPtXvt_pootDFUL4NG17k0e18P18PXRrfsqxVRtY_LF1P18Xs2E1p6gFMyDix6KSjkpt6OfnERb9o21Fm78fmJYi18msWlm3zZqb9DbSyzXFFyzoDSNXuoqeFUUmSnfI2OeUe5D90dCU
     * token_type : bearer
     * expires_in : 239999999
     * userName : omer@gmail.com
     * ID : c482af4d-6743-45ab-80cc-7393c9ec23e5
     * .issued : Sat, 12 May 2018 18:11:11 GMT
     * .expires : Fri, 19 Dec 2025 12:51:11 GMT
     * userID : c482af4d-6743-45ab-80cc-7393c9ec23e5
     */

    private String access_token;
    private String token_type;
    private int expires_in;
    private String userName;
    private String ID;
    @SerializedName(".issued")
    private String _$Issued330;
    @SerializedName(".expires")
    private String _$Expires191;
    private String userID;

    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }

    public String getToken_type() {
        return token_type;
    }

    public void setToken_type(String token_type) {
        this.token_type = token_type;
    }

    public int getExpires_in() {
        return expires_in;
    }

    public void setExpires_in(int expires_in) {
        this.expires_in = expires_in;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String get_$Issued330() {
        return _$Issued330;
    }

    public void set_$Issued330(String _$Issued330) {
        this._$Issued330 = _$Issued330;
    }

    public String get_$Expires191() {
        return _$Expires191;
    }

    public void set_$Expires191(String _$Expires191) {
        this._$Expires191 = _$Expires191;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }
}
