package com.rushorder.fragments.dialogs;


import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;


import com.rushorder.R;
import com.rushorder.constants.Constants;
import com.rushorder.fragments.PreOrderFragment;
import com.rushorder.models.PreOrderDataResponse;
import com.rushorder.utils.JsonTranslator;

import java.util.ArrayList;

public class PreOrderTimingDialog extends DialogFragment implements View.OnClickListener {

    private TextView tvHeader;
    private String selectedTime = "";
    private RecyclerView rvTiming;
    private ArrayList<PreOrderDataResponse.TimimgBean> timingList;
    private LinearLayout llCancel, llConfirm;
    private PreOrderDataResponse preOrderDataResponse;


    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.WRAP_CONTENT;
            dialog.getWindow().setLayout(width, height);
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_menu_sub_items, container, false);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        setBasicViews(view);
        return view;
    }

    private void setBasicViews(View view) {

        try {

            String preOrderResponse = getArguments().getString(Constants.Keys.RESPONSE_KEY);
            preOrderDataResponse = JsonTranslator.getInstance().parse(preOrderResponse, PreOrderDataResponse.class);

            rvTiming = view.findViewById(R.id.rvItems);
            llCancel = view.findViewById(R.id.llCancel);
            tvHeader = view.findViewById(R.id.tvHeader);
            llConfirm = view.findViewById(R.id.llConfirm);
            tvHeader.setText("Select Time");


        } catch (Exception e) {
            e.printStackTrace();
        }

        setClickListeners();
        setAdapter();
    }

    private void setClickListeners() {
        llCancel.setOnClickListener(this);
        llConfirm.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        if (v == llConfirm) {
            if (!selectedTime.isEmpty()) {
                ((PreOrderFragment) getParentFragment()).timeSelected(selectedTime);
                dismiss();
            }
        } else {

            dismiss();
        }
    }


    void setAdapter() {
        timingList = preOrderDataResponse.getTimimg();
//        LinearLayoutManager manager = new LinearLayoutManager(getActivity()) {
//            @Override
//            public boolean canScrollVertically() {
//                return false;
//            }
//        };


        rvTiming.setLayoutManager(new LinearLayoutManager(getActivity()));

        rvTiming.setAdapter(new TimingsAdapter(timingList));
    }


    class TimingsAdapter extends RecyclerView.Adapter<TimingsAdapter.Holder> {

        private ArrayList<PreOrderDataResponse.TimimgBean> timingList;
        private String lastCheckId = "-1";

        public TimingsAdapter(ArrayList<PreOrderDataResponse.TimimgBean> timingList) {
            this.timingList = timingList;
        }

        @Override
        public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_preorder_timing, parent, false);
            return new Holder(view);
        }

        @Override
        public void onBindViewHolder(Holder holder, int position) {
            PreOrderDataResponse.TimimgBean model = timingList.get(position);

            holder.tvName.setText("" + model.getTime());

            if (model.get$id().equals(lastCheckId)) {
                holder.rbSelect.setChecked(true);
            } else {
                holder.rbSelect.setChecked(false);
            }

        }

        @Override
        public int getItemCount() {
            return timingList.size();
        }

        class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {
            private TextView tvName;
            private RadioButton rbSelect;

            public Holder(View itemView) {
                super(itemView);
                tvName = itemView.findViewById(R.id.tvName);
                rbSelect = itemView.findViewById(R.id.rbSelect);
                itemView.setOnClickListener(this);
            }

            @Override
            public void onClick(View v) {
                PreOrderDataResponse.TimimgBean model = timingList.get(getLayoutPosition());
                lastCheckId = model.get$id();
                selectedTime = model.getTime();
                notifyDataSetChanged();
            }

        }
    }

    public static class TimingModel {
        private String time;
        private int id;

        public TimingModel(String time, int id) {
            this.time = time;
            this.id = id;
        }

        public String getTime() {
            return time;
        }

        public void setTime(String time) {
            this.time = time;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }
    }
}




