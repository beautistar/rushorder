package com.rushorder.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.rushorder.R;


public class ChatFragment extends BaseFragment implements View.OnClickListener {

    private ImageView ivMenu;

    public static ChatFragment getInstance() {
        return new ChatFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_chat, container, false);
        initViews(view);
        setListeners();

        return view;
    }

    @Override
    protected void initViews(View view) {
        ivMenu = view.findViewById(R.id.ivMenu);
        super.initViews(view);
    }

    @Override
    protected void setListeners() {
        ivMenu.setOnClickListener(this);
        super.setListeners();
    }

    @Override
    public void onClick(View v) {
        getParentActivity().openDrawer();
    }
}
