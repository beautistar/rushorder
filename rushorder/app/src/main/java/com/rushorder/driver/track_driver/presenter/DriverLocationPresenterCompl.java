package com.rushorder.driver.track_driver.presenter;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.rushorder.R;
import com.rushorder.apis.Apis;
import com.rushorder.app.AppController;
import com.rushorder.constants.Constants;
import com.rushorder.driver.config.SessionManagerDriver;
import com.rushorder.driver.track_driver.view.IDriverLocationView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class DriverLocationPresenterCompl implements IDriverLocationPresenter {
    private IDriverLocationView iDriverLocationView;
    private Context context;
    SessionManagerDriver sessionManagerDriver;
    public DriverLocationPresenterCompl(IDriverLocationView iDriverLocationView, Context context) {
        this.iDriverLocationView = iDriverLocationView;
        this.context = context;
        sessionManagerDriver = new SessionManagerDriver(context);
    }

    @Override
    public void doneOrderDelivery(String orderId, String status) {
        iDriverLocationView.setProgressbarVisibility(View.VISIBLE);

        String url = Apis.ChangeDriverStatus +"userId="+ sessionManagerDriver.getUserId()+ "&orderId=" + orderId+"&status="+ Constants.DRIVER_RIDING_DELIVERED;

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                response -> {
                    try {
                        Log.e("responseFacebook", response);
                        JSONObject jsonObject = new JSONObject(response);
                        String success = jsonObject.getString("success");

                        if (success.equals("1")) {
                            setOrderStatusToDeliverd(orderId);

                        } else {
                            iDriverLocationView.onResultOrderDone(false);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    iDriverLocationView.setProgressbarVisibility(View.GONE);
                },
                error -> {
                    iDriverLocationView.setProgressbarVisibility(View.GONE);
                }) {

        };
        AppController.setmRequestQueue(stringRequest);
    }

    private void setOrderStatusToDeliverd(String orderId) {
        iDriverLocationView.setProgressbarVisibility(View.VISIBLE);

        String url = Apis.changeOrderStatus + "orderId=" + orderId+"&orderstatusId="+ Constants.ORDER_STATUS_DELIVERED;

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                response -> {
                    try {
                        Log.e("responseFacebook", response);
                        JSONObject jsonObject = new JSONObject(response);
                        String success = jsonObject.getString("success");

                        if (success.equals("1")) {
                            iDriverLocationView.onResultOrderDone(true);


                        } else {
                            iDriverLocationView.onResultOrderDone(false);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    iDriverLocationView.setProgressbarVisibility(View.GONE);
                },
                error -> {
                    iDriverLocationView.setProgressbarVisibility(View.GONE);
                }) {

        };
        AppController.setmRequestQueue(stringRequest);
    }


    @Override
    public void location() {
        LocationManager lm = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        boolean gps_enabled = false;
        boolean network_enabled = false;

        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception ex) {
        }

        try {
            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch (Exception ex) {
        }
        Log.e("gps_enabled",gps_enabled+"   "+ network_enabled);
        if (!gps_enabled ) {
            // notify user

            Log.e("gps_enabled Inside",gps_enabled+"   "+ network_enabled);
            AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
            builder1.setMessage(context.getString(R.string.gps_network_not_enabled));
            builder1.setCancelable(true);

            builder1.setPositiveButton(
                    context.getString(R.string.open_location_settings),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            context.startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));

                            dialog.cancel();
                        }
                    });

            builder1.setNegativeButton(
                    context.getString(R.string.Cancel),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });

            AlertDialog alert11 = builder1.create();
            alert11.show();


//            new AlertDialog.Builder(context)
//                    .setMessage(R.string.gps_network_not_enabled)
//
//                    .setPositiveButton(R.string.open_location_settings, new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface paramDialogInterface, int paramInt) {
//                            context.startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
//                            paramDialogInterface.dismiss();
//                        }
//                    })
//                    .setNegativeButton(R.string.Cancel, null)
//                    .show();
        }

    }

    @Override
    public void getOrderDestination(String orderId) {
        iDriverLocationView.setProgressbarVisibility(View.VISIBLE);

        String url = Apis.GetOrderLocation +"orderId="+ orderId;

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                response -> {
                    try {
                        Log.e("responseFacebook", response);
                        JSONObject jsonObject = new JSONObject(response);
                        String success = jsonObject.getString("success");

                        if (success.equals("1")) {
                            String data = jsonObject.getString("data");
                            JSONObject dataObj = new JSONObject(data);
                            String Destination_Lat = dataObj.getString("Destination_Lat");
                            String Destination_Long = dataObj.getString("Destination_Long");
                            String DeliveryAddress = dataObj.getString("DeliveryAddress");
                            iDriverLocationView.onResultOrderLocation(true,Destination_Lat,Destination_Long,DeliveryAddress);


                        } else {
                            iDriverLocationView.onResultOrderLocation(false,"","","");
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    iDriverLocationView.setProgressbarVisibility(View.GONE);
                },
                error -> {
                    iDriverLocationView.onResultOrderLocation(false,"","","");
                    iDriverLocationView.setProgressbarVisibility(View.GONE);
                }) {

        };
        AppController.setmRequestQueue(stringRequest);
    }

    @Override
    public void updateUserLocation(String orderId, Location location) {
        String url = Apis.UpdateDriverLocation + "userId=" + sessionManagerDriver.getUserId() + "&orderId=" + orderId + "&latitude=" + location.getLatitude() + "&longitude=" + location.getLongitude();
        Log.e("UpdateOrderLocation", url);
        iDriverLocationView.setProgressbarVisibility(View.VISIBLE);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("response", response);
                         iDriverLocationView.setProgressbarVisibility(View.GONE);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        iDriverLocationView.setProgressbarVisibility(View.GONE);
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
//                params.put("orderId", orderId);
//                params.put("lat", location.getLatitude()+"");
//                params.put("lng", location.getLongitude()+"");
//                Log.e("userId",userId);
                return params;
            }

        };

        AppController.setmRequestQueue(stringRequest);


    }

}
