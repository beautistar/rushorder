package com.rushorder.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.rushorder.R;
import com.rushorder.apis.Apis;
import com.rushorder.cache.CacheData;
import com.rushorder.constants.Constants;
import com.rushorder.fragments.adapters.RestaurantDetailsPagerAdapter;
import com.rushorder.interfaces.VolleyCallBack;
import com.rushorder.models.Cart;
import com.rushorder.models.RestaurantInfoResponse;
import com.rushorder.models.RestaurantsResponse;
import com.rushorder.utils.JsonTranslator;
import com.iarcuschin.simpleratingbar.SimpleRatingBar;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

/**
 * Created by Aleem on 1/27/2018.
 */

public class RestaurantDetailsFragment extends BaseFragment implements View.OnClickListener {

    private String apiKey;
    private TabLayout tbDetail;
    private String restaurantId;
    private ViewPager vpDetails;
    private RelativeLayout rlCart;
    private ImageView ivRestaurant;
    private SimpleRatingBar restaurantRating;
    private RestaurantDetailsPagerAdapter adapter;
    private TextView tvRestaurantName, tvRestaurantDesc, tvDelivery, tvMinSpent, tvDistance, tvStatus, tvDiscount, tvReviewCount;

    public static RestaurantDetailsFragment getInstance() {
        return new RestaurantDetailsFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_restaurant_details, container, false);
        initViews(view);
        getRestaurantDetails();
        setListeners();
        return view;
    }

    @Override
    protected void initViews(View view) {


        rlCart = view.findViewById(R.id.rlCart);
        tvMinSpent = view.findViewById(R.id.tvMin);
        tbDetail = view.findViewById(R.id.tblDetail);
        vpDetails = view.findViewById(R.id.vpDetail);
        tvDelivery = view.findViewById(R.id.tvDelivery);
        tvDiscount = view.findViewById(R.id.tvDiscount);
        ivRestaurant= view.findViewById(R.id.ivRestaurant);
        restaurantRating = view.findViewById(R.id.rbRating);
        tvReviewCount = view.findViewById(R.id.tvReviewsCount);
        tvStatus = view.findViewById(R.id.tvRestaurantsStatus);
        tvDistance = view.findViewById(R.id.tvRestaurantDistance);
        tvRestaurantName = view.findViewById(R.id.tvRestaurantName);
        tvRestaurantDesc = view.findViewById(R.id.tvRestaurantDesc);
        vpDetails.setOffscreenPageLimit(1);
        tbDetail.setupWithViewPager(vpDetails);

        setValues();

        super.initViews(view);
    }

    @Override
    protected void setValues() {

//        RestaurantDetailsResponse.RestaurantDetailsBean model = CacheData.
//                RestaurantsDetails.
//                getInstance().
//                getRestaurantDetailsResponse().
//                getRestaurant_details();

        int itemPosition = getArguments().getInt(Constants.Keys.ITEM_POSITION_KEY);

        RestaurantsResponse model = CacheData.Restaurants.getInstance().getRestaurantsList().get(itemPosition);

        restaurantId = model.getId();
        restaurantRating.setRating(model.getRaiting());
        tvRestaurantName.setText("" + model.getName());
        tvRestaurantDesc.setText("" + model.getAddress());
//        tvReviewCount.setText("" + model.getReviewCount());
        tvDelivery.setText("" + model.getCookingTime() + " Minutes");
        tvMinSpent.setText(getString(R.string.pound_sign) + " " + model.getMinDelPrice());
//        tvDistance.setText("" + Utils.formatToTwoDecimalPlaces("" + model.getDistance()));

        Picasso.with(getActivity())
                .load(model.getImg())
                .placeholder(R.drawable.restaurant_logo)
                .error(R.drawable.restaurant_logo)
                .into(ivRestaurant);
        super.setValues();
    }


    void updateCartValue() {
        ((TextView) rlCart.findViewById(R.id.tvCartCount)).setText("" + Cart.getInstance().getOrderDetail().size());
    }

    void setAdapter() {
        adapter = new RestaurantDetailsPagerAdapter(getChildFragmentManager(), getArguments().getInt(Constants.Keys.ITEM_POSITION_KEY));
        vpDetails.setAdapter(adapter);
    }

    @Override
    protected void setListeners() {
        llBack.setOnClickListener(this);
        rlCart.setOnClickListener(this);
        restaurantRating.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });
        super.setListeners();
    }

    @Override
    public void onClick(View v) {
        if (v == rlCart) {
            switchFragment(CartDetailFragment.getInstance());
        } else {

            onBackPressed();
        }
    }


    void getRestaurantDetails() {

        Apis.Restaurant.getRestaurantDetails(getActivity(), true, restaurantId, new VolleyCallBack.JSONResponse() {
            @Override
            public void onResponse(JSONObject _response, String _tag) {
                try {
                    RestaurantInfoResponse detailResponse = JsonTranslator.getInstance().parse(_response.toString(), RestaurantInfoResponse.class);
                    CacheData.RestaurantsDetails.getInstance().setRestaurantDetailsResponse(detailResponse);
                    setValues();
                    setAdapter();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorMessage(String _errorMessage) {
                showErrorDialog(_errorMessage);
            }

            @Override
            public void onErrorResponse(VolleyError _error, String _tag) {
                showErrorDialog(getString(R.string.something_went_wrong));
            }

            @Override
            public void isConnected(boolean _connected, String _tag) {
                showErrorDialog(getString(R.string.no_internet));
            }
        });

    }

    @Override
    public void onResume() {
        updateCartValue();
        super.onResume();
    }
}
