package com.rushorder.utils;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;

import com.rushorder.R;
import com.rushorder.animations.OptAnimationLoader;


public abstract class BaseAlert extends Dialog implements View.OnClickListener {

    private View contentView;
    private AnimationSet alertShowAnim, alertHideAnim;
    private int layoutResId;
    private int animShowResId, animHideResId;

    public BaseAlert(Context context, int layoutResId) {
        this(context, layoutResId, R.style.Alert);
    }

    public BaseAlert(Context context, int layoutResId, int style) {
        this(context, layoutResId, style,  R.anim.alert_show, R.anim.alert_hide);
    }

    public BaseAlert(Context context, int layoutResId, int style, int animShowResId, int animHideResId) {

        super(context, style);
        setCancelable(true);

        this.layoutResId = layoutResId;
        alertShowAnim = (AnimationSet) OptAnimationLoader.loadAnimation(getContext(),animShowResId);
        alertHideAnim = (AnimationSet) OptAnimationLoader.loadAnimation(getContext(), animHideResId);

        alertHideAnim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                contentView.setVisibility(View.GONE);
                contentView.post(new Runnable() {
                    @Override
                    public void run() {
                        BaseAlert.super.dismiss();
                    }
                });
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(layoutResId);
        contentView = getWindow().getDecorView().findViewById(android.R.id.content);
    }

    @Override
    protected void onStart() {
        contentView.startAnimation(alertShowAnim);
    }

    @Override
    public void cancel() {
        dismissWithAnim();
    }

    @Override
    public void dismiss() {
        dismissWithAnim();
    }

    private void dismissWithAnim() {
        contentView.startAnimation(alertHideAnim);
    }

    @Override
    public void onClick(View v) {

    }
}
