package com.rushorder.fragments;


import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.awesomedialog.blennersilva.awesomedialoglibrary.AwesomeErrorDialog;
import com.awesomedialog.blennersilva.awesomedialoglibrary.interfaces.Closure;
import com.muddzdev.styleabletoastlibrary.StyleableToast;
import com.rushorder.R;
import com.rushorder.apis.Apis;
import com.rushorder.cache.CacheData;
import com.rushorder.constants.Constants;
import com.rushorder.fragments.ViewBinder.OrderHistoryHeaderViewBinder;
import com.rushorder.fragments.ViewBinder.OrderHistoryItemViewBinder;
import com.rushorder.interfaces.OrderHistoryCallBack;
import com.rushorder.interfaces.VolleyCallBack;
import com.rushorder.managers.PrefManager;
import com.rushorder.models.Cart;
import com.rushorder.models.HistoryHeader;
import com.rushorder.models.HistoryResponse;
import com.rushorder.models.SignInResponse;
import com.rushorder.utils.AlertUtils;
import com.rushorder.utils.JsonTranslator;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import tellh.com.stickyheaderview_rv.adapter.DataBean;
import tellh.com.stickyheaderview_rv.adapter.StickyHeaderViewAdapter;

/**
 * Created by Aleem on 1/23/2018.
 */

public class OrderHistoryFragment extends BaseFragment implements OrderHistoryCallBack, View.OnClickListener {

    private ImageView ivMenu;
    private TextView tvTrackOrder;
    private SignInResponse loginDetails;
    private RecyclerView rvStickyHeader;
    private StickyHeaderViewAdapter adapter;
    private ArrayList<DataBean> orderHistoryList;

    public static OrderHistoryFragment getInstance() {
        return new OrderHistoryFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_order_history, container, false);

        initViews(view);
        return view;
    }

    @Override
    protected void initViews(View view) {
        ivMenu = view.findViewById(R.id.ivMenu);
        tvTrackOrder = view.findViewById(R.id.tvTrackOrder);
        rvStickyHeader = view.findViewById(R.id.rvStickyHeader);
        rvStickyHeader.setLayoutManager(new LinearLayoutManager(getActivity()));
        if (PrefManager.getInstance().getBoolean(PrefManager.IS_LOGIN)) {
            getOrderHistory();
        } else {
            showErrorAlert("Please login to view order history");
        }
        setListeners();
        super.initViews(view);
    }


    private void setAdapter() {

        adapter = new StickyHeaderViewAdapter(orderHistoryList)
                .RegisterItemType(new OrderHistoryItemViewBinder(this))
                .RegisterItemType(new OrderHistoryHeaderViewBinder(this));
        rvStickyHeader.setAdapter(adapter);
    }

    @Override
    protected void setListeners() {
        ivMenu.setOnClickListener(this);
        tvTrackOrder.setOnClickListener(this);
        super.setListeners();
    }


    private void getOrderHistory() {

        SignInResponse signInModel = PrefManager.getInstance().getObject(PrefManager.USER_INFO, SignInResponse.class);

        Apis.Order.getOrderHistory(getActivity(), true, "", signInModel.getUserID(), new VolleyCallBack.ArrayResponse() {
            @Override
            public void onResponse(JSONArray _response, String _tag) {
                try {
                    Log.e("  ",_response.toString());
                    List<HistoryResponse> list = JsonTranslator.getInstance().parseHistoryJsonToList(_response.toString());
                    orderHistoryList = CacheData.Order.getInstance().getOrderHistoryList(list);

                    setAdapter();

                } catch (Exception ex) {
                    ex.printStackTrace();
                    StyleableToast.makeText(Objects.requireNonNull(getActivity()), "No record found", Toast.LENGTH_LONG, R.style.ErrorToastStyle).show();
                }

            }

            @Override
            public void onErrorMessage(String _errorMessage) {
                showErrorDialog(_errorMessage);
            }

            @Override
            public void onErrorResponse(VolleyError _error, String _tag) {
                showErrorDialog(getString(R.string.something_went_wrong));
            }

            @Override
            public void isConnected(boolean _connected, String _tag) {
                showErrorDialog(getString(R.string.no_internet));
            }
        });


    }


    @Override
    public void headerCallback(HistoryHeader headerModel, int Position, int type) {
        if (type == Constants.FEED_BACK) {
            startFeedBackFragment("" + headerModel.getPkcode(), "" + headerModel.getFkrest());
        } else if (type == Constants.REPEAT) {
            startTrackOrderFragment(headerModel.getPkcode(), "" + headerModel.getFkrest());

//            Cart.getInstance().AddToCart(headerModel, CacheData.Order.getInstance().getOrderChilds().get(headerModel.getId()));
//            checkOrder(headerModel);
        }
    }

    @Override
    public void childCallback(HistoryResponse.ItemBean childModel, int Position, int type) {
        int parentIndex = Constants.INVALID_INDEX;
        if (type == Constants.FEED_BACK) {

            parentIndex = Integer.parseInt(childModel.getItem_parent_index());
            HistoryHeader itemHeader = (HistoryHeader) CacheData.Order.getInstance().getOrderHistoryList().get(parentIndex);

            startFeedBackFragment("" + itemHeader.getPkcode(), "" + itemHeader.getFkrest());

        } else if (type == Constants.REPEAT) {
            try {

                parentIndex = Integer.parseInt(childModel.getItem_parent_index());
                HistoryHeader itemHeader = (HistoryHeader) CacheData.Order.getInstance().getOrderHistoryList().get(parentIndex);

                checkOrder(itemHeader);

            } catch (Exception ex) {
                ex.printStackTrace();
            }

        }
    }

    private void checkOrder(HistoryHeader itemHeader) {

        String apiKey = "" + itemHeader.getPkcode();

        if (Cart.getInstance().isCartFull()) {
            if (apiKey.equals(PrefManager.getInstance().getString(PrefManager.RES_ID))) {
                repeatOrder(itemHeader);
            } else {
                showWarningDialog(getString(R.string.cart_will_be_cleared), apiKey, itemHeader);
            }
        } else {
            repeatOrder(itemHeader);
        }
    }

    private void repeatOrder(HistoryHeader itemHeader) {

        PrefManager.getInstance().saveSelectedRestaurantApiKey("" + itemHeader.getPkcode());
        Cart.getInstance().AddToCart(itemHeader, CacheData.Order.getInstance().getOrderChilds().get("" + itemHeader.getPkcode()));
//        openCartDetails(itemHeader.getResturent_detail().getMinimum_spending());
        openCartDetails(20);
    }


    private void showWarningDialog(String message, final String apiKey, final HistoryHeader itemHeader) {

        AlertUtils.showAlert(getActivity(), "Warning !", message, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                Cart.getInstance().ClearCart();
                repeatOrder(itemHeader);
            }
        });

    }


    private void openCartDetails(float minimum_spending) {

        Bundle bundle = new Bundle();
        bundle.putFloat(Constants.Keys.MINIMUM_SPENDING, minimum_spending);
        switchFragment(CartDetailFragment.getInstance(), bundle);
    }

    private void startFeedBackFragment(String orderId, String resId) {
        Bundle bundle = new Bundle();
        bundle.putString(Constants.Keys.ORDER_ID, orderId);
        bundle.putString(Constants.Keys.RESTAURANT_KEY, resId);
        switchFragment(FeedbackFragment.getInstance(), bundle);

    }

    private void startTrackOrderFragment(int orderId, String resId) {
//        Log.e("orderIdMM",orderId+"");
//        startActivity(new Intent(getActivity(), TrackOrderMapsActivity.class)
//        .putExtra("orderId",orderId+"")
//        );

        Bundle bundle = new Bundle();
        bundle.putString(Constants.Keys.ORDER_ID, orderId+"");
//        bundle.putString(Constants.Keys.RESTAURANT_KEY, resId);
        switchFragment(TrackOrderFragment.getInstance(), bundle);
    }


    protected void showErrorAlert(String message) {
        new AwesomeErrorDialog(getActivity()).setButtonText(getString(R.string.dialog_ok_button)).setTitle(Constants.ERROR.toUpperCase())
                .setMessage(message).setCancelable(true).setErrorButtonClick(new Closure() {
            @Override
            public void exec() {
                switchFragment(SignInFragment.getInstance());
            }
        }).show();
    }


    @Override
    public void onClick(View v) {
        if (v == tvTrackOrder) {
            switchFragment(MapFragment.getInstance());
        } else {
            getParentActivity().openDrawer();

        }

    }
}
