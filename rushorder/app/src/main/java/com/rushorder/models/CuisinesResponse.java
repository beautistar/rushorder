package com.rushorder.models;

/**
 * Created by Aleem on 2/23/18.
 */

public class CuisinesResponse {


    /**
     * Id : 1
     * Name : Pakistani
     * IsActive : true
     * IsDelete : false
     */

    private int Id;
    private String Name;
    private boolean IsActive;
    private boolean IsDelete;


    public CuisinesResponse(int id, String name) {
        Id = id;
        Name = name;
    }

    public int getId() {
        return Id;
    }

    public void setId(int Id) {
        this.Id = Id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public boolean isIsActive() {
        return IsActive;
    }

    public void setIsActive(boolean IsActive) {
        this.IsActive = IsActive;
    }

    public boolean isIsDelete() {
        return IsDelete;
    }

    public void setIsDelete(boolean IsDelete) {
        this.IsDelete = IsDelete;
    }
}
