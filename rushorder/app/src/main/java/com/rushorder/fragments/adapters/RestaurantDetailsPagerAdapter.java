package com.rushorder.fragments.adapters;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.ViewGroup;

import com.rushorder.constants.Constants;
import com.rushorder.fragments.RestaurantInfoFragment;
import com.rushorder.fragments.RestaurantMenuFragment;
import com.rushorder.fragments.RestaurantReviewsFragment;

/**
 * Created by Aleem on 1/27/2018.
 */

public class RestaurantDetailsPagerAdapter extends FragmentStatePagerAdapter {

    private String titles[] = {"MENU", "REVIEWS", "INFO"};
//    private String titles[] = {"MENU", "INFO"};

//      private String titles[] = {"MENU"};

    private int itemPosition;
    Bundle bundle = new Bundle();

    public RestaurantDetailsPagerAdapter(FragmentManager fm, int itemPosition) {
        super(fm);
        this.itemPosition = itemPosition;
        bundle.putInt(Constants.Keys.ITEM_POSITION_KEY, itemPosition);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                RestaurantMenuFragment menuFragment = RestaurantMenuFragment.getInstance();
                menuFragment.setArguments(bundle);
                return menuFragment;
            case 1:
                RestaurantReviewsFragment reviewsFragment = RestaurantReviewsFragment.getInstance();
                reviewsFragment.setArguments(bundle);
                return reviewsFragment;
            case 2:
                return RestaurantInfoFragment.getInstance();
        }
        return new Fragment();
    }

    @Override
    public int getCount() {
        return titles.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return titles[position];
    }

    @Override
    public Parcelable saveState() {
        return null;
    }

    @Override
    public void finishUpdate(ViewGroup container) {
        try {
            super.finishUpdate(container);
        } catch (NullPointerException nullPointerException) {
            System.out.println("Catch the NullPointerException in FragmentPagerAdapter.finishUpdate");
        }
    }
}