package com.rushorder.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.awesomedialog.blennersilva.awesomedialoglibrary.AwesomeErrorDialog;
import com.awesomedialog.blennersilva.awesomedialoglibrary.interfaces.Closure;
import com.facebook.login.LoginManager;
import com.google.firebase.auth.FirebaseAuth;
import com.rushorder.R;
import com.rushorder.apis.Apis;
import com.rushorder.app.AppController;
import com.rushorder.chat.ChatActivity;
import com.rushorder.constants.Constants;
import com.rushorder.fragments.AddressFragment;
import com.rushorder.fragments.CartDetailFragment;
import com.rushorder.fragments.ChatFragment;
import com.rushorder.fragments.DonateFragment;
import com.rushorder.fragments.HomeFragment;
import com.rushorder.fragments.LoyaltyFragment;
import com.rushorder.fragments.OrderHistoryFragment;
import com.rushorder.fragments.PrivacyPolicyFragment;
import com.rushorder.fragments.RestaurantContactFragment;
import com.rushorder.fragments.RestaurantListFragment;
import com.rushorder.fragments.SignInChoiceFragment;
import com.rushorder.fragments.SignInFragment;
import com.rushorder.interfaces.DrawerCallBack;
import com.rushorder.managers.PrefManager;
import com.rushorder.models.Cart;
import com.rushorder.models.SignInResponse;
import com.rushorder.utils.JsonTranslator;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;


public class MainActivity extends BaseActivity implements DrawerCallBack {
    public static final int TYPE_PROFILE = 0;

    private LinearLayout llDonate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        initViewsWithDrawer(false, false);
        setListeners();

        Log.e("isLogin", PrefManager.getInstance().getBoolean(PrefManager.IS_LOGIN) + "");

    }



    @Override
    protected void initViewsWithDrawer(boolean isToolBar, boolean isDrawerHeader) {
        super.initViewsWithDrawer(isToolBar, isDrawerHeader);
        setUpHomeFragment(new HomeFragment());
        llDonate = findViewById(R.id.llDonate);

    }

    @Override
    protected void setListeners() {

        drawerAdapter.setMenuClickListener(this);

        bnvMain.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                if (item.getItemId() == R.id.action_nav_restaurants) {
                    if (!(getCurrentFragment() instanceof RestaurantListFragment))
                        switchFragmentWithBackStack(RestaurantListFragment.getInstance());
                } else if (item.getItemId() == R.id.action_nav_orders) {
                    if (!(getCurrentFragment() instanceof OrderHistoryFragment))
                        switchFragmentWithBackStack(OrderHistoryFragment.getInstance());
                } else if (item.getItemId() == R.id.action_nav_more) {
                    if (!(getCurrentFragment() instanceof CartDetailFragment))
                        switchFragmentWithBackStack(CartDetailFragment.getInstance());
                } else if (item.getItemId() == R.id.action_nav_home) {
                    if (!(getCurrentFragment() instanceof HomeFragment))
                        clearAllFragments();
                }
                return true;
            }
        });

        llDonate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switchSelectedMenuFragment(DonateFragment.getInstance());
                closeDrawer();
            }
        });
        super.setListeners();
    }

    public void checkUserLogin() {

        if (!PrefManager.getInstance().getString(PrefManager.USER_INFO).isEmpty()) {
            setUserLoginDrawerConfiguration();
        } else {
            setGuestUserDrawerConfiguration();
        }
    }

    private void setGuestUserDrawerConfiguration() {
    }

    private void setUserLoginDrawerConfiguration() {
        try {

            SignInResponse response = JsonTranslator.getInstance().parse(PrefManager.getInstance().getString(PrefManager.USER_INFO), SignInResponse.class);
//            tvEmail.setText("Points = 1");
            tvName.setText(response.getUserName());
            drawerAdapter.notifyDataSetChanged();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void setDrawerHeaderValues() {

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }


    private void resetDrawerHeader() {
    }


    public void selectBottomMenu(int action_nav) {
        bnvMain.setSelectedItemId(action_nav);
    }


    private void resetPreferences() {
        if (PrefManager.getInstance().getBoolean(PrefManager.IS_SOCIAL_LOGIN)) {
            LoginManager.getInstance().logOut();
            FirebaseAuth.getInstance().signOut();
        }

        tvName.setText("");
        tvEmail.setText("");
        tvLevelDrawer.setText("");
        PrefManager.getInstance().clearAll();
        Cart.getInstance().ClearCart();
        clearAllFragments();
    }


    public void moveToCart() {
        popFragment();
        popFragment();
    }

    @Override
    public void onMenuItemClicked(int menuItemId, int position) {
        switch (menuItemId) {

            case R.string.navdrawer_item_home:
                clearAllFragments();
                break;
            case R.string.navdrawer_item_my_orders:
                switchSelectedMenuFragment(OrderHistoryFragment.getInstance());
                break;
            case R.string.navdrawer_item_contact_us:
                switchSelectedMenuFragment(RestaurantContactFragment.getInstance());
                break;
            case R.string.navdrawer_item_terms:
                switchSelectedMenuFragment(PrivacyPolicyFragment.getInstance());
                break;
            case R.string.navdrawer_item_policy:
                switchSelectedMenuFragment(PrivacyPolicyFragment.getInstance());
                break;
            case R.string.navdrawer_item_live_chat:
                if (PrefManager.getInstance().getBoolean(PrefManager.IS_LOGIN)) {

                    startActivity(new Intent(this, ChatActivity.class));
                }
                else
                {
                    showErrorAlert("please login to chat");
                }
//                switchSelectedMenuFragment(ChatFragment.getInstance());
                break;
            case R.string.navdrawer_item_address:
                switchSelectedMenuFragment(AddressFragment.getInstance());
                break;
            case R.string.navdrawer_item_loyalty:
                switchSelectedMenuFragment(LoyaltyFragment.getInstance());
                break;
            case 0:
                checkLogin();
                break;
        }
        closeDrawer();
    }
    protected void showErrorAlert(String message) {
        new AwesomeErrorDialog(this).setButtonText(getString(R.string.dialog_ok_button)).setTitle(Constants.ERROR.toUpperCase())
                .setMessage(message).setCancelable(true).setErrorButtonClick(new Closure() {
            @Override
            public void exec() {
                switchFragment(SignInFragment.getInstance(), Constants.FragmentAnimation.NONE);
                if (AppController.getCurrentActivity() instanceof MainActivity) {
                    MainActivity activity = (MainActivity) AppController.getCurrentActivity();
                    activity.switchFragmentWithBackStack(SignInFragment.getInstance());

                }
                }
        }).show();
    }
//        if (AppController.getCurrentActivity() instanceof MainActivity) {
//        MainActivity activity = (MainActivity) AppController.getCurrentActivity();
//        activity.switchFragmentWithBackStack(fragment);

//    }


    private void checkLogin() {

        boolean isLogin = PrefManager.getInstance().getBoolean(PrefManager.IS_LOGIN);
        if (isLogin) {
            resetPreferences();
            drawerAdapter.notifyDataSetChanged();
        } else {
            switchSelectedMenuFragment(SignInChoiceFragment.getInstance());
        }
    }


}
