package com.rushorder.activities.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.rushorder.R;
import com.rushorder.constants.Constants;
import com.rushorder.interfaces.DrawerCallBack;
import com.rushorder.managers.PrefManager;


public class DrawerAdapter extends RecyclerView.Adapter<DrawerAdapter.Holder> {

    private DrawerCallBack callBack;
    private int selectedMenu = Constants.INVALID_VALUE;

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_drawer_item, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {

        try {
            if (Constants.NAV_ICON_RES_ID[position] == 0)
            {
                ConfigureLoginView(holder);
            } else {
                holder.ivMenuItem.setImageResource(Constants.NAV_ICON_RES_ID[position]);
                holder.tvMenuItemName.setText(Constants.NAV_TITLE_RES_ID[position]);
                ConfigureSelectionView(holder, position);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void ConfigureSelectionView(Holder holder, int position) {
        if (Constants.NAV_TITLE_RES_ID[position] == selectedMenu) {
            holder.highlightView.setVisibility(View.VISIBLE);
        } else {
            holder.highlightView.setVisibility(View.GONE);
        }
    }

    private void ConfigureLoginView(Holder holder) {
        boolean isLogin = PrefManager.getInstance().getBoolean(PrefManager.IS_LOGIN);
        if (isLogin) {
            holder.ivMenuItem.setImageResource(R.drawable.ic_nav_logout);
            holder.tvMenuItemName.setText("" + holder.itemView.getContext().getString(R.string.navdrawer_item_sign_out));
        } else {
            holder.ivMenuItem.setImageResource(R.drawable.ic_nav_login);
            holder.tvMenuItemName.setText("" + holder.itemView.getContext().getString(R.string.navdrawer_item_sign_in));
        }
    }


    @Override
    public int getItemCount() {
        return Constants.NAV_TITLE_RES_ID.length;
    }

    public void setMenuClickListener(DrawerCallBack callBack) {
        this.callBack = callBack;

    }

    class Holder extends RecyclerView.ViewHolder {
        private View highlightView;
        private ImageView ivMenuItem;
        private TextView tvMenuItemName;

        public Holder(View itemView) {
            super(itemView);

            ivMenuItem = itemView.findViewById(R.id.iv_MenuItem);
            highlightView = itemView.findViewById(R.id.highlight_view);
            tvMenuItemName = itemView.findViewById(R.id.tv_MenuItemName);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    selectedMenu = Constants.NAV_TITLE_RES_ID[getLayoutPosition()];
                    callBack.onMenuItemClicked(Constants.NAV_TITLE_RES_ID[getLayoutPosition()], getLayoutPosition());
                    notifyDataSetChanged();
                }
            });
        }
    }
}
