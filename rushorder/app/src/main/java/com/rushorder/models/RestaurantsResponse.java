package com.rushorder.models;

public class RestaurantsResponse {


    /**
     * $id : 1
     * Id : 1
     * Name : Aroma
     * Url : www.alltimestories.com
     * Img : /Uploads/Restaurants/Feature Img.jpg
     * Description : description
     * Phone1 : 03223220009
     * Phone2 : 03223220009
     * PostCode : sr47qs
     * SupportEmail : support@email.com
     * Address : United Kingdom
     * distance : 0.346090736631156
     * MinDelPrice : 0
     * CookingTime : 0
     * Raiting : 0
     */

    private String $id;
    private String Id;
    private String Name;
    private String Url;
    private String Img;
    private String Description;
    private String Phone1;
    private String Phone2;
    private String PostCode;
    private String SupportEmail;
    private String Address;
    private float distance;
    private float MinDelPrice;
    private float CookingTime;
    private float Raiting;

    public String get$id() {
        return $id;
    }

    public void set$id(String $id) {
        this.$id = $id;
    }

    public String getId() {
        return Id;
    }

    public void setId(String Id) {
        this.Id = Id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getUrl() {
        return Url;
    }

    public void setUrl(String Url) {
        this.Url = Url;
    }

    public String getImg() {
        return "http://rushorder.online/" +Img;
    }

    public void setImg(String Img) {
        this.Img = Img;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String Description) {
        this.Description = Description;
    }

    public String getPhone1() {
        return Phone1;
    }

    public void setPhone1(String Phone1) {
        this.Phone1 = Phone1;
    }

    public String getPhone2() {
        return Phone2;
    }

    public void setPhone2(String Phone2) {
        this.Phone2 = Phone2;
    }

    public String getPostCode() {
        return PostCode;
    }

    public void setPostCode(String PostCode) {
        this.PostCode = PostCode;
    }

    public String getSupportEmail() {
        return SupportEmail;
    }

    public void setSupportEmail(String SupportEmail) {
        this.SupportEmail = SupportEmail;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String Address) {
        this.Address = Address;
    }

    public float getDistance() {
        return distance;
    }

    public void setDistance(float distance) {
        this.distance = distance;
    }

    public float getMinDelPrice() {
        return MinDelPrice;
    }

    public void setMinDelPrice(int MinDelPrice) {
        this.MinDelPrice = MinDelPrice;
    }

    public float getCookingTime() {
        return CookingTime;
    }

    public void setCookingTime(int CookingTime) {
        this.CookingTime = CookingTime;
    }

    public float getRaiting() {
        return Raiting;
    }

    public void setRaiting(int Raiting) {
        this.Raiting = Raiting;
    }
}
