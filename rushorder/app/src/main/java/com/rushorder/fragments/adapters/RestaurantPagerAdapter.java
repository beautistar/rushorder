package com.rushorder.fragments.adapters;


import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.rushorder.R;
import com.rushorder.interfaces.ClickCallBack;
import com.rushorder.models.RestaurantsResponse;
import com.rushorder.views.CircleImageView;
import com.rushorder.views.TriangleRectangleLabelView;
import com.iarcuschin.simpleratingbar.SimpleRatingBar;
import com.squareup.picasso.Picasso;

import java.lang.ref.WeakReference;
import java.util.List;

public class RestaurantPagerAdapter extends PagerAdapter {
    private WeakReference<ClickCallBack> callback;

    private List<RestaurantsResponse> restaurantsList;

    public RestaurantPagerAdapter(ClickCallBack callback, List<RestaurantsResponse> restaurantsList) {
        this.callback = new WeakReference<>(callback);
        this.restaurantsList = restaurantsList;
    }

    @Override
    public int getCount() {
        if (restaurantsList != null)
            return restaurantsList.size();
        else
            return 0;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {


        View itemView = LayoutInflater.from(container.getContext()).inflate(R.layout.pgr_reataurant_row, container, false);

        TextView tvMinSpent = itemView.findViewById(R.id.tvMin);
        TextView tvDelivery = itemView.findViewById(R.id.tvDelivery);
        TextView tvDiscount = itemView.findViewById(R.id.tvDiscount);
        TextView tvStatus = itemView.findViewById(R.id.tvRestaurantsStatus);
        TextView tvReviewsCount = itemView.findViewById(R.id.tvReviewsCount);
        TextView tvDistance = itemView.findViewById(R.id.tvRestaurantDistance);
        SimpleRatingBar restaurantRating = itemView.findViewById(R.id.rbRating);
        CircleImageView ivRestaurants = itemView.findViewById(R.id.ivRestaurant);
        TextView tvRestaurantName = itemView.findViewById(R.id.tvRestaurantName);
        TextView tvRestaurantDesc = itemView.findViewById(R.id.tvRestaurantDesc);
        TriangleRectangleLabelView trvDiscount = itemView.findViewById(R.id.trvDiscount);
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.get().callBack(restaurantsList.get(position), position);
            }
        });

        RestaurantsResponse model = restaurantsList.get(position);

        try {

            tvDistance.setText("" + model.getDistance());
            tvRestaurantName.setText("" + model.getName());
            restaurantRating.setRating(model.getRaiting());
            tvRestaurantDesc.setText("" + model.getDescription());
            tvDelivery.setText("" + model.getCookingTime() + " Minutes");
            tvMinSpent.setText("" + itemView.getContext().getString(R.string.pound_sign) + " " + model.getMinDelPrice());
//            tvReviewsCount.setText("" + model.getReviewCount());

//            tvDistance.setText("" + Utils.formatToTwoDecimalPlaces(model.getDistance()));


            Picasso.with(itemView.getContext())
                    .load(model.getImg())
                    .fit()
                    .placeholder(R.drawable.restaurant_logo)
                    .error(R.drawable.restaurant_logo)
                    .into(ivRestaurants);

//            if (model.getDiscount() != null && !model.getDiscount().isEmpty()) {
//                trvDiscount.setText("" + model.getDiscount());
//                trvDiscount.setVisibility(View.VISIBLE);
//            } else {
                trvDiscount.setVisibility(View.GONE);
//            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }


        container.addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }
}
