package com.rushorder.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.android.volley.VolleyError;

import com.rushorder.R;
import com.rushorder.apis.Apis;
import com.rushorder.cache.CacheData;
import com.rushorder.driver.config.SessionManagerDriver;
import com.rushorder.interfaces.VolleyCallBack;
import com.rushorder.managers.PrefManager;
import com.rushorder.models.CuisinesResponse;
import com.rushorder.utils.JsonTranslator;
import com.rushorder.utils.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

public class SplashActivity extends BaseActivity {

    private TextView tvLogo;
    private String deviceToken = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utils.hideStatusBar(this);
        setContentView(R.layout.activity_splash);


        initViews();
    }


    @Override
    protected void initViews() {
        super.initViews();

        tvLogo = findViewById(R.id.tvLogo);
        Utils.Encryption.getSha1(this, "lazefood");
//        deviceToken = FirebaseInstanceId.getInstance().getToken();
//        getCuisines();

//        if (!PrefManager.getInstance().getBoolean(PrefManager.DEVICE_REGISTERED) && deviceToken != null) {
//            registerDevice();
//        } else {


        animate();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                getCuisines();
            }
        }, 2000);
    }


    private void animate() {

        tvLogo.startAnimation(AnimationUtils.loadAnimation(this, R.anim.fade_in_logo));

    }


    void getCuisines() {
        Apis.Restaurant.getCuisines(this, false, "", new VolleyCallBack.ArrayResponse() {
            @Override
            public void onResponse(JSONArray _response, String _tag) {

                List<CuisinesResponse> accountResponse = JsonTranslator.getInstance().parseJsonToList(_response.toString());
                CacheData.Filters.getInstance().setCuisinesList(accountResponse);

                if (new SessionManagerDriver(SplashActivity.this).isLoggedIn()) {
                    startActivity(new Intent(SplashActivity.this, OrderIdDriverActivity.class));
                    finish();
                }
                else
                    startMainActivity();

            }

            @Override
            public void onErrorMessage(String _errorMessage) {
                Utils.SnackBarMessage.showSnackBarInfinite(parentLayout, _errorMessage, getString(R.string.retry), new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        getCuisines();
                    }
                });
            }

            @Override
            public void onErrorResponse(VolleyError _error, String _tag) {
                Utils.SnackBarMessage.showSnackBarInfinite(parentLayout, getString(R.string.something_went_wrong), getString(R.string.retry), new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        getCuisines();
                    }
                });
            }

            @Override
            public void isConnected(boolean _connected, String _tag) {
                Utils.SnackBarMessage.showSnackBarInfinite(parentLayout, getString(R.string.no_internet), getString(R.string.retry), new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        getCuisines();
                    }
                });
            }
        });
    }


    void getRestaurantDetails() {

        Apis.Restaurant.getRestaurantDetails(this, false, getString(R.string.api_key), new VolleyCallBack.JSONResponse() {
            @Override
            public void onResponse(JSONObject _response, String _tag) {
                try {
//                    RestaurantDetailsResponse detailResponse = JsonTranslator.getInstance().parse(_response.toString(), RestaurantDetailsResponse.class);
//                    if (detailResponse.isSuccess()) {
//                        CacheData.RestaurantsDetails.getInstance().setDetails(detailResponse.getRestaurant_details());
//
//                        if (!PrefManager.getInstance().getBoolean(PrefManager.DEVICE_REGISTERED) && deviceToken != null) {
//                            registerDevice();
//                        } else {
//                            startMainActivity();
//                        }

//                    } else {
//                        showErrorDialog(detailResponse.getData().getMessage());
//                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorMessage(String _errorMessage) {
                Utils.SnackBarMessage.showSnackBarInfinite(parentLayout, _errorMessage, getString(R.string.retry), new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        getRestaurantDetails();
                    }
                });
            }

            @Override
            public void onErrorResponse(VolleyError _error, String _tag) {
                Utils.SnackBarMessage.showSnackBarInfinite(parentLayout, getString(R.string.something_went_wrong), getString(R.string.retry), new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        getRestaurantDetails();
                    }
                });
            }

            @Override
            public void isConnected(boolean _connected, String _tag) {
                Utils.SnackBarMessage.showSnackBarInfinite(parentLayout, getString(R.string.no_internet), getString(R.string.retry), new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        getRestaurantDetails();
                    }
                });
            }
        });

    }


    void registerDevice() {
        Apis.User.registerDevice(this, false, deviceToken, new VolleyCallBack.JSONResponse() {

            @Override
            public void onResponse(JSONObject _response, String _tag) {
                try {
                    PrefManager.getInstance().saveDeviceRegistrationDiscount(_response.getString("discount"));
                    startMainActivity();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            @Override
            public void onErrorMessage(String _errorMessage) {
                Utils.SnackBarMessage.showSnackBarShort(parentLayout, "" + _errorMessage);
                registerDevice();
            }

            @Override
            public void onErrorResponse(VolleyError _error, String _tag) {
                Utils.SnackBarMessage.showSnackBarInfinite(parentLayout, getString(R.string.something_went_wrong), getString(R.string.retry), new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        registerDevice();
                    }
                });
            }

            @Override
            public void isConnected(boolean _connected, String _tag) {
                Utils.SnackBarMessage.showSnackBarInfinite(parentLayout, getString(R.string.no_internet), getString(R.string.retry), new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        registerDevice();
                    }
                });
            }
        });
    }

    private void startMainActivity() {
        finish();
        startNewActivity(MainActivity.class);
    }

}
