package com.rushorder.managers;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkInfo;
import android.os.Build;
import android.support.annotation.NonNull;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.JsonArray;
import com.rushorder.app.AppController;
import com.rushorder.constants.Constants;
import com.rushorder.interfaces.VolleyCallBack;
import com.rushorder.utils.AlertHelper;
import com.rushorder.utils.CustomArrayRequest;
import com.rushorder.utils.CustomJsonRequest;


import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


public class ConnectionManager {
    private static ConnectionManager mInstance;


    public ConnectionManager() {
    }

    /**
     * @return
     */
    public static ConnectionManager getInstance() {
        if (mInstance == null) {
            mInstance = new ConnectionManager();
        }
        return mInstance;
    }

    /**
     * Internet connection checker
     *
     * @param context
     * @return
     */

    public static boolean isNetworkAvailable(@NonNull Context context) {

        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Network[] networks = connectivityManager.getAllNetworks();
            NetworkInfo networkInfo;
            for (Network mNetwork : networks) {
                networkInfo = connectivityManager.getNetworkInfo(mNetwork);
                if (networkInfo != null) {
                    if (networkInfo.getState().equals(NetworkInfo.State.CONNECTED)) {
                        return true;
                    }
                }
            }
        } else {
            if (connectivityManager != null) {
                //noinspection deprecation
                NetworkInfo[] info = connectivityManager.getAllNetworkInfo();
                if (info != null) {
                    for (NetworkInfo anInfo : info) {
                        if (anInfo.getState() == NetworkInfo.State.CONNECTED) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;

    }


    /**
     * * Volley JsonObjectRequest
     * Method POST / PUT / DELETE  with rv_header_row
     *
     * @param activity
     * @param isDialog
     * @param serverURL
     * @param jsonParamObject
     * @param headers
     * @param jsonResponse
     */
    public void volleyJsonObjectRequest(final Activity activity,
                                        final boolean isDialog,
                                        final String serverURL,
                                        final String requestTag,
                                        final JSONObject jsonParamObject,
                                        final HashMap<String, String> headers,
                                        final VolleyCallBack.JSONResponse jsonResponse) {


        if (isNetworkAvailable(activity)) {
            final AlertHelper dialog = AlertHelper.showProgress(activity, isDialog);
            JsonObjectRequest serverRequest = new JsonObjectRequest(Request.Method.POST, serverURL, jsonParamObject, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    dialog.dismissProgress();
                    jsonResponse.onResponse(response, requestTag);
                }

            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    dialog.dismissProgress();
                    volleyErrorHandler(error, jsonResponse, requestTag);
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    return headers;
                }
            };

            AppController.getInstance().addToRequestQueue(serverRequest, requestTag);
        } else {
            jsonResponse.isConnected(false, requestTag);
        }
    }


    /**
     * * Volley JsonObjectRequest
     * Method POST / PUT / DELETE  without rv_header_row
     *
     * @param activity
     * @param isDialog
     * @param serverURL
     * @param jsonParamObject
     * @param jsonResponse
     */
    public void volleyJsonObjectRequest(final Activity activity,
                                        final boolean isDialog,
                                        final String serverURL,
                                        final String requestTag,
                                        final JSONObject jsonParamObject,
                                        final VolleyCallBack.JSONResponse jsonResponse) {

        Log.e("jsonParamObject",jsonParamObject.toString());
        if (isNetworkAvailable(activity)) {
            final AlertHelper dialog = AlertHelper.showProgress(activity, isDialog);
            CustomJsonRequest serverRequest = new CustomJsonRequest(Request.Method.POST, serverURL, jsonParamObject, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    dialog.dismissProgress();
                    jsonResponse.onResponse(response, requestTag);
                }

            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    dialog.dismissProgress();
                    volleyErrorHandler(error, jsonResponse, requestTag);
                }
            });

            AppController.getInstance().addToRequestQueue(serverRequest, requestTag);
        } else {
            jsonResponse.isConnected(false, requestTag);
        }
    }


    /**
     * /**
     * VOlley JsonObjectRequest With headers
     * <p>
     * Method GET
     *
     * @param activity
     * @param isDialog
     * @param serverURL
     * @param requestTag
     * @param headers
     * @param jsonResponse
     */
    public void volleyJsonObjectRequest(final Activity activity,
                                        final boolean isDialog,
                                        final String serverURL,
                                        final String requestTag,
                                        final HashMap<String, String> headers,
                                        final VolleyCallBack.JSONResponse jsonResponse) {


        if (isNetworkAvailable(activity)) {

            final AlertHelper dialog = AlertHelper.showProgress(activity, isDialog);
            JsonObjectRequest serverRequest = new JsonObjectRequest(Request.Method.GET, serverURL, null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    dialog.dismissProgress();
                    jsonResponse.onResponse(response, requestTag);
                }

            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    dialog.dismissProgress();
                    volleyErrorHandler(error, jsonResponse, requestTag);
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    return headers;
                }

            };
            AppController.getInstance().addToRequestQueue(serverRequest, requestTag);
        } else {
            jsonResponse.isConnected(false, requestTag);
        }
    }


    /**
     * /**
     * VOlley JsonObjectRequest Without headers
     * <p>
     * Method GET
     *
     * @param activity
     * @param isDialog
     * @param serverURL
     * @param requestTag
     * @param jsonResponse
     */
    public void volleyJsonObjectRequest(final Activity activity,
                                        final boolean isDialog,
                                        final String serverURL,
                                        final String requestTag,
                                        final VolleyCallBack.JSONResponse jsonResponse) {


        if (isNetworkAvailable(activity)) {

            final AlertHelper dialog = AlertHelper.showProgress(activity, isDialog);
            JsonObjectRequest serverRequest = new JsonObjectRequest(Request.Method.GET, serverURL, null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    dialog.dismissProgress();
                    jsonResponse.onResponse(response, requestTag);
                }

            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    dialog.dismissProgress();
                    volleyErrorHandler(error, jsonResponse, requestTag);
                }
            });
            AppController.getInstance().addToRequestQueue(serverRequest, requestTag);
        } else {
            jsonResponse.isConnected(false, requestTag);
        }
    }


    /**
     * * * Volley StringRequest
     * Method POST / PUT / DELETE  with header
     *
     * @param activity
     * @param isDialog
     * @param serverURL
     * @param requestTag
     * @param params
     * @param headers
     * @param stringResponse
     */

    public void volleyStringRequest(final Activity activity,
                                    final boolean isDialog,
                                    final String serverURL,
                                    final String requestTag,
                                    final HashMap<String, String> params,
                                    final HashMap<String, String> headers,
                                    final VolleyCallBack.StringResponse stringResponse) {


        if (isNetworkAvailable(activity)) {
            final AlertHelper dialog = AlertHelper.showProgress(activity, isDialog);
            StringRequest serverRequest = new StringRequest(Request.Method.POST, serverURL, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    dialog.dismissProgress();
                    stringResponse.onResponse(response, requestTag);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    dialog.dismissProgress();
                    volleyErrorHandler(error, stringResponse, requestTag);
                }
            }) {

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    return headers;
                }
            };

            AppController.getInstance().addToRequestQueue(serverRequest, requestTag);
        } else {
            stringResponse.isConnected(false, requestTag);
        }
    }


    /**
     * * * Volley StringRequest
     * Method POST / PUT / DELETE  without header
     *
     * @param activity
     * @param isDialog
     * @param serverURL
     * @param requestTag
     * @param params
     * @param stringResponse
     */


    public void volleyStringRequest(final Activity activity,
                                    final boolean isDialog,
                                    final String serverURL,
                                    final String requestTag,
                                    final HashMap<String, String> params,
                                    final VolleyCallBack.StringResponse stringResponse) {


        if (isNetworkAvailable(activity)) {
            final AlertHelper dialog = AlertHelper.showProgress(activity, isDialog);
            StringRequest serverRequest = new StringRequest(Request.Method.POST, serverURL, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    dialog.dismissProgress();
                    stringResponse.onResponse(response, requestTag);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    dialog.dismissProgress();
                    volleyErrorHandler(error, stringResponse, requestTag);
                }
            }) {

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    return params;
                }
            };

            AppController.getInstance().addToRequestQueue(serverRequest, requestTag);
        } else {
            stringResponse.isConnected(false, requestTag);
        }
    }


    public void volleyStringBodyRequest(final Activity activity,
                                        final boolean isDialog,
                                        final String serverURL,
                                        final String requestTag,
                                        final String params,
                                        final VolleyCallBack.StringResponse stringResponse) {


        if (isNetworkAvailable(activity)) {
            final AlertHelper dialog = AlertHelper.showProgress(activity, isDialog);
            StringRequest serverRequest = new StringRequest(Request.Method.POST, serverURL, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    dialog.dismissProgress();
                    stringResponse.onResponse(response, requestTag);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    dialog.dismissProgress();
                    volleyErrorHandler(error, stringResponse, requestTag);
                }
            }) {
                @Override
                public byte[] getBody() throws AuthFailureError {
                    return params.getBytes();
                }

                @Override
                public String getBodyContentType() {
                    return "application/x-www-form-urlencoded";
                }
            };

            AppController.getInstance().addToRequestQueue(serverRequest, requestTag);
        } else {
            stringResponse.isConnected(false, requestTag);
        }
    }


    /**
     * * * Volley StringRequest without headers
     * Method GET
     *
     * @param activity
     * @param isDialog
     * @param serverURL
     * @param requestTag
     * @param stringResponse
     */

    public void volleyStringRequest(final Activity activity,
                                    final boolean isDialog,
                                    final String serverURL,
                                    final String requestTag,
                                    final VolleyCallBack.StringResponse stringResponse) {


        if (isNetworkAvailable(activity)) {
            final AlertHelper dialog = AlertHelper.showProgress(activity, isDialog);

            StringRequest serverRequest = new StringRequest(Request.Method.GET, serverURL, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    dialog.dismissProgress();
                    stringResponse.onResponse(response, requestTag);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    dialog.dismissProgress();
                    volleyErrorHandler(error, stringResponse, requestTag);
                }
            });

            AppController.getInstance().addToRequestQueue(serverRequest, requestTag);
        } else {
            stringResponse.isConnected(false, requestTag);
        }
    }

    /**
     * Volley JsonArrayRequest Without headers
     * <p>
     * Method Post
     *
     * @param activity
     * @param isDialog
     * @param serverURL
     * @param requestTag
     * @param jsonResponse
     */


    public void volleyJsonArrayRequest(final Activity activity,
                                       final boolean isDialog,
                                       final String serverURL,
                                       final String requestTag,
                                       final JsonArray arrayParams,
                                       final HashMap<String, String> params,
                                       final VolleyCallBack.ArrayResponse jsonResponse) {


        if (isNetworkAvailable(activity)) {

            final AlertHelper dialog = AlertHelper.showProgress(activity, isDialog);
            JsonArrayRequest serverRequest = new JsonArrayRequest(Request.Method.POST, serverURL, null, new Response.Listener<JSONArray>() {
                @Override
                public void onResponse(JSONArray response) {
                    dialog.dismissProgress();
                    jsonResponse.onResponse(response, requestTag);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    dialog.dismissProgress();
                    volleyErrorHandler(error, jsonResponse, requestTag);
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    return params;
                }

            };
            AppController.getInstance().addToRequestQueue(serverRequest, requestTag);
        } else {
            jsonResponse.isConnected(false, requestTag);
        }
    }


    public void volleyArrayRequest(final Activity activity,
                                   final boolean isDialog,
                                   final String serverURL,
                                   final String requestTag,
                                   final JSONObject jsonParamObject,
                                   final VolleyCallBack.ArrayResponse jsonResponse) {


        if (isNetworkAvailable(activity)) {

            final AlertHelper dialog = AlertHelper.showProgress(activity, isDialog);

            CustomArrayRequest serverRequest = new CustomArrayRequest(Request.Method.POST, serverURL, jsonParamObject, new Response.Listener<JSONArray>() {
                @Override
                public void onResponse(JSONArray response) {
                    dialog.dismissProgress();
                    jsonResponse.onResponse(response, requestTag);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    dialog.dismissProgress();
                    volleyErrorHandler(error, jsonResponse, requestTag);
                }

            });

            AppController.getInstance().addToRequestQueue(serverRequest, requestTag);
        } else {
            jsonResponse.isConnected(false, requestTag);
        }
    }


    /**
     * Volley JsonArrayRequest With headers
     * <p>
     * Method GET
     *
     * @param activity
     * @param isDialog
     * @param serverURL
     * @param requestTag
     * @param headers
     * @param jsonResponse
     */


    public void volleyJsonArrayRequest(final Activity activity,
                                       final boolean isDialog,
                                       final String serverURL,
                                       final String requestTag,
                                       final HashMap<String, String> headers,
                                       final VolleyCallBack.ArrayResponse jsonResponse) {


        if (isNetworkAvailable(activity)) {

            final AlertHelper dialog = AlertHelper.showProgress(activity, isDialog);
            JsonArrayRequest serverRequest = new JsonArrayRequest(Request.Method.GET, serverURL, null, new Response.Listener<JSONArray>() {
                @Override
                public void onResponse(JSONArray response) {
                    dialog.dismissProgress();
                    jsonResponse.onResponse(response, requestTag);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    dialog.dismissProgress();
                    volleyErrorHandler(error, jsonResponse, requestTag);
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    return headers;
                }

            };
            AppController.getInstance().addToRequestQueue(serverRequest, requestTag);
        } else {
            jsonResponse.isConnected(false, requestTag);
        }
    }


    /**
     * Volley JsonArrayRequest Without headers
     * <p>
     * Method GET
     *
     * @param activity
     * @param isDialog
     * @param serverURL
     * @param requestTag
     * @param jsonResponse
     */


    public void volleyJsonArrayRequest(final Activity activity,
                                       final boolean isDialog,
                                       final String serverURL,
                                       final String requestTag,
                                       final VolleyCallBack.ArrayResponse jsonResponse) {


        if (isNetworkAvailable(activity)) {

            final AlertHelper dialog = AlertHelper.showProgress(activity, isDialog);
            JsonArrayRequest serverRequest = new JsonArrayRequest(Request.Method.GET, serverURL, null, new Response.Listener<JSONArray>() {
                @Override
                public void onResponse(JSONArray response) {
                    dialog.dismissProgress();
                    jsonResponse.onResponse(response, requestTag);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    dialog.dismissProgress();
                    volleyErrorHandler(error, jsonResponse, requestTag);
                }
            });
            AppController.getInstance().addToRequestQueue(serverRequest, requestTag);
        } else {
            jsonResponse.isConnected(false, requestTag);
        }
    }


    /**
     * Volley Error Handler
     *
     * @param error
     * @param volleyResponse
     */
    private void volleyErrorHandler(VolleyError error, VolleyCallBack.Callbacks volleyResponse, String requestTag) {
        if (error instanceof TimeoutError || error instanceof NoConnectionError) {
            volleyResponse.onErrorResponse(error, requestTag);
        } else if (error instanceof AuthFailureError) {
            volleyResponse.onErrorResponse(error, requestTag);
        } else if (error instanceof ServerError) {
            checkErrorMessage(error, volleyResponse, requestTag);
        } else if (error instanceof NetworkError) {
            volleyResponse.onErrorResponse(error, requestTag);
        } else if (error instanceof ParseError) {
            checkErrorMessage(error, volleyResponse, requestTag);
//            volleyResponse.onErrorResponse(error, requestTag);
        }
    }

    /**
     * Check Server Error Message
     *
     * @param error
     * @param volleyResponse
     */

    private void checkErrorMessage(VolleyError error, VolleyCallBack.Callbacks volleyResponse, String requestTag) {
        try {


            Log.d("Volley_Error", "Error: " + error
                    + "\nStatus Code " + error.networkResponse.statusCode
                    + "\nResponse Data " + error.networkResponse.data
                    + "\nCause " + error.getCause()
                    + "\nmessage" + error.getMessage());


            String responseBody = new String(error.networkResponse.data, "utf-8");

            if (requestTag.equals(Constants.Tags.SIGN_UP)) {

                try {
                    JSONObject jsonObject = new JSONObject(responseBody);

                    String message = "";
                    if (jsonObject.getJSONObject("ModelState").has("model.Password")) {
                        message = jsonObject.getJSONObject("ModelState").getJSONArray("model.Password").getString(0);
                    } else {
                        message = jsonObject.getJSONObject("ModelState").getJSONArray("").getString(0);
                    }
                    volleyResponse.onErrorMessage(message);

                } catch (Exception ex) {
                    volleyResponse.onErrorMessage(responseBody);
                }
            } else if (requestTag.equals(Constants.Tags.SIGN_IN)) {
                try {

                    JSONObject jsonObect = new JSONObject(responseBody);
                    String message = jsonObect.getString("error_description");
                    volleyResponse.onErrorMessage(message);


//                    if (requestTag.equals(Constants.Tags.SIGN_IN)) {
//                        volleyResponse.onErrorMessage(jsonObject.getString("error_description"));
//                    } else {
//                        volleyResponse.onErrorMessage(jsonObject.getString("Message"));
//
//                    }
                } catch (Exception ex) {

                    volleyResponse.onErrorMessage(responseBody);
                }
            } else {
                volleyResponse.onErrorMessage(responseBody);
            }

        } catch (Exception ex) {
            volleyResponse.onErrorResponse(error, requestTag);
            ex.printStackTrace();
        }

    }
}
