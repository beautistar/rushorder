package com.rushorder.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.internal.NavigationMenuView;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import com.awesomedialog.blennersilva.awesomedialoglibrary.AwesomeErrorDialog;
import com.awesomedialog.blennersilva.awesomedialoglibrary.AwesomeSuccessDialog;
import com.awesomedialog.blennersilva.awesomedialoglibrary.interfaces.Closure;
import com.rushorder.R;
import com.rushorder.activities.adapter.DrawerAdapter;
import com.rushorder.app.AppController;
import com.rushorder.constants.Constants;
import com.rushorder.constants.Constants.FragmentAnimation;
import com.rushorder.fragments.CartDetailFragment;
import com.rushorder.fragments.HomeFragment;
import com.rushorder.fragments.OrderHistoryFragment;
import com.rushorder.fragments.RestaurantListFragment;
import com.rushorder.managers.PrefManager;
import com.rushorder.models.SignInResponse;
import com.rushorder.utils.AlertUtils;
import com.rushorder.utils.JsonTranslator;

import java.util.Stack;


/**
 * Created by Aleem on 11/10/2017.
 */

public abstract class BaseActivity extends AppCompatActivity {

    protected Dialog dialog;
    protected Toolbar toolbar;
    protected View headerLayout;
    protected RecyclerView rvMenu;
    protected TextView tvName;
    public TextView tvEmail,tvLevelDrawer;
    protected DrawerLayout drawerLayout;
    protected DrawerAdapter drawerAdapter;
    protected BottomNavigationView bnvMain;
    protected NavigationView navigationView;
    protected Stack<Fragment> fragmentsStack;
    protected CoordinatorLayout parentLayout;


    protected void initViews() {
        parentLayout = findViewById(R.id.parentLayout);
    }

    protected void initViewsWithDrawer(boolean isToolBar, boolean isDrawerHeader) {
        fragmentsStack = new Stack<>();
        drawerAdapter = new DrawerAdapter();


        tvEmail = findViewById(R.id.tvEmail);
        tvName = findViewById(R.id.tvName);
        tvLevelDrawer = findViewById(R.id.tvLevelDrawer);

        bnvMain = findViewById(R.id.bnvMain);
        rvMenu = findViewById(R.id.rvDrawerMenu);

        parentLayout = findViewById(R.id.parentLayout);
        drawerLayout = findViewById(R.id.drawerLayout);
        navigationView = findViewById(R.id.navigationView);


        if (isDrawerHeader) {
            headerLayout = navigationView.getHeaderView(R.layout.drawer_header);
        }

        if (isToolBar) {
            setUpToolbar();
        }

        setUpNavigationDrawer(isToolBar);
        setUpAdapter();
        checkUserLogin();
    }


    protected void initViewsWithToolBar() {
//        toolbar = findViewById(R.id.toolbar);
//        parentLayout = findViewById(R.id.parentLayout);
//        setUpToolbar();
    }
    public void showSnackbar(View editText, String message) {
        hideKeyBoard(editText);

        Snackbar snackbar = Snackbar
                .make(editText, message, Snackbar.LENGTH_LONG);
        snackbar.show();
    }

    public void hideKeyBoard(View editText) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
    }

    protected void setUpToolbar() {
        setSupportActionBar(toolbar);
        final ActionBar supportActionBar = getSupportActionBar();
        assert supportActionBar != null;
//        supportActionBar.setHomeAsUpIndicator(R.drawable.ic_action_menu);
        supportActionBar.setDisplayHomeAsUpEnabled(true);
    }

    protected void setUpNavigationDrawer(boolean isToolBar) {

        ActionBarDrawerToggle toggle;
        if (isToolBar) {
            toggle = new ActionBarDrawerToggle(
                    this,
                    drawerLayout, toolbar,
                    R.string.navigation_drawer_open,
                    R.string.navigation_drawer_close);
        } else {

            toggle = new ActionBarDrawerToggle(
                    this,
                    drawerLayout,
                    R.string.navigation_drawer_open,
                    R.string.navigation_drawer_close);
        }


        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();
        setDrawerAnimation();
        disableNavigationViewScrollbars(navigationView);
    }

    private void setUpAdapter() {
        rvMenu.setLayoutManager(new LinearLayoutManager(this));
        rvMenu.setAdapter(drawerAdapter);
    }

    private void setDrawerAnimation() {
//        Add drawer listener to get slide offset
//      drawerLayout.getChildAt(0).setTranslationX(slideOffset * drawerView.getWidth());
//      drawerLayout.bringChildToFront(navigationView);
//      drawerLayout.requestLayout();


//        drawerLayout.setViewScale(Gravity.START, 0.9f);
//        drawerLayout.setRadius(Gravity.START, 35);
//        drawerLayout.setViewElevation(Gravity.START, 20);
    }

    protected void setValues() {

    }


    protected void setUpHomeFragment(Fragment fragment) {
        fragmentsStack.push(fragment);
        switchFragment(fragment, FragmentAnimation.NONE);
    }

    protected void setUpHomeFragment(Fragment fragment, Bundle bundle) {
        if (bundle != null) {
            fragment.setArguments(bundle);
        }
        fragmentsStack.push(fragment);
        switchFragment(fragment, FragmentAnimation.NONE);
    }


    protected void switchFragment(Fragment fragment, FragmentAnimation fragmentAnimation) {

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        try {

            if (fragmentAnimation == FragmentAnimation.SLIDE_RIGHT) {
                fragmentTransaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.slide_in_left, R.anim.slide_out_right);
            } else if (fragmentAnimation == FragmentAnimation.SLIDE_LEFT) {
                fragmentTransaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_right, R.anim.slide_out_left);
            }

            fragmentTransaction.replace(R.id.parentLayout, fragment).commit();
        } catch (IllegalStateException e) {
            fragmentTransaction.replace(R.id.parentLayout, fragment).commitAllowingStateLoss();
        }
    }

    /**
     * Clear all fragments and make the home fragment visible
     */
    public void clearAllFragments() {

        fragmentsStack.clear();
        setUpHomeFragment(new RestaurantListFragment());
    }

    public void switchSelectedMenuFragment(Fragment fragment) {
        fragmentsStack.clear();
        fragmentsStack.push(new RestaurantListFragment());
        fragmentsStack.push(fragment);
        switchFragment(fragment, Constants.FragmentAnimation.NONE);
        selectBottomNavMenu(fragment);

    }

    public void switchFragmentWithBackStack(Fragment fragment) {
        fragmentsStack.push(fragment);
        switchFragment(fragment, FragmentAnimation.SLIDE_LEFT);
        selectBottomNavMenu(fragment);
    }

    private void selectBottomNavMenu(Fragment fragment) {
        if (fragment instanceof HomeFragment && bnvMain.getSelectedItemId() != R.id.action_nav_home) {
            bnvMain.setSelectedItemId(R.id.action_nav_home);
        } else if (fragment instanceof OrderHistoryFragment && bnvMain.getSelectedItemId() != R.id.action_nav_orders) {
            bnvMain.setSelectedItemId(R.id.action_nav_orders);
        } else if (fragment instanceof RestaurantListFragment && bnvMain.getSelectedItemId() != R.id.action_nav_restaurants) {
            bnvMain.setSelectedItemId(R.id.action_nav_restaurants);
        } else if (fragment instanceof CartDetailFragment && bnvMain.getSelectedItemId() != R.id.action_nav_more) {
            bnvMain.setSelectedItemId(R.id.action_nav_more);
        }
    }

    /**
     * if the size is 5 then get the index of second last fragment i.e, 5-2 = 3 (Array indices are 0-4)
     */
    protected void popFragment() {
        int fragmentIndex = fragmentsStack.size() - 2;
        switchFragment(fragmentsStack.elementAt(fragmentIndex), FragmentAnimation.SLIDE_RIGHT);
        fragmentsStack.pop();

        selectBottomNavMenu(fragmentsStack.elementAt(fragmentIndex));
    }

    protected void setListeners() {
    }

    protected void showDialog() {
    }

    protected void hideDialog() {
    }


    protected void onResume() {
        super.onResume();
        AppController.setCurrentActivity(this);
    }

    protected void onPause() {
        clearReferences();
        super.onPause();
    }

    protected void onDestroy() {
        clearReferences();
        super.onDestroy();
    }

    private void clearReferences() {
        Activity currentActivity = AppController.getCurrentActivity();

        if (this.equals(currentActivity)) {
            AppController.setCurrentActivity(null);
        }
    }

    /**
     * Check if there is any fragment on stack to move back to that fragment
     */

    @Override
    public void onBackPressed() {

        if (fragmentsStack != null && fragmentsStack.size() > 1) {
            popFragment();
            if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                drawerLayout.closeDrawer(GravityCompat.START);
            }
        } else {
            if(drawerLayout!=null) {
                if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                    drawerLayout.closeDrawer(GravityCompat.START);
                } else {
                    showExitAlert("Do you want to exit ?");
                }
            }
            else
                finish();
        }
    }

    public void openDrawer() {
        drawerLayout.openDrawer(GravityCompat.START);
    }

    public void closeDrawer() {
        drawerLayout.closeDrawer(GravityCompat.START);
    }

    public Fragment getCurrentFragment() {
        return fragmentsStack.lastElement();
    }


    protected <T> void startNewActivity(Class<T> activity) {
        Intent intent = new Intent(this, activity);
        startActivity(intent);
    }

    protected <T> void startNewActivity(Class<T> activity, Bundle bundle) {
        Intent intent = new Intent(this, activity);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    // hide navigation scrollbars
    private void disableNavigationViewScrollbars(NavigationView navigationView) {
        if (navigationView != null) {
            NavigationMenuView navigationMenuView = (NavigationMenuView) navigationView.getChildAt(0);
            if (navigationMenuView != null) {
                navigationMenuView.setVerticalScrollBarEnabled(false);
            }
        }
    }

    public void showExitAlert(String message) {

        AlertUtils.showAlert(this, "Alert !", message, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                finish();
            }
        });

    }

    protected void showSuccessDialog(String message) {
        dialog = new AwesomeSuccessDialog(this).setPositiveButtonText(getString(R.string.dialog_ok_button)).setTitle(Constants.STATUS_SUCCESS.toUpperCase())
                .setMessage(message).setCancelable(true).setPositiveButtonClick(new Closure() {
                    @Override
                    public void exec() {
                        dialog.hide();
                    }
                }).show();
    }

    protected void showErrorDialog(String message) {
        dialog = new AwesomeErrorDialog(this).setButtonText(getString(R.string.dialog_ok_button)).setTitle(Constants.ERROR.toUpperCase())
                .setMessage(message).setCancelable(true).setErrorButtonClick(new Closure() {
                    @Override
                    public void exec() {
                        dialog.hide();
                    }
                }).show();
    }


    public void checkUserLogin() {

        if (!PrefManager.getInstance().getString(PrefManager.USER_INFO).isEmpty()) {

            setUserLoginDrawerConfiguration();

        } else {
            setGuestUserDrawerConfiguration();
        }
    }


    private void setGuestUserDrawerConfiguration() {


    }

    private void setUserLoginDrawerConfiguration() {
        try {

            SignInResponse response = JsonTranslator.getInstance().parse(PrefManager.getInstance().getString(PrefManager.USER_INFO), SignInResponse.class);
//            tvEmail.setText("Points = 1");
            tvName.setText(response.getUserName());
//            SignInResponse response = JsonTranslator.getInstance().parse(PrefManager.getInstance().getString(PrefManager.USER_INFO), SignInResponse.class);
//            tvEmail.setText(PrefManager.getInstance().getString(PrefManager.EMAIL));
//            tvName.setText(response.getUserName());

            drawerAdapter.notifyDataSetChanged();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }


}


