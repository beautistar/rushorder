package com.rushorder.models;


import java.util.List;

import com.rushorder.models.RestaurantInfoResponse.CategoryViewModelsBean.SubCategoriesBean.ItemsBean.AddonCategoriesBean.AddonItemsBean;

/**
 * Created by Aleem on 12/10/2017.
 */

public class CartModel {

    private String ItemName;
    private String Price;
    private int Item_Id;
    private int Quantity;
    private String ItemNote;

    List<AddonItemsBean> SoldAddonItem;

    public CartModel(String name, String price, int productid, int qty, String description, List<AddonItemsBean> selectedAddons) {
        this.ItemName = name;
        this.Price = price;
        this.Item_Id = productid;
        this.Quantity = qty;
        this.ItemNote = description;
        this.SoldAddonItem = selectedAddons;

        setIdValues();
    }

    private void setIdValues() {

        for (int index = 0; index < SoldAddonItem.size(); index++) {
            SoldAddonItem.get(index).setId(SoldAddonItem.get(index).getId());
            SoldAddonItem.get(index).setAddonItem_Id(SoldAddonItem.get(index).getId());
//            SoldAddonItem.get(index).setAddon_cat_id(SoldAddonItem.get(index).getParent_id());
            SoldAddonItem.get(index).setAddonCategory_Id(SoldAddonItem.get(index).getAddonCategory_Id());
        }
    }

    public String getItemName() {
        return ItemName;
    }

    public void setItemName(String name) {
        this.ItemName = name;
    }

    public String getPrice() {
        return Price;
    }

    public void setPrice(String price) {
        this.Price = price;
    }

    public int getItem_Id() {
        return Item_Id;
    }

    public void setItem_Id(int item_Id) {
        this.Item_Id = item_Id;
    }

    public int getQuantity() {
        return Quantity;
    }

    public void setQuantity(int quantity) {
        this.Quantity = quantity;
    }

    public List<AddonItemsBean> getSoldAddonItem() {
        return SoldAddonItem;
    }

    public void setSoldAddonItem(List<AddonItemsBean> soldAddonItem) {
        this.SoldAddonItem = soldAddonItem;
    }
}
