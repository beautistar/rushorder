package com.rushorder.ResideMenu;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.rushorder.R;
import com.rushorder.managers.PrefManager;
import com.rushorder.models.SignInResponse;

public class ResideMenuItem extends LinearLayout {

    private int id;
    private ImageView iv_icon;
    private TextView tv_title, tvPersonName, tvEmail;
    private View highlightView;
    private boolean isChecked = false;
    public static final int TYPE_PROFILE = 0;
    private SignInResponse signInResponse;


    public ResideMenuItem(Context context) {
        super(context);
        initViews(context);
    }

    public ResideMenuItem(Context context, int iconId, int title, boolean isChecked) {
        super(context);
        this.id = iconId;
        this.isChecked = isChecked;
        initViews(context);
        if (iconId != TYPE_PROFILE) {
            iv_icon.setImageResource(iconId);
            tv_title.setText(title);
        }

    }

    public ResideMenuItem(Context context, int icon, String title) {
        super(context);
        initViews(context);
        iv_icon.setImageResource(icon);
        tv_title.setText(title);
    }

    private void initViews(Context context) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (id == TYPE_PROFILE) {
            inflater.inflate(R.layout.drawer_header, this);
            tvPersonName = findViewById(R.id.tvPersonName);
            tvEmail = (TextView) findViewById(R.id.tvEmail);
            if (PrefManager.getInstance().getBoolean(PrefManager.IS_LOGIN)) {

                signInResponse = PrefManager.getInstance().getObject(PrefManager.USER_INFO, SignInResponse.class);
                if (signInResponse != null) {

                    tvPersonName.setText("" + signInResponse.getUserName());
//                    tvEmail.setText("" + signInResponse.getEmail());
                }

            }
        } else {
            inflater.inflate(R.layout.menu_item_layout, this);
            iv_icon = (ImageView) findViewById(R.id.iv_icon);
            tv_title = (TextView) findViewById(R.id.tv_title);
            highlightView = (View) findViewById(R.id.highlight_view);


        }

    }

    /**
     * set the icon color;
     *
     * @param icon
     */
    public void setIcon(int icon) {
        iv_icon.setImageResource(icon);
    }

    /**
     * set the title with resource
     * ;
     *
     * @param title
     */
    public void setTitle(int title) {
        tv_title.setText(title);
    }

    /**
     * set the title with string;
     *
     * @param title
     */
    public void setTitle(String title) {
        tv_title.setText(title);
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setIndicatorVisible(boolean indicatorVisible) {
        highlightView.setVisibility(indicatorVisible ? View.VISIBLE : View.INVISIBLE);
    }


    public void updateProfileDetails(boolean showUserDetails) {
        signInResponse = PrefManager.getInstance().getObject(PrefManager.USER_INFO, SignInResponse.class);
        if (signInResponse != null) {

            if (showUserDetails) {
                tvPersonName.setText("" + signInResponse.getUserName());
//                tvEmail.setText("" + signInResponse.getData().getEmail());
            }
        } else {
            tvPersonName.setText("");
            tvEmail.setText("");
        }


    }
}
