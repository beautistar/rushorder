package com.rushorder.models;

import com.rushorder.models.RestaurantInfoResponse.CategoryViewModelsBean.SubCategoriesBean.ItemsBean.AddonCategoriesBean.AddonItemsBean;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Aleem on 12/10/2017.
 */

public class Cart {



//              "TotalAmount": 2.0,
//             "DiscountAmount": 3.0,
//             "UserComment": "sample string 4",
//             "OrderDate": "2018-11-16T18:21:21.4238708+05:00",
//             "IsPreorder": true,
//             "PreOrderTime": "00:00:00.1234567",
//             "PreOrderDate": "2018-11-16T18:21:21.4418636+05:00",
//             "PaymentType": "sample string 6",
//             "OrderType": "sample string 7",
//             "ServiceCharges": 8.0,
//             "Restaurant_Id": 10,
//             "Coupon_Id": 1,
//             "DeliveryCharges": 11.0,
//             "address": "sample string 12",
//             "postalcode": "sample string 13",
//             "doornumber": "sample string 14",
//             "town": "sample string 15",
//             "phonenumber": "sample string 16",
//             "StripeToken": "sample string 17",
//             "ApplicationUser_Id": "sample string 23",



    private transient static Cart mInstance;

    private int Restaurant_Id;
    private boolean IsPreorder;
    private String OrderType = "";
    private String TotalAmount = "";
    private String DiscountAmount = "0";

    private  float ServiceCharges = 0.0f;
    private  float DeliveryCharges = 0.0f;

    private String Coupon_Id = "";


    private String town = "";
    private String address = "";
    private String OrderDate = "";
    private String doornumber = "0";
    private String StripeToken = "";
    private String PaymentType = "";
    private String PreOrderDate = "";
    private String coupon_value = "0";
    private String delivery_price = "0";
    private String ApplicationUser_Id = "";
    private String discount_description = "";
    private boolean discount_info;
    private String distance = "";
    private String firstname = "";
    private String lastname = "";
    private String coupon_type = "";
    private String phonenumber = "";
    private String postalcode = "";
    private boolean preorder_is_preorder;
    private String token = "token";
    private String PreOrderTime = "";
    private String order_description = "";
    private String UserComment = "";
    private String email = "";
    private String DeliveryAddress = "";
    private String lat = "";
    private String Long = "";

    public String getDeliveryAddress() {
        return DeliveryAddress;
    }

    public void setDeliveryAddress(String deliveryAddress) {
        DeliveryAddress = deliveryAddress;
    }

    public String getLong() {
        return Long;
    }

    public void setLong(String aLong) {
        Long = aLong;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }






    private ArrayList<CartModel> OrderDetail;


    Cart() {
        OrderDetail = new ArrayList<>();
    }

    public static synchronized Cart getInstance() {

        if (mInstance == null) {
            mInstance = new Cart();
        }
        return mInstance;
    }

    public int getRestaurant_Id() {
        return Restaurant_Id;
    }

    public void setRestaurant_Id(int restaurant_Id) {
        Restaurant_Id = restaurant_Id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUserComment() {
        return UserComment;
    }

    public void setUserComment(String userComment) {
        this.UserComment = userComment;
    }

    public String getCoupon_Id() {
        return Coupon_Id;
    }

    public void setCoupon_Id(String coupon_Id) {
        this.Coupon_Id = coupon_Id;
    }

    public String getCoupon_type() {
        return coupon_type;
    }

    public void setCoupon_type(String coupon_type) {
        this.coupon_type = coupon_type;
    }

    public String getCoupon_value() {
        return coupon_value;
    }

    public void setCoupon_value(String coupon_value) {
        this.coupon_value = coupon_value;
    }

    public String getDelivery_price() {
        return delivery_price;
    }

    public void setDelivery_price(String delivery_price) {
        this.delivery_price = delivery_price;
    }

    public String getDiscountAmount() {
        return DiscountAmount;
    }

    public void setDiscountAmount(String discountAmount) {
        this.DiscountAmount = discountAmount;
    }

    public String getDiscount_description() {
        return discount_description;
    }

    public void setDiscount_description(String discount_description) {
        this.discount_description = discount_description;
    }

    public boolean isDiscount_info() {
        return discount_info;
    }

    public void setDiscount_info(boolean discount_info) {
        this.discount_info = discount_info;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getDoornumber() {
        return doornumber;
    }

    public void setDoornumber(String doornumber) {
        this.doornumber = doornumber;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getOrderType() {
        return OrderType;
    }

    public void setOrderType(String orderType) {
        this.OrderType = orderType;
    }

    public String getPaymentType() {
        return PaymentType;
    }

    public void setPaymentType(String paymentType) {
        this.PaymentType = paymentType;
    }

    public String getPhonenumber() {
        return phonenumber;
    }

    public void setPhonenumber(String phonenumber) {
        this.phonenumber = phonenumber;
    }

    public String getPostalcode() {
        return postalcode;
    }

    public void setPostalcode(String postalcode) {
        this.postalcode = postalcode;
    }

    public boolean isPreorder() {
        return IsPreorder;
    }

    public void setPreorder(boolean preorder) {
        this.IsPreorder = preorder;
    }

    public boolean isPreorder_is_preorder() {
        return preorder_is_preorder;
    }

    public void setPreorder_is_preorder(boolean preorder_is_preorder) {
        this.preorder_is_preorder = preorder_is_preorder;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getTotalAmount() {
        return TotalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.TotalAmount = totalAmount;
    }

    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public String getStripeToken() {
        return StripeToken;
    }

    public void setStripeToken(String stripeToken) {
        this.StripeToken = stripeToken;
    }

    public String getApplicationUser_Id() {
        return ApplicationUser_Id;
    }

    public void setApplicationUser_Id(String applicationUser_Id) {
        this.ApplicationUser_Id = applicationUser_Id;
    }

    public String getPreOrderDate() {
        return PreOrderDate;
    }

    public void setPreOrderDate(String preOrderDate) {
        this.PreOrderDate = preOrderDate;
    }

    public String getOrder_description() {
        return order_description;
    }

    public void setOrder_description(String order_description) {
        this.order_description = order_description;
    }

    public String getPreOrderTime() {
        return PreOrderTime;
    }

    public void setPreOrderTime(String preOrderTime) {
        this.PreOrderTime = preOrderTime;
    }

    public ArrayList<CartModel> getOrderDetail() {
        return OrderDetail;
    }

    public int getCartSize() {
        return (OrderDetail != null) ? OrderDetail.size() : 0;
    }


    public void setOrderDetail(ArrayList<CartModel> orderDetail) {
        this.OrderDetail = orderDetail;
    }

    public void AddToCart(HistoryHeader itemHeader, List<HistoryResponse.ItemBean> itemBeanList) {

        try {
            if (itemBeanList != null)
            {
                for (HistoryResponse.ItemBean itemBean : itemBeanList) {
                    ArrayList<AddonItemsBean> addOns = new ArrayList<>();

                    if (itemBean.getAddonItems() != null) {

                        for (HistoryResponse.ItemBean.SoldAddonItemBean child : itemBean.getAddonItems()) {

                            AddonItemsBean item = new AddonItemsBean();
                            item.setId(child.getAddonItemID());
                            item.setName(child.getName());
//                            item.setPrice("" + child.getPrice());
                            item.setPrice(child.getPrice());
                            addOns.add(item);
                        }
                    }
                    getOrderDetail().add(new CartModel(itemBean.getItemName(),
                            "" + itemBean.getPrice(),
                            itemBean.getItemId(),
                            itemBean.getQty(),
                            itemBean.getItemnote(),
                            addOns));
                }
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    public void ClearCart() {
        mInstance = new Cart();
    }

    public boolean isCartFull() {
        return Cart.getInstance().getOrderDetail().size() > 0;
    }
}
