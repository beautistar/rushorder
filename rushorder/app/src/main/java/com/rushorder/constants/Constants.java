package com.rushorder.constants;

import android.os.Bundle;

import com.rushorder.R;

/**
 * Created by Aleem on 12/4/2017.
 */

public class Constants {

    public static final String DATA = "data";
    public static final String ERROR = "error";
    public static final int INVALID_INDEX = -1;
    public static final int INVALID_VALUE = -1;
    public static final String SHOP_OPEN = "Status";
    public static final String MESSAGE = "message";
    public static final String DELIVERY = "delivery";
    public final static String DECIMAL_PLACE = "#0.00";
    public static final String COLLECTION = "collection";
    public static final String MULTI_SELECTION = "multi";
    public static final String SINGLE_SELECTION = "radio";
    public static final String POST_REVIEW = "postReview";
    public static final String ALL_REVIEWS = "AllReviews";
    public static final String STATUS_SUCCESS = "success";
    public static final String EMPTY_ADD_ON_NEXT_ID = "0";

    public static final String BLUE_LEVEL = "Blue Level";
    public static final String BRONZE = "Bronze";
    public static final String SILVER = "Silver";
    public static final String GOLD  = "gold";
    public static final String PLATINUM = "Platinum";
    public static final String ORDER_STATUS_DONE = "4";
    public static final String DRIVER_RIDING_STARTED = "Started";
    public static final String DRIVER_RIDING_DELIVERED = "Delivered";
    public static final String ORDER_STATUS_DELIVERED = "4";

    public final static int REPEAT = 234;
    public final static int FEED_BACK = 235;
    public final static int ITEM_CLICK = 236;

    public final static Bundle NO_BUNDLE = null;


    public static class URL {

        //Base Urls
//        public static final String BASE_URL = "http://api.lazefood.com/api/";
//        public static final String BASE_URL = "http://rushorderapis.mindslab.info/";
//        public static final String BASE_URL = "http://rushorderapis.mindslab.info/api/";

        public static final String BASE_URL = "http://apis.rushorder.online/api/";


        //       User Urls

        public static final String USER_LOGIN = BASE_URL + "token";
        public static final String USER_REGISTER = BASE_URL + "Account/Register";

        public static final String USER_SOCIAL_LOGIN = BASE_URL + "customer/sociallogin";
        public static final String FORGOT_PASSWORD = BASE_URL + "Account/ForgotPassword?";
        public static final String UPDATE_USER_INFO = BASE_URL + "customer/updateprofile";
        public static final String REGISTER_DEVICE = BASE_URL + "customer/registerDevice";

        //        Orders

        public static final String SAVE_ORDER = BASE_URL + "order/checkout";
        public static final String PRE_ORDER_DATA = BASE_URL + "ShowRestaurantTimes?";
        public static final String ORDER_HISTORY = BASE_URL + "order/history?";
        public static final String CHECK_SHOP_OPEN = BASE_URL + "IsRestaurantOpen?";
        public static final String SEARCH_POST_CODE = BASE_URL + "GetDeliveryFee?";
        public static final String SAVE_FEED_BACK = BASE_URL + "order/review";
        public static final String VALIDATE_COUPON_CODE = BASE_URL + "order/coupon?";
        public static final String SERVICE_CHARGE = BASE_URL + "ServiceCharges?";

        //       Restaurant

        public static final String CUISINES = BASE_URL + "restaurant/consines";

        public static final String MENU_ADDONS = BASE_URL + "menu/addoncat_items";

        public static final String CUSTOMER_REVIEWS = BASE_URL + "order/review?";

        public static final String RESTAURANT_MENU = BASE_URL + "menu/restaurant_menu";
        public static final String CONTACT_QUERY = BASE_URL + "ContactUs";

        public static final String RESTAURANT_MENU_DETAILS = BASE_URL + "restaurant/Menu?";


        public static final String PRIVACY_POLICY = BASE_URL + "PrivacyPolicy";

        public static final String ORDER_STATUS = BASE_URL + "order/status?";

        //        public static final String SEARCH_RESTAURANTS = BASE_URL + "restaurant/searchResturant";
//        public static final String SEARCH_RESTAURANTS = BASE_URL + "ResturantByPostCode?";
//        public static final String SEARCH_RESTAURANTS = BASE_URL + "FilterResturants?";
        public static final String SEARCH_RESTAURANTS = BASE_URL + "restaurant/Filter";



        public static final String TERMS_AND_CONDITIONS = BASE_URL + "TermsConditions";


    }

    public static class Keys {
        public static final String SORT = "sort";
        public static final String TOTAL = "total";
        public static final String CUISINE = "cuisine";
        public static final String ORDER_ID = "orderId";
        public static final String DISCOUNT = "discount";
        public static final String ORDER_NOTE = "OrderNote";
        public static final String POST_CODE = "PostCode";
        public static final String ORDER_TYPE = "orderType";
        public static final String GRAND_TOTAL = "grandTotal";
        public static final String LAT = "lat";
        public static final String LNG = "lng";

        public static final String RESPONSE_KEY = "response";
        public static final String RESTAURANT_KEY = "restaurantId";
        public static final String TOTAL_PRICE = "totalPrice";
        public static final String IS_PREORDER = "isPreOrder";
        public static final String PREORDER_TIME = "preOrderTime";
        public static final String PREORDER_DATE = "preOrderDate";
        public static final String PREORDER_NOTE = "preOrderNote";
        public static final String MENU_ADDONS_KEY = "menuAddons";
        public static final String IS_FROM_CART_KEY = "isFromCart";
        public static final String IS_REPEAT_ORDER = "isRepeatOrder";
        public static final String PAY_WITH_AMOUNT = "payWithAmount";
        public static final String ITEM_POSITION_KEY = "itemPosition";
        public static final String ITEM_PARENT_POSITION_KEY = "itemPosition";
        public static final String DELIVERY_CHARGE = "deliveryCharge";
        public static final String MINIMUM_SPENDING = "minimumSpending";
        public static final String PAGER_ITEM_POSITION_KEY = "pagerItemIndex";
    }

    public static class Tags {
        public static final String SIGN_UP = "signUp";
        public static final String SIGN_IN = "signIn";
    }

    public enum FragmentAnimation {
        SLIDE_RIGHT, SLIDE_LEFT, NONE
    }


    public static final int[] NAV_TITLE_RES_ID = new int[]{
            R.string.navdrawer_item_home,
            R.string.navdrawer_item_my_orders,
            R.string.navdrawer_item_contact_us,
            R.string.navdrawer_item_address,
            R.string.navdrawer_item_terms,
            R.string.navdrawer_item_live_chat,
            R.string.navdrawer_item_loyalty,
            0
    };
    // icons for navdrawer items (indices must correspond to above array)
    public static final int[] NAV_ICON_RES_ID = new int[]{
            R.drawable.ic_home,
            R.drawable.ic_nav_my_orders,
            R.drawable.ic_nav_contact,
            R.drawable.ic_address,
            R.drawable.ic_nav_terms,
            R.drawable.ic_chat,
            R.drawable.ic_loyalty,

            0
    };


}
