package com.rushorder.models;


public class PostCodeResponse {


    /**
     * $id : 1
     * Id : 0
     * PostCode : sr47jpp
     * PostTown : null
     * Street : null
     * mile : 0.153391162188689
     * price : 0
     */

    private String $id;
    private int Id;
    private String PostCode;
    private String PostTown;
    private String Street;
    private double mile;
    private String price;
    private String Message;
    private boolean Status;

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }


    public boolean isStatus() {
        return Status;
    }

    public void setStatus(boolean status) {
        Status = status;
    }

    public String get$id() {
        return $id;
    }

    public void set$id(String $id) {
        this.$id = $id;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getPostCode() {
        return PostCode;
    }

    public void setPostCode(String postCode) {
        PostCode = postCode;
    }

    public String getPostTown() {
        return (PostTown == null) ? "" : PostTown;

    }

    public void setPostTown(String postTown) {
        PostTown = postTown;
    }

    public String getStreet() {
        return (Street == null) ? "" : Street;
    }

    public void setStreet(String street) {
        Street = street;
    }

    public double getMile() {
        return mile;
    }

    public void setMile(double mile) {
        this.mile = mile;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}
