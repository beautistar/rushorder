package com.rushorder.fragments.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.RadioButton;
import android.widget.TextView;

import com.rushorder.R;
import com.rushorder.models.AddonCategoryModel;
//import com.lazefood.models.RestaurantDetailsResponse.DataBean.MenusBean.SubCategoriesBean.ItemsBean.AddonCategories.AddonItemBean;
import com.rushorder.models.RestaurantInfoResponse.CategoryViewModelsBean.SubCategoriesBean.ItemsBean.AddonCategoriesBean.AddonItemsBean;

import java.util.LinkedHashMap;
import java.util.List;

public class MenuAddonExpandableAdapter extends BaseExpandableListAdapter {

    private List<AddonCategoryModel> headingList;

    private LinkedHashMap<AddonCategoryModel, List<AddonItemsBean>> selectedAddonsMap = new LinkedHashMap<>();

    private LinkedHashMap<AddonCategoryModel, List<AddonItemsBean>> parentList = new LinkedHashMap<>();

    public MenuAddonExpandableAdapter(LinkedHashMap<AddonCategoryModel, List<AddonItemsBean>> parentList, List<AddonCategoryModel> headingList) {
        this.parentList = parentList;
        this.headingList = headingList;
    }


    @Override
    public int getGroupCount() {
        return headingList.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {

        return (parentList.get(headingList.get(groupPosition)) != null) ? (parentList.get(headingList.get(groupPosition)).size()) : 0;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return headingList.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return parentList.get(headingList.get(groupPosition)).get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {


        final AddonCategoryModel model = (AddonCategoryModel) getGroup(groupPosition);

        HeadingViewHolder headingViewHolder;
        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_header_row, parent, false);
            headingViewHolder = new HeadingViewHolder();

            headingViewHolder.tvHeading = (TextView) convertView.findViewById(R.id.tv_prefix);
            convertView.setTag(headingViewHolder);
        } else {
            headingViewHolder = (HeadingViewHolder) convertView.getTag();
        }
        headingViewHolder.tvHeading.setText("" + model.getName());

        return convertView;
    }

    @Override
    public View getChildView(final int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

        final AddonItemsBean model = (AddonItemsBean) getChild(groupPosition, childPosition);
        final AddonCategoryModel headerModel = (AddonCategoryModel) getGroup(groupPosition);
        ChildViewHolder childViewHolder;
        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_addon_item_row, parent, false);
            childViewHolder = new ChildViewHolder();


            childViewHolder.tvPrice = convertView.findViewById(R.id.tvPrice);
            childViewHolder.rbSelect = convertView.findViewById(R.id.rbSelect);
            childViewHolder.tvAddOnName = convertView.findViewById(R.id.tvName);
            childViewHolder.tvCurrency = convertView.findViewById(R.id.tvCurrency);

            convertView.setTag(childViewHolder);
        } else {
            childViewHolder = (ChildViewHolder) convertView.getTag();
        }


        childViewHolder.tvAddOnName.setText("" + model.getName());
        childViewHolder.tvPrice.setText("" + model.getPrice());

//        if (model.getPrice().equals("0")) {
        if (model.getPrice() == 0) {
            childViewHolder.tvPrice.setVisibility(View.GONE);
            childViewHolder.tvCurrency.setVisibility(View.GONE);
        } else {
            childViewHolder.tvPrice.setVisibility(View.VISIBLE);
            childViewHolder.tvCurrency.setVisibility(View.VISIBLE);
        }

        if (selectedAddonsMap.containsKey(headerModel)) {
            if (selectedAddonsMap.get(headerModel).contains(model)) {
                ((RadioButton) convertView.findViewById(R.id.rbSelect)).setChecked(true);
            } else {
                ((RadioButton) convertView.findViewById(R.id.rbSelect)).setChecked(false);
            }

        } else {
            ((RadioButton) convertView.findViewById(R.id.rbSelect)).setChecked(false);
        }


        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }


    private class HeadingViewHolder {
        private TextView tvHeading;
    }

    private class ChildViewHolder {
        private TextView tvAddOnName, tvPrice, tvCurrency;
        private RadioButton rbSelect;
    }

    public LinkedHashMap<AddonCategoryModel, List<AddonItemsBean>> getSelectedAddonsMap() {
        return selectedAddonsMap;
    }

    public void setSelectedAddonsMap(LinkedHashMap<AddonCategoryModel, List<AddonItemsBean>> selectedAddonsMap) {
        this.selectedAddonsMap = selectedAddonsMap;
    }


}
