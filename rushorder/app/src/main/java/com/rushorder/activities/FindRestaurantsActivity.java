package com.rushorder.activities;


import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.rushorder.R;
import com.rushorder.managers.PrefManager;
import com.rushorder.utils.Utils;

public class FindRestaurantsActivity extends BaseActivity implements View.OnClickListener {

    private String postCode = "";
    private EditText etPostCode;
    private LinearLayout llSubmit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Utils.hideStatusBar(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_restaurants);
        initViews();
        setListeners();
    }


    @Override
    protected void initViews() {
        llSubmit = findViewById(R.id.llSubmit);
        etPostCode = findViewById(R.id.etPostCode);
        super.initViews();
    }

    @Override
    protected void setListeners() {
        llSubmit.setOnClickListener(this);
        super.setListeners();
    }

    @Override
    public void onClick(View v) {
        postCode = etPostCode.getText().toString();
        if (!postCode.isEmpty()) {
            PrefManager.getInstance().saveSearchPostCode(postCode);
            startMainActivity();
        } else {
            Utils.SnackBarMessage.showSnackBarLong(parentLayout, getString(R.string.enter_post_code));
        }
    }

    private void startMainActivity() {
        finish();
        startNewActivity(MainActivity.class);
    }

}
