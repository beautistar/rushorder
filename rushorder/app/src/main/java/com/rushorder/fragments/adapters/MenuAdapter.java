package com.rushorder.fragments.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.rushorder.R;
import com.rushorder.cache.CacheData;
import com.rushorder.interfaces.ClickCallBack;
import com.rushorder.models.RestaurantInfoResponse;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Aleem on 1/27/2018.
 */

public class MenuAdapter extends RecyclerView.Adapter<MenuAdapter.Holder> implements Filterable {


    private WeakReference<ClickCallBack> callback;
    private List<RestaurantInfoResponse.CategoryViewModelsBean> menuList;
    private List<RestaurantInfoResponse.CategoryViewModelsBean> filteredMenuList;

    public MenuAdapter(ClickCallBack callback) {

        this.callback = new WeakReference<>(callback);
        this.menuList = CacheData.Menu.getInstance().getMenuResponse().getCategoryViewModels();
        this.filteredMenuList = CacheData.Menu.getInstance().getMenuResponse().getCategoryViewModels();
    }


    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_menu_row, parent, false);
        return new Holder(itemView, callback);
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        RestaurantInfoResponse.CategoryViewModelsBean model = filteredMenuList.get(position);
        holder.tvMenuTitle.setText("" + model.getName());
    }

    @Override
    public int getItemCount() {
        return filteredMenuList.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    filteredMenuList = menuList;
                } else {
                    ArrayList<RestaurantInfoResponse.CategoryViewModelsBean> filteredList = new ArrayList<>();
                    for (RestaurantInfoResponse.CategoryViewModelsBean row : menuList) {

                        if (row.getName().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }
                    filteredMenuList = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = filteredMenuList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                filteredMenuList = (List<RestaurantInfoResponse.CategoryViewModelsBean>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }


    public class Holder extends RecyclerView.ViewHolder {

        private TextView tvMenuTitle;

        public Holder(View itemView, final WeakReference<ClickCallBack> callback) {
            super(itemView);
            tvMenuTitle = itemView.findViewById(R.id.tvName);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    callback.get().callBack(filteredMenuList.get(getLayoutPosition()), getLayoutPosition());
                }
            });
        }
    }
}
