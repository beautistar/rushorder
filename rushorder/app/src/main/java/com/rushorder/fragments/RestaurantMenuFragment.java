package com.rushorder.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.rushorder.R;
import com.rushorder.cache.CacheData;
import com.rushorder.constants.Constants;
import com.rushorder.fragments.adapters.MenuAdapter;
import com.rushorder.interfaces.ClickCallBack;
import com.rushorder.utils.Utils;
import com.rushorder.views.CRecyclerView;

/**
 * Created by Aleem on 1/27/2018.
 */

public class RestaurantMenuFragment extends BaseFragment implements ClickCallBack {

    private String apiKey;
    private TextView tvEmpty;
    private MenuAdapter adapter;
    private EditText etSearchBar;
    private CRecyclerView rvMenu;
    private int itemPosition = Constants.INVALID_INDEX;


    public static RestaurantMenuFragment getInstance() {
        return new RestaurantMenuFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_menu, container, false);
        initViews(view);
        return view;
    }

    @Override
    protected void initViews(View view) {
        rvMenu = view.findViewById(R.id.rvMenu);
        tvEmpty = view.findViewById(R.id.tvEmpty);
        etSearchBar = view.findViewById(R.id.etSearchBar);
        setValues();
        setListeners();
        super.initViews(view);
    }

    @Override
    protected void setValues() {

        itemPosition = getArguments().getInt(Constants.Keys.ITEM_POSITION_KEY);
        apiKey = CacheData.Restaurants.getInstance().getRestaurantsList().get(itemPosition).getId();
        setAdapter();
        super.setValues();
    }

    private void setAdapter() {

        adapter = new MenuAdapter(this);
        rvMenu.setLayoutManager(new LinearLayoutManager(getActivity()));
//        rvMenu.setItemAnimator(new DefaultItemAnimator());
//        rvMenu.addItemDecoration(new CDividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL, 36));
        rvMenu.setEmptyView(tvEmpty);
        rvMenu.setHasFixedSize(true);
        rvMenu.setAdapter(adapter);

    }

    @Override
    protected void setListeners() {

        etSearchBar.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                adapter.getFilter().filter(s);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        super.setListeners();
    }

    @Override
    public void callBack() {

    }

    @Override
    public void callBack(Object _model, int position) {
        Utils.hideSoftKeyboard(getActivity(), etSearchBar);
        startMenuItemFragment(position);
    }


    private void startMenuItemFragment(int position) {
        Bundle bundle = new Bundle();
        bundle.putInt(Constants.Keys.ITEM_POSITION_KEY, position);
        switchFragment(MenuItemsFragment.getInstance(), bundle);
    }


//    void getMenu() {
//        Apis.Menu.getMenu(getActivity(), true, apiKey, new VolleyCallBack.JSONResponse() {
//
//            @Override
//            public void onResponse(JSONObject _response, String _tag) {
//                try {
//                    MenuResponse menuResponse = JsonTranslator.getInstance().parse(_response.toString(), MenuResponse.class);
//                    if (menuResponse.isSuccess()) {
//
//                    }
//                } catch (Exception ex) {
//                    ex.printStackTrace();
//                }
//            }
//
//            @Override
//            public void onErrorMessage(String _errorMessage) {
//                showErrorDialog("" + _errorMessage);
//            }
//
//            @Override
//            public void onErrorResponse(VolleyError _error, String _tag) {
//                showErrorDialog(getString(R.string.something_went_wrong));
//
//            }
//
//            @Override
//            public void isConnected(boolean _connected, String _tag) {
//                showErrorDialog(getString(R.string.no_internet));
//            }
//        });
//    }


}
