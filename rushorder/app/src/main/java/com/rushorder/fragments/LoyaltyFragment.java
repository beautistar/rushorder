package com.rushorder.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.rushorder.R;
import com.rushorder.managers.PrefManager;
import com.rushorder.models.SignInResponse;
import com.rushorder.utils.JsonTranslator;
import com.rushorder.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


public class LoyaltyFragment extends BaseFragment {

    public static LoyaltyFragment getInstance() {
        return new LoyaltyFragment();
    }
    private TextView tvRewardPoints,tvLevel;
    ProgressBar pbLoyaltyProgress;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_loyalty, container, false);
        initViews(view);
        setListeners();
        setValues();
        return view;
    }
    @Override
    public void onResume() {
        super.onResume();

        Log.e("CalledResume","CalledResume");
        if (PrefManager.getInstance().getBoolean(PrefManager.IS_LOGIN))
            getRewardPoints();

    }

    private void getRewardPoints() {
        pbLoyaltyProgress.setVisibility(View.VISIBLE);
        String userId = "";
        try {
            SignInResponse response = JsonTranslator.getInstance().parse(PrefManager.getInstance().getString(PrefManager.USER_INFO), SignInResponse.class);
            userId = response.getUserID();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        String url = "http://apis.rushorder.online/api/Account/GetReward?userid=" + userId;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                response -> {
                    try {
                        Log.e("responseFacebook", response);
                        JSONObject jsonObject = new JSONObject(response);
                        String point = jsonObject.getString("Point");
                        String points = "Points = " + point;
                        tvRewardPoints.setText(points);

                        int rewardPoint = Integer.valueOf(point);
                        tvLevel.setText(Utils.getLevel(rewardPoint));
                    } catch (JSONException e) {
                        pbLoyaltyProgress.setVisibility(View.GONE);
                        e.printStackTrace();
                    }
                    pbLoyaltyProgress.setVisibility(View.GONE);
                },
                error -> {
                    pbLoyaltyProgress.setVisibility(View.GONE);
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                Log.e("paramsLogin", params.toString());
                return params;
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(stringRequest);

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }

    @Override
    protected void initViews(View view) {
        super.initViews(view);
        tvRewardPoints = view.findViewById(R.id.tvRewardPoints);
        tvLevel = view.findViewById(R.id.tvLevel);
        pbLoyaltyProgress = view.findViewById(R.id.pbLoyaltyProgress);

    }

    @Override
    protected void setListeners() {
        llBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }
}
