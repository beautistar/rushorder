package com.rushorder.fragments.dialogs;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;

import com.rushorder.R;
import com.rushorder.cache.CacheData;
import com.rushorder.constants.Constants;
import com.rushorder.fragments.MenuItemsFragment;
import com.rushorder.fragments.adapters.MenuAddonExpandableAdapter;
import com.rushorder.models.AddonCategoryModel;
import com.rushorder.models.RestaurantInfoResponse.CategoryViewModelsBean.SubCategoriesBean.ItemsBean;

//import com.lazefood.models.RestaurantDetailsResponse.DataBean.MenusBean.SubCategoriesBean.ItemsBean.AddonCategories;
import com.rushorder.models.RestaurantInfoResponse.CategoryViewModelsBean.SubCategoriesBean.ItemsBean.AddonCategoriesBean;


import com.rushorder.models.RestaurantInfoResponse.CategoryViewModelsBean.SubCategoriesBean.ItemsBean.AddonCategoriesBean.AddonItemsBean;
import com.muddzdev.styleabletoastlibrary.StyleableToast;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Created by Aleem on 12/10/2017.
 */

public class MenuAddonsDialog extends DialogFragment implements View.OnClickListener {

    private EditText etNote;
    private int itemPosition;
    private ImageView ivCross;
    private ImageView ivAdd, ivMinus;
    private ExpandableListView elvAddons;
    private MenuAddonExpandableAdapter adapter;
    private TextView tvHeader, tvAddToCart, tvQty;
    private List<AddonItemsBean> selectedAddons;

    private List<AddonCategoryModel> headingList;
    private List<AddonCategoriesBean> addonCategories;

    private LinkedHashMap<AddonCategoryModel, List<AddonItemsBean>> addonMap;
    private LinkedHashMap<AddonCategoryModel, List<AddonItemsBean>> selectedAddonsMap = new LinkedHashMap<>();


    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.WRAP_CONTENT;
            dialog.getWindow().setLayout(width, height);
            dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_menu_addons, container, false);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        initViews(view);
        setValues();
        setListeners();
        return view;
    }

    private void initViews(View view) {

        tvQty = view.findViewById(R.id.tvQty);
        ivAdd = view.findViewById(R.id.ivAdd);
        etNote = view.findViewById(R.id.etNote);
        ivMinus = view.findViewById(R.id.ivMinus);
        ivCross = view.findViewById(R.id.ivCross);
        tvHeader = view.findViewById(R.id.tvHeader);
        elvAddons = view.findViewById(R.id.elvAddons);
        tvAddToCart = view.findViewById(R.id.tvAddToCart);
    }

    protected void setValues() {

        try {

            selectedAddons = new ArrayList<>();
            itemPosition = getArguments().getInt(Constants.Keys.ITEM_POSITION_KEY);
            addonCategories = ((ItemsBean) CacheData.Menu.getInstance().getMenuItems().get(itemPosition)).getAddonCategories();

//            tvHeader.setText("" + addonItemsResponse.getAddon_category().getDescription());
            formatAddonsData();


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void formatAddonsData() {

        addonMap = new LinkedHashMap<>();
        headingList = new ArrayList<>();

        for (int index = 0; index < addonCategories.size(); index++) {

            AddonCategoryModel model = new AddonCategoryModel.Builder()
                    .name(addonCategories.get(index).getName())
//                    .descp(addonCategories.get(index).getDescp())
                    .pkcode(addonCategories.get(index).getId())
                    .parent_id(addonCategories.get(index).getItem_Id())
//                    .fkrestaurant(addonCategories.get(index).getFkrestaurant())
                    .max_sellection(addonCategories.get(index).getMaxSelection())
                    .build();

            headingList.add(model);
            addonMap.put(model, addonCategories.get(index).getAddonItems());
        }

        setAddonAdapter(addonMap, headingList);
    }

    private void setAddonAdapter(LinkedHashMap<AddonCategoryModel, List<AddonItemsBean>> addonMap, List<AddonCategoryModel> headingList) {
        adapter = new MenuAddonExpandableAdapter(addonMap, headingList);
        elvAddons.setAdapter(adapter);

        expandGroups();
    }

    private void expandGroups() {

        for (int i = 0; i < adapter.getGroupCount(); i++) {
            elvAddons.expandGroup(i);
        }
    }

    private void setListeners() {

        ivAdd.setOnClickListener(this);
        ivCross.setOnClickListener(this);
        ivMinus.setOnClickListener(this);
        tvAddToCart.setOnClickListener(this);

        elvAddons.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {

                AddonCategoryModel headerModel = headingList.get(groupPosition);
                AddonItemsBean childModel = addonMap.get(headerModel).get(childPosition);

                if (selectedAddonsMap.containsKey(headerModel)) {

                    addOrRemoveItem(headerModel, childModel, selectedAddonsMap, groupPosition);

                } else {

                    selectedAddons = new ArrayList<>();
                    selectedAddons.add(childModel);
                    selectedAddonsMap.put(headerModel, selectedAddons);

                    if (selectedAddonsMap.get(headerModel).size() == headerModel.getMax_sellection()) {
                        elvAddons.collapseGroup(groupPosition);
                    }
                }

                adapter.setSelectedAddonsMap(selectedAddonsMap);
                adapter.notifyDataSetChanged();

                return false;
            }
        });
    }

    private void addOrRemoveItem(AddonCategoryModel headerModel, AddonItemsBean childModel, LinkedHashMap<AddonCategoryModel, List<AddonItemsBean>> selectedAddonsMap, int groupPosition) {

        if (selectedAddonsMap.get(headerModel).contains(childModel)) {
            selectedAddonsMap.get(headerModel).remove(childModel);
            if (selectedAddonsMap.get(headerModel).size() == 0) {
                selectedAddons.remove(headerModel);
            }
        } else {

            if (headerModel.getMax_sellection() != 0 && selectedAddonsMap.get(headerModel).size() < headerModel.getMax_sellection()) {
                selectedAddonsMap.get(headerModel).add(childModel);

                if (selectedAddonsMap.get(headerModel).size() == headerModel.getMax_sellection()) {
                    elvAddons.collapseGroup(groupPosition);
                }

            } else {

                if (headerModel.getMax_sellection() == 0) {
                    selectedAddonsMap.get(headerModel).add(childModel);
                } else {
                    StyleableToast.makeText(getActivity(), "You can not select more than " + headerModel.getMax_sellection(), R.style.ErrorToastStyle).show();

                }
            }


        }

    }


    @Override
    public void onClick(View v) {

        if (v == ivAdd) {
            addItemQuantity();
        } else if (v == ivMinus) {
            subtractItemQuantity();
        } else if (v == ivCross) {
            dismiss();
        } else {
            addToCart();
        }


//
//        if (v == addToCart) {
//
//            if (selectedAddons.size() > 0 || addonMaxCount == 0) {
//                if (!nextAddOnItemId.equals(Constants.EMPTY_ADD_ON_NEXT_ID)) {
//                    ((MenuItemsFragment) getParentFragment()).addToCartWithRecursion(nextAddOnItemId, selectedAddons);
//                } else {
//                    ((MenuItemsFragment) getParentFragment()).AddItemToCart(selectedAddons);
//                }
//                dismiss();
//            } else {
//                StyleableToast.makeText(getActivity(), "Please select items then process to next", R.style.ErrorToastStyle).show();
//            }
//        } else {
//            ((MenuItemsFragment) getParentFragment()).resetCartModel();
//            dismiss();
//        }
    }

    private void addToCart() {

        if (allRequiredSelected()) {
            if (isMaxSelected()) {
                int qty = Integer.parseInt(tvQty.getText().toString());
                ((MenuItemsFragment) getParentFragment()).AddItemToCart(selectedAddons, qty, etNote.getText().toString());
                dismiss();
            }
        }


    }

    private boolean allRequiredSelected() {

        for (AddonCategoryModel model : addonMap.keySet()) {
            if (model.getMax_sellection() > 0) {
                if (!selectedAddonsMap.containsKey(model)) {
                    StyleableToast.makeText(getActivity(), "You must select " + model.getMax_sellection() + " items from " + model.getName(), R.style.ErrorToastStyle).show();
                    return false;
                }
            }
        }

        return true;
    }


    private boolean isMaxSelected() {


        selectedAddons = new ArrayList<>();

        for (AddonCategoryModel model : selectedAddonsMap.keySet()) {

            selectedAddons.addAll(selectedAddonsMap.get(model));

            if (selectedAddonsMap.get(model).size() < model.getMax_sellection()) {
                StyleableToast.makeText(getActivity(), "You must select " + model.getMax_sellection() + " items from " + model.getName(), R.style.ErrorToastStyle).show();
                selectedAddons.clear();
                break;
            }
        }

        if (selectedAddons.size() > 0) {
            return true;
        } else {
            return false;
        }


    }

    private void subtractItemQuantity() {

        int itemQuantity = Integer.parseInt(tvQty.getText().toString());

        if (itemQuantity > 1) {
            itemQuantity--;
        }

        updateQtyValue(itemQuantity);
    }

    private void updateQtyValue(int itemQuantity) {
        tvQty.setText("" + itemQuantity);
    }

    private void addItemQuantity() {
        int itemQuantity = Integer.parseInt(tvQty.getText().toString());
        itemQuantity++;
        updateQtyValue(itemQuantity);
    }


}
