package com.rushorder.models;

/**
 * Created by Aleem on 12/20/2017.
 */

public class OrderPostResponse {


    /**
     * $id : 1
     * Status : true
     * Message : Successfully Order Submitted!
     * OrderId : 1785
     */

    private String $id;
    private boolean Status;
    private String Message;
    private int OrderId;

    public String get$id() {
        return $id;
    }

    public void set$id(String $id) {
        this.$id = $id;
    }

    public boolean isStatus() {
        return Status;
    }

    public void setStatus(boolean Status) {
        this.Status = Status;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public int getOrderId() {
        return OrderId;
    }

    public void setOrderId(int OrderId) {
        this.OrderId = OrderId;
    }
}
