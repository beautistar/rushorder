package com.rushorder.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.rushorder.R;
import com.rushorder.activities.MainActivity;
import com.rushorder.cache.CacheData;
import com.rushorder.constants.Constants;
import com.rushorder.location.LocationAutoCompleteView;
import com.rushorder.location.interfaces.OnQueryCompleteListener;
import com.rushorder.managers.PrefManager;
import com.rushorder.models.SignInResponse;
import com.rushorder.utils.JsonTranslator;
import com.rushorder.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Aleem on 2/12/18.
 */

public class HomeFragment extends BaseFragment implements View.OnClickListener, OnQueryCompleteListener, OnMapReadyCallback {


    private GoogleMap map;
    private double lat, lng;
    private Place selectedPlace;
    private LinearLayout llSubmit;
    private LocationAutoCompleteView autoCompleteLocation;
    MainActivity mainActivity;

    public static HomeFragment getInstance() {
        return new HomeFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_place_picker, container, false);
        initViews(view);
        setValues();
        setListeners();
        return view;
    }

    @Override
    protected void initViews(View view) {
        mainActivity = (MainActivity)getActivity();
        autoCompleteLocation = view.findViewById(R.id.autocomplete_view);
        llSubmit = view.findViewById(R.id.llSubmit);
        super.initViews(view);
    }

    @Override
    protected void setListeners() {
        llSubmit.setOnClickListener(this);
        super.setListeners();
    }

    @Override
    public void onResume() {
        super.onResume();

//        Log.e("CalledResume","CalledResume");
//        if (PrefManager.getInstance().getBoolean(PrefManager.IS_LOGIN))
//            getRewardPoints();

    }


    @Override
    protected void setValues() {
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        super.setValues();
    }

    @Override
    public void onClick(View v) {
        checkLocation();

    }
    private void checkLocation() {
        if (selectedPlace != null) {

            try {
                CacheData.Restaurants.getInstance().clearRestaurantList();
                moveToRestaurantList();

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {

            Toast.makeText(getActivity(), "Please select location", Toast.LENGTH_SHORT).show();

//            Utils.SnackBarMessage.showSnackBarLong(parentLayout, getString(R.string.enter_post_code));

        }
    }


    private void checkPostCode() {
//        postCode = etPostCode.getText().toString();
//        if (!postCode.isEmpty()) {
//            PrefManager.getInstance().saveSearchPostCode(postCode);
//            CacheData.Restaurants.getInstance().clearRestaurantList();
//            moveToRestaurantList();
//        } else {
//            Utils.SnackBarMessage.showSnackBarLong(parentLayout, getString(R.string.enter_post_code));
//        }
    }

    private void moveToRestaurantList() {
        Bundle bundle = new Bundle();
        bundle.putString(Constants.Keys.LAT, "" + lat);
        bundle.putString(Constants.Keys.LNG, "" + lng);

        RestaurantListFragment fragment = RestaurantListFragment.getInstance();
        fragment.setArguments(bundle);
        getParentActivity().switchSelectedMenuFragment(fragment);

//        getParentActivity().selectBottomMenu(R.id.action_nav_restaurants);
    }


    @Override
    public void onTextClear() {
        map.clear();
    }

    @Override
    public void onPlaceSelected(Place selectedPlace) {
        try {
            map.clear();

            this.selectedPlace = selectedPlace;
            this.lat = selectedPlace.getLatLng().latitude;
            this.lng = selectedPlace.getLatLng().longitude;
            map.addMarker(new MarkerOptions().position(selectedPlace.getLatLng()));
            map.moveCamera(CameraUpdateFactory.newLatLngZoom(selectedPlace.getLatLng(), 16));

//            PrefManager.getInstance().saveLocation(selectedPlace.getLatLng().latitude, selectedPlace.getLatLng().longitude);
            hideKeyboard();
        } catch (Exception ex) {
            ex.printStackTrace();
        }


    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        map = googleMap;
        LatLng madrid = new LatLng(29.378586, 47.990341);
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(madrid, 16));
        autoCompleteLocation.setOnQueryCompleteListener(this);
    }

    private void hideKeyboard() {
        // Check if no view has focus:
        View view = getActivity().getCurrentFocus();
        if (view != null) {
            InputMethodManager inputManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }
}
