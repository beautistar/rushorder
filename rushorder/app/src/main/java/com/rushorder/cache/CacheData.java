package com.rushorder.cache;


import android.support.v4.util.ArrayMap;

import com.rushorder.models.CuisinesResponse;
import com.rushorder.models.HistoryHeader;
import com.rushorder.models.HistoryResponse;
import com.rushorder.models.OrderHistoryItemHeader;
import com.rushorder.models.OrderHistoryResponse;
import com.rushorder.models.RestaurantInfoResponse;
import com.rushorder.models.RestaurantsResponse;

import java.util.ArrayList;
import java.util.List;

import tellh.com.stickyheaderview_rv.adapter.DataBean;


/**
 * Created by Aleem on 12/4/2017.
 */

public class CacheData {


    public static class Menu {

        private static Menu mInstance;
        private ArrayList<DataBean> menuItems;
        private RestaurantInfoResponse menuResponse;
        private ArrayList<ArrayList<RestaurantInfoResponse.CategoryViewModelsBean>> parentMenu;

        Menu() {
            parentMenu = new ArrayList<>();
        }


        public void setResponse(RestaurantInfoResponse response) {
            this.menuResponse = response;
            formatMenu(response);
        }

        public static synchronized Menu getInstance() {
            if (mInstance == null) {
                mInstance = new Menu();
            }
            return mInstance;
        }

        public RestaurantInfoResponse getMenuResponse() {
            return menuResponse;
        }

        public ArrayList<ArrayList<RestaurantInfoResponse.CategoryViewModelsBean>> getParentMenu() {
            return parentMenu;
        }


        private void formatMenu(RestaurantInfoResponse response) {
            int outerIndex;
            int menuTotalSize = response.getCategoryViewModels().size();

            ArrayList<RestaurantInfoResponse.CategoryViewModelsBean> menus = new ArrayList<>();
            for (outerIndex = 0; outerIndex < menuTotalSize; outerIndex++) {

                menus.add(response.getCategoryViewModels().get(outerIndex));

                if (menus.size() == 9) {
                    parentMenu.add(menus);
                    menus = new ArrayList<>();
                }
                if (outerIndex == menuTotalSize - 1) {
                    if (!parentMenu.contains(menus)) {
                        parentMenu.add(menus);
                    }
                }
            }
        }

        public ArrayList<DataBean> getMenuItems() {
            return menuItems;
        }

        public void setMenuItems(ArrayList<DataBean> menuItems) {
            this.menuItems = menuItems;
        }

        public void clearMenu() {
            mInstance = new Menu();
        }

    }


    public static class Order {

        private static Order mInstance;
        private ArrayList<DataBean> orderHistoryList = new ArrayList<>();

        private ArrayMap<String, List<HistoryResponse.ItemBean>> orderChilds = new ArrayMap<>();
        private ArrayMap<OrderHistoryItemHeader, List<OrderHistoryResponse.OrdersBean.CartBean.ChildsBean>> parentList = new ArrayMap<>();
        private List<OrderHistoryResponse.OrdersBean.CartBean.ChildsBean> childList = new ArrayList<>();


        public static synchronized Order getInstance() {
            if (mInstance == null) {
                mInstance = new Order();
            }
            return mInstance;
        }

        public ArrayMap<String, List<HistoryResponse.ItemBean>> getOrderChilds() {
            return orderChilds;
        }

        public void setOrderChilds(ArrayMap<String, List<HistoryResponse.ItemBean>> orderChilds) {
            this.orderChilds = orderChilds;
        }

        public ArrayMap<OrderHistoryItemHeader, List<OrderHistoryResponse.OrdersBean.CartBean.ChildsBean>> getParentList() {
            return parentList;
        }

        public void setParentList(ArrayMap<OrderHistoryItemHeader, List<OrderHistoryResponse.OrdersBean.CartBean.ChildsBean>> parentList) {
            this.parentList = parentList;
        }

        public List<OrderHistoryResponse.OrdersBean.CartBean.ChildsBean> getChildList() {
            return childList;
        }

        public void setChildList(List<OrderHistoryResponse.OrdersBean.CartBean.ChildsBean> childList) {
            this.childList = childList;
        }


        public ArrayList<DataBean> getOrderHistoryList() {
            return orderHistoryList;
        }

//        public ArrayList<DataBean> getOrderHistoryList(OrderHistoryResponse response) {
//            orderHistoryList = new ArrayList<>();
//
//            for (OrderHistoryResponse.OrdersBean order : response.getOrders()) {
//
//                OrderHistoryHeader itemHeader = setOrderInfo(order);
//                orderHistoryList.add(itemHeader);
//                int headerIndex = orderHistoryList.size() - 1;
//                for (OrderHistoryResponse.OrdersBean.CartBean cart : order.getOrderDetail()) {
//                    int cartItemSize = (order.getOrderDetail() != null) ? order.getOrderDetail().size() : 0;
//                    addCartItem(cart, cartItemSize, headerIndex);
//                }
//
//                orderChilds.put(itemHeader.getId(), order.getOrderDetail());
//            }
//            return orderHistoryList;
//        }


        public ArrayList<DataBean> getOrderHistoryList(List<HistoryResponse> orderList) {
            orderHistoryList = new ArrayList<>();

            for (HistoryResponse order : orderList) {

                HistoryHeader itemHeader = setOrderInfo(order);

                orderHistoryList.add(itemHeader);
                int headerIndex = orderHistoryList.size() - 1;
                for (HistoryResponse.ItemBean itemBean : order.getOrderDetails()) {
                    int cartItemSize = (order.getOrderDetails() != null) ? order.getOrderDetails().size() : 0;
                    addCartItem(itemBean, cartItemSize, headerIndex);
                }

                orderChilds.put("" + itemHeader.getPkcode(), order.getOrderDetails());
            }
            return orderHistoryList;
        }

        private void addCartItem(HistoryResponse.ItemBean itemBean, int cartItemSize, int headerIndex) {
            itemBean.setCartItemSize(cartItemSize);
            itemBean.setItem_parent_index("" + headerIndex);
            orderHistoryList.add(itemBean);
        }


//        private OrderHistoryHeader setOrderInfo(OrderHistoryResponse.OrdersBean order) {
//
//            return new OrderHistoryHeader.IBinder()
//                    .setId(order.getId())
//                    .setRestaurant_id(order.getRestaurant_id())
//                    .setUser_id(order.getUser_id())
//                    .setTotalAmount(order.getTotalAmount())
//                    .setDescription(order.getDescription())
//                    .setCoupon(order.getCoupon())
//                    .setCoupon_value(order.getCoupon_value())
//                    .setCoupon_type(order.getCoupon_type())
//                    .setCoupon_applies_to(order.getCoupon_applies_to())
//                    .setCoupon_categories(order.getCoupon_categories())
//                    .setDiscountAmount(order.getDiscountAmount())
//                    .setDiscount_desc(order.getDiscount_desc())
//                    .setStatus(order.getStatus())
//                    .setDate(order.getDate())
//                    .setOrder_count(order.getOrder_count())
//                    .setType(order.getType())
//                    .setIs_mobile_order(order.getIs_mobile_order())
//                    .setOrder_stats(order.getOrder_stats())
//                    .setPaymentType(order.getPaymentType())
//                    .setPayment_status(order.getPayment_status())
//                    .setDelivery_charges(order.getDelivery_charges())
//                    .setDelivery_type(order.getDelivery_type())
//                    .setCard_surcharge(order.getCard_surcharge())
//                    .setAddon(order.getAddon())
//                    .setAddby(order.getAddby())
//                    .setCarry_bag(order.getCarry_bag())
//                    .setOrder_format_status(order.getOrder_format_status())
//                    .setCustomer_name(order.getCustomer_name())
//                    .setCustomer_phone(order.getCustomer_phone())
//                    .setMerchant_responce(order.getMerchant_responce())
//                    .setIs_preorder(order.getIs_preorder())
//                    .setPreorder_date(order.getPreorder_date())
//                    .setPreOrderDate(order.getPreOrderDate())
//                    .setPrint_delay(order.getPrint_delay())
//                    .setEatinn_table_id(order.getEatinn_table_id())
//                    .setEatinn_table_name(order.getEatinn_table_name())
//                    .setEatinn_floorname(order.getEatinn_floorname())
//                    .setEatinn_payment_status(order.getEatinn_payment_status())
//                    .setBill_persons(order.getBill_persons())
//                    .setOrder_description(order.getOrder_description())
//                    .setApi_key(order.getResturent_detail().getApi_key())
//                    .setShow_feedback_button(order.isShow_feedback_button())
//                    .setResturent_detail(order.getResturent_detail())
//                    .setChildSize((order.getOrderDetail() != null) ? order.getOrderDetail().size() : 0)
//                    .build();
//        }
//    }


        private HistoryHeader setOrderInfo(HistoryResponse order) {

            return new HistoryHeader.Builder()
                    .pkcode(order.getId())
                    .fkrest(order.getRestaurant_id())
                    .usercmnt(order.getUsercmnt())
                    .tamount(order.getTotal())
                    .fkcoupon(order.getFkcoupon())
                    .discountamt(order.getDiscount())
                    .discoutcmnt(order.getDiscoutcmnt())
                    .fkordersts(order.getFkordersts())
                    .orderdate(order.getOrderdate())
                    .dailytrno(order.getDailytrno())
                    .fkordertypsorc(order.getFkordertypsorc())
                    .paymnttype(order.getPaymnttype())
                    .ordertype(order.getOrderType())
                    .fkmilechrg(order.getFkmilechrg())
                    .servicechrg(order.getServicechrg())
                    .carybagamt(order.getCarybagamt())
                    .preorder(order.isIs_preorder())
                    .prorderdate(order.getProrderdate())
                    .fkfloor(order.getFkfloor())
                    .fktable(order.getFktable())
                    .isdelete(order.isIsdelete())
                    .ServingTime(order.getServingTime())
                    .prordertime(order.getPrordertime())
                    .build();
        }
    }

    public static class Restaurants {

        private static Restaurants mInstance;
        private List<RestaurantsResponse> restaurantsList = new ArrayList<>();

        public static synchronized Restaurants getInstance() {
            if (mInstance == null) {
                mInstance = new Restaurants();
            }
            return mInstance;
        }

        public List<RestaurantsResponse> getRestaurantsList() {
            return restaurantsList;
        }

        public void setRestaurantsList(List<RestaurantsResponse> restaurantsList) {
            this.restaurantsList = restaurantsList;
        }

        public void clearRestaurantList() {
            if (restaurantsList != null) {
                restaurantsList.clear();
            }
        }
    }


    public static class RestaurantsDetails {

        private static RestaurantsDetails mInstance;
        private RestaurantInfoResponse restaurantDetailsResponse;
        private RestaurantInfoResponse.ReataurantTimingsBean details;

        public static synchronized RestaurantsDetails getInstance() {
            if (mInstance == null) {
                mInstance = new RestaurantsDetails();
            }
            return mInstance;
        }


        public RestaurantInfoResponse.ReataurantTimingsBean getDetails() {
            return details;
        }

        public void setDetails(RestaurantInfoResponse.ReataurantTimingsBean details) {
            this.details = details;
        }

        public RestaurantInfoResponse getRestaurantDetailsResponse() {
            return restaurantDetailsResponse;
        }

        public void setRestaurantDetailsResponse(RestaurantInfoResponse restaurantDetailsResponse) {
            this.restaurantDetailsResponse = restaurantDetailsResponse;

            CacheData.Menu.getInstance().setResponse(restaurantDetailsResponse);
        }
    }


    public static class Filters {

        private static Filters mInstance;
        private List<CuisinesResponse> cuisinesList = new ArrayList<>();

        public static synchronized Filters getInstance() {
            if (mInstance == null) {
                mInstance = new Filters();
            }
            return mInstance;
        }

        public List<CuisinesResponse> getCuisinesList() {
            return cuisinesList;
        }

        public void setCuisinesList(List<CuisinesResponse> cuisinesList) {
            this.cuisinesList = cuisinesList;

            if (this.cuisinesList != null && this.cuisinesList.size() > 0) {
                CuisinesResponse cuisinesResponse = new CuisinesResponse(0, "All");
                cuisinesList.add(0, cuisinesResponse);
            }
        }

        public void clearCuisinesList() {
            if (cuisinesList != null) {
                cuisinesList.clear();
            }
        }
    }


}
