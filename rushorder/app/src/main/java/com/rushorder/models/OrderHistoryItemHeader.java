package com.rushorder.models;


import com.rushorder.R;

import tellh.com.stickyheaderview_rv.adapter.DataBean;
import tellh.com.stickyheaderview_rv.adapter.StickyHeaderViewAdapter;

/**
 * Created by Aleem on 12/17/2017.
 */

public class OrderHistoryItemHeader extends DataBean {

    private String id;
    private String order_id;
    private String item_id;
    private String item_name;
    private String item_comments;
    private String item_qty;
    private String item_price;
    private String item_parent;
    private String item_parent_index;
    private String item_index;
    private String status;
    private String bill_no;
    private int childsSize;

    public OrderHistoryItemHeader(IBinder iBinder) {

        this.id = iBinder.id;
        this.order_id = iBinder.order_id;
        this.item_id = iBinder.item_id;
        this.item_name = iBinder.item_name;
        this.item_comments = iBinder.item_comments;
        this.item_qty = iBinder.item_qty;
        this.item_price = iBinder.item_price;
        this.item_parent = iBinder.item_parent;
        this.item_parent_index = iBinder.item_parent_index;
        this.item_index = iBinder.item_index;
        this.status = iBinder.status;
        this.bill_no = iBinder.bill_no;
        this.childsSize = iBinder.childsSize;
    }


    public String getId() {
        return id;
    }

    public String getOrder_id() {
        return order_id;
    }

    public String getItem_id() {
        return item_id;
    }

    public String getItem_name() {
        return item_name;
    }

    public String getItem_comments() {
        return item_comments;
    }

    public String getItem_qty() {
        return item_qty;
    }

    public String getItem_price() {
        return item_price;
    }

    public String getItem_parent() {
        return item_parent;
    }

    public String getItem_parent_index() {
        return item_parent_index;
    }

    public String getItem_index() {
        return item_index;
    }

    public String getStatus() {
        return status;
    }

    public String getBill_no() {
        return bill_no;
    }

    public int getChildsSize() {
        return childsSize;
    }

    @Override
    public int getItemLayoutId(StickyHeaderViewAdapter adapter) {
        return R.layout.rv_order_history_header;
    }

    @Override
    public boolean shouldSticky() {
        return true;
    }

    public static class IBinder {

        private String id;
        private String order_id;
        private String item_id;
        private String item_name;
        private String item_comments;
        private String item_qty;
        private String item_price;
        private String item_parent;
        private String item_parent_index;
        private String item_index;
        private String status;
        private String bill_no;
        private int childsSize;


        public IBinder setId(String id) {
            this.id = id;
            return this;
        }

        public IBinder setOrder_id(String order_id) {
            this.order_id = order_id;
            return this;
        }

        public IBinder setItem_id(String item_id) {
            this.item_id = item_id;
            return this;
        }

        public IBinder setItem_name(String item_name) {
            this.item_name = item_name;
            return this;
        }

        public IBinder setItem_comments(String item_comments) {
            this.item_comments = item_comments;
            return this;

        }

        public IBinder setItem_qty(String item_qty) {
            this.item_qty = item_qty;
            return this;
        }

        public IBinder setItem_price(String item_price) {
            this.item_price = item_price;
            return this;
        }

        public IBinder setItem_parent(String item_parent) {
            this.item_parent = item_parent;
            return this;
        }

        public IBinder setItem_parent_index(String item_parent_index) {
            this.item_parent_index = item_parent_index;
            return this;
        }

        public IBinder setItem_index(String item_index) {
            this.item_index = item_index;
            return this;
        }

        public IBinder setStatus(String status) {
            this.status = status;
            return this;
        }

        public IBinder setBill_no(String bill_no) {
            this.bill_no = bill_no;
            return this;
        }

        public IBinder setChildsSize(int childsSize) {
            this.childsSize = childsSize;
            return this;
        }

        public OrderHistoryItemHeader build() {
            return new OrderHistoryItemHeader(this);
        }
    }
}
