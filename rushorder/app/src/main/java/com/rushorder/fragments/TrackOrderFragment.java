package com.rushorder.fragments;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.rushorder.R;
import com.rushorder.activities.TrackOrderMapsActivity;
import com.rushorder.apis.Apis;
import com.rushorder.app.AppController;
import com.rushorder.constants.Constants;
import com.rushorder.interfaces.VolleyCallBack;
import com.rushorder.managers.PrefManager;
import com.rushorder.models.SignInResponse;
import com.rushorder.models.TrackOrdersResponse;
import com.rushorder.utils.JsonTranslator;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class TrackOrderFragment extends BaseFragment {

    private String orderId = "";
    private TextView tvOrderId;
    private TextView tvStatus;
    private TextView tvNumber;
    private TextView tvOrderTime;


    @BindView(R.id.btnTrackOrder)
    Button btnTrackOrder;
    @BindView(R.id.pbTrackOrder)
    ProgressBar pbTrackOrder;

    public static TrackOrderFragment getInstance() {
        return new TrackOrderFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_track_order, container, false);
        ButterKnife.bind(this, view);
        initViews(view);
        setListeners();
        setValues();
        getOrderTrackingStatus();
        return view;
    }

    @OnClick(R.id.btnTrackOrder)
    public void onTrackOrderClick()
    {
                startActivity(new Intent(getActivity(), TrackOrderMapsActivity.class)
        .putExtra("orderId",orderId+"")
        );

    }
    @Override
    protected void initViews(View view) {
        super.initViews(view);
        tvNumber = view.findViewById(R.id.tvNumber);
        tvOrderId = view.findViewById(R.id.tvOrderId);
        tvStatus = view.findViewById(R.id.tvOrderStatus);
        tvOrderTime = view.findViewById(R.id.tvOrderTime);

    }

    @Override
    protected void setListeners() {
        llBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        tvNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri number = Uri.parse("tel:+965-99315517");
                Intent callIntent = new Intent(Intent.ACTION_DIAL, number);
                startActivity(callIntent);
            }
        });
        super.setListeners();
    }

    @Override
    protected void setValues() {
        if (getArguments() != null) {
            orderId = getArguments().getString(Constants.Keys.ORDER_ID);
            trackOrder();
        }

        super.setValues();
    }

    void trackOrder() {


        Apis.Restaurant.trackOrder(getActivity(), true, "", orderId, new VolleyCallBack.StringResponse() {
            @Override
            public void onResponse(String _response, String _tag) {
                try {
                    Log.e("track_order_response",_response);
                    TrackOrdersResponse trackOrdersResponse = JsonTranslator.getInstance().parse(_response, TrackOrdersResponse.class);
                    setUpDetails(trackOrdersResponse);


                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            @Override
            public void onErrorMessage(String _errorMessage) {
                showErrorDialog("" + _errorMessage);
            }

            @Override
            public void onErrorResponse(VolleyError _error, String _tag) {
                showErrorDialog("" + getString(R.string.something_went_wrong));
            }

            @Override
            public void isConnected(boolean _connected, String _tag) {
                showErrorDialog("" + getString(R.string.no_internet));
            }
        });

    }

    private void setUpDetails(TrackOrdersResponse trackOrdersResponse) {
        tvStatus.setText("" + trackOrdersResponse.getStatus());
        tvOrderId.setText("" + orderId);
        tvOrderTime.setText("" + trackOrdersResponse.getDeliveryTime());

    }

    private void getOrderTrackingStatus() {

        String userId = "";
        try {
            SignInResponse response = JsonTranslator.getInstance().parse(PrefManager.getInstance().getString(PrefManager.USER_INFO), SignInResponse.class);
            userId = response.getUserID();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        String url = Apis.GetDriverLocation +"userId="+userId+ "&orderId=" + orderId;

        Log.e("url",url);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                response -> {
                    try {
                        Log.e("GetDriverLocation", response);
                        JSONObject jsonObject = new JSONObject(response);
                        String success = jsonObject.getString("success");

                        if (success.equals("1")) {
                            String data = jsonObject.getString("data");
                            JSONObject dataObj = new JSONObject(data);
                            String Status = dataObj.getString("Status");
                            if (Status.equals(Constants.DRIVER_RIDING_STARTED)) {
                                btnTrackOrder.setEnabled(true);
                                btnTrackOrder.setText(Objects.requireNonNull(getActivity()).getString(R.string.track_order));
                            }else {
                                btnTrackOrder.setEnabled(false);
                                btnTrackOrder.setText(Objects.requireNonNull(getActivity()).getString(R.string.tracking_not_available));
                            }
                        } else {
                            btnTrackOrder.setEnabled(false);
                            btnTrackOrder.setText(Objects.requireNonNull(getActivity()).getString(R.string.tracking_not_available));

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                },
                error -> {
                }) {

        };
        AppController.setmRequestQueue(stringRequest);
    }

}
