package com.rushorder.interfaces;


import com.rushorder.models.HistoryHeader;
import com.rushorder.models.HistoryResponse;

/**
 * Created by Aleem on 12/17/2017.
 */

public interface OrderHistoryCallBack {

    void headerCallback(HistoryHeader headerModel, int Position, int type);

    void childCallback(HistoryResponse.ItemBean childModel, int Position, int type);
}
