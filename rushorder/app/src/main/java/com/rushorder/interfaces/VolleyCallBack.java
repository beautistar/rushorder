package com.rushorder.interfaces;


import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONObject;


public class VolleyCallBack {

    /**
     * Volley JSON Response
     */
    public static  abstract class JSONResponse implements Callbacks {

        public void onResponse(String _response, String _tag) {
        }

        public void onResponse(JSONArray _response, String _tag) {
        }

    }

    /**
     * Volley String Response
     */
    public static  abstract class StringResponse implements Callbacks {
        public void onResponse(JSONObject _response, String _tag) {
        }

        public void onResponse(JSONArray _response, String _tag) {
        }

    }

    /**
     * Volley Array Response
     */
    public static abstract class ArrayResponse implements Callbacks {
        public void onResponse(JSONObject _response, String _tag) {
        }

        public void onResponse(String _response, String _tag) {
        }

    }


    public interface Callbacks {

        public void onResponse(JSONArray _response, String _tag);

        public void onResponse(JSONObject _response, String _tag);

        public  void onResponse(String _response, String _tag);

        public void onErrorMessage(String _errorMessage);

        public  void onErrorResponse(VolleyError _error, String _tag);

        public void isConnected(boolean _connected, String _tag);
    }

}
