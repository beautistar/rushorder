package com.rushorder.notification;


import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;


import com.rushorder.R;
import com.rushorder.activities.MainActivity;
import com.rushorder.constants.Constants;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Random;


public class FireBaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "Top Taste";

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // If the application is in the foreground handle both data and notification messages here.
        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.


        String title = remoteMessage.getData().get("title");
        String body = (remoteMessage.getData().get(Constants.MESSAGE) != null) ? remoteMessage.getData().get(Constants.MESSAGE) : "";

        sendNotification(getString(R.string.app_name), body);
    }
    // [END receive_message]

    /**
     * @param title
     * @param body
     */
    private void sendNotification(String title, String body) {
        Intent intent = new Intent(this, MainActivity.class);
//        Bundle b = new Bundle();
//
//        if (!postId.equals("")) {
//            b.putString("notification", "post");
//            b.putString("post_id", postId);
//        } else {
//            b.putString("notification", "notification");
//            b.putString("post_id", postId);
//        }
//        intent.putExtras(b);

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, "CH_ID")
                .setContentTitle(title)
                .setContentText(body)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        boolean useSilhouette = Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP;
        if (useSilhouette) {
            try {
//                notificationBuilder.setSmallIcon(R.mipmap.ic_stat_app_logo); // need to get the logo
                notificationBuilder.setSmallIcon(R.mipmap.ic_launcher);
            } catch (Exception e) {
                notificationBuilder.setSmallIcon(R.mipmap.ic_launcher);
            }
        } else {
            notificationBuilder.setSmallIcon(R.mipmap.ic_launcher);
        }

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        //Random id number for notification
        Random random = new Random();
        int m = random.nextInt(9999 - 1000) + 1000;

        notificationManager.notify(m/* ID of notification */, notificationBuilder.build());
    }
}