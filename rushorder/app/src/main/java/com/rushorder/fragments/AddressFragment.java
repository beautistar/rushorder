package com.rushorder.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.rushorder.R;
import com.rushorder.managers.PrefManager;

/**
 * Created by Aleem on 2/24/2018.
 */

public class AddressFragment extends BaseFragment implements View.OnClickListener {


    private ImageView ivMenu;
    private LinearLayout llSave;
    private EditText etStreet, etTown, etPhone, etDoorNumber;
    private String street = "", town = "", phone = "", doorNumber = "";

    public static AddressFragment getInstance() {
        return new AddressFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_address, container, false);
        initViews(view);
        setListeners();
        setValues();
        return view;
    }

    @Override
    protected void initViews(View view) {

        llSave = view.findViewById(R.id.llSave);
        ivMenu = view.findViewById(R.id.ivMenu);
        etTown = view.findViewById(R.id.etTown);
        etPhone = view.findViewById(R.id.etPhone);
        etStreet = view.findViewById(R.id.etStreet);
        etDoorNumber = view.findViewById(R.id.etDoorNumber);
        super.initViews(view);
    }

    @Override
    protected void setListeners() {

        ivMenu.setOnClickListener(this);
        llSave.setOnClickListener(this);
        super.setListeners();
    }

    @Override
    protected void setValues() {
        setFormVeriables();
        super.setValues();
    }

    @Override
    public void onClick(View view) {


        if (view == ivMenu) {
            getParentActivity().openDrawer();
        } else {

            town = etTown.getText().toString();
            phone = etPhone.getText().toString();
            street = etStreet.getText().toString();
            doorNumber = etDoorNumber.getText().toString();


            if (validateFields()) {
                saveInformation();

            }

        }


    }

    private void saveInformation() {

        PrefManager.getInstance().saveAddress(street, town, doorNumber, phone);
        showSuccessDialog("Saved Successfully");
    }

    private void setFormVeriables() {
        etStreet.setText("" + PrefManager.getInstance().getString(PrefManager.STREET));
        etTown.setText("" + PrefManager.getInstance().getString(PrefManager.TOWN));
        etPhone.setText("" + PrefManager.getInstance().getString(PrefManager.PHONE));
        etDoorNumber.setText("" + PrefManager.getInstance().getString(PrefManager.DOOR_NUMBER));

    }

    private boolean validateFields() {
        if (town.isEmpty() || street.isEmpty() || doorNumber.isEmpty() || phone.isEmpty()) {
            showErrorDialog("Please fill all the fields");
            return false;

        } else {
            return true;
        }

    }


}


