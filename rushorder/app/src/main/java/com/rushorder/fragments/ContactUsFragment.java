package com.rushorder.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.rushorder.R;
import com.rushorder.apis.Apis;
import com.rushorder.interfaces.VolleyCallBack;
import com.rushorder.models.RestaurantDetailsResponse;
import com.rushorder.utils.JsonTranslator;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Aleem on 1/27/2018.
 */

public class ContactUsFragment extends BaseFragment implements OnMapReadyCallback, View.OnClickListener {

    private ImageView ivMenu;
    private GoogleMap googleMap;
    private SupportMapFragment mapFragment;
    private RestaurantDetailsResponse detailResponse;
    private ImageView ivFacebook, ivInstagram, ivTwitter, ivPintrest;
    private TextView tvMondayTime, tvTuesdayTime, tvWednesdayTime, tvThursdayTime, tvFridayTime, tvSaturdayTime, tvSundayTime, tvPhoneNumber, tvAddress;

    public static ContactUsFragment getInstance() {
        return new ContactUsFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_contact_us, container, false);
        initViews(view);
        return view;
    }

    @Override
    protected void initViews(View view) {

        ivMenu = view.findViewById(R.id.ivMenu);
        tvAddress = view.findViewById(R.id.tvAddress);
        ivTwitter = view.findViewById(R.id.ivTwitter);
        ivFacebook = view.findViewById(R.id.ivFacebook);
        tvPhoneNumber = view.findViewById(R.id.tvPhone);
        ivPintrest = view.findViewById(R.id.ivPintrest);
        ivInstagram = view.findViewById(R.id.ivInstagram);
        tvMondayTime = view.findViewById(R.id.tvMondayTime);
        tvFridayTime = view.findViewById(R.id.tvFridayTime);
        tvSundayTime = view.findViewById(R.id.tvSundayTime);
        tvTuesdayTime = view.findViewById(R.id.tvTuesdayTime);
        tvSaturdayTime = view.findViewById(R.id.tvSaturdayTime);
        tvThursdayTime = view.findViewById(R.id.tvThursdayTime);
        tvWednesdayTime = view.findViewById(R.id.tvWednesdayTime);


        setMapFragment();
        setListeners();

        super.initViews(view);
    }


    void setMapFragment() {

        mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.mapsFragment);
        mapFragment.getMapAsync(this);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        this.googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        this.googleMap.getUiSettings().setZoomControlsEnabled(true);
        this.googleMap.getUiSettings().setZoomGesturesEnabled(true);
        this.googleMap.getUiSettings().setCompassEnabled(true);
        this.googleMap.getUiSettings().setMyLocationButtonEnabled(true);
        getRestaurantDetails();
    }

    @Override
    protected void setListeners() {
        ivTwitter.setOnClickListener(this);
        ivFacebook.setOnClickListener(this);
        ivPintrest.setOnClickListener(this);
        ivInstagram.setOnClickListener(this);
        ivMenu.setOnClickListener(this);
        super.setListeners();
    }

    void getRestaurantDetails() {

        Apis.Restaurant.getRestaurantDetails(getActivity(), true, "", new VolleyCallBack.JSONResponse() {
            @Override
            public void onResponse(JSONObject _response, String _tag) {
                try {
                    detailResponse = JsonTranslator.getInstance().parse(_response.toString(), RestaurantDetailsResponse.class);
                    if (detailResponse.isSuccess()) {
                        setOpeningTimings(detailResponse.getRestaurant_details().getOpen_timings());
                        setMapLocation(detailResponse.getRestaurant_details());
                        setContactDetail(detailResponse.getRestaurant_details());
                    } else {
                        showErrorDialog(detailResponse.getData().getMessage());
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorMessage(String _errorMessage) {
                showErrorDialog(_errorMessage);
            }

            @Override
            public void onErrorResponse(VolleyError _error, String _tag) {
                showErrorDialog(getString(R.string.something_went_wrong));
            }

            @Override
            public void isConnected(boolean _connected, String _tag) {
                showErrorDialog(getString(R.string.no_internet));
            }
        });

    }

    private void setContactDetail(RestaurantDetailsResponse.RestaurantDetailsBean restaurant_details) {
        tvPhoneNumber.setText("" + restaurant_details.getLand_line());
        tvAddress.setText("" + restaurant_details.getAddress());
    }


    private void setOpeningTimings(ArrayList<RestaurantDetailsResponse.RestaurantDetailsBean.OpenTimingsBean> open_timings) {

        for (int index = 0; index < open_timings.size(); index++) {

            RestaurantDetailsResponse.RestaurantDetailsBean.OpenTimingsBean model = open_timings.get(index);

            if (open_timings.get(index).equals("Monday")) {
                tvMondayTime.setText("" + model.getOpening_time() + " -- " + model.getClosing_time());

            } else if (model.getDay().equals("Tuesday")) {
                tvTuesdayTime.setText("" + model.getOpening_time() + " -- " + model.getClosing_time());

            } else if (model.getDay().equals("Wednesday")) {
                tvWednesdayTime.setText("" + model.getOpening_time() + " -- " + model.getClosing_time());

            } else if (model.getDay().equals("Thursday")) {
                tvThursdayTime.setText("" + model.getOpening_time() + " -- " + model.getClosing_time());

            } else if (model.getDay().equals("Friday")) {
                tvFridayTime.setText("" + model.getOpening_time() + " -- " + model.getClosing_time());

            } else if (model.getDay().equals("Saturday")) {
                tvSaturdayTime.setText("" + model.getOpening_time() + " -- " + model.getClosing_time());

            } else if (model.getDay().equals("Sunday")) {
                tvSundayTime.setText("" + model.getOpening_time() + " -- " + model.getClosing_time());
            }
        }
    }

    private void setMapLocation(RestaurantDetailsResponse.RestaurantDetailsBean restaurant_details) {
        try {

            double latitude = Double.parseDouble(restaurant_details.getShop_latitude());
            double longitude = Double.parseDouble(restaurant_details.getShop_longitude());
            googleMap.addMarker(new MarkerOptions().position(new LatLng(latitude, longitude)).title("Top Taste"));
            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitude, longitude), 15.0f));

//            googleMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(latitude, longitude)));
        } catch (Exception ex) {
            ex.printStackTrace();

        }
    }


    @Override
    public void onClick(View view) {

        if (view == ivMenu) {
            getParentActivity().openDrawer();
        } else {
            Intent intent;
            if (view == ivFacebook) {
                intent = new Intent(Intent.ACTION_VIEW, Uri.parse(detailResponse.getRestaurant_details().getFacebook()));
            } else if (view == ivInstagram) {
                intent = new Intent(Intent.ACTION_VIEW, Uri.parse(detailResponse.getRestaurant_details().getInstagram()));
            } else if (view == ivTwitter) {
                intent = new Intent(Intent.ACTION_VIEW, Uri.parse(detailResponse.getRestaurant_details().getTwitter()));
            } else {
                intent = new Intent(Intent.ACTION_VIEW, Uri.parse(detailResponse.getRestaurant_details().getPinterest()));
            }

            startActivity(intent);

        }

    }
}
