package com.rushorder.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.rushorder.R;
import com.rushorder.interfaces.ClickCallBack;

public class MapFragment extends BaseFragment implements
        ClickCallBack, OnMapReadyCallback, View.OnClickListener {

    private GoogleMap googleMap;
    private SupportMapFragment mapFragment;

    public static MapFragment getInstance() {
        return new MapFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_map, container, false);
        initViews(view);
        setListeners();
        setValues();
        return view;
    }

    @Override
    protected void initViews(View view) {
        super.initViews(view);
    }

    @Override
    protected void setValues() {
        setMapFragment();
        super.setValues();
    }

    void setMapFragment() {
        mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.mapsFragment);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        this.googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        this.googleMap.getUiSettings().setCompassEnabled(true);
        this.googleMap.getUiSettings().setZoomControlsEnabled(true);
        this.googleMap.getUiSettings().setZoomGesturesEnabled(true);
        this.googleMap.getUiSettings().setMyLocationButtonEnabled(true);

    }

    @Override
    protected void setListeners() {

        llBack.setOnClickListener(this);

        super.setListeners();
    }


    @Override
    public void callBack() {

    }

    @Override
    public void callBack(Object _model, int position) {

    }

    @Override
    public void onClick(View v) {
        if (v == llBack) {
            onBackPressed();
        } else {
            switchFragment(RestaurantListFragment.getInstance());
        }

    }

}
