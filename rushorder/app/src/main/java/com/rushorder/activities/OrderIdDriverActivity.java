package com.rushorder.activities;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.rushorder.R;
import com.rushorder.apis.Apis;
import com.rushorder.app.AppController;
import com.rushorder.constants.Constants;
import com.rushorder.driver.config.SessionManagerDriver;
import com.rushorder.driver.track_driver.DriverLocationActivity;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;

public class OrderIdDriverActivity extends BaseActivity {
    public static final int requestReadContactPermissionCode = 10001;
    @BindView(R.id.etOrderId)
    EditText etOrderId;
    @BindView(R.id.tvDriverName)
    TextView tvDriverName;
    SessionManagerDriver sessionManagerDriver;
    boolean locationPermission;

    @BindView(R.id.pbOrderId)
    ProgressBar pbOrderId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_id_driver);
        ButterKnife.bind(this);
        initView();
        setDriverValues();
    }

    private void setDriverValues() {
        tvDriverName.setText(sessionManagerDriver.getEmail());
    }

    private void initView() {
        sessionManagerDriver = new SessionManagerDriver(this);
    }


    @OnClick(R.id.llEnterOrderId)
    public void onClickOrderId() {
        if (!(etOrderId.getText().length() > 0)) {
            showSnackbar(etOrderId, "Enter OrderId");
        } else {
            Log.e("checkingPerm", checkingPermissionIsEnabledOrNot() + "");
            if (checkingPermissionIsEnabledOrNot()) {

                checkOrderAvailable();
            } else {
                requestMultiplePermission();
            }

        }
    }

    @OnClick(R.id.tvLogoutDriver)
    public void onSignoutClicked() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setCancelable(false);
        dialog.setTitle("Alert");
        dialog.setMessage("Do you want to Logout?");
        dialog.setPositiveButton("Yes", (dialog1, id1) -> {
            sessionManagerDriver.logoutUser();
            finish();

        })
                .setNegativeButton("No", (dialog12, which) -> dialog12.dismiss());
        final AlertDialog alert = dialog.create();
        alert.setCancelable(true);
        alert.show();

    }

    public boolean checkingPermissionIsEnabledOrNot() {
        int FirstPermissionResult = ContextCompat.checkSelfPermission(this, ACCESS_FINE_LOCATION);
        return FirstPermissionResult == PackageManager.PERMISSION_GRANTED;

    }

    private void requestMultiplePermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]
                    {
                            ACCESS_FINE_LOCATION
                    }, requestReadContactPermissionCode);
        }

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case requestReadContactPermissionCode:
                locationPermission = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                if (locationPermission) {
                } else {
//                    requestMultiplePermission();
                }

                break;

        }
    }

    private void checkOrderAvailable() {
        pbOrderId.setVisibility(View.VISIBLE);
        String url = Apis.CheckOrderAvailable + "orderId=" + etOrderId.getText().toString();
        Log.e("url", url);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                response -> {
                    try {
                        Log.e("CheckOrderAvailable", response);
                        JSONObject jsonObject = new JSONObject(response);
                        String success = jsonObject.getString("success");

                        if (success.equals("1")) {
//                            String data = jsonObject.getString("data");
//                            JSONObject dataObj = new JSONObject(data);
                            String status = jsonObject.getString("driverStatus");
                            if (status.equals(Constants.DRIVER_RIDING_DELIVERED))
                                showSnackbar(etOrderId, "Order Already Delivered. Please Check order details");
                            else {

                                setDriverRidingStatus();
                            }

                        } else {
                            showSnackbar(etOrderId, "Enter valid Order Id");
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    pbOrderId.setVisibility(View.GONE);
                },
                error -> {
                    pbOrderId.setVisibility(View.GONE);
                }) {

        };
        AppController.setmRequestQueue(stringRequest);
    }

    private void setDriverRidingStatus() {
        pbOrderId.setVisibility(View.VISIBLE);

        String url = Apis.ChangeDriverStatus + "userId=" + sessionManagerDriver.getUserId() + "&orderId=" + etOrderId.getText().toString() + "&status=" + Constants.DRIVER_RIDING_STARTED;
        Log.e("url", url);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                response -> {
                    try {
                        Log.e("ChangeDriverStatus", response);
                        JSONObject jsonObject = new JSONObject(response);
                        String success = jsonObject.getString("success");

                        if (success.equals("1")) {

//                            String data = jsonObject.getString("data");
//                            JSONObject dataObj = new JSONObject(data);
//                            String status = dataObj.getString("Status");
//                            if (status.equals(Constants.DRIVER_RIDING_DELIVERED))
//                                showSnackbar(etOrderId, "Order Already Delivered. Please Check order details");
//                            else {

                            startActivity(new Intent(this, DriverLocationActivity.class)
                                    .putExtra("orderId", etOrderId.getText().toString())
                            );

                            etOrderId.setText("");
//                            }


                        } else {
                            showSnackbar(etOrderId, "Not able to start ride. Please try again");
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    pbOrderId.setVisibility(View.GONE);
                },
                error -> {
                    pbOrderId.setVisibility(View.GONE);
                }) {

        };
        AppController.setmRequestQueue(stringRequest);
    }

}
