package com.rushorder.models;

import java.util.List;

/**
 * Created by Aleem on 12/10/2017.
 */

public class AddonItemsResponse {


    private AddonCategoryBean addon_category;
    private List<ItemsBean> items;

    public AddonCategoryBean getAddon_category() {
        return addon_category;
    }

    public void setAddon_category(AddonCategoryBean addon_category) {
        this.addon_category = addon_category;
    }

    public List<ItemsBean> getItems() {
        return items;
    }

    public void setItems(List<ItemsBean> items) {
        this.items = items;
    }

    public static class AddonCategoryBean {
        /**
         * id : 46
         * addon_name : PD10:2nd Pizza List
         * description : Please select your second pizza
         * next_move_id : 60
         * list_type : radio
         */

        private String id;
        private String addon_name;
        private String description;
        private String next_move_id;
        private String list_type;
        private int max_sellection;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getAddon_name() {
            return addon_name;
        }

        public void setAddon_name(String addon_name) {
            this.addon_name = addon_name;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getNext_move_id() {
            return next_move_id;
        }

        public void setNext_move_id(String next_move_id) {
            this.next_move_id = next_move_id;
        }

        public String getList_type() {
            return list_type;
        }

        public void setList_type(String list_type) {
            this.list_type = list_type;
        }

        public int getMax_sellection() {
            return max_sellection;
        }

        public void setMax_sellection(int max_sellection) {
            this.max_sellection = max_sellection;
        }
    }

    public static class ItemsBean {
        /**
         * id : 1670
         * name : 2nd: Margherita
         * price : 0
         * type : radio
         * addon_cat_id : 46
         * next_move_id : 60
         * sort_order : 1
         */

        private String id;
        private String name;
        private String price;
        private String type;
        private String addon_cat_id;
        private String next_move_id;
        private String sort_order;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getAddon_cat_id() {
            return addon_cat_id;
        }

        public void setAddon_cat_id(String addon_cat_id) {
            this.addon_cat_id = addon_cat_id;
        }

        public String getNext_move_id() {
            return next_move_id;
        }

        public void setNext_move_id(String next_move_id) {
            this.next_move_id = next_move_id;
        }

        public String getSort_order() {
            return sort_order;
        }

        public void setSort_order(String sort_order) {
            this.sort_order = sort_order;
        }
    }
}
