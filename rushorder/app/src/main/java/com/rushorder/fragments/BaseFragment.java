package com.rushorder.fragments;


import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.LinearLayout;

import com.awesomedialog.blennersilva.awesomedialoglibrary.AwesomeErrorDialog;
import com.awesomedialog.blennersilva.awesomedialoglibrary.AwesomeSuccessDialog;
import com.awesomedialog.blennersilva.awesomedialoglibrary.interfaces.Closure;
import com.rushorder.R;
import com.rushorder.activities.MainActivity;
import com.rushorder.app.AppController;
import com.rushorder.constants.Constants;



public class BaseFragment extends Fragment {
    private Dialog dialog;
    private MainActivity activity;
    protected LinearLayout llBack;
    protected CoordinatorLayout parentLayout;


    protected void initViews(View view) {
        llBack = view.findViewById(R.id.llBack);
        parentLayout = view.findViewById(R.id.parentLayout);
    }

    protected void setListeners() {
    }

    protected void setValues() {
    }

    protected void showSuccessDialog(String message) {
        dialog = new AwesomeSuccessDialog(AppController.getCurrentActivity()).setPositiveButtonText(getString(R.string.dialog_ok_button)).setTitle(Constants.STATUS_SUCCESS.toUpperCase())
                .setMessage(message).setCancelable(true).setPositiveButtonClick(new Closure() {
                    @Override
                    public void exec() {
                        dialog.hide();
                    }
                }).show();
    }

    protected void showErrorDialog(String message) {
        dialog = new AwesomeErrorDialog(AppController.getCurrentActivity()).setButtonText(getString(R.string.dialog_ok_button)).setTitle(Constants.ERROR.toUpperCase())
                .setMessage(message).setCancelable(true).setErrorButtonClick(new Closure() {
                    @Override
                    public void exec() {
                        dialog.hide();
                    }
                }).show();
    }

    protected void hideDialog() {
        dialog.hide();
    }

    protected void onBackPressed() {
        activity.onBackPressed();

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.activity = (MainActivity) activity;
        //        this.activity = AppController.getCurrentActivity();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.activity = (MainActivity) context;
//        this.activity = AppController.getCurrentActivity();
    }

    protected void switchFragment(Fragment fragment, Bundle bundle) {
        MainActivity activity = (MainActivity) AppController.getCurrentActivity();

        if (bundle != null) {
            fragment.setArguments(bundle);
        }

        if (activity != null) {
            activity.switchFragmentWithBackStack(fragment);
        }
    }

    protected void switchFragment(Fragment fragment) {

        if (AppController.getCurrentActivity() instanceof MainActivity) {
            MainActivity activity = (MainActivity) AppController.getCurrentActivity();
            activity.switchFragmentWithBackStack(fragment);

        }


    }

    protected void setTitle(String title) {
//        ((ChatActivity) AppController.getCurrentActivity()).setToolBarTitle(title);
    }

    protected void clearAllFragments() {

        if (AppController.getCurrentActivity() instanceof MainActivity) {
            MainActivity activity = (MainActivity) AppController.getCurrentActivity();
            activity.clearAllFragments();

        }


    }


    protected MainActivity getParentActivity() {
        return activity;
    }


    protected <T> void startNewActivity(Class<T> activity) {
        Intent intent = new Intent(getActivity(), activity);
        startActivity(intent);
    }

    protected <T> void startNewActivity(Class<T> activity, Bundle bundle) {
        Intent intent = new Intent(getActivity(), activity);
        intent.putExtras(bundle);
        startActivity(intent);
    }
}
