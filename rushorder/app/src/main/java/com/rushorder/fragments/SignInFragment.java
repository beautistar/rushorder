package com.rushorder.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.rushorder.R;
import com.rushorder.activities.DriverLoginActivity;
import com.rushorder.activities.MainActivity;
import com.rushorder.apis.Apis;
import com.rushorder.app.AppController;
import com.rushorder.constants.Constants;
import com.rushorder.interfaces.VolleyCallBack;
import com.rushorder.managers.PrefManager;
import com.rushorder.utils.AlertHelper;
import com.rushorder.utils.FieldsValidator;
import com.rushorder.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.List;


public class SignInFragment extends BaseFragment implements View.OnClickListener {
    private String userEmail;
    private String password;
    private AlertHelper loader;
    private boolean isFromCart;
    private FirebaseAuth mAuth;
    private LoginButton loginButton;
    private boolean isSocial = false;
    private ProfileTracker profileTracker;
    private SignInButton gmailSignInButton;
    private LinearLayout llLogin, llFlogin;
    private TextView tvSignUp, tvForgotPass;
    private CallbackManager callbackManager;
    private EditText etUserEmail, etPassword;
    private static final int RC_SIGN_IN = 123;
    private ImageView ivGmailLogin, ivFbLogin;
    private AccessTokenTracker accessTokenTracker;
    private GoogleSignInClient mGoogleSignInClient;
    private String socialId = "", socialFName = "", socialLName = "", socialDob = "", socialEmail = "", type;

    public static SignInFragment getInstance() {
        return new SignInFragment();
    }

    private TextView tvDriverLogin;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sign_in, container, false);
        initViews(view);
        return view;
    }


    @Override
    protected void initViews(View view) {
        llLogin = view.findViewById(R.id.llLogin);
        etUserEmail = view.findViewById(R.id.etEmail);
        tvSignUp = view.findViewById(R.id.tvSignUpLink);
        etPassword = view.findViewById(R.id.etPassword);
        tvForgotPass = view.findViewById(R.id.tvForgotPass);
        loginButton = view.findViewById(R.id.loginFaceBook_button);
        gmailSignInButton = view.findViewById(R.id.gmailSignInButton);
        tvDriverLogin = view.findViewById(R.id.tvDriverLogin);
        setListeners();
        setValues();
        super.initViews(view);
    }


    @Override
    protected void setValues() {
        callbackManager = CallbackManager.Factory.create();

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        mGoogleSignInClient = GoogleSignIn.getClient(getActivity(), gso);
        mAuth = FirebaseAuth.getInstance();

        if (getArguments() != null && getArguments().containsKey(Constants.Keys.IS_FROM_CART_KEY)) {
            isFromCart = getArguments().getBoolean(Constants.Keys.IS_FROM_CART_KEY);
        }
        super.setValues();
    }

    @Override
    protected void setListeners() {

        llLogin.setOnClickListener(this);
        tvSignUp.setOnClickListener(this);
        tvForgotPass.setOnClickListener(this);
        tvDriverLogin.setOnClickListener(this);
        super.setListeners();
    }


    @Override
    public void onClick(View v) {
        if (v == llLogin) {
            getUserInfo();
        } else if (v == tvDriverLogin) {
            startDriverLogin();
        } else if (v == tvSignUp) {
            startSignUpFragment();
        } else if (v == ivFbLogin) {
            handleFacebookConnect();
        } else if (v == ivGmailLogin) {
            handleGmailLogin();
        } else {
            startForgotPasswordFragment();
        }
    }
    private void startDriverLogin() {
        startActivity(new Intent(getActivity(), DriverLoginActivity.class));
        getActivity().finish();
    }


    private void handleGmailLogin() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }


    private void getUserInfo() {
        userEmail = etUserEmail.getText().toString();
        password = etPassword.getText().toString();

        if (!userEmail.isEmpty() && !password.isEmpty()) {
            if (validateFields()) {
                loginUser();
            }
        } else {
            Utils.SnackBarMessage.showSnackBarShort(parentLayout, getString(R.string.please_fill_empty_fields));
        }
    }

    private boolean validateFields() {
        if (!FieldsValidator.getInstance().validateEmail(userEmail)) {
            Utils.SnackBarMessage.showSnackBarShort(parentLayout, getString(R.string.please_enter_valid_email));
            return false;
        }
        return true;
    }

    private void loginUser() {


        Apis.User.signIn(getActivity(), true, userEmail, password, "", new VolleyCallBack.StringResponse() {

            @Override
            public void onResponse(String _response, String _tag) {
                try {
                    Log.e("login_response", _response);
                    PrefManager.getInstance().saveUserInfo(_response, _response);
                    showSuccessDialog("Successfully logged in");
                    startUserLoginConfiguration();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            @Override
            public void onErrorMessage(String _errorMessage) {
                showErrorDialog("" + _errorMessage);
            }

            @Override
            public void onErrorResponse(VolleyError _error, String _tag) {
                showErrorDialog(getString(R.string.something_went_wrong));
            }

            @Override
            public void isConnected(boolean _connected, String _tag) {
                showErrorDialog(getString(R.string.no_internet));
            }
        });


    }

    private void startUserLoginConfiguration() {
        MainActivity activity = (MainActivity) AppController.getCurrentActivity();
        if (activity != null) {
            activity.checkUserLogin();
//            activity.onBackPressed();
            if (isFromCart) {
                activity.onBackPressed();
            } else {
                activity.clearAllFragments();
            }

        }

    }


    private void handleFacebookConnect() {


        List<String> permissionNeeds = Arrays.asList("user_birthday", "public_profile", "email");
        loginButton.setReadPermissions(permissionNeeds);
        loginButton.setFragment(this);
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                loader = AlertHelper.showProgress(getActivity(), true);
                GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {

                        try {
                            if (object.has("id"))
                                socialId = object.getString("id");
                            if (object.has("name"))
                                socialFName = object.getString("first_name");
                            socialLName = object.getString("last_name");
                            if (object.has("email"))
                                socialEmail = object.getString("email");
                            if (object.has("birthday"))
                                socialDob = object.getString("birthday");
                            type = "facebook";

                            performSocialLogin();
                            startFacebookProfileTracker();

                        } catch (JSONException e) {
                            loader.dismissProgress();
                            Toast.makeText(getActivity(), e.toString(), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,first_name,last_name,email,gender,birthday");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
                loader.dismissProgress();
                Toast.makeText(getActivity(), "Cancelled", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(FacebookException error) {
                loader.dismissProgress();
                Toast.makeText(getActivity(), error.toString(), Toast.LENGTH_SHORT).show();
            }

        });

        loginButton.performClick();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                loader = AlertHelper.showProgress(getActivity(), true);
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = task.getResult(ApiException.class);
                firebaseAuthWithGoogle(account);
            } catch (ApiException e) {
                // Google Sign In failed, update UI appropriately
                loader.dismissProgress();
                Toast.makeText(getActivity(), "Cancelled", Toast.LENGTH_SHORT).show();
//                showErrorDialog(getString(R.string.something_went_wrong));
                e.printStackTrace();
            }
        } else {
            callbackManager.onActivityResult(requestCode, resultCode, data);

        }

    }


    void startFacebookProfileTracker() {

        accessTokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(AccessToken oldToken, AccessToken newToken) {
                if (newToken == null) {
//                    ((ChatActivity) AppController.getCurrentActivity()).logoutUser();
                }
            }
        };

        profileTracker = new ProfileTracker() {
            @Override
            protected void onCurrentProfileChanged(Profile oldProfile, Profile newProfile) {


            }
        };
        accessTokenTracker.startTracking();
        profileTracker.startTracking();
    }


    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {
        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(getActivity(), new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
//                            Log.d(TAG, "signInWithCredential:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            if (user != null) {

                                socialId = user.getUid();
                                socialEmail = user.getEmail();
                                socialFName = user.getDisplayName();
                                socialLName = user.getDisplayName();
                                type = "gmail";

                                performSocialLogin();
                            }

//                            updateUI(user);
                        } else {
                            loader.dismissProgress();
                            task.getException();
                            // If sign in fails, display a message to the user.
//                            Log.w(TAG, "signInWithCredential:failure", task.getException());

                        }

                    }
                });
    }


    void performSocialLogin() {

        Apis.User.socialLogin(getActivity(), false, socialId, socialEmail, socialFName, socialLName, type, new VolleyCallBack.JSONResponse() {

            @Override
            public void onResponse(JSONObject _response, String _tag) {
                try {
                    loader.dismissProgress();
                    if (_response.getBoolean(Constants.STATUS_SUCCESS)) {
                        showSuccessDialog("Successfully logged in");
                        PrefManager.getInstance().saveUserInfo(_response.toString(), true);
                        startUserLoginConfiguration();
                    }

                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            @Override
            public void onErrorMessage(String _errorMessage) {

                loader.dismissProgress();
                showErrorDialog("" + _errorMessage);

            }

            @Override
            public void onErrorResponse(VolleyError _error, String _tag) {
                loader.dismissProgress();
                showErrorDialog(getString(R.string.something_went_wrong));

            }

            @Override
            public void isConnected(boolean _connected, String _tag) {
                loader.dismissProgress();
                showErrorDialog(getString(R.string.no_internet));
            }
        });


    }

    void startSignUpFragment() {
        MainActivity activity = (MainActivity) AppController.getCurrentActivity();
        if (activity != null) {
            Bundle bundle = new Bundle();
            bundle.putBoolean(Constants.Keys.IS_FROM_CART_KEY, isFromCart);
            SignUpFragment fragment = SignUpFragment.getInstance();
            fragment.setArguments(bundle);
            activity.switchFragmentWithBackStack(fragment);
        }
    }

    private void startForgotPasswordFragment() {
        MainActivity activity = (MainActivity) AppController.getCurrentActivity();
        if (activity != null) {
            activity.switchFragmentWithBackStack(ForgotPasswordFragment.getInstance());
        }
    }

}
