package com.rushorder.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.rushorder.R;
import com.rushorder.apis.Apis;
import com.rushorder.cache.CacheData;
import com.rushorder.constants.Constants;
import com.rushorder.fragments.adapters.CustomerReviewsAdapter;
import com.rushorder.interfaces.ClickCallBack;
import com.rushorder.interfaces.VolleyCallBack;
import com.rushorder.models.CustomerReviewsResponse;
import com.rushorder.utils.JsonTranslator;
import com.rushorder.utils.PaginationScrollListener;
import com.iarcuschin.simpleratingbar.SimpleRatingBar;

import org.json.JSONArray;

import java.util.List;

/**
 * Created by Aleem on 1/27/2018.
 */

public class RestaurantReviewsFragment extends BaseFragment implements ClickCallBack, View.OnClickListener {

    private int itemPosition;
    private ProgressBar pbLoader;
    private RecyclerView rvReviews;
    private TextView tvOverAllRating;
    private SimpleRatingBar rbOverAllrating;
    private TextView tvEmpty, tvReviewsCount;
    private CustomerReviewsAdapter adapter;


    /**
     * For RecyclerView Adapter Pagination
     */
    private int currentPage = 0;
    private int TOTAL_PAGES = 1000;
    private boolean isLoading = false;
    private boolean isLastPage = false;
    private static final int PAGE_START = 0;

    private boolean performNextApiCall = true;
    private static final int FILTER_REQUEST_CODE = 905;
    private LinearLayoutManager linearLayoutManager;

    public static RestaurantReviewsFragment getInstance() {
        return new RestaurantReviewsFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_reviews, container, false);
        initViews(view);
        setListeners();
        setValues();
        return view;
    }

    @Override
    protected void initViews(View view) {

        tvEmpty = view.findViewById(R.id.tvEmpty);
        rvReviews = view.findViewById(R.id.rvReviews);
        pbLoader = view.findViewById(R.id.pbLoader);

        tvReviewsCount = view.findViewById(R.id.tvReviewsCount);
        rbOverAllrating = view.findViewById(R.id.rbOverAllRating);
        tvOverAllRating = view.findViewById(R.id.tvOverAllRating);

        rbOverAllrating.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });

//        tvAllReviews = view.findViewById(R.id.tvAllReviews);
//        tvPostReview = view.findViewById(R.id.tvPostReview);
//        llPostReview = view.findViewById(R.id.llPostReview);
//        llAllReviewSelector = view.findViewById(R.id.llAllReviewSelector);
//        llPostReviewSelector = view.findViewById(R.id.llPostReviewSelector);

        super.initViews(view);
    }

    @Override
    protected void setListeners() {
//        llAllReviewSelector.setOnClickListener(this);
//        llPostReviewSelector.setOnClickListener(this);

        super.setListeners();
    }

    @Override
    public void onClick(View v) {
//        if (v == llAllReviewSelector) {
//            showAllReviews();
//        } else if (v == llPostReviewSelector) {
//            showPostReview();
//        }
    }

    @Override
    protected void setValues() {
        itemPosition = getArguments().getInt(Constants.Keys.ITEM_POSITION_KEY);
//        setAdapter();
        resetState();
        loadFirstPage();

        super.setValues();
    }
//
//    private void showAllReviews() {
//
//        tvAllReviews.setTextColor(getResources().getColor(R.color.white));
//        tvPostReview.setTextColor(getResources().getColor(R.color.colorPrimary));
//        llAllReviewSelector.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.left_selected_shape, null));
//        llPostReviewSelector.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.right_unselected_shape, null));
//        rvReviews.setVisibility(View.VISIBLE);
//        llPostReview.setVisibility(View.GONE);
//    }
//
//    private void showPostReview() {
//
//        tvAllReviews.setTextColor(getResources().getColor(R.color.colorPrimary));
//        tvPostReview.setTextColor(getResources().getColor(R.color.white));
//        llAllReviewSelector.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.left_unselected_shape, null));
//        llPostReviewSelector.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.right_selected_shape, null));
//        tvEmpty.setVisibility(View.GONE);
//        rvReviews.setVisibility(View.GONE);
//        llPostReview.setVisibility(View.VISIBLE);
//
//    }


    void setAdapter(List<CustomerReviewsResponse.ReviewsBean> reviewList) {

        adapter = new CustomerReviewsAdapter(this, reviewList);
        linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        rvReviews.setLayoutManager(linearLayoutManager);
        rvReviews.setAdapter(adapter);

//        addScrollListener();

    }


    private void addScrollListener() {

        rvReviews.addOnScrollListener(new PaginationScrollListener(linearLayoutManager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                currentPage += 1;
                if (performNextApiCall) {
//                    loadNextPage();
                }
            }

            @Override
            public int getTotalPageCount() {
                return TOTAL_PAGES;
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });

    }


    void loadFirstPage() {

        String restaurantId = CacheData.Restaurants.getInstance().getRestaurantsList().get(itemPosition).getId();

        Apis.Restaurant.getCustomerReviews(getActivity(), false, "", restaurantId, "" + currentPage, new VolleyCallBack.ArrayResponse() {
            @Override
            public void onResponse(JSONArray _response, String _tag) {
                try {

                    checkProgressbar();

                    List<CustomerReviewsResponse.ReviewsBean> reviewList = JsonTranslator.getInstance().parseReviewsJsonToList(_response.toString());
                    setAdapter(reviewList);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorMessage(String _errorMessage) {
                showErrorDialog(_errorMessage);
            }

            @Override
            public void onErrorResponse(VolleyError _error, String _tag) {
                showErrorDialog(getString(R.string.something_went_wrong));
            }

            @Override
            public void isConnected(boolean _connected, String _tag) {
                showErrorDialog(getString(R.string.no_internet));
            }
        });

    }


//    void loadNextPage() {
//
//        String restaurantId = CacheData.Restaurants.getInstance().getRestaurantsList().get(itemPosition).getId();
//
//        Apis.Restaurant.getCustomerReviews(getActivity(), false, "", restaurantId, "" + currentPage, new VolleyCallBack.JSONResponse() {
//            @Override
//            public void onResponse(JSONObject _response, String _tag) {
//                try {
//
//                    checkProgressbar();
//
//
//                    CustomerReviewsResponse customerReviewsResponse = JsonTranslator.getInstance().parse(_response.toString(), CustomerReviewsResponse.class);
//
//                    if (customerReviewsResponse.getReviews().size() < 1) {
//                        performNextApiCall = false;
//                    }
//                    adapter.removeLoadingFooter();
//                    isLoading = false;
//
//
//                    if (customerReviewsResponse.isHave_data()) {
//                        adapter.addAll(customerReviewsResponse.getReviews());
//                        if (currentPage <= TOTAL_PAGES) {
//                            adapter.addLoadingFooter();
//                        } else {
//                            isLastPage = true;
//                        }
//                    } else {
//
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//
//            @Override
//            public void onErrorMessage(String _errorMessage) {
//                removeLoader();
//
//            }
//
//            @Override
//            public void onErrorResponse(VolleyError _error, String _tag) {
//                removeLoader();
//            }
//
//            @Override
//            public void isConnected(boolean _connected, String _tag) {
//                adapter.showRetry(true, getString(R.string.no_internet));
//            }
//        });
//
//    }


    private void checkProgressbar() {
        if (pbLoader.getVisibility() == View.VISIBLE) {
            pbLoader.setVisibility(View.GONE);
        } else if (tvEmpty.getVisibility() == View.VISIBLE) {
            tvEmpty.setVisibility(View.GONE);
        }

    }

    private void removeLoader() {
//        performNextApiCall = false;
//        adapter.removeLoadingFooter();
//        isLoading = false;
    }


    /**
     * Pagination State reset
     */
    private void resetState() {
        isLoading = false;
        isLastPage = false;
        performNextApiCall = true;
        currentPage = PAGE_START;
    }


    private void setValues(float total_overall, float food_overall, float deliver_overall, float amount_overall, float service_overall, int reviewsCount) {


        total_overall = (food_overall + deliver_overall + amount_overall + service_overall)/4;
        tvOverAllRating.setText("" + total_overall + " /5");
        rbOverAllrating.setRating(total_overall);

        tvReviewsCount.setText("Based on " + reviewsCount + " reviews");
//        tvQuality.setText("" + food_overall + " /5");
//        tvFoodDelivery.setText("" + Math.round(deliver_overall) + " /5");
//        tvService.setText("" + Math.round(amount_overall) + " /5");
    }

    @Override
    public void callBack() {

    }

    @Override
    public void callBack(Object _model, int position) {

    }
}
