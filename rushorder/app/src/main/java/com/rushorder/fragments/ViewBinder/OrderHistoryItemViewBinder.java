package com.rushorder.fragments.ViewBinder;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.rushorder.R;
import com.rushorder.cache.CacheData;
import com.rushorder.constants.Constants;
import com.rushorder.fragments.OrderHistoryFragment;
import com.rushorder.interfaces.OrderHistoryCallBack;
import com.rushorder.models.HistoryHeader;
import com.rushorder.models.HistoryResponse;
import com.rushorder.utils.Utils;
import com.rushorder.views.TriangleRectangleLabelView;
import com.iarcuschin.simpleratingbar.SimpleRatingBar;

import java.lang.ref.WeakReference;

import tellh.com.stickyheaderview_rv.adapter.StickyHeaderViewAdapter;
import tellh.com.stickyheaderview_rv.adapter.ViewBinder;

/**
 * Created by Aleem on 12/17/2017.
 */

public class OrderHistoryItemViewBinder extends ViewBinder<HistoryResponse.ItemBean, OrderHistoryItemViewBinder.ViewHolder> {
    private WeakReference<OrderHistoryCallBack> callback;

    public OrderHistoryItemViewBinder(OrderHistoryFragment orderHistoryFragment) {
        callback = new WeakReference<OrderHistoryCallBack>(orderHistoryFragment);
    }

    @Override
    public OrderHistoryItemViewBinder.ViewHolder provideViewHolder(View itemView) {
        return new OrderHistoryItemViewBinder.ViewHolder(itemView, callback);
    }

    @Override
    public void bindView(StickyHeaderViewAdapter stickyHeaderViewAdapter, OrderHistoryItemViewBinder.ViewHolder viewHolder, int index, HistoryResponse.ItemBean item) {

        try {
            int parentIndex = Integer.parseInt(item.getItem_parent_index());
            HistoryHeader historyHeader = (HistoryHeader) CacheData.Order.getInstance().getOrderHistoryList().get(parentIndex);

//            viewHolder.tvRestaurantName.setText("" + historyHeader.getResturent_detail().getItemName());
//            viewHolder.tvRestaurantDesc.setText("" + historyHeader.getResturent_detail().getDescription());
//            viewHolder.tvRestaurantsStatus.setText("" + historyHeader.getResturent_detail().getDelivery_type());
//
//            if ((historyHeader.getResturent_detail().getIcon_name() != null)) {
//                Picasso.with(viewHolder.itemView.getContext())
//                        .load(historyHeader.getResturent_detail().getIcon_name())
//                        .into(viewHolder.ivRestaurant);
//            }
//
//
////            viewHolder.tvQty.setText("" + item.getItem_qty());
////            viewHolder.tvDate.setText("" + historyHeader.getDate());
//
//            if (historyHeader.isShow_feedback_button()) {
//                viewHolder.llFeedback.setVisibility(View.VISIBLE);
////                viewHolder.llRating.setVisibility(View.GONE);
//            } else {
//                viewHolder.llFeedback.setVisibility(View.GONE);
////                viewHolder.llRating.setVisibility(View.VISIBLE);
////                viewHolder.srbRating.setReviewStar(item.getReviewStar());
//            }

            viewHolder.tvItemName.setText("" + item.getItemName());
//            viewHolder.tvPrice.setText("" + Utils.formatToTwoDecimalPlaces(historyHeader.getTotalAmount()));
            viewHolder.tvPrice.setText("" + Utils.formatToTwoDecimalPlaces(item.getPrice()));
//            viewHolder.tvItemDesc.setText("" + historyHeader.getOrder_description());
            viewHolder.tvItemDesc.setText("Quantity" + item.getQty());

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    @Override
    public int getItemLayoutId(StickyHeaderViewAdapter adapter) {
        return R.layout.rv_order_history_child_row;
    }


    public static class ViewHolder extends ViewBinder.ViewHolder implements View.OnClickListener {
        private ImageView ivRestaurant;
        private SimpleRatingBar srbRating;
        private LinearLayout llRating, llFeedback, llRepeat;
        private TriangleRectangleLabelView trvRepeatOrder;
        private WeakReference<OrderHistoryCallBack> callback;
        private TextView tvRestaurantName, tvRestaurantDesc, tvItemName,
                tvItemDesc, tvPrice, tvRestaurantsStatus, trvDiscount, tvQty, tvTotal, tvDate;


        public ViewHolder(final View rootView, final WeakReference<OrderHistoryCallBack> callback) {
            super(rootView);
            this.callback = callback;
//            tvQty = rootView.findViewById(R.id.tvQty);
            tvDate = rootView.findViewById(R.id.tvDate);
            tvPrice = rootView.findViewById(R.id.tvPrice);
            tvItemName = rootView.findViewById(R.id.tvItemName);
            tvItemDesc = rootView.findViewById(R.id.tvItemDesc);
            ivRestaurant = rootView.findViewById(R.id.ivRestaurant);
            trvRepeatOrder = rootView.findViewById(R.id.trvRepeatOrder);
            tvRestaurantName = rootView.findViewById(R.id.tvRestaurantName);
            tvRestaurantDesc = rootView.findViewById(R.id.tvRestaurantDesc);
            tvRestaurantsStatus = rootView.findViewById(R.id.tvRestaurantsStatus);


//            llFooter = rootView.findViewById(R.id.llFooter);
            llRating = rootView.findViewById(R.id.llRating);
            llRepeat = rootView.findViewById(R.id.llRepeat);
            llFeedback = rootView.findViewById(R.id.llFeedBack);

            rootView.setOnClickListener(this);
            llFeedback.setOnClickListener(this);
            llRating.setOnClickListener(this);
            llRepeat.setOnClickListener(this);

        }

        @Override
        public void onClick(View view) {
            int position = getLayoutPosition();
            if (view == llRepeat) {
                callback.get().childCallback((HistoryResponse.ItemBean) CacheData.Order.getInstance().getOrderHistoryList().get(position),
                        position,
                        Constants.REPEAT);
            } else if (view == llFeedback) {
                callback.get().childCallback((HistoryResponse.ItemBean) CacheData.Order.getInstance().getOrderHistoryList().get(position),
                        position,
                        Constants.FEED_BACK);
            } else {

            }


        }
    }
}

