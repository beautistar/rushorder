package com.rushorder.fragments.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.rushorder.R;
import com.rushorder.interfaces.ClickCallBack;
import com.rushorder.models.CustomerReviewsResponse;
import com.iarcuschin.simpleratingbar.SimpleRatingBar;

import java.lang.ref.WeakReference;
import java.util.List;

/**
 * Created by Aleem on 1/29/2018.
 */

public class CustomerReviewsAdapter extends RecyclerView.Adapter<CustomerReviewsAdapter.Holder> {


    private WeakReference<ClickCallBack> callback;
    private List<CustomerReviewsResponse.ReviewsBean> reviewsList;

    public CustomerReviewsAdapter(ClickCallBack callback, List<CustomerReviewsResponse.ReviewsBean> reviewList) {

        this.callback = new WeakReference<>(callback);
        this.reviewsList = reviewList;
    }


    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        Holder holder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View itemView = inflater.inflate(R.layout.rv_customer_reviews_row, parent, false);
        holder = new Holder(itemView, callback);
        return holder;
    }

    @Override
    public void onBindViewHolder( Holder holder, int position) {

        CustomerReviewsResponse.ReviewsBean model = reviewsList.get(position);
        try {

            holder.tvName.setText("" + model.getUserName());
//            holder.tvTime.setText("" + model.getTime());
            holder.rbCustomerRating.setRating(model.getRaiting());
            holder.tvDescription.setText("" + model.getComment());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }



    @Override
    public int getItemCount() {

        return (reviewsList != null) ? reviewsList.size() : 0;
    }


    public static class Holder extends RecyclerView.ViewHolder {

        private SimpleRatingBar rbCustomerRating;
        private TextView tvName, tvDescription, tvTime, tvLocation;


        public Holder(View itemView, final WeakReference<ClickCallBack> callback) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tvName);
            tvTime = itemView.findViewById(R.id.tvTimesAgo);
            tvLocation = itemView.findViewById(R.id.tvLocation);
            rbCustomerRating = itemView.findViewById(R.id.rbRating);
            tvDescription = itemView.findViewById(R.id.tvDescription);

            rbCustomerRating.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    return true;
                }
            });


        }
    }
}
