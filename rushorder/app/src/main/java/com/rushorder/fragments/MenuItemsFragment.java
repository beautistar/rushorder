package com.rushorder.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.util.ArrayMap;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.rushorder.R;
import com.rushorder.cache.CacheData;
import com.rushorder.constants.Constants;
import com.rushorder.fragments.ViewBinder.MenuItemHeaderViewBinder;
import com.rushorder.fragments.ViewBinder.MenuItemViewBinder;
import com.rushorder.fragments.dialogs.MenuAddonsDialog;
import com.rushorder.interfaces.ClickCallBack;
import com.rushorder.models.Cart;
import com.rushorder.models.CartModel;
import com.rushorder.models.ItemHeader;
import com.rushorder.models.RestaurantInfoResponse.CategoryViewModelsBean.SubCategoriesBean.ItemsBean;
import com.rushorder.models.RestaurantInfoResponse.CategoryViewModelsBean.SubCategoriesBean.ItemsBean.AddonCategoriesBean.AddonItemsBean;
import com.rushorder.models.RestaurantInfoResponse;
import com.muddzdev.styleabletoastlibrary.StyleableToast;

import java.util.ArrayList;
import java.util.List;

import tellh.com.stickyheaderview_rv.adapter.DataBean;
import tellh.com.stickyheaderview_rv.adapter.StickyHeaderViewAdapter;

/**
 * Created by Aleem on 12/10/2017.
 */

public class MenuItemsFragment extends BaseFragment implements ClickCallBack, View.OnClickListener {

    private int firstIndex = 0;
    private CartModel cartModel;
    private ItemsBean itemsModel;
    private EditText etSearchBar;
    private RelativeLayout rlCart;
    private TextView tvToolbarTitle;
    private RecyclerView rvStickyHeader;
    private ArrayList<DataBean> menuItems;
    private StickyHeaderViewAdapter adapter;
    private int itemIndex = Constants.INVALID_INDEX;
    private ArrayList<AddonItemsBean> selectedAddons;
    private List<RestaurantInfoResponse.CategoryViewModelsBean.SubCategoriesBean> menuItemList = new ArrayList<>();


    private ArrayList<DataBean> filteredMenuItemList;
    private ArrayList<RestaurantInfoResponse.CategoryViewModelsBean.SubCategoriesBean.ItemsBean> menuItemChildList = new ArrayList<>();
    private ArrayMap<ItemHeader, ArrayList<RestaurantInfoResponse.CategoryViewModelsBean.SubCategoriesBean.ItemsBean>> menuItemParentList = new ArrayMap<>();
    private ArrayMap<ItemHeader, ArrayList<RestaurantInfoResponse.CategoryViewModelsBean.SubCategoriesBean.ItemsBean>> menuFilteredItemParentList = new ArrayMap<>();

    public static MenuItemsFragment getInstance() {
        return new MenuItemsFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_menu_items, container, false);
        initViews(view);
        setListeners();
        return view;
    }

    @Override
    protected void initViews(View view) {


        rlCart = view.findViewById(R.id.rlCart);
        etSearchBar = view.findViewById(R.id.etSearchBar);
        tvToolbarTitle = view.findViewById(R.id.tvToolbarTitle);

        setValues();
        rvStickyHeader = view.findViewById(R.id.rvStickyHeader);
        rvStickyHeader.setLayoutManager(new LinearLayoutManager(getActivity()));
        formatListData();
        setAdapter();
        super.initViews(view);
    }

    @Override
    public void onResume() {

        super.onResume();
    }

    @Override
    protected void setValues() {

        menuItems = new ArrayList<>();
        selectedAddons = new ArrayList<>();
        menuItemParentList = new ArrayMap<>();
        menuItemParentList = new ArrayMap<>();
        menuItemChildList = new ArrayList<>();
        filteredMenuItemList = new ArrayList<>();
        menuFilteredItemParentList = new ArrayMap<>();


        itemIndex = getArguments().getInt(Constants.Keys.ITEM_POSITION_KEY);
        ((TextView) rlCart.findViewById(R.id.tvCartCount)).setText("" + Cart.getInstance().getOrderDetail().size());
        menuItemList = CacheData.Menu.getInstance().getMenuResponse().getCategoryViewModels().get(itemIndex).getSubCategories();

        String menuName = CacheData.Menu.getInstance().getMenuResponse().getCategoryViewModels().get(itemIndex).getName();
        tvToolbarTitle.setText("" + menuName);
        super.setValues();
    }

    private void formatListData() {

        for (RestaurantInfoResponse.CategoryViewModelsBean.SubCategoriesBean subCat : menuItemList) {
            ItemHeader header = new ItemHeader(subCat.getName());
            menuItems.add(header);

            for (RestaurantInfoResponse.CategoryViewModelsBean.SubCategoriesBean.ItemsBean item : subCat.getItems()) {

                item.setParentIndex(menuItems.indexOf(header));
                menuItems.add(item);

                menuItemChildList.add(item);
            }
            menuItemParentList.put(header, menuItemChildList);
            menuItemChildList = new ArrayList<>();
        }

        filteredMenuItemList.addAll(menuItems);
        CacheData.Menu.getInstance().setMenuItems(menuItems);
    }

    private void setAdapter() {

        adapter = new StickyHeaderViewAdapter(filteredMenuItemList)
                .RegisterItemType(new MenuItemViewBinder(this,getActivity()))
                .RegisterItemType(new MenuItemHeaderViewBinder());
        rvStickyHeader.setAdapter(adapter);
    }

    @Override
    protected void setListeners() {
        llBack.setOnClickListener(this);
        rlCart.setOnClickListener(this);

        etSearchBar.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                filterData(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        super.setListeners();
    }

    @Override
    public void callBack() {

    }

    @Override
    public void callBack(Object _model, int position) {
        itemsModel = (ItemsBean) _model;

        if (position != Constants.INVALID_INDEX &&
                ((ItemsBean) _model).getAddonCategories() != null
                && ((ItemsBean) _model).getAddonCategories().size() != 0) {

            showAddOnsDialog(position);
        } else {

            AddItemToCart(itemsModel);
        }
    }

    private void AddItemToCart(ItemsBean itemsModel) {

        int productId = itemsModel.getId();
        cartModel = new CartModel(itemsModel.getName(), "" + itemsModel.getPrice(), productId, 1, "", new ArrayList<AddonItemsBean>());

        Cart.getInstance().getOrderDetail().add(cartModel);
        StyleableToast.makeText(getActivity(), "Successfully added to cart!", R.style.SuccessToastStyle).show();

        updateToolbarShopCount();
        resetCartModel();
    }


    private void showAddOnsDialog(int position) {

        Bundle bundle = new Bundle();
        bundle.putInt(Constants.Keys.ITEM_POSITION_KEY, position);
        MenuAddonsDialog dialog = new MenuAddonsDialog();
        //dialog.setCancelable(false);
        dialog.setArguments(bundle);
        dialog.show(getChildFragmentManager(), "AddonsFragment");
    }

    public void AddItemToCart(List<ItemsBean.AddonCategoriesBean.AddonItemsBean> selectedAddons, int qty, String desc) {

        int productId = itemsModel.getId();
        cartModel = new CartModel(itemsModel.getName(), "" + itemsModel.getPrice(), productId, qty, desc, selectedAddons);

        Cart.getInstance().getOrderDetail().add(cartModel);
        StyleableToast.makeText(getActivity(), "Successfully added to cart!", R.style.SuccessToastStyle).show();

        updateToolbarShopCount();
        resetCartModel();
    }


    private void updateToolbarShopCount() {
        ((TextView) rlCart.findViewById(R.id.tvCartCount)).setText("" + Cart.getInstance().getOrderDetail().size());

    }

    public void resetCartModel() {
        cartModel = null;
    }

    @Override
    public void onClick(View v) {
        if (v == llBack) {
            onBackPressed();
        } else {
            switchFragment(CartDetailFragment.getInstance());
        }
    }


    public void filterData(String query) {

        query = query.toLowerCase();
        menuFilteredItemParentList.clear();
        if (query.isEmpty()) {
            menuFilteredItemParentList = new ArrayMap<>(menuItemParentList);
            setFilterMenuList(menuFilteredItemParentList);

        } else {

            for (ArrayMap.Entry<ItemHeader, ArrayList<RestaurantInfoResponse.CategoryViewModelsBean.SubCategoriesBean.ItemsBean>> entry : menuItemParentList.entrySet()) {

                ArrayList<RestaurantInfoResponse.CategoryViewModelsBean.SubCategoriesBean.ItemsBean> itemArrayList = entry.getValue();
                ArrayList<RestaurantInfoResponse.CategoryViewModelsBean.SubCategoriesBean.ItemsBean> newList = new ArrayList<>();

                for (RestaurantInfoResponse.CategoryViewModelsBean.SubCategoriesBean.ItemsBean item : itemArrayList) {

                    if (item.getName().toLowerCase().contains(query) || entry.getKey().getPrefix().toLowerCase().contains(query)) {
                        newList.add(item);
                        menuFilteredItemParentList.put(entry.getKey(), newList);
                    }
                }

            }

            setFilterMenuList(menuFilteredItemParentList);
        }

        setAdapter();
    }

    private void setFilterMenuList(ArrayMap<ItemHeader, ArrayList<RestaurantInfoResponse.CategoryViewModelsBean.SubCategoriesBean.ItemsBean>> parentMap) {
        filteredMenuItemList.clear();
        for (ItemHeader key : parentMap.keySet()) {
            filteredMenuItemList.add(key);
            filteredMenuItemList.addAll(parentMap.get(key));
        }
    }
}
