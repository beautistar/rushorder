package com.rushorder.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.rushorder.R;
import com.rushorder.apis.Apis;
import com.rushorder.interfaces.VolleyCallBack;

import org.json.JSONObject;

/**
 * Created by Aleem on 1/27/2018.
 */

public class TermsAndConditionsFragment extends BaseFragment implements View.OnClickListener {

    private ImageView ivMenu;
    private TextView tvTerms;

    public static TermsAndConditionsFragment getInstance() {
        return new TermsAndConditionsFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_terms_and_conditions, container, false);
        initViews(view);
        return view;
    }

    @Override
    protected void initViews(View view) {

        ivMenu = view.findViewById(R.id.ivMenu);
        tvTerms = view.findViewById(R.id.tvTerms);
//        getTermsAndConditions();
        setListeners();
        super.initViews(view);
    }

    @Override
    protected void setListeners() {
        ivMenu.setOnClickListener(this);
        super.setListeners();
    }

    @Override
    public void onClick(View v) {
        if (v == ivMenu) {
            getParentActivity().openDrawer();
        } else {

        }
    }


    void getTermsAndConditions() {


        Apis.Restaurant.getTermsAndConditions(getActivity(), true, "", new VolleyCallBack.StringResponse() {
            @Override
            public void onResponse(String _response, String _tag) {
                try {

                    JSONObject jsonObject= new JSONObject(_response);
                    tvTerms.setText(jsonObject.getString("trmcondition"));

//                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
//                        tvTerms.setText(Html.fromHtml(_response, Html.FROM_HTML_MODE_COMPACT));
//                    } else {
//                        tvTerms.setText(Html.fromHtml(_response));
//                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            @Override
            public void onErrorMessage(String _errorMessage) {
                showErrorDialog("" + _errorMessage);
            }

            @Override
            public void onErrorResponse(VolleyError _error, String _tag) {
                showErrorDialog("" + getString(R.string.something_went_wrong));
            }

            @Override
            public void isConnected(boolean _connected, String _tag) {
                showErrorDialog("" + getString(R.string.no_internet));
            }
        });

    }
}
