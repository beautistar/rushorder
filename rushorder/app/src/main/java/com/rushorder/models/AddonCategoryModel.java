package com.rushorder.models;

public class AddonCategoryModel {

    private int pkcode;
    private String name;
    private String descp;
    private int parent_id;
    private int fkrestaurant;
    private int max_sellection;


    public int getPkcode() {
        return pkcode;
    }

    public String getName() {
        return name;
    }

    public String getDescp() {
        return descp;
    }

    public int getParent_id() {
        return parent_id;
    }

    public int getFkrestaurant() {
        return fkrestaurant;
    }

    public int getMax_sellection() {
        return max_sellection;
    }

    private AddonCategoryModel(Builder builder) {
        pkcode = builder.pkcode;
        name = builder.name;
        descp = builder.descp;
        parent_id = builder.parent_id;
        fkrestaurant = builder.fkrestaurant;
        max_sellection = builder.max_sellection;
    }


    public static final class Builder {
        private int pkcode;
        private String name;
        private String descp;
        private int parent_id;
        private int fkrestaurant;
        private int max_sellection;

        public Builder() {
        }

        public Builder pkcode(int val) {
            pkcode = val;
            return this;
        }

        public Builder name(String val) {
            name = val;
            return this;
        }

        public Builder descp(String val) {
            descp = val;
            return this;
        }

        public Builder parent_id(int val) {
            parent_id = val;
            return this;
        }

        public Builder fkrestaurant(int val) {
            fkrestaurant = val;
            return this;
        }

        public Builder max_sellection(int val) {
            max_sellection = val;
            return this;
        }

        public AddonCategoryModel build() {
            return new AddonCategoryModel(this);
        }
    }
}
