package com.rushorder.activities;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.rushorder.R;
import com.rushorder.cache.CacheData;
import com.rushorder.constants.Constants;
import com.rushorder.fragments.adapters.CuisineAdapter;
import com.rushorder.interfaces.ClickCallBack;
import com.rushorder.models.CuisinesResponse;
import com.rushorder.utils.Utils;

public class FilterActivity extends BaseActivity implements View.OnClickListener, ClickCallBack {


    private RecyclerView rvCuisine;
    private String sort = "", cuisine = "";
    private LinearLayout llSubmit, llBack;
    private ImageView ivDistance, ivNew, ivAlphabetical;
    private RelativeLayout rlDistance, rlNew, rlAlphabetical;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Utils.hideStatusBar(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter);
        initViews();
        setValues();
        setListeners();
    }


    @Override
    protected void initViews() {
        super.initViews();

        ivNew = findViewById(R.id.ivNew);
        rlNew = findViewById(R.id.rlNew);
        llBack = findViewById(R.id.llBack);
        llSubmit = findViewById(R.id.llSubmit);
        rvCuisine = findViewById(R.id.rvCuisine);
        rlDistance = findViewById(R.id.rlDistance);
        ivDistance = findViewById(R.id.ivDistance);
        rlAlphabetical = findViewById(R.id.rlAlphabetical);
        ivAlphabetical = findViewById(R.id.ivAlphabetical);


    }

    @Override
    protected void setValues() {

//        if (CacheData.Filters.getInstance().getCuisinesList().size() == 0) {
//            CacheData.Filters.getInstance().getCuisinesList().add(new CuisinesResponse("1", "All"));
//            CacheData.Filters.getInstance().getCuisinesList().add(new CuisinesResponse("2", "Chinese"));
//            CacheData.Filters.getInstance().getCuisinesList().add(new CuisinesResponse("3", "Indian"));
//        }

        setAdapter();

        super.setValues();
    }


    private void setAdapter() {

        rvCuisine.setLayoutManager(new LinearLayoutManager(this));
        rvCuisine.setAdapter(new CuisineAdapter(this, CacheData.Filters.getInstance().getCuisinesList()));
    }

    @Override
    protected void setListeners() {

        rlNew.setOnClickListener(this);
        llBack.setOnClickListener(this);
        llSubmit.setOnClickListener(this);
        rlDistance.setOnClickListener(this);
        rlAlphabetical.setOnClickListener(this);
        super.setListeners();
    }

    @Override
    public void onClick(View v) {
        if (v == rlNew) {
            sort = "New";
            updateView(ivNew);
        } else if (v == rlDistance) {
            sort = "Distance";
            updateView(ivDistance);
        } else if (v == rlAlphabetical) {
            sort = "A-Z";
            updateView(ivAlphabetical);
        } else if (v == llSubmit) {

            Intent intent = new Intent();
            intent.putExtra(Constants.Keys.SORT, sort);
            intent.putExtra(Constants.Keys.CUISINE, cuisine);
            setResult(RESULT_OK, intent);
            finish();

        } else {
            finish();
        }
    }

    private void updateView(ImageView ivTick) {
        View[] ivTicks = {ivDistance, ivNew, ivAlphabetical};
        for (int index = 0; index < ivTicks.length; index++) {
            if (ivTicks[index] == ivTick) {
                ivTicks[index].setVisibility(View.VISIBLE);
            } else {
                ivTicks[index].setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public void callBack() {
    }

    @Override
    public void callBack(Object _model, int position) {
        cuisine = ((CuisinesResponse) _model).getName();

    }
}
