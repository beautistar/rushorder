package com.rushorder.fragments.ViewBinder;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.rushorder.cache.CacheData;
import com.rushorder.constants.Constants;
import com.rushorder.R;
import com.rushorder.fragments.OrderHistoryFragment;
import com.rushorder.interfaces.OrderHistoryCallBack;
import com.rushorder.models.HistoryHeader;

import java.lang.ref.WeakReference;

import tellh.com.stickyheaderview_rv.adapter.StickyHeaderViewAdapter;
import tellh.com.stickyheaderview_rv.adapter.ViewBinder;

/**
 * Created by Aleem on 12/17/2017.
 */

public class OrderHistoryHeaderViewBinder extends ViewBinder<HistoryHeader, OrderHistoryHeaderViewBinder.ViewHolder> {
    private static final int NO_CHILD = 0;
    private WeakReference<OrderHistoryCallBack> callback;

    public OrderHistoryHeaderViewBinder(OrderHistoryFragment orderHistoryFragment) {
        callback = new WeakReference<OrderHistoryCallBack>(orderHistoryFragment);
    }

    @Override
    public OrderHistoryHeaderViewBinder.ViewHolder provideViewHolder(View itemView) {
        return new OrderHistoryHeaderViewBinder.ViewHolder(itemView, callback);
    }

    @Override
    public void bindView(StickyHeaderViewAdapter adapter, OrderHistoryHeaderViewBinder.ViewHolder holder, int position, HistoryHeader entity) {

//        boolean showFeedBack = entity.isShow_feedback_button();

        holder.tvDate.setText("Order# " + entity.getPkcode());

        try {

//            SimpleDateFormat fromServer = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//            SimpleDateFormat myFormat = new SimpleDateFormat("dd-MM-yy hh:mm a");
//            String date = myFormat.format(fromServer.parse(entity.getOrderdate()));
//            holder.tvDate.setText("" + entity.getOrderdate());


//            holder.tvRestaurantName.setText("" + entity.getResturent_detail().getItemName());
//            holder.tvRestaurantDesc.setText("" + entity.getResturent_detail().getDescription());
//            holder.tvRestaurantsStatus.setText("" + entity.getResturent_detail().getDelivery_type());
//
//            if ((entity.getResturent_detail().getIcon_name() != null)) {
//                Picasso.with(holder.itemView.getContext())
//                        .load(entity.getResturent_detail().getIcon_name())
//                        .into(holder.ivRestaurant);
//            }
//
//            if (entity.isShow_feedback_button()) {
//                holder.llFeedback.setVisibility(View.VISIBLE);
//            } else {
//                holder.llFeedback.setVisibility(View.GONE);
//            }


        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    @Override
    public int getItemLayoutId(StickyHeaderViewAdapter adapter) {
        return R.layout.rv_order_history_header;
    }


    static class ViewHolder extends ViewBinder.ViewHolder implements View.OnClickListener {
        private ImageView ivRestaurant;
        private TextView tvOrderId, tvDate;
        private WeakReference<OrderHistoryCallBack> callback;
        private LinearLayout llRating, llFeedback, llRepeat;
        private TextView tvRestaurantName, tvRestaurantDesc, tvRestaurantsStatus;


        public ViewHolder(final View rootView, final WeakReference<OrderHistoryCallBack> callback) {
            super(rootView);
            this.callback = callback;
            tvDate = rootView.findViewById(R.id.tvDate);
            tvOrderId = rootView.findViewById(R.id.tvOrderId);
            ivRestaurant = rootView.findViewById(R.id.ivRestaurant);
            tvRestaurantName = rootView.findViewById(R.id.tvRestaurantName);
            tvRestaurantDesc = rootView.findViewById(R.id.tvRestaurantDesc);
            tvRestaurantsStatus = rootView.findViewById(R.id.tvRestaurantsStatus);

            llRating = rootView.findViewById(R.id.llRating);
            llRepeat = rootView.findViewById(R.id.llRepeat);
            llFeedback = rootView.findViewById(R.id.llFeedBack);

            rootView.setOnClickListener(this);
            llRating.setOnClickListener(this);
            llRepeat.setOnClickListener(this);
            llFeedback.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int position = getLayoutPosition();
            if (view == llRepeat) {
                callback.get().headerCallback((HistoryHeader) CacheData.Order.getInstance().getOrderHistoryList().get(position),
                        position,
                        Constants.REPEAT);

            } else if (view == llFeedback) {
                callback.get().headerCallback((HistoryHeader) CacheData.Order.getInstance().getOrderHistoryList().get(position),
                        position,
                        Constants.FEED_BACK);
            } else {

            }
        }
    }
}
