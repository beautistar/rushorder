package com.rushorder.activities;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.rushorder.R;
import com.rushorder.apis.Apis;
import com.rushorder.managers.PrefManager;
import com.rushorder.models.SignInResponse;
import com.rushorder.utils.JsonTranslator;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TrackOrderMapsActivity extends FragmentActivity implements OnMapReadyCallback {

    String orderId;
    private GoogleMap mMap;
    @BindView(R.id.pbOrderLocation)
    ProgressBar pbOrderLocation;
    Handler mHandler;
    Runnable runnable;
    Timer timer;
    @BindView(R.id.tvMenuNameToolbar)
    TextView tvMenuNameToolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_track_order_maps);

        getIntents();
        ButterKnife.bind(this);

        tvMenuNameToolbar.setText("Track order");

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        Objects.requireNonNull(mapFragment).getMapAsync(this);
    }

    private void getIntents() {
        orderId = getIntent().getStringExtra("orderId");
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mHandler = new Handler();
        pbOrderLocation.setVisibility(View.VISIBLE);

        getCurrentLocations();
    }
    @OnClick(R.id.llBackToolbar)
    public void onBackLicked()
    {
        finish();
    }
    private void getCurrentLocations() {

        TimerTask doAsynchronousTask = new TimerTask() {
            @Override
            public void run() {

                mHandler.post(runnable = () -> {
                    getOrderCurrentLocation();
                });
            }
        };
        timer = new Timer();
        timer.schedule(doAsynchronousTask, 0, 8000);

    }

    private void getOrderCurrentLocation() {
        String userId = "";
        try {
            SignInResponse response = JsonTranslator.getInstance().parse(PrefManager.getInstance().getString(PrefManager.USER_INFO), SignInResponse.class);
            userId = response.getUserID();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        String url = Apis.GetDriverLocation +"userId="+userId+ "&orderId=" + orderId;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                response -> {
                    try {
                        Log.e("responseFacebook", response);
                        JSONObject jsonObject = new JSONObject(response);

                        String success = jsonObject.getString("success");
                        if(success.equals("1")) {
                            String data = jsonObject.getString("data");
                            JSONObject dataObj = new JSONObject(data);
                            String Lat = dataObj.getString("Lat");
                            String Lng = dataObj.getString("Long");
                            if (Lat.length() > 0 && Lng.length() > 0) {

                                Double latitude = Double.valueOf(Lat);
                                Double longitude = Double.valueOf(Lng);
                                if (mMap != null)
                                    mMap.clear();

                                LatLng sydney = new LatLng(latitude, longitude);
                                mMap.addMarker(new MarkerOptions().position(sydney).title(""));
                                mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
                                LatLng latLng = new LatLng(latitude, longitude);
                                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15.0f));

                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    pbOrderLocation.setVisibility(View.GONE);
                },
                error -> {
                    pbOrderLocation.setVisibility(View.GONE);
                }) {

        };
        RequestQueue requestQueue = Volley.newRequestQueue(TrackOrderMapsActivity.this);
        requestQueue.add(stringRequest);

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }
}
