package com.rushorder.models;


public class CouponCodeResponse {


    /**
     * Id : 1
     * Code : copone123
     * value : 0
     * ItemID : 0
     * Precentage : true
     */

    private int Id;
    private String Code;
    private String value;
    private int ItemID;
    private boolean Precentage;

    public int getId() {
        return Id;
    }

    public void setId(int Id) {
        this.Id = Id;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String Code) {
        this.Code = Code;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public int getItemID() {
        return ItemID;
    }

    public void setItemID(int ItemID) {
        this.ItemID = ItemID;
    }

    public boolean isPrecentage() {
        return Precentage;
    }

    public void setPrecentage(boolean Precentage) {
        this.Precentage = Precentage;
    }
}
