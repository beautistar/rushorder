package com.rushorder.driver.track_driver.view;

public interface IDriverLocationView {
    void onResultOrderDone(boolean result);
    void setProgressbarVisibility(int visibility);
    void onResultOrderLocation(boolean result, String Destination_Lat, String Destination_Long,String DeliveryAddress);
}
