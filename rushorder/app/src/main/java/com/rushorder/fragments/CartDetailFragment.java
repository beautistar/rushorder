package com.rushorder.fragments;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.rushorder.R;
import com.rushorder.apis.Apis;
import com.rushorder.cache.CacheData;
import com.rushorder.constants.Constants;
import com.rushorder.fragments.adapters.CartDetailAdapter;
import com.rushorder.interfaces.ClickCallBack;
import com.rushorder.interfaces.VolleyCallBack;
import com.rushorder.managers.PrefManager;
import com.rushorder.models.Cart;
import com.rushorder.models.CartModel;
import com.rushorder.models.CouponCodeResponse;
import com.rushorder.utils.JsonTranslator;
import com.rushorder.utils.Utils;


import org.json.JSONException;
import org.json.JSONObject;

import com.rushorder.models.RestaurantInfoResponse.CategoryViewModelsBean.SubCategoriesBean.ItemsBean.AddonCategoriesBean.AddonItemsBean;


/**
 * Created by Aleem on 12/10/2017.
 */

public class CartDetailFragment extends BaseFragment implements ClickCallBack, View.OnClickListener {

    private String note = "";
    private EditText etNote;
    private boolean cartEmpty;
    private String couponeCode;
    private RecyclerView rvCart;
    private EditText etCouponCode;
    private CartDetailAdapter adapter;
    private LinearLayout llCouponApply;
    private float minimumSpending = 0.0f;
    private LinearLayout llCheckoutOrder;
    private CouponCodeResponse codeResponse;
    private int itemPosition = Constants.INVALID_INDEX;
    private static final String COUPON_PERCENTAGE_TYPE = "percentage";
    private float totalPrice = 0f, grandPrice = 0f, discount = 0f;
    private TextView tvTotalPrice, tvDiscount, tvGTotal, tvCartCount;

    public static CartDetailFragment getInstance() {
        return new CartDetailFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_cart_details, container, false);
        initViews(view);
        setListeners();
        return view;
    }


    @Override
    protected void initViews(View view) {

        if (getArguments() != null) {
            minimumSpending = getArguments().getFloat(Constants.Keys.MINIMUM_SPENDING);
        } else {
            if (CacheData
                    .RestaurantsDetails
                    .getInstance()
                    .getRestaurantDetailsResponse() != null) {

                //uncomment

                minimumSpending = CacheData
                        .RestaurantsDetails
                        .getInstance()
                        .getRestaurantDetailsResponse()
                        .getMinDelPrice();
            }

        }
        etNote = view.findViewById(R.id.etNote);
        rvCart = view.findViewById(R.id.rvCartDetails);
        tvTotalPrice = view.findViewById(R.id.tvTotal);
        tvDiscount = view.findViewById(R.id.tvDiscount);
        tvGTotal = view.findViewById(R.id.tvGrandTotal);
        tvCartCount = view.findViewById(R.id.tvCartCount);
        etCouponCode = view.findViewById(R.id.etCouponCode);
        llCouponApply = view.findViewById(R.id.llCouponApply);
        llCheckoutOrder = view.findViewById(R.id.llCheckoutOrder);
        setAdapter();
        calculatePrice();
        super.initViews(view);
    }


    @Override
    protected void setListeners() {
        llBack.setOnClickListener(this);
        llCouponApply.setOnClickListener(this);
        llCheckoutOrder.setOnClickListener(this);
        super.setListeners();
    }

    @Override
    protected void setValues() {
        tvCartCount.setText("You have " + Cart.getInstance().getCartSize() + " items in your cart");
        super.setValues();
    }

    private void calculatePrice() {
        try {
            totalPrice = 0f;
            for (CartModel model : Cart.getInstance().getOrderDetail()) {
                float itemPrice = 0f;
                for (AddonItemsBean item : model.getSoldAddonItem()) {
//                    itemPrice += Float.parseFloat(item.getPrice());
                    itemPrice += item.getPrice();
                }
                itemPrice = (itemPrice + Float.parseFloat(model.getPrice())) * (model.getQuantity());
                totalPrice += itemPrice;
            }

            grandPrice = totalPrice - discount;
            setTotalBill();

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private void setTotalBill() {

        tvTotalPrice.setText("KWD" + " " + Utils.formatToTwoDecimalPlaces(totalPrice));
        tvGTotal.setText("KWD" + " " + Utils.formatToTwoDecimalPlaces(grandPrice));

        if (codeResponse != null && codeResponse.isPrecentage()) {

            tvDiscount.setText("%" + " " + Utils.formatToTwoDecimalPlaces(codeResponse.getValue()));
        } else {

            tvDiscount.setText("KWD" + " " + Utils.formatToTwoDecimalPlaces(discount));
        }
        setValues();

    }

    void setAdapter() {
        adapter = new CartDetailAdapter(this);
        rvCart.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvCart.setAdapter(adapter);
    }

    @Override
    public void callBack() {
        setAdapter();
    }

    @Override
    public void callBack(Object _model, int position) {
        totalPrice = 0f;

//        adapter.notifyItemChanged(position);
        adapter.notifyDataSetChanged();
        calculatePrice();
    }

    @Override
    public void onClick(View v) {

        if (v == llBack) {
            onBackPressed();
        } else if (v == llCouponApply) {

            if (isLoggedIn()) {
                if (Cart.getInstance().isCartFull()) {
                    validateCouponCode();
                } else {
                    showErrorDialog(getString(R.string.add_item_to_cart));
                }
            } else {
                showAlert(getString(R.string.login_to_proceed));
            }

        } else {

            if (isLoggedIn()) {
                if (Cart.getInstance().isCartFull()) {
                    note = etNote.getText().toString();

                    if (isMinimumSpending()) {
                        startProcessOrderFragment();
//                        checkRestaurantOpen();
                    } else {
                        showErrorDialog("Order can not be placed.Your amount is less than " + minimumSpending);
                    }
                } else {
                    showErrorDialog(getString(R.string.add_item_to_cart));
                }
            } else {
                showAlert(getString(R.string.login_to_proceed));
            }
        }
    }


    private void validateCouponCode() {
        if (!etCouponCode.getText().toString().isEmpty()) {
            couponeCode = etCouponCode.getText().toString();
            getDiscount();
        } else {
            showErrorDialog("Please enter the CouponCode");
        }
    }

    public void showAlert(String message) {
        android.support.v7.app.AlertDialog.Builder alertDialog = new android.support.v7.app.AlertDialog.Builder(getActivity())
                .setMessage(message)
                .setTitle("Alert !")
                .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        startLoginFragment();
                        dialog.dismiss();
                    }
                }).setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }

    private void startLoginFragment() {
        Bundle bundle = new Bundle();
        bundle.putBoolean(Constants.Keys.IS_FROM_CART_KEY, true);
        switchFragment(SignInFragment.getInstance(), bundle);
    }

    private boolean isLoggedIn() {
//        return true;
        return PrefManager.getInstance().getBoolean(PrefManager.IS_LOGIN);
    }


    private void checkRestaurantOpen() {

        Apis.Order.checkShopOpen(getActivity(), true, PrefManager.getInstance().getString(PrefManager.RES_ID), new VolleyCallBack.JSONResponse() {
            @Override
            public void onResponse(JSONObject _response, String _tag) {
                try {
                    if (_response.getBoolean(Constants.SHOP_OPEN)) {
                        startProcessOrderFragment();
                    } else {
                        startPreOrderFragment();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorMessage(String _errorMessage) {
                showErrorDialog(_errorMessage);
            }

            @Override
            public void onErrorResponse(VolleyError _error, String _tag) {
                showErrorDialog(getString(R.string.something_went_wrong));
            }

            @Override
            public void isConnected(boolean _connected, String _tag) {
                showErrorDialog(getString(R.string.no_internet));
            }
        });
    }


    private void startPreOrderFragment() {

        Bundle bundle = new Bundle();
        bundle.putString(Constants.Keys.ORDER_NOTE, note);
        bundle.putFloat(Constants.Keys.DISCOUNT, discount);
        bundle.putBoolean(Constants.Keys.IS_PREORDER, true);
        bundle.putFloat(Constants.Keys.TOTAL_PRICE, totalPrice);
        bundle.putFloat(Constants.Keys.GRAND_TOTAL, grandPrice);
        switchFragment(PreOrderFragment.getInstance(), bundle);
    }

    private void startProcessOrderFragment() {

        Bundle bundle = new Bundle();
        bundle.putString(Constants.Keys.PREORDER_NOTE, note);
        bundle.putFloat(Constants.Keys.DISCOUNT, discount);
        bundle.putBoolean(Constants.Keys.IS_PREORDER, false);
        bundle.putFloat(Constants.Keys.TOTAL_PRICE, totalPrice);
        bundle.putFloat(Constants.Keys.GRAND_TOTAL, grandPrice);
        switchFragment(ProcessOrderFragment.getInstance(), bundle);
    }


    private void getDiscount() {
        Utils.hideSoftKeyboard(getActivity(), etCouponCode);
        String restId = PrefManager.getInstance().getString(PrefManager.RES_ID);

        Apis.User.validateCouponCode(getActivity(), true, couponeCode, restId, "", new VolleyCallBack.JSONResponse() {

            @Override
            public void onResponse(JSONObject _response, String _tag) {
                try {

                    codeResponse = JsonTranslator.getInstance().parse(_response.toString(), CouponCodeResponse.class);

                    if (codeResponse.isPrecentage()) {
                        discount = (totalPrice * Float.parseFloat(codeResponse.getValue())) / 100;
                    } else {
                        discount = Float.parseFloat((codeResponse.getValue() != null && !codeResponse.getValue().isEmpty()) ? codeResponse.getValue() : "0.0");
                    }


                    if (totalPrice <= discount) {
                        discount = 0f;
                        showErrorDialog("Discount can not be applied on this amount.Please add more items in cart");
                    } else {
                        calculatePrice();
                        showSuccessDialog("Discount applied successfully");
                        setDiscountDetails(codeResponse);
                    }

                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            @Override
            public void onErrorMessage(String _errorMessage) {
                showErrorDialog("" + _errorMessage);
            }

            @Override
            public void onErrorResponse(VolleyError _error, String _tag) {
                showErrorDialog(getString(R.string.something_went_wrong));
            }

            @Override
            public void isConnected(boolean _connected, String _tag) {
                showErrorDialog(getString(R.string.no_internet));
            }
        });
    }

    private void setDiscountDetails(CouponCodeResponse codeResponse) {

        Cart.getInstance().setDiscountAmount("" + totalPrice);
        Cart.getInstance().setCoupon_Id("" + this.codeResponse.getId());
//        Cart.getInstance().setCoupon_type(this.codeResponse.getCoupon_data().getType());
//        Cart.getInstance().setDiscount_description(this.codeResponse.getDiscount_description());
        Cart.getInstance().setCoupon_value("" + discount);
    }

    public boolean isMinimumSpending() {
        return (totalPrice > minimumSpending);
    }
}

