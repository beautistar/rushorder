package com.rushorder.fragments;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.rushorder.R;
import com.rushorder.apis.Apis;
import com.rushorder.interfaces.VolleyCallBack;

import org.json.JSONObject;

/**
 * Created by Aleem on 1/27/2018.
 */

public class PrivacyPolicyFragment extends BaseFragment implements View.OnClickListener {

    private WebView webView;
    private ImageView ivMenu;
    private TextView tvPrivacy;
    private ProgressBar pbLoader;


    public static PrivacyPolicyFragment getInstance() {
        return new PrivacyPolicyFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_privacy_policy, container, false);
        initViews(view);
        setListeners();
        setValues();

        return view;
    }

    @Override
    protected void initViews(View view) {
        ivMenu = view.findViewById(R.id.ivMenu);
        webView= view.findViewById(R.id.webview);
        tvPrivacy = view.findViewById(R.id.tvPrivacy);
        pbLoader = view.findViewById(R.id.pbLoader);

//        getPrivacyPolicy();
        super.initViews(view);
    }

    @Override
    protected void setValues() {

        webView.setWebViewClient(new CustomWebViewClient(getActivity()));
        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadUrl("http://orderpolicy.rushorder.online");

        super.setValues();
    }

    @Override
    protected void setListeners() {
        ivMenu.setOnClickListener(this);
        super.setListeners();
    }

    @Override
    public void onClick(View v) {
        if (v == ivMenu) {
            getParentActivity().openDrawer();
        } else {

        }
    }





    void getPrivacyPolicy() {


        Apis.Restaurant.getPrivacyPolicy(getActivity(), true, "", new VolleyCallBack.StringResponse() {
            @Override
            public void onResponse(String _response, String _tag) {
                try {
                    JSONObject jsonObject= new JSONObject(_response);
                    tvPrivacy.setText(jsonObject.getString("PrivacyPolicy"));

//                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
//                        tvPrivacy.setText(Html.fromHtml(_response, Html.FROM_HTML_MODE_COMPACT));
//                    } else {
//                        tvPrivacy.setText(Html.fromHtml(_response));
//                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            @Override
            public void onErrorMessage(String _errorMessage) {
                showErrorDialog("" + _errorMessage);
            }

            @Override
            public void onErrorResponse(VolleyError _error, String _tag) {
                showErrorDialog("" + getString(R.string.something_went_wrong));
            }

            @Override
            public void isConnected(boolean _connected, String _tag) {
                showErrorDialog("" + getString(R.string.no_internet));
            }
        });

    }




    private class CustomWebViewClient extends WebViewClient {

        private Context context;

        public CustomWebViewClient(Context _context) {
            this.context = _context;
        }

        @SuppressWarnings("deprecation")
        @Override

        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return false;
        }

        @TargetApi(Build.VERSION_CODES.N)
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {

            view.loadUrl(request.getUrl().toString());

            return false;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            pbLoader.setVisibility(View.GONE);
            webView.setVisibility(View.VISIBLE);

//            dialog.dismiss();
//            checkBottomLayout();

        }
    }//End of custom webview client

}
